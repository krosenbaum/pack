// Copyright (C) 2009-2011 by Konrad Rosenbaum <konrad@silmor.de>
// protected under the GNU LGPL version 3 or at your option any newer.
// See COPYING.LGPL file that comes with this distribution.
//


#ifndef WOLF_NULLABLE_H
#define WOLF_NULLABLE_H

#ifndef WOLF_BASE_EXPORT
#define WOLF_BASE_EXPORT Q_DECL_IMPORT
#endif

#include <cstddef>

///special class for Null values
///this was a legacy class before nullptr was introduced, use nullptr now!
class WOLF_BASE_EXPORT NullValue
{
	public: 
		NullValue(){} 
		NullValue(const NullValue&){}
		NullValue(const std::nullptr_t&){}
		
// 		operator void*()const{return (void*)0;}
		template<typename T> operator T*()const{return (T*)nullptr;}
		
		bool operator==(const NullValue&)const{return true;}
		bool operator!=(const NullValue&)const{return false;}
		bool operator==(const std::nullptr_t&)const{return true;}
		bool operator!=(const std::nullptr_t&)const{return false;}

		template<typename T>bool operator==(const T*p)const{return p==nullptr;}
		template<typename T>bool operator!=(const T*p)const{return p!=nullptr;}
};

///special null value
const NullValue nullval;

/**wrapper around scalar values that makes them NULL-able, i.e. the virtual value NULL is added to the range of the wrapped value; wrapped classes must have a default constructor and =, == and != operators*/
template<class T>class WOLF_BASE_EXPORT Nullable
{
	public:
		/**creates a NULL value*/
		Nullable():isnull(true),elem(){}
		/**creates a NULL value*/
		Nullable(const NullValue&):isnull(true),elem(){}
		/**creates a NULL value*/
		//TODO: find an implementation that works without auto-casting 0 to nullptr while not using explicit
		explicit Nullable(const std::nullptr_t&):isnull(true),elem(){}
		/**creates a wrapped non-NULL value that is equivalent to the original*/
		Nullable(const T&t):isnull(false),elem(t){}
		/**copies a nullable value*/
		Nullable(const Nullable<T>&t):isnull(t.isnull),elem(t.elem){}
		
		/**makes this instance non-NULL and equivalent to the given value*/
		Nullable<T>& operator=(const T&t){isnull=false;elem=t;return *this;}
		/**copies the given nullable value*/
		Nullable<T>& operator=(const Nullable<T>&t){isnull=t.isnull;elem=t.elem;return *this;}
		
		/**returns whether this instance is NULL*/
		bool isNull()const{return isnull;}
		
		/**converts this nullable to the original wrapped class - uses the default constructor if it is NULL*/
		operator T()const{if(isnull)return T();else return elem;}
		
		/**returns a reference to the wrapped value - the result is undefined if it us currently NULL*/
		T& value(){return elem;}
		/**returns the original value, if it is currently NULL it returns the given default value, if no default is given an instance created with the default constructor is returned*/
		T value(const T&defaultval=T())const{if(isnull)return defaultval;else return elem;}
		///Helper to automatically convert to base type
		T valueOrDefault()const{return value(T());}

		//convenience aliases, sometimes x.value().value().value() seems a bit weird!
		/**returns a reference to the wrapped value - the result is undefined if it us currently NULL*/
		T& data(){return elem;}
		/**returns the original value, if it is currently NULL it returns the given default value, if no default is given an instance created with the default constructor is returned*/
		T data(const T&defaultval=T())const{if(isnull)return defaultval;else return elem;}
		///Helper to automatically convert to base type
		T dataOrDefault()const{return value(T());}

		///converts the nullable to a pointer of the wrapped class - acting like a smart pointer
		T* operator ->()const{if(isnull)return nullptr;else return const_cast<T*>(&elem);}
		
		/**compares this instance with a value of the original class, NULL is different from ANY instance of the original class*/
		bool operator==(const T&t)const{if(isnull)return false;else return elem == t;}
		/**compares this instance with a value of the original class, NULL is different from ANY instance of the original class*/
		bool operator!=(const T&t)const{if(isnull)return true;else return elem != t;}

		/**compares two nullable instances, if both are NULL it returns true*/
		bool operator==(const Nullable<T>&t)const{
			if(isnull != t.isnull)return false;
			if(isnull)return true;
			else return elem == t.elem;}
		/**compares two nullable instances, if both are NULL it returns false*/
		bool operator!=(const Nullable<T>&t)const{
			if(isnull != t.isnull)return true;
			if(isnull)return false;
			else return elem != t.elem;}
		
		///compares the Nullable with the special null value
		bool operator==(const NullValue&)const{return isnull;}
		///compares the Nullable with the special null value
		bool operator!=(const NullValue&)const{return !isnull;}
		///compares the Nullable with the special null value
		bool operator==(const std::nullptr_t&)const{return isnull;}
		///compares the Nullable with the special null value
		bool operator!=(const std::nullptr_t&)const{return !isnull;}
	private:
		bool isnull;
		T elem;
};

#include<QString>
#include<QMetaType>
//convenience wrappers
Q_DECLARE_METATYPE(NullValue);
Q_DECLARE_METATYPE(Nullable<bool>)
Q_DECLARE_METATYPE(Nullable<qint32>)
Q_DECLARE_METATYPE(Nullable<qint64>)
Q_DECLARE_METATYPE(Nullable<quint32>)
Q_DECLARE_METATYPE(Nullable<quint64>)
Q_DECLARE_METATYPE(Nullable<QString>)
Q_DECLARE_METATYPE(Nullable<QByteArray>)
Q_DECLARE_SMART_POINTER_METATYPE(Nullable)

inline WOLF_BASE_EXPORT bool operator==(Nullable<qint64> i1,int i2){return i1.operator==(i2);}
inline WOLF_BASE_EXPORT bool operator==(Nullable<quint32> i1,int i2){return i1.operator==(i2);}
inline WOLF_BASE_EXPORT bool operator==(Nullable<quint64> i1,int i2){return i1.operator==(i2);}
inline WOLF_BASE_EXPORT bool operator==(Nullable<quint64> i1,unsigned int i2){return i1.operator==(i2);}

#endif
