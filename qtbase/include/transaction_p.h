// Copyright (C) 2009-2011 by Konrad Rosenbaum <konrad@silmor.de>
// protected under the GNU LGPL version 3 or at your option any newer.
// See COPYING.LGPL file that comes with this distribution.
//

#ifndef WOLF_TRANSACTION_PH
#define WOLF_TRANSACTION_PH

#include <QByteArray>
#include <QString>
#include <QPointer>

#include "WTransaction"

class QTimer;
class QNetworkReply;

///\internal private logger class
class WOLF_BASE_EXPORT WTransaction::Log{
	public:
		/**instantiates the logger
		
		A logger object is used by transactions to output information about the message exchange that is going on. Depending on the log level of the interface the logger will log very tersely or verbosely. It can log messages immediately or store them to be logged in case of errors.
		
		\param parent the transaction this is about
		\param request the name of the request this transaction issues
		\param interface the name of the interface that is used */
		Log(WTransaction*parent,const QString&request,const QString&interface=QString());
		///deletes the logger
		~Log();
		
		///logs/stores a request message
		void setRequ(const QString&,const QString&);
		///logs/stores a response message
		void setResp(const QString&,const QString&);
		///logs an error, if the interface is set up to log only on errors it also logs the stored request and response
		void setError(const QString&);
		///logs some informational message
		void setInfo(const QString&);
	private:
		QString req,rsp;
		QString trn,ifc;
		WTransaction*parent;
		int lvl,cnt;
		
		void outStr(const QString&);
};

///\internal private iface
class WTransaction_Private:public QObject {
	Q_OBJECT
	public:
		WTransaction_Private(WTransaction*);
		virtual ~WTransaction_Private();
		/**internal: execute a query synchronously on the web, used by subclasses*/
		virtual QByteArray executeQuery(QString,QByteArray);
		
		/**internal: execute a query on the web asynchronously*/
		virtual void startQuery(QString,QByteArray);
		
	public slots:
		/**internal: triggers when the transaction times out*/
		virtual void webTimeout();
		/**internal: triggers when the response data is available*/
		virtual void webReady();
		/**internal: triggers when the response errors out*/
		virtual void webError();
		/**internal: collect query data*/
		virtual void endQuery();
	signals:
		/** \internal this signal is raised when the transaction on the HTTP level finished*/
		void webFinished();
		/** used by high level objects, emitted when data is available */
		void dataAvailable();
		
	public:
		WTransaction::Stage m_stage;
		QString m_errtype,m_errstr,m_iface,m_wobstatus;
		WTransaction::Log*m_log;
		QPointer<QNetworkReply>m_rsp;
		QByteArray m_rspdata;
		QPointer<QTimer>m_qtimeout;
		
	public slots:
		///attach this private object to a master
		WTransaction_Private* attach(WTransaction*);
		///detach this object from a master, delete it if there are no more masters
		void detach(QObject*);
	private:
		///internal reference counter
		int m_refctr;
};

/// \internal wrapper around the logger
class WOLF_BASE_EXPORT WTransaction::LogWrap{
	public:
		/**instantiates a log wrapper using the logger of the parent or creates a new one in the parent*/
		LogWrap(WTransaction*parent,const QString&request,const QString&interface=QString());
		
		///logs/stores a request message
		void setRequ(const QString&,const QString&);
		///logs/stores a response message
		void setResp(const QString&,const QString&);
		///logs an error, if the interface is set up to log only on errors it also logs the stored request and response
		void setError(const QString&);
		///logs some informational message
		void setInfo(const QString&);
	private:
		WTransaction*parent;
};

class WOLF_BASE_EXPORT WTransaction_PrivateBase {
	private:
		int refctr;
	protected:
		WTransaction_PrivateBase();
		virtual ~WTransaction_PrivateBase();
	public:
		virtual void attach(){refctr++;}
		virtual void detach(){refctr--;if(refctr==0)delete this;}
};

#endif
