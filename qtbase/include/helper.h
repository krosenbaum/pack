// Copyright (C) 2009-2011 by Konrad Rosenbaum <konrad@silmor.de>
// protected under the GNU LGPL version 3 or at your option any newer.
// See COPYING.LGPL file that comes with this distribution.
//


#ifndef WOLF_HELPER_H
#define WOLF_HELPER_H

#include <QList>
#include <QObject>

class QDomElement;
class QDomNode;

#ifndef WOLF_BASE_EXPORT
#define WOLF_BASE_EXPORT Q_DECL_IMPORT
#endif

class WOLF_BASE_EXPORT WHelper:public QObject
{
	Q_OBJECT
	protected:
		/**helper for de-serializers: returns direct child elements with given tag name (necessary because QDomElement::elementsByTagName traverses all children)*/
		static QList<QDomElement>elementsByTagName(const QDomElement&,QString);
		
		/**helper for XML decoding: transforms string to boolean*/
		bool str2bool(QString s);
};

#endif
