// Copyright (C) 2009-2011 by Konrad Rosenbaum <konrad@silmor.de>
// protected under the GNU LGPL version 3 or at your option any newer.
// See COPYING.LGPL file that comes with this distribution.
//


#ifndef WOB_INTERFACE_H
#define WOB_INTERFACE_H

#include <QMap>
#include <QObject>
#include <QUrl>
#include <QSslError>

#include <WOb>

class WobNetworkAccessManager;
class QNetworkRequest;
class QNetworkReply;
class WServerRequest;
class WServerReply;

#ifndef WOLF_BASE_EXPORT
#define WOLF_BASE_EXPORT Q_DECL_IMPORT
#endif

/// base class of all interfaces
class WOLF_BASE_EXPORT WInterface:public QObject
{
	Q_OBJECT
	Q_PROPERTY(QUrl url READ url)
	Q_PROPERTY(bool useProxy READ useProxy)
	Q_PROPERTY(bool useProxyAuth READ useProxyAuth)
	Q_PROPERTY(QString proxyHost READ proxyHost)
	Q_PROPERTY(unsigned short proxyPort READ proxyPort)
	Q_PROPERTY(QString proxyUser READ proxyUser)
	Q_PROPERTY(QString proxyPassword READ proxyPassword)
	Q_PROPERTY(int webTimeout READ webTimeout)
	Q_PROPERTY(QString name READ name)
	Q_ENUMS(LogLevel)
	Q_PROPERTY(LogLevel logLevel READ logLevel)
	protected:
		///used by child classes for instantiation
		WInterface(QString name);
		
		friend class WTransaction;
		friend class WTransaction_Private;
		///called by WTransaction to post a request
		QNetworkReply* post(const QNetworkRequest&request,const QByteArray&data);
		
		friend class WServer;
		///called by WServer to execute a transaction
		virtual WServerReply execute(const WServerRequest&);
		
	public:
		virtual ~WInterface();
		/**overwrite if you need additional headers (eg. session-ids) for certain transactions (transaction name is handed in as string argument); per default returns empty map*/
		virtual QMap<QString,QString> headers(QString)const;
		
		/**returns the URL of the interface*/
		QUrl url()const{return m_url;}
		
		/**returns whether the interface uses a proxy*/
		bool useProxy()const{return m_proxyhost!="";}
		/**returns whether the proxy needs authentication*/
		bool useProxyAuth()const{return m_proxyuser!="";}
		/**returns the proxy host name*/
		QString proxyHost()const{return m_proxyhost;}
		/**returns the proxy port*/
		unsigned short proxyPort()const{return m_proxyport;}
		/**returns the proxy user name*/
		QString proxyUser()const{return m_proxyuser;}
		/**returns the proxy password*/
		QString proxyPassword()const{return m_proxypass;}
		
		/**return timeout in seconds*/
		int webTimeout()const{return m_wtimeout;}
		
		/**get the name for this interface (returns empty string if not registered*/
		QString name()const;
		
		/**returns the instance registered under that name*/
		static WInterface*instance(QString);
		
		/**log settings*/
		enum LogLevel {
			/**no logging*/
			LogNone=0,
			/**minimal logging*/
			LogMinimal=1,
			/**informational logging (more than minimal, not much yet)*/
			LogInfo=2,
			/**like LogInfo, but logs additional details on error*/
			LogOnError=0x12,
			/**always log details*/
			LogDetailed=0xff
		};
		
		/**returns the current log level*/
		LogLevel logLevel()const{return loglvl;}
		
		///returns version info about the library
		QString libraryVersionInfo(WOb::VersionInfo)const;
		///returns version info about the library
		static QString staticLibraryVersionInfo(WOb::VersionInfo);
		///returns version info about the WOC that was used to generate the interface
		virtual QString wocVersionInfo(WOb::VersionInfo)const=0;
		///returns version info about the interface itself
		virtual QString versionInfo(WOb::VersionInfo)const=0;
		
		///convenience function: return the protocol version of this interface
		virtual QString commVersion()const{return versionInfo(WOb::VersionComm);}
		///convenience function: return the oldest protocol version this interface is compatible with
		virtual QString needCommVersion()const{return versionInfo(WOb::VersionNeedComm);}
		///convenience function: return human readable version
		virtual QString version()const{return versionInfo(WOb::VersionHR);}
		
	public slots:
		/**set log level*/
		void setLogLevel(WInterface::LogLevel l){loglvl=l;}
		
		/**set timeout for page loads in seconds*/
		void setWebTimeout(int t){if(t>0)m_wtimeout=t;}
		
		/**resets the proxy settings to no proxy use*/
		void resetProxy(){m_proxyhost="";m_proxyport=0;m_proxyuser="";m_proxypass="";}
		/**sets the proxy settings*/
		void setProxy(QString proxyhost,unsigned short proxyport,QString proxyuser=QString(),QString proxypassword=QString());
	
		/**sets the URL of the interface*/
		void setUrl(QUrl u){m_url=u;}
		
		/**handles SSL errors, per default ignores them, overwrite it if you need more sophisticated behavior*/
		virtual void sslErrors(QNetworkReply *,const QList<QSslError>&);
		
	private:
		static QMap<QString,WInterface*>inst;
		QUrl m_url;
		QString m_proxyhost,m_proxyuser,m_proxypass;
		unsigned short m_proxyport;
		int m_wtimeout;
		LogLevel loglvl;
		WobNetworkAccessManager*m_netman;
};

#endif
