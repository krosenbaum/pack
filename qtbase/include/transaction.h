// Copyright (C) 2009-2011 by Konrad Rosenbaum <konrad@silmor.de>
// protected under the GNU LGPL version 3 or at your option any newer.
// See COPYING.LGPL file that comes with this distribution.
//

#ifndef WOLF_TRANSACTION_H
#define WOLF_TRANSACTION_H

#include <QObject>
#include <QList>
#include <QString>

#include "nullable.h"
#include "WHelper"
#include <QPointer>

class WTransaction_Private;

/**base class of all transactions*/
class WOLF_BASE_EXPORT WTransaction:public WHelper
{
	Q_OBJECT
	Q_ENUMS(Stage)
	Q_PROPERTY(Stage stage READ stage)
	Q_PROPERTY(bool isInProgress READ isInProgress)
	Q_PROPERTY(bool isFinished READ isFinished)
	Q_PROPERTY(bool isUnstarted READ isUnstarted)
	Q_PROPERTY(bool hasError READ hasError)
	Q_PROPERTY(QString errorType READ errorType)
	Q_PROPERTY(QString errorString READ errorString)
	Q_PROPERTY(QString interfaceName READ interface)
	public:
		///sets the logging prefix for transactions, per default it is "T"
		static void setLogPrefix(QString);

		/**stage the transaction is in*/
		enum Stage {
			Uninitialized=0,///<transaction has not started yet
			Request=1,///<transaction is running
			Success=6,///<transaction ended successfully
			Error=10,///<transaction ended with an error
		};
		
		/**returns the stage the transaction is in*/
		virtual Stage stage()const;
		///returns true if the transaction is currently in progress
		virtual bool isInProgress()const{return stage()==Request;}
		///returns true if the transaction is finished
		virtual bool isFinished()const{return stage()&2;}
		///returns true if the transaction has not started yet
		virtual bool isUnstarted()const{return stage()==Uninitialized;}
		
		/**returns whether the transaction had an error while executing*/
		virtual bool hasError()const;
		/**returns the error type (usually the component causing it)*/
		virtual QString errorType()const;
		/**returns the (translated) human readable error string*/
		virtual QString errorString()const;
		
		/**returns the interface that is used for the transaction*/
		virtual QString interface()const;
		
		/**deconstructor*/
		virtual ~WTransaction();
		
		/// \private internal classes
		class Log;
		class LogWrap;
		
		/// \private ancestor of other WaitFor classes
		class WOLF_BASE_EXPORT WaitFor_Private {
			public:
				virtual ~WaitFor_Private(){}
				WaitFor_Private& operator<<(WTransaction&);
				WaitFor_Private& operator<<(WTransaction*);
			protected:
				WaitFor_Private(int t){m_tmout=t;}
				virtual void loop()=0;
				QList<QPointer<WTransaction> > m_trn;
				int m_tmout;
		};
		
		/**use this class to wait for any of a number of transactions */
		class WOLF_BASE_EXPORT WaitForAny:public WaitFor_Private{
			public:
				WaitForAny(int tmout=0):WaitFor_Private(tmout){}
			        virtual ~WaitForAny(){loop();}
			protected:
				void loop();
		};
		/**use this class to wait for all of a number of transactions */
		class WOLF_BASE_EXPORT WaitForAll:public WaitFor_Private{
			public:
				WaitForAll(int tmout=0):WaitFor_Private(tmout){}
			        virtual ~WaitForAll(){loop();}
			protected:
				void loop();
		};
		
	public slots:
		/**waits for this transaction to finish, returns true if the transaction was successful, returns immediately if the transaction is not running
		\param tmout the maximum amount of milli seconds to wait, if 0 it will not time out*/
		virtual bool waitForFinished(int tmout=30000);
		
	protected:
		/**internal: construct the transaction*/
		WTransaction(QString iface=QString());
		/**internal: copy the transaction*/
		WTransaction(const WTransaction&);
		
		/**internal: copy the transaction*/
		virtual WTransaction& operator=(const WTransaction&);
		
		/**internal: execute a query synchronously on the web, used by subclasses*/
		virtual QByteArray executeQuery(QString,QByteArray);
		
		/**internal: execute a query on the web asynchronously*/
		virtual void startQuery(QString,QByteArray);
		
		/**internal: encode an error, used by server*/
		virtual QByteArray encodeError()const;
		
	protected slots:
		/**internal: collect query data*/
		virtual void endQuery();
	signals:
		/** this signal is raised when the transaction is finished successfully or with an error, data or an error information is available*/
		void finished();
	protected:
		friend class WTransaction::Log;
		friend class WTransaction::LogWrap;
		friend class WTransaction_Private;
		WTransaction_Private *d;
                
                ///internal: returns true if this transaction can log requests (default: true)
                virtual bool canLogRequest()const;
                ///internal: returns true if this transaction can log responses (default: true)
                virtual bool canLogResponse()const;
};

#endif
