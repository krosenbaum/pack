// Copyright (C) 2009-2011 by Konrad Rosenbaum <konrad@silmor.de>
// protected under the GNU LGPL version 3 or at your option any newer.
// See COPYING.LGPL file that comes with this distribution.
//


#ifndef WOLF_EXCEPTION_H
#define WOLF_EXCEPTION_H

#include <QString>

#ifndef WOLF_BASE_EXPORT
#define WOLF_BASE_EXPORT Q_DECL_IMPORT
#endif

class WOLF_BASE_EXPORT WException
{
	protected:
		WException(QString e,QString c){err=e;comp=c;}
	public:
		QString error()const{return err;}
		QString component()const{return comp;}
	private:
		QString err,comp;
};

#endif
