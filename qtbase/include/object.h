// Copyright (C) 2009-2011 by Konrad Rosenbaum <konrad@silmor.de>
// protected under the GNU LGPL version 3 or at your option any newer.
// See COPYING.LGPL file that comes with this distribution.
//

#ifndef WOLF_WOBJECT_H
#define WOLF_WOBJECT_H

#include <QObject>
#include <QList>
#include <QCoreApplication>

#include "nullable.h"
#include "exception.h"

class QDomElement;
class QDomDocument;

/**base class of all web based objects*/
class WOLF_BASE_EXPORT WObject
{
	Q_GADGET
	protected:
		WObject(){}
		WObject(const WObject&){}
		WObject(WObject&&){}
		WObject(const QDomElement&){}
		virtual ~WObject(){}
		virtual QDomElement toXml(QDomDocument&,QString name="Object");
		virtual WObject& operator=(const WObject&){return *this;}
		virtual WObject& operator=(WObject&&){return *this;}
		virtual void toXml(QDomDocument&,QDomElement&){}
		virtual bool operator==(const WObject&)const{return true;}
		virtual bool operator!=(const WObject&)const{return false;}

		/**helper for de-serializers: returns direct child elements with given tag name (necessary because QDomElement::elementsByTagName traverses all children)*/
		static QList<QDomElement>elementsByTagName(const QDomElement&,QString);

		/**helper for XML decoding: transforms string to boolean*/
		bool str2bool(QString s);
};

/**this exception is thrown if the deserialization of an object fails on the XML parser level*/
class WOLF_BASE_EXPORT WDeserializerException:public WException
{
	public:
		WDeserializerException(QString e):WException(e,"Deserializer"){}
};

/**the WOBJECT macro defines the necessary constructors if you just want to extend an abstract class without overwriting the constructors yourself*/
#define WOBJECT(wob) public: \
 wob():wob ## Abstract(){} \
 wob(const wob&w):wob ## Abstract(w){} \
 wob(wob&&w):wob ## Abstract(w){} \
 wob(const wob ## Abstract&w):wob ## Abstract(w){} \
 wob(wob ## Abstract&&w):wob ## Abstract(w){} \
 wob(const QDomElement&w):wob ## Abstract(w){} \
 wob& operator=(const wob&w){wob ## Abstract::operator=(w);return *this;} \
 wob& operator=(const wob ## Abstract&w){wob ## Abstract::operator=(w);return *this;} \
 wob& operator=(wob&&w){wob ## Abstract::operator=(w);return *this;} \
 wob& operator=(wob ## Abstract&&w){wob ## Abstract::operator=(w);return *this;} \
private:\
 QString tr(const char*text,const char*comment=nullptr)const{return QCoreApplication::translate("" # wob ,text,comment);}


#endif
