// Copyright (C) 2010-2011 by Konrad Rosenbaum <konrad@silmor.de>
// protected under the GNU LGPL version 3 or at your option any newer.
// See COPYING.LGPL file that comes with this distribution.
//

#ifndef WOB_SERVER_H
#define WOB_SERVER_H

#include <QObject>
#include <QHostAddress>
#include <QMap>
#include <QMetaType>

class QTcpServer;
class QLocalServer;
class WInterface;

#ifndef WOLF_BASE_EXPORT
#define WOLF_BASE_EXPORT Q_DECL_IMPORT
#endif

///Encapsulates the request as it comes in via (S)CGI
class WOLF_BASE_EXPORT WServerRequest
{
	public:
		///instantiates an empty request
		WServerRequest();
		///copies a request
		WServerRequest(const WServerRequest&);
		
		///copies a request
		WServerRequest& operator=(const WServerRequest&);
		
		///checks HTTP level header for its existence, in (S)CGI these are prefixed with "HTTP_" -> so hasHeader("Host") checks for the CGI header/variable HTTP_HOST to exist
		bool hasHeader(const QString&)const;
		///returns true if the given low-level variable/header exists
		bool hasCgiHeader(const QString&)const;
		///returns the value of the given HTTP level header
		QByteArray header(const QString&)const;
		///returns the value of the given low-level variable/header
		QByteArray cgiHeader(const QString&)const;
		///returns the request body, or an empty array if there was none
		QByteArray bodyData()const;
		
		///returns true if this is a valid SCGI request
		bool isValid()const;
		
		///dumps the request in a human readable format
		QByteArray dump()const;
		
		///returns the path (Request-Uri) that has been requested by the client
		QString pathInfo()const;
		///returns the (virtual) Host that the client wants to access
		QString hostInfo()const;
		
	protected:
		///used by WServerReceiver to instantiate requests
		WServerRequest(const QByteArray&);
		void setBody(const QByteArray&);
		friend class WServer;
		friend class WServerReceiver;
		
	private:
		QMap<QByteArray,QByteArray>mhead;
		QByteArray mbody;
};
Q_DECLARE_METATYPE(WServerRequest);

///reply object for the server side: contains the data sent back to the client
class WOLF_BASE_EXPORT WServerReply
{
	public:
		///creates an invalid reply (500 Server Error)
		WServerReply();
		///copies the reply object
		WServerReply(const WServerReply&)=default;
		///copies the reply object
		WServerReply& operator=(const WServerReply&)=default;
		
		///sets the return status of the reply
		bool setStatus(short code,QString str);
		///sets a specific header, Content-Length cannot be overridden
		bool setHeader(const QString&,const QString&);
		///converts the string to UTF-8 and sets it as the message body
		void setBody(const QString&);
		///sets the message body and the Content-Length header accordingly
		void setBody(const QByteArray&);
		
	protected:
		friend class WServer;
		/// \internal converts the reply to the SCGI wire format ready to be sent
		QByteArray toWireFormat()const;
	private:
		short mStatCode;
		QByteArray mStatStr,mBody;
		QMap<QString,QPair<QString,QString> >mHead;
};

/** Represents an SCGI server as a proxy to server side interfaces.
 * 
 * Use the cgi2scgi helper to translate between CGI and SCGI.
*/
class WOLF_BASE_EXPORT WServer:public QObject
{
	Q_OBJECT
	public:
		/** creates a new server listening to a local socket 
		 * 
		 * \param path the (file) name of the socket (or named pipe on Windows)
		 * \param removeIfExist if true the socket file is removed before trying to create it again (this is a good idea on Unix/Linux, since existing sockets cannot be created)
		 * \param parent the owning QObject of this server */
		WServer(const QString&path,bool removeIfExist=true,QObject*parent=0);
		/** creates a new server listening to a TCP socket
		 * 
		 * \param addr the address to listen on
		 * \param port the TCP port to listen on
		 * \param parent owning QObject of this server */
		WServer(const QHostAddress&addr,unsigned short port,QObject*parent=0);
		///deletes the server object
		virtual ~WServer()=default;
		
		///returns true if this is a working server (i.e. the constructor did not have errors)
		bool isActive()const{return tcpserv!=0 || locserv!=0;}
		
		///returns the current receive timeout in seconds
		///this is the time that a request has to be completely received
		int receiveTimeout()const{return mrecvtimeout;}
		
		///returns the TCP port if it runs on TCP
		quint16 tcpPort()const;
		
	public slots:
		/** registers an interface to handle a specific path
		 * 
		 * \param path the relative path inside the server's address space
		 * \param ifc the interface that handles this path */
		void registerInterface(const QString&path,WInterface*ifc);
		/** registers a specific content to be served on a path
		 * 
		 * \param path the relative path inside the server's address space
		 * \param content the page content to deliver */
		void registerStatic(const QString&path,const QString&content);
		///deletes whatever registration exists at a specific path
		void unregisterPath(const QString&);
		///registers a custom page for a 404 Not Found error
		void register404(const QString&);
		///registers a custom page for a 400 Invalid Request error
		void register400(const QString&);
		///sets the receive timeout of the server
		void setReceiveTimeout(int);
		///restricts the hosts that can connect to this server
		void restrictSourceHosts(const QPair<QHostAddress,int>&);
		///enable or disable the /debug path, if enabled a request at that path simply returns the content of the request as a response
		void enableDebugUrl(bool enable=true);
		
	private slots:
		/// \internal called when a new connection is available
		void newConnection();
		/// \internal creates a WServerReceiver object to receive and parse the connection
		void handleConnection(QIODevice*);
		/// \internal called to actually handle requests once they are fully received
		void handleRequest(WServerRequest,QIODevice*);
	private:
		void handleRequestHelper(WServerRequest,QIODevice*);
		QTcpServer*tcpserv;
		QLocalServer*locserv;
		QMap<QString,WInterface*>iface;
		QMap<QString,QString>statcontent;
		QString err404,err400;
		int mrecvtimeout;
		QPair<QHostAddress,int>sourcehost;
		bool mdebugenabled;
};

#endif
