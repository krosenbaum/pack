// Copyright (C) 2009-2011 by Konrad Rosenbaum <konrad@silmor.de>
// protected under the GNU LGPL version 3 or at your option any newer.
// See COPYING.LGPL file that comes with this distribution.
//

#include "WHelper"

#include "helper_p.h"

QList<QDomElement>WHelper::elementsByTagName(const QDomElement&root,QString tag)
{
	return helper_elementsByTagName(root,tag);
}

bool WHelper::str2bool(QString s)
{
	return helper_str2bool(s);
}
