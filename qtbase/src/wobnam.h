// Copyright (C) 2013 by Konrad Rosenbaum <konrad@silmor.de>
// protected under the GNU LGPL version 3 or at your option any newer.
// See COPYING.LGPL file that comes with this distribution.
//

#ifndef WOB_NAM_H
#define WOB_NAM_H


#include <QNetworkAccessManager>
class QNetworkReply;

/** Extended version of QNetworkAccessManager that can handle SCGI directly.
 * 
 * This is used by PACK to be able to connect to Qt based PACK servers directly
 * instead of tunneling through a web server and/or CGI.
 * 
 * URL formats:
 * scgi://host:port/path -> TCP
 * scgissl://host:port/path -> SSL
 * scgilocal://dummyhost/path?_lpath_=/path/to/sock -> local socket, the socket path name is encoded in the _lpath_ attribute
 */
class WobNetworkAccessManager:public QNetworkAccessManager
{
protected:
	///creates a request for a standard operation
	virtual QNetworkReply * createRequest(Operation op, const QNetworkRequest & req, QIODevice * outgoingData = 0);
public:
	///creates a new Network Access Manager
	explicit WobNetworkAccessManager(QObject* parent = 0);
	///creates a request with a custom operation
	virtual QNetworkReply * sendCustomRequest(const QNetworkRequest & request, const QByteArray & verb, QIODevice * data = 0);
};


#endif