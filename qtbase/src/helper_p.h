#ifndef WOB_HELPER_PH
#define WOB_HELPER_PH

#include <QDomElement>
#include <QDomNode>

static inline QList<QDomElement>helper_elementsByTagName(const QDomElement&root,QString tag)
{
	QDomNodeList cn=root.childNodes();
	QList<QDomElement>ret;
	for(int i=0;i<cn.size();i++){
		QDomElement e=cn.at(i).toElement();
		if(e.isNull())continue;
		if(e.tagName()!=tag)continue;
		ret<<e;
	}
	return ret;
}

static inline bool helper_str2bool(QString s)
{
	s=s.trimmed().toLower();
	bool ok;
	int i=s.toInt(&ok);
	if(ok)return i!=0;
	if(s=="true" || s=="yes")return true;
	else return false;
}


#endif
