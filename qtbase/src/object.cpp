// Copyright (C) 2009-2011 by Konrad Rosenbaum <konrad@silmor.de>
// protected under the GNU LGPL version 3 or at your option any newer.
// See COPYING.LGPL file that comes with this distribution.
//

#include "WObject"

#include "Nullable"
#include <QDomDocument>
#include <QString>
#include <QDebug>
#include <QVariant>
#include <QVariantList>

QDomElement WObject::toXml(QDomDocument&doc,QString name)
{
	return doc.createElement(name);
}

#include "helper_p.h"

QList<QDomElement>WObject::elementsByTagName(const QDomElement&root,QString tag)
{
	return helper_elementsByTagName(root,tag);
}

bool WObject::str2bool(QString s)
{
	return helper_str2bool(s);
}


static inline bool w(bool b){qDebug()<<"registering"<<b;return b;}

static int itype0=
	qRegisterMetaType<Nullable<int> >()+
	qRegisterMetaType<Nullable<qint32> >()+
	qRegisterMetaType<Nullable<qint64> >()+
	qRegisterMetaType<Nullable<quint32> >()+
	qRegisterMetaType<Nullable<quint64> >()+
	qRegisterMetaType<Nullable<QString> >()+
	qRegisterMetaType<Nullable<QByteArray> >()+
	qRegisterMetaType<Nullable<bool> >();

static bool btype0=
	//NULLABLE
	//String/bytes to self
	QMetaType::registerConverter<Nullable<QString>,QString>(&Nullable<QString>::valueOrDefault)|
	QMetaType::registerConverter<Nullable<QByteArray>,QByteArray>(&Nullable<QByteArray>::valueOrDefault)|
	//int/bool to self
	QMetaType::registerConverter<Nullable<bool>,bool>(&Nullable<bool>::valueOrDefault)|
	QMetaType::registerConverter<Nullable<int>,int>(&Nullable<int>::valueOrDefault)|
	QMetaType::registerConverter<Nullable<qint32>,qint32>(&Nullable<qint32>::valueOrDefault)|
	QMetaType::registerConverter<Nullable<qint64>,qint64>(&Nullable<qint64>::valueOrDefault)|
	QMetaType::registerConverter<Nullable<quint32>,quint32>(&Nullable<quint32>::valueOrDefault)|
	QMetaType::registerConverter<Nullable<quint64>,quint64>(&Nullable<quint64>::valueOrDefault)|
	//int upgrade with keeping sign
	QMetaType::registerConverter<Nullable<int>,qint64>([](Nullable<int>v)->qint64{return v.valueOrDefault();})|
	QMetaType::registerConverter<Nullable<qint32>,qint64>([](Nullable<qint32>v)->qint64{return v.valueOrDefault();})|
	QMetaType::registerConverter<Nullable<quint32>,quint64>([](Nullable<quint32>v)->quint64{return v.valueOrDefault();})|
	//risky int upgrade unsigned<->signed
	QMetaType::registerConverter<Nullable<int>,quint64>([](Nullable<int>v)->quint64{return v.valueOrDefault();})|
	QMetaType::registerConverter<Nullable<qint32>,quint64>([](Nullable<qint32>v)->quint64{return v.valueOrDefault();})|
	QMetaType::registerConverter<Nullable<qint64>,quint64>([](Nullable<qint64>v)->quint64{return v.valueOrDefault();})|
	QMetaType::registerConverter<Nullable<quint32>,qint64>([](Nullable<quint32>v)->qint64{return v.valueOrDefault();})|
	QMetaType::registerConverter<Nullable<quint64>,qint64>([](Nullable<quint64>v)->qint64{return v.valueOrDefault();})|
	//int to string
	QMetaType::registerConverter<Nullable<int>,QString>([](Nullable<int> v)->QString{return QString::number(v.valueOrDefault());})|
	QMetaType::registerConverter<Nullable<qint32>,QString>([](Nullable<qint32> v)->QString{return QString::number(v.valueOrDefault());})|
	QMetaType::registerConverter<Nullable<qint64>,QString>([](Nullable<qint64> v)->QString{return QString::number(v.valueOrDefault());})|
	QMetaType::registerConverter<Nullable<quint32>,QString>([](Nullable<quint32> v)->QString{return QString::number(v.valueOrDefault());})|
	QMetaType::registerConverter<Nullable<quint64>,QString>([](Nullable<quint64> v)->QString{return QString::number(v.valueOrDefault());})|
	//LIST
	//string is predefined
	//int/bool
	QMetaType::registerConverter<QList<bool>,QVariantList>([](const QList<bool>&n){QVariantList r;for(auto v:n)r<<QVariant::fromValue(v);return r;})|
	QMetaType::registerConverter<QList<qint32>,QVariantList>([](const QList<qint32>&n){QVariantList r;for(auto v:n)r<<QVariant::fromValue(v);return r;})|
	QMetaType::registerConverter<QList<qint64>,QVariantList>([](const QList<qint64>&n){QVariantList r;for(auto v:n)r<<QVariant::fromValue(v);return r;})|
	QMetaType::registerConverter<QList<quint32>,QVariantList>([](const QList<quint32>&n){QVariantList r;for(auto v:n)r<<QVariant::fromValue(v);return r;})|
	QMetaType::registerConverter<QList<quint64>,QVariantList>([](const QList<quint64>&n){QVariantList r;for(auto v:n)r<<QVariant::fromValue(v);return r;})|
	QMetaType::registerConverter<QList<int>,QVariantList>([](const QList<int>&n){QVariantList r;for(auto v:n)r<<QVariant::fromValue(v);return r;})|
	true;
