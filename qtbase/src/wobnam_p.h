// Copyright (C) 2013 by Konrad Rosenbaum <konrad@silmor.de>
// protected under the GNU LGPL version 3 or at your option any newer.
// See COPYING.LGPL file that comes with this distribution.
//

#ifndef WOB_NAM_P_H
#define WOB_NAM_P_H


#include <QNetworkReply>
#include <QNetworkRequest>
#include <QSslSocket>
#include <QLocalSocket>

/// \internal SCGI client reply
class WobScgiNetworkReplyImpl:public QNetworkReply
{
	Q_OBJECT
public:
	/// creates a new reply handler/receiver
	WobScgiNetworkReplyImpl(QNetworkRequest,QByteArray op,QIODevice*, QObject* parent);
	///abort the reception (just closes the stream, does not actually abort anything yet)
	void abort()override;
	///closes the reply stream, call this only if you are no longer interested in the answer
	void close()override;
	///returns the amount of body data available to be read
	qint64 bytesAvailable() const override;
	bool isSequential() const override{return true;}
	///returns the complete size of the body
	qint64 size()const override;

public slots:
	///relays SSL errors
	virtual void ignoreSslErrors()override;
    
protected:
	///implementation of reading body data
	qint64 readData(char *data, qint64 maxSize) override;

private slots:
	/// \internal TCP socket errors
	void sockerror(QAbstractSocket::SocketError);
	/// \internal Local socket errors
	void lsockerror(QLocalSocket::LocalSocketError);
	/// \internal starts writing request data
	void startwrite();
	/// \internal the connection was aborted/closed by the peer
	void sockabort();
	/// \internal more response data available for reading
	void sockread();

private:
	QByteArray outcontent,incontent,inhead;
	qint64 offset;
	QIODevice*sock;
	QByteArray op;
	
	enum ReadMode {HeadRead,ContentRead}readMode;
	
	/// \internal helper to finish the response and signal the owner
	void finishUp();
};


#endif