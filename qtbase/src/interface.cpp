// Copyright (C) 2009-2013 by Konrad Rosenbaum <konrad@silmor.de>
// protected under the GNU LGPL version 3 or at your option any newer.
// See COPYING.LGPL file that comes with this distribution.
//

#include <WInterface>
#include <WServer>

#include <QMutex>
#include <QMutexLocker>
#include <QNetworkReply>
#include <QNetworkRequest>
#include <QNetworkProxy>

#include "wobnam.h"

QMap<QString,WInterface*> WInterface::inst;

#if QT_VERSION > 0x060000
#include <QRecursiveMutex>
static QRecursiveMutex mtx;
#else
static QMutex mtx(QMutex::Recursive);
#endif

WInterface*WInterface::instance(QString name)
{
	QMutexLocker ml(&mtx);
	if(inst.contains(name))return inst[name];
	else return 0;
}

WInterface::WInterface(QString name)
{
	m_proxyport=0;
	m_wtimeout=30;
	loglvl=LogOnError;
	QMutexLocker ml(&mtx);
	if(inst.contains(name)){
		delete inst[name];
	}
	inst.insert(name,this);
	m_netman=new WobNetworkAccessManager(this);
	connect(m_netman,SIGNAL(sslErrors(QNetworkReply*,QList<QSslError>)), this,SLOT(sslErrors(QNetworkReply*,QList<QSslError>)));
}

QString WInterface::name()const
{
	QMutexLocker ml(&mtx);
	return inst.key((WInterface*)this);
}

WInterface::~WInterface()
{
	QMutexLocker ml(&mtx);
	inst.remove(inst.key((WInterface*)this));
}

QMap<QString,QString> WInterface::headers(QString)const
{
	return QMap<QString,QString>();
}

void WInterface::sslErrors(QNetworkReply *reply,const QList<QSslError>&)
{
	if(!reply)return;
	reply->ignoreSslErrors();
}

QNetworkReply* WInterface::post(const QNetworkRequest&request,const QByteArray&data )
{
	return m_netman->post(request,data);
}

void WInterface::setProxy(QString proxyhost,unsigned short proxyport,QString proxyuser,QString proxypassword)
{
	m_proxyhost=proxyhost;
	m_proxyport=proxyport;
	m_proxyuser=proxyuser;
	m_proxypass=proxypassword;
	m_netman->setProxy(QNetworkProxy(QNetworkProxy::HttpProxy,proxyhost,proxyport,proxyuser,proxypassword));
}

WServerReply WInterface::execute(const WServerRequest&)
{
	WServerReply rp;
	rp.setStatus(500,"Not implemented");
	rp.setHeader("Content-Type","text/html; charset=utf-8");
	rp.setBody((QString)"<h1>Sorry</h1>Not implemented.");
	return rp;
}

#include "../../vinfo/staticVersion.h"

QString WInterface::libraryVersionInfo(WOb::VersionInfo vi)const
{
	return WOCgenerated_versionInfo(vi);
}
QString WInterface::staticLibraryVersionInfo(WOb::VersionInfo vi)
{
	return WOCgenerated_versionInfo(vi);
}
