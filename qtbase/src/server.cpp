// Copyright (C) 2009-2011 by Konrad Rosenbaum <konrad@silmor.de>
// protected under the GNU LGPL version 3 or at your option any newer.
// See COPYING.LGPL file that comes with this distribution.
//

#include "server.h"
#include "server_p.h"
#include <WInterface>

#include <QDebug>
#include <QFile>
#include <QLocalServer>
#include <QLocalSocket>
#include <QRegExp>
#include <QStringList>
#include <QTcpServer>
#include <QTcpSocket>
#include <QTimer>

static int metaid=qRegisterMetaType<WServerRequest>();

#define ERR404 "<h1>Path does not exist</h1>\nSorry, the path you requested does not exist.\n"
#define ERR400 "<h1>Invalid Request</h1>\nSorry, the request was invalid.\n"

WServer::WServer(const QString&path,bool removeIfExist,QObject*parent)
:QObject(parent)
{
	mrecvtimeout=30000;
	tcpserv=0;
	mdebugenabled=false;
	if(removeIfExist){
		QFile::remove(path);
	}
	locserv=new QLocalServer(this);
	if(!locserv->listen(path)){
		qDebug()<<"Error creating server socket on path"<<path;
		delete locserv;
		locserv=0;
		return;
	}
	connect(locserv,SIGNAL(newConnection()),this,SLOT(newConnection()));
	err404=ERR404;
	err400=ERR400;
}

WServer::WServer(const QHostAddress&host,unsigned short port,QObject*parent)
:QObject(parent)
{
	mrecvtimeout=30000;
	locserv=0;
	mdebugenabled=false;
	tcpserv=new QTcpServer(this);
	if(!tcpserv->listen(host,port)){
		qDebug()<<"Unable to create server socket for host"<<host.toString()<<"port"<<port;
		delete tcpserv;
		tcpserv=0;
		return;
	}
	connect(tcpserv,SIGNAL(newConnection()),this,SLOT(newConnection()));
	err404=ERR404;
	err400=ERR400;
}

void WServer::enableDebugUrl(bool enable){mdebugenabled=enable;}

quint16 WServer::tcpPort()const
{
        if(tcpserv==nullptr)return 0;
        return tcpserv->serverPort();
}

static inline QString normalizepath(const QString &p)
{
	QStringList src=QString(p).replace("\\","/").split("/",Qt::SkipEmptyParts);
	QStringList tgt;
	for(int i=0;i<src.size();i++){
		if(src[i]==".")continue;
		if(src[i]=="..")tgt.removeLast();
		else tgt<<QUrl::toPercentEncoding(src[i]);
	}
	QString ret;
	for(int i=0;i<tgt.size();i++){
		ret+="/";
		ret+=tgt[i];
	}
	return ret;
}

void WServer::registerInterface(const QString&p,WInterface*ifc)
{
	QString path=normalizepath(p);
	iface.remove(path);
	statcontent.remove(path);
	if(ifc!=0)
		iface.insert(path,ifc);
}

void WServer::registerStatic(const QString&p,const QString&c)
{
	QString path=normalizepath(p);
	iface.remove(path);
	statcontent.remove(path);
	statcontent.insert(path,c);
}

void WServer::unregisterPath(const QString&p)
{
	QString path=normalizepath(p);
	iface.remove(path);
	statcontent.remove(path);
}

void WServer::register404(const QString&e){err404=e;}
void WServer::register400(const QString&e){err400=e;}

void WServer::newConnection()
{
	//get connection
	if(tcpserv!=0){
		while(tcpserv->hasPendingConnections()){
			QTcpSocket*sock=tcpserv->nextPendingConnection();
// 			qDebug()<<"new TCP connection from"<<sock->peerAddress()<<"port"<<sock->peerPort();
			if(sourcehost.second>0){
				if(!sock->peerAddress().isInSubnet(sourcehost)){
					sock->close();
					sock->deleteLater();
// 					qDebug()<<"dropping TCP connection";
					continue;
				}
			}
			handleConnection(sock);
		}
	}
	if(locserv!=0){
		while(locserv->hasPendingConnections()){
// 			qDebug()<<"new Local connection";
			handleConnection(locserv->nextPendingConnection());
		}
	}
}

void WServer::handleConnection(QIODevice*conn)
{
	new WServerReceiver(this,conn);
}

static inline bool pathMatch(QString parent,QString subpath)
{
	int ps=parent.size();
	int ss=subpath.size();
	if(ss<ps)return false;
	if(ss==ps)return subpath==parent;
	return subpath.left(ps)==parent && subpath[ps]=='/';
}

static bool qstringlonger(const QString&s1,const QString&s2)
{
	return s1.size()>s2.size();
}

void WServer::handleRequestHelper(WServerRequest rq,QIODevice*sock)
{
// 	qDebug()<<"handling request for host"<<rq.hostInfo()<<"path"<<rq.pathInfo();
// 	qDebug()<<rq.dump();
	//reject invalid requests outright
	if(!rq.isValid()){
// 		qDebug()<<"rejecting invalid request with code 400";
		sock->write(QByteArray("Status: 400 Invalid Request\r\nContent-Type: text/html; charset=utf-8\r\n\r\n")+err400.toUtf8());
		return;
	}
	QString path=rq.pathInfo();
	//check for debug mode
	if(mdebugenabled && pathMatch("/debug",path)){
// 		qDebug()<<"debug: dumping request data as response";
		sock->write(QByteArray("Status: 200 Ok\r\nContent-Type: text/plain\r\n\r\n"));
		sock->write(rq.dump());
		return;
	}
	//find a match
	QStringList k=statcontent.keys();
	k<<iface.keys();
	std::sort(k.begin(),k.end(),qstringlonger);
	for(int i=0;i<k.size();i++)
		if(pathMatch(k[i],path)){
			//find a static match
			if(statcontent.contains(k[i])){
// 				qDebug()<<"returning static content";
				sock->write(QByteArray("Status: 200 Ok\r\nContent-Type: text/html; charset=utf-8\r\n\r\n"));
				sock->write(statcontent[k[i]].toUtf8());
				return;
			}else
			//find dynamic match
			if(iface.contains(k[i])){
// 				qDebug()<<"returning dynamic content";
				WServerReply rp=iface[k[i]]->execute(rq);
				sock->write(rp.toWireFormat());
				return;
			}
		}
	//no match, send a 404
// 	qDebug()<<"no match found, sending 404";
	sock->write(QByteArray("Status: 404 File not found\r\nContent-Type: text/html; charset=utf-8\r\n\r\n")+err404.toUtf8());
}
void WServer::handleRequest(WServerRequest rq,QIODevice*sock)
{
	handleRequestHelper(rq,sock);
	//flush buffers
	QTcpSocket *tc=qobject_cast<QTcpSocket*>(sock);
	if(tc!=0)tc->flush();
	QLocalSocket*ls=qobject_cast<QLocalSocket*>(sock);
	if(ls!=0)ls->flush();
	//close it
	connect(sock,SIGNAL(disconnected()),sock,SLOT(deleteLater()));
	sock->close();
}

void WServer::restrictSourceHosts(const QPair<QHostAddress,int>&addr)
{
	sourcehost=addr;
}

void WServer::setReceiveTimeout(int r)
{
	if(r<=0)return;
	mrecvtimeout=r;
}

WServerRequest::WServerRequest(){}
WServerRequest::WServerRequest(const WServerRequest&r)
{
	mbody=r.mbody;
	mhead=r.mhead;
}
WServerRequest::WServerRequest(const  QByteArray&b)
{
	QList<QByteArray>ba=b.split((char)0);
	for(int i=1;i<ba.size();i+=2)
		mhead.insert(ba[i-1],ba[i]);
}

WServerRequest& WServerRequest::operator=(const WServerRequest&r)
{
	mbody=r.mbody;
	mhead=r.mhead;
	return *this;
}

static inline QString headerVar(QString h)
{
	return h.toUpper().replace("-","_");
}

bool WServerRequest::hasCgiHeader(const QString&h)const
{
	return mhead.contains(headerVar(h).toLatin1());
}

bool WServerRequest::hasHeader(const QString&h)const
{
	return hasCgiHeader("HTTP_"+h);
}

QByteArray WServerRequest::header(const QString&h)const
{
	return cgiHeader("HTTP_"+h);
}

QByteArray WServerRequest::cgiHeader(const QString&h)const
{
	return mhead.value(headerVar(h).toLatin1());
}
QString WServerRequest::pathInfo()const
{
	QString p=QString::fromLatin1(cgiHeader("Request-Uri"));
	if(p=="")return "/";
	else return p;
}
QString WServerRequest::hostInfo()const
{
	return QString::fromLatin1(header("Host"));
}
QByteArray WServerRequest::bodyData()const
{
	return mbody;
}

void WServerRequest::setBody(const QByteArray&b){mbody=b;}

bool WServerRequest::isValid()const
{
	return hasCgiHeader("SCGI");
}

QByteArray WServerRequest::dump()const
{
	QByteArray ret,hn;
	foreach(hn,mhead.keys()){
		ret+=hn+": "+mhead[hn]+"\r\n";
	}
	ret+="\r\n"+mbody;
	return ret;
}

WServerReply::WServerReply()
{
	mStatCode=500;
	mStatStr="Internal Error";
}
bool WServerReply::setStatus(short code,QString str)
{
	//check the return code
	if(code<100 || code>=1000)return false;
	//check the string
	if(str.contains('\r')||str.contains('\n'))return false;
	if(str.size()>500)return false;
	//assign
	mStatCode=code;
	mStatStr=str.toLatin1();
	return true;
}

static QRegExp headerreg("[A-Za-z0-9-_]+");

bool WServerReply::setHeader(const QString&h,const QString&c)
{
	//check header
	if(!headerreg.exactMatch(h))return false;
	//check content
	if(c.contains('\r')||c.contains('\n'))return false;
	//check length
	if((h.size()+c.size()+2)>1024)return false;
	//check for reserved headers
	QString hd=h.toUpper().replace("-","_");
	if(hd=="STATUS" || hd=="CONTENT_LENGTH")return false;
	//set
	mHead.insert(hd,QPair<QString,QString>(h,c));
	return true;
}
void WServerReply::setBody(const QString&b)
{
	setBody(b.toUtf8());
}
void WServerReply::setBody(const QByteArray&b)
{
	mBody=b;
	mHead.insert("CONTENT_LENGTH", QPair<QString,QString>("Content-Length", QByteArray::number(b.size())));
}

QByteArray WServerReply::toWireFormat()const
{
	QByteArray ret;
	//top header
	ret="Status: "+QByteArray::number(mStatCode)+" "+mStatStr+"\r\n";
	QStringList hl=mHead.keys();
	for(int i=0;i<hl.size();i++)
		ret+=mHead[hl[i]].first.toLatin1()+": "+mHead[hl[i]].second.toLatin1()+"\r\n";
	//body
	ret+="\r\n"+mBody;
	//return it
	return ret;
}

WServerReceiver::WServerReceiver(WServer*s,QIODevice*d)
	:QObject(s)
{
	//init
	server=s;
	sock=d;
	mode=StartMode;
	//set up for reading/closing
	connect(d,SIGNAL(readyRead()),this,SLOT(receiveMore()));
	connect(d,SIGNAL(disconnected()),this,SLOT(deleteLater()));
	if(qobject_cast<QLocalSocket*>(d)!=0)
		connect(d,SIGNAL(error(QLocalSocket::LocalSocketError)),this,SLOT(deleteLater()));
	else
		connect(d,SIGNAL(error(QAbstractSocket::SocketError)),this,SLOT(deleteLater()));
	//set up time out
	QTimer::singleShot(s->receiveTimeout(),this,SLOT(deleteLater()));
	//set up for handling
	connect(this,SIGNAL(readyForProcess(WServerRequest,QIODevice*)), s,SLOT(handleRequest(WServerRequest,QIODevice*)), Qt::QueuedConnection);
	//read initial data
	receiveMore();
}

WServerReceiver::~WServerReceiver()
{
	if(sock!=0)sock->deleteLater();
}

void WServerReceiver::receiveMore()
{
	//read
	qint64 ba=sock->bytesAvailable();
	if(ba<=0)return;
// 	qDebug()<<"reading next"<<ba<<"bytes";
	buffer+=sock->read(ba);
	//interpret
	if(mode==StartMode){
		if(buffer.contains(':')){
			int p=buffer.indexOf(':');
			explen=buffer.left(p).toInt();
			buffer=buffer.mid(p+1);
			mode=HeaderMode;
// 			qDebug()<<"proceeding to header of size"<<explen;
		}
	}
	if(mode==HeaderMode){
		if(buffer.size()>=explen){
			QByteArray head=buffer.left(explen);
			buffer=buffer.mid(explen);
			request=WServerRequest(head);
			if(request.hasCgiHeader("Content-Length"))
				explen=request.cgiHeader("Content-Length").toInt();
			else
				explen=0;
			if(explen>0)
				mode=BodyMode;
			else
				mode=Completed;
// 			qDebug()<<"header complete next is"<<explen;
		}
	}
	if(mode==BodyMode){
		if(buffer.size()>explen){//this is not an off-by-one, see below
			request.setBody(buffer.mid(1,explen));
// 			qDebug()<<"body completed";
			mode=Completed;
		}
	}
	//check for completion
	if(mode==Completed){
// 		qDebug()<<"request completed, now handling it";
		emit readyForProcess(request,sock);
		sock=0;
		deleteLater();
	}
}
