// Copyright (C) 2009-2011 by Konrad Rosenbaum <konrad@silmor.de>
// protected under the GNU LGPL version 3 or at your option any newer.
// See COPYING.LGPL file that comes with this distribution.
//

#include "WTransaction_Private"
#include "WTransaction"
#include "WInterface"

#include <QCoreApplication>
#include <QDateTime>
#include <QDebug>
#include <QDomDocument>
#include <QDomElement>
#include <QEventLoop>
// #include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QNetworkRequest>
#include <QStringList>
#include <QTimer>
#include <QUrl>

WTransaction::WTransaction(QString ifc)
{
	d=new WTransaction_Private(this);
	d->m_stage=Uninitialized;
	d->m_iface=ifc;
	d->m_log=0;
}
WTransaction::WTransaction(const WTransaction&t)
	:WHelper()
{
	d=t.d->attach(this);
	connect(this,SIGNAL(webFinished()),this,SLOT(endQuery()));
}

WTransaction::~WTransaction()
{
	d=0;
}

WTransaction& WTransaction::operator=(const WTransaction&t)
{
	d->detach(this);
	d=t.d->attach(this);
	return *this;
}

static inline QString esc(const QByteArray ar)
{
	QString r;
	for(int i=0;i<ar.size();i++){
		unsigned char a=ar[i];
		if(a=='\\')r+="\\\\";else
		if(a=='\n' || a=='\r' || (a>=32 && a<=127))r+=(char)a;
		else r+="\\x"+QByteArray((char*)&a,1).toHex();
	}
	return r;
}

static inline QString hd2str(const QList<QNetworkReply::RawHeaderPair>&hl)
{
	QString r;
	for(int i=0;i<hl.size();i++)
		r+=QString::fromLatin1(hl[i].first)+"="+QString::fromLatin1(hl[i].second)+"\n";
	return r;
}

static QList<QNetworkReply::RawHeaderPair> rqpairs(const QNetworkRequest&req)
{
	QList<QByteArray>hds=req.rawHeaderList();
	QList<QNetworkReply::RawHeaderPair>ret;
	for(int i=0;i<hds.size();i++)
		ret<<QNetworkReply::RawHeaderPair(hds[i],req.rawHeader(hds[i]));
	return ret;
}

void WTransaction::startQuery(QString hreq,QByteArray data){d->startQuery(hreq,data);}
void WTransaction_Private::startQuery(QString hreq,QByteArray data)
{
	QNetworkRequest req;
	//find interface
	WInterface *iface=WInterface::instance(m_iface);
	if(!iface){
		if(m_log)m_log->setError("cannot find interface.");
		else qDebug("Error: transaction cannot find interface.");
		m_stage=WTransaction::Error;
		m_errtype="_iface";
		m_errstr=tr("interface not found");
		return;
	}
	//set target
	QUrl url=iface->url();
	int port=url.port();
	if(port<=0){
		const QString&s=url.scheme().toLower();
		if(s=="http")port=80;
		else if(s=="https")port=443;
	}
	if(m_log)m_log->setInfo(QString("New HTTP Request %1 URL %2\n\tto host=%3 port=%4 scheme=%5")
		.arg(hreq)
		.arg(url.toString())
		.arg(url.host())
		.arg(port)
		.arg(url.scheme()));
	req.setUrl(url);
	//set headers
	QMap<QString,QString>hdrs=iface->headers(hreq);
	QStringList hdrn=hdrs.keys();
	for(int i=0;i<hdrn.size();i++)
		req.setRawHeader("X-"+hdrn[i].toLatin1(),hdrs[hdrn[i]].toLatin1());
	req.setRawHeader("X-WobRequest",hreq.toLatin1());
	req.setHeader(QNetworkRequest::ContentLengthHeader,QByteArray::number(data.size()));
	req.setHeader(QNetworkRequest::ContentTypeHeader,"application/x-webobject; charset=UTF-8");
	//send it (with body)
	m_rsp=iface->post(req,data);
	//latest point of deletion: end of this transaction
	m_rsp->setParent(this);
	//log request
	if(m_log){
		m_log->setRequ(hd2str(rqpairs(req)),esc(data));
	}
	//we are requesting now...
	m_stage=WTransaction::Request;
	//schedule receiving
	if(m_qtimeout.isNull()){
		m_qtimeout=new QTimer(this);
		connect(m_qtimeout,SIGNAL(timeout()),this,SLOT(webTimeout()));
	}
	m_qtimeout->setSingleShot(true);
	m_qtimeout->start(iface->webTimeout()*1000);
	connect(m_rsp,SIGNAL(finished()),this,SLOT(webReady()));
	connect(m_rsp,SIGNAL(error(QNetworkReply::NetworkError)),this,SLOT(webError()));

}

//actually implemented in child classes
void WTransaction::endQuery(){}
//called from child classes
void WTransaction_Private::endQuery()
{
	//sanity check
	if(m_rsp.isNull())return;
	m_rspdata=QByteArray();
	//unschedule timeout
	if(!m_qtimeout.isNull())
		m_qtimeout->stop();
	//delete response object when done here
	QObject rspown;
	connect(&rspown,SIGNAL(destroyed(QObject*)),m_rsp,SLOT(deleteLater()));
	//process result
	if(m_stage==WTransaction::Error && m_errtype=="_timeout"){
		//it did not finish yet, caught a timeout.
		m_rsp->abort();
		m_errstr=tr("Web Request timed out.");
		if(m_log)m_log->setError("Request timed out.");
		return;
	}else
	//finished with low-level error?
	if(m_stage==WTransaction::Error){
		m_errstr="HTTP Error: "+m_rsp->errorString();
		if(m_log)m_log->setError(QString("Request finished with HTTP level error: %1").arg(m_errstr));
		return;
	}
	//get data
	m_wobstatus=m_rsp->rawHeader("X-WobResponse-Status");
	m_wobstatus=m_wobstatus.replace("\"","").trimmed().toLower();
	m_rspdata=m_rsp->readAll();
	if(m_log)m_log->setResp(hd2str(m_rsp->rawHeaderPairs()),esc(m_rspdata));
	//check for high level error
	int statcode=m_rsp->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt();
	if(statcode!=200){
		m_errstr=tr("HTTP Error, return code %1 %2") .arg(statcode).arg(m_rsp->attribute(QNetworkRequest::HttpReasonPhraseAttribute).toString());
		m_errtype="_HTTP";
		m_stage=WTransaction::Error;
		if(m_log)m_log->setError(QString("Request finished with HTTP level error: %1").arg(m_errstr));
		return;
	}
	if(m_stage==WTransaction::Request){
		m_stage=WTransaction::Error;
		m_errtype="_internal";
		m_errstr="Ooops: still in Request phase at end of response handling.";
		return;
	}
	//remaining high level errors are handled by the generated code
	emit dataAvailable();
}

QByteArray WTransaction::executeQuery(QString hreq,QByteArray data){return d->executeQuery(hreq,data);}
QByteArray WTransaction_Private::executeQuery(QString hreq,QByteArray data)
{
	//sanity check
	if(!m_rsp.isNull() || m_stage==WTransaction::Request)
		return QByteArray();
	
	//set up request
	startQuery(hreq,data);
	if(m_rsp.isNull() || m_stage!=WTransaction::Request)
		return QByteArray();
	
	/////////////////////////////////////////////////////////////////////
	//start loop
	QEventLoop loop(this);
	connect(this,SIGNAL(webFinished()),&loop,SLOT(quit()),Qt::QueuedConnection);
	loop.exec();
	/////////////////////////////////////////////////////////////////////
	
	if(m_stage==WTransaction::Success)
		return m_rspdata;
	else
		return QByteArray();
}

bool WTransaction::waitForFinished(int tmout)
{
	//is it running?
	if(d->m_stage==Success)return true;
	if(d->m_stage!=Request)return false;
	//wait for it
	QEventLoop loop;
	connect(this,SIGNAL(finished()),&loop,SLOT(quit()));
	if(tmout>0)
		QTimer::singleShot(tmout,&loop,SLOT(quit()));
	loop.exec();
	//return
	return d->m_stage==Success;
}

void WTransaction_Private::webTimeout()
{
	if(m_stage!=WTransaction::Request)return;
// 	qDebug("web timeout");
	if(m_log)m_log->setInfo(QString("timeout in request"));
	m_stage=WTransaction::Error;
	m_errtype="_timeout";
	emit webFinished();
}

void WTransaction_Private::webReady()
{
	if(m_stage!=WTransaction::Request)return;
	if(m_log)m_log->setInfo(QString("finished request"));
	m_stage=WTransaction::Success;
	m_errtype="";
	emit webFinished();
}

void WTransaction_Private::webError()
{
	if(m_stage!=WTransaction::Request)return;
	if(m_log)m_log->setInfo(QString("error in request"));
	m_stage=WTransaction::Error;
	m_errtype="_web";
	emit webFinished();
}

QString WTransaction::errorString()const
{
	return QCoreApplication::translate("php::",d->m_errstr.toUtf8(),0);
}

WTransaction::Stage WTransaction::stage()const{return d->m_stage;}
bool WTransaction::hasError()const{return d->m_stage==Error;}
QString WTransaction::errorType()const{return d->m_errtype;}
QString WTransaction::interface()const{return d->m_iface;}

QByteArray WTransaction::encodeError()const
{
	QDomDocument doc;
	//TODO: add namespace
	QDomElement root=doc.createElement("WobError");
	root.setAttribute("type",errorType());
	root.appendChild(doc.createTextNode(errorString()));
	doc.appendChild(root);
	return doc.toByteArray();
}

bool WTransaction::canLogRequest()const{return true;}
bool WTransaction::canLogResponse()const{return true;}

/*****************************************************************************/

static int logid=0;
static QByteArray logprefix="T";

void WTransaction::setLogPrefix(QString p){logprefix=p.toLatin1();}

WTransaction::Log::Log(WTransaction*p,const QString&s,const QString&i)
{
	Q_ASSERT_X(p!=0,"WTransaction::Log constructor","Logger Parent must be a valid transaction object.");
	parent=p;
	if(parent->d->m_log!=0)delete parent->d->m_log;
	parent->d->m_log=this;
	trn=s;
	if(i.isNull())
		ifc=p->interface();
	else
		ifc=i;
	cnt=logid++;
	WInterface*in=WInterface::instance(p->d->m_iface);
	if(in)lvl=in->logLevel();
	else lvl=WInterface::LogOnError;
	if(lvl>WInterface::LogNone)
		outStr(QString("Running transaction %1 on interface %2").arg(s).arg(ifc));
}

WTransaction::Log::~Log()
{
	if(parent->d->m_log==this)
		parent->d->m_log=0;
}

void WTransaction::Log::outStr(const QString&s)
{
	QStringList sl=s.split("\n");
	for(int i=0;i<sl.size();i++)
		qDebug("%s%i: %s",logprefix.data(),cnt,sl[i].toLatin1().data());
}

void WTransaction::Log::setRequ(const QString&h,const QString&d)
{
	if(lvl<=WInterface::LogInfo)return;
	QString r;
        if(parent && parent->canLogRequest())
                r=QString("Transaction %1, Request:\n--Header--\n%2\n--Body--\n%3\n--End--")
                        .arg(trn).arg(h).arg(d);
        else
                r=QString("Transaction %1, Request:\n--Header--\n%2\n--Cannot Log Body: Log Forbidden--")
                        .arg(trn).arg(h);
	if(lvl==WInterface::LogDetailed)
		outStr(r);
	else
	if(lvl==WInterface::LogOnError)
		req=r;
}

void WTransaction::Log::setResp(const QString&h,const QString&d)
{
	if(lvl<=WInterface::LogInfo)return;
	QString r;
        if(parent && parent->canLogResponse())
                r=QString("Transaction %1, Response:\n--Header--\n%2\n--Body--\n%3\n--End--")
                        .arg(trn).arg(h).arg(d);
        else
                r=QString("Transaction %1, Response:\n--Header--\n%2\n--Cannot Log Body: Log Forbidden--")
                        .arg(trn).arg(h);
	if(lvl==WInterface::LogDetailed)
		outStr(r);
	else
	if(lvl==WInterface::LogOnError)
		rsp=r;
}

void WTransaction::Log::setInfo(const QString&s)
{
	if(lvl>=WInterface::LogInfo)
		outStr(QString("Transaction %1 info: %2").arg(trn).arg(s));
}

void WTransaction::Log::setError(const QString&s)
{
	//bail out if no logging
	if(lvl<WInterface::LogMinimal)return;
	//only for LogOnError, these have been stored, replay them:
	if(req.size()>0){
		outStr(req);
		req=QByteArray();
	}
	if(rsp.size()>0){
		outStr(rsp);
		rsp=QByteArray();
	}
	//show actual error
	outStr(QString("Transaction %1 Error: %2").arg(trn).arg(s));
}

WTransaction::LogWrap::LogWrap(WTransaction*p,const QString&r,const QString&i)
{
	Q_ASSERT_X(p!=0,"LogWrap constructor","expecting parent to be valid");
	parent=p;
	if(p->d->m_log==0)new Log(p,r,i);
}

void WTransaction::LogWrap::setRequ(const QString&s1,const QString&s2)
{
	if(parent->d->m_log)
		parent->d->m_log->setRequ(s1,s2);
}
void WTransaction::LogWrap::setResp(const QString&s1,const QString&s2)
{
	if(parent->d->m_log)
		parent->d->m_log->setResp(s1,s2);
}
void WTransaction::LogWrap::setError(const QString&s)
{
	if(parent->d->m_log)
		parent->d->m_log->setError(s);
}
void WTransaction::LogWrap::setInfo(const QString&s)
{
	if(parent->d->m_log)
		parent->d->m_log->setInfo(s);
}


/*****************************************************************************/

WTransaction_Private::WTransaction_Private(WTransaction*trn)
{
	m_stage=WTransaction::Uninitialized;
	m_log=0;
	m_refctr=0;
	attach(trn);
	connect(this,SIGNAL(webFinished()),this,SLOT(endQuery()));
// 	qDebug()<<"constructed a private object"<<(long long)this;
}

WTransaction_Private::~WTransaction_Private()
{
// 	qDebug()<<"destructing private object"<<(long long)this;
}

WTransaction_Private* WTransaction_Private::attach(WTransaction*trn)
{
	m_refctr++;
	connect(trn,SIGNAL(destroyed(QObject*)),this,SLOT(detach(QObject*)));
	connect(this,SIGNAL(dataAvailable()),trn,SLOT(endQuery()));
	return this;
}

void WTransaction_Private::detach(QObject*trn)
{
	m_refctr--;
	if(m_refctr==0)
		deleteLater();
	else if(trn){
		disconnect(trn);
		trn->disconnect(this);
	}
}

WTransaction_PrivateBase::WTransaction_PrivateBase()
{
	refctr=1;
// 	qDebug()<<"created private base"<<(long long)this;
}

WTransaction_PrivateBase::~WTransaction_PrivateBase()
{
// 	qDebug()<<"destructed private base"<<(long long)this;
}

/*****************************************************************************/

WTransaction::WaitFor_Private& WTransaction::WaitFor_Private::operator<<(WTransaction&t)
{
	m_trn<< &t;
	return *this;
}
WTransaction::WaitFor_Private& WTransaction::WaitFor_Private::operator<<(WTransaction*t)
{
	m_trn<< t;
	return *this;
}

void WTransaction::WaitForAny::loop()
{
	QEventLoop loop;
	//reduce list and connect to signals
	for(int i=0;i<m_trn.size();i++){
		if(m_trn[i].isNull())continue;
		if(!m_trn[i]->isInProgress())continue;
		connect(m_trn[i],SIGNAL(finished()),&loop,SLOT(quit()));
	}
	//set up timeout
	if(m_tmout>0)
		QTimer::singleShot(m_tmout,&loop,SLOT(quit()));
	//start loop
	loop.exec();
}
void WTransaction::WaitForAll::loop()
{
	QDateTime start=QDateTime::currentDateTime();
	do{
		int rtm=0;
		//check timeout
		if(m_tmout>0){
			rtm=m_tmout-start.msecsTo(QDateTime::currentDateTime());
			if(rtm<=0)
				return;
		}
		//reduce list
		QList<QPointer<WTransaction> > tmp;
		for(int i=0;i<m_trn.size();i++){
			if(m_trn[i].isNull())continue;
			if(!m_trn[i]->isInProgress())continue;
			tmp<<m_trn[i];
		}
		if(tmp.size()==0)return;
		m_trn=tmp;
		//create loop
		QEventLoop loop;
		for(int i=0;i<m_trn.size();i++)
			connect(m_trn[i],SIGNAL(finished()),&loop,SLOT(quit()));
		//set up timeout
		if(rtm>0)
			QTimer::singleShot(rtm,&loop,SLOT(quit()));
		//start loop
		loop.exec();
	}while(true);
}
