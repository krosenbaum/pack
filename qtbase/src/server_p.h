// Copyright (C) 2009-2011 by Konrad Rosenbaum <konrad@silmor.de>
// protected under the GNU LGPL version 3 or at your option any newer.
// See COPYING.LGPL file that comes with this distribution.
//

#ifndef WOLF_SERVER_PH
#define WOLF_SERVER_PH

#include "server.h"

/// \internal helper class for WServer - helps with receiving and parsing SCGI requests
class WServerReceiver:public QObject
{
	Q_OBJECT
	public:
		///constructs a new receiver
		/// \param srv the server this receiver belongs to
		/// \param sock the socket to handle
		WServerReceiver(WServer*srv,QIODevice*sock);
		///deletes the receiver object and its socket if it had an error or timeout
		virtual ~WServerReceiver();
	private slots:
		/// \internal helper to receive data
		void receiveMore();
		
	signals:
		///returns the completely parsed request back to the server to handle it
		void readyForProcess(WServerRequest,QIODevice*);
		
	private:
		enum Mode{StartMode,HeaderMode,BodyMode,Completed};
		Mode mode;
		int explen;
		QByteArray head,body,buffer;
		WServer*server;
		QIODevice*sock;
		WServerRequest request;
};

#endif
