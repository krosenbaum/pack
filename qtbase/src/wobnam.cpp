// Copyright (C) 2013 by Konrad Rosenbaum <konrad@silmor.de>
// protected under the GNU LGPL version 3 or at your option any newer.
// See COPYING.LGPL file that comes with this distribution.
//

#include "wobnam.h"
#include "wobnam_p.h"

#include <QStringList>
#include <QDebug>
#include <QUrlQuery>

#include <stdlib.h>

WobNetworkAccessManager::WobNetworkAccessManager(QObject* parent)
	: QNetworkAccessManager(parent)
{
}


static inline QByteArray translateOperation(QNetworkAccessManager::Operation op)
{
	switch(op){
		case QNetworkAccessManager::HeadOperation:return "HEAD";
		case QNetworkAccessManager::GetOperation:return "GET";
		case QNetworkAccessManager::PutOperation:return "PUT";
		case QNetworkAccessManager::PostOperation:return "POST";
		case QNetworkAccessManager::DeleteOperation:return "DELETE";
		default:return "NOOP";
	}
}

QNetworkReply* WobNetworkAccessManager::createRequest(QNetworkAccessManager::Operation op, const QNetworkRequest& req, QIODevice* outgoingData)
{
	//check whether it is scgi or scgissl
	QUrl url=req.url();
	if(url.scheme()=="scgi" || url.scheme()=="scgissl" || url.scheme()=="scgilocal"){
		return new WobScgiNetworkReplyImpl(req,translateOperation(op),outgoingData,this);
	}
	return QNetworkAccessManager::createRequest(op, req, outgoingData);
}

QNetworkReply* WobNetworkAccessManager::sendCustomRequest(const QNetworkRequest& req, const QByteArray& op, QIODevice* data)
{
	//check whether it is scgi or scgissl
	QUrl url=req.url();
	if(url.scheme()=="scgi" || url.scheme()=="scgissl" || url.scheme()=="scgilocal"){
		return new WobScgiNetworkReplyImpl(req,op,data,this);
	}
	return QNetworkAccessManager::sendCustomRequest(req, op, data);
}


WobScgiNetworkReplyImpl::WobScgiNetworkReplyImpl(QNetworkRequest req, QByteArray op_, QIODevice* input, QObject* parent)
	: QNetworkReply(parent),op(op_)
{
	setRequest(req);
	offset=0;
	readMode=HeadRead;
	setOpenMode(QIODevice::ReadOnly|QIODevice::Unbuffered);
	//get input
	if(input)incontent=input->readAll();
	//create connection
	if(req.url().scheme()=="scgi"){
		QTcpSocket* tsock=new QTcpSocket(this);
		sock=tsock;
		connect(tsock,SIGNAL(error(QAbstractSocket::SocketError)),
			this,SLOT(sockerror(QAbstractSocket::SocketError)));
		connect(tsock,SIGNAL(connected()),
			this,SLOT(startwrite()));
		connect(tsock,SIGNAL(disconnected()),
			this,SLOT(sockabort()));
		tsock->connectToHost(req.url().host(),req.url().port(9080));
	}else if(req.url().scheme()=="scgissl"){
		QSslSocket *ssock=new QSslSocket(this);
		sock=ssock;
		connect(sock,SIGNAL(error(QAbstractSocket::SocketError)),
			this,SLOT(sockerror(QAbstractSocket::SocketError)));
		connect(ssock,SIGNAL(sslErrors(QList<QSslError>)),
			this,SIGNAL(sslErrors(QList<QSslError>)));
		connect(ssock,SIGNAL(encrypted()),
			this,SLOT(startwrite()));
		connect(sock,SIGNAL(disconnected()),
			this,SLOT(sockabort()));
		ssock->connectToHostEncrypted(req.url().host(),req.url().port(9443));
	}else{
		//create socket
		QLocalSocket *lsock=new QLocalSocket(this);
		sock=lsock;
		connect(lsock,SIGNAL(error(QLocalSocket::LocalSocketError)),
			this,SLOT(lsockerror(QLocalSocket::LocalSocketError)));
		connect(lsock,SIGNAL(connected()),
			this,SLOT(startwrite()));
		connect(lsock,SIGNAL(disconnected()),
			this,SLOT(sockabort()));
		//parse url
		QUrl url=req.url();
		QUrlQuery query(url);
		const QString path=query.queryItemValue("_lpath_");
		query.removeQueryItem("_lpath_");
		url.setQuery(query);
		req.setUrl(url);
		setRequest(req);
		//connnect
		lsock->connectToServer(path);
	}
	connect(sock,SIGNAL(readyRead()),this,SLOT(sockread()));
}


qint64 WobScgiNetworkReplyImpl::readData(char* data, qint64 maxSize)
{
	if(data==0)return 0;
	qint64 mmax=bytesAvailable();
	if(mmax>maxSize)mmax=maxSize;
	if(mmax<=0)return mmax;
	memcpy(data,outcontent.constData()+offset,mmax);
	offset+=mmax;
	return mmax;
}

qint64 WobScgiNetworkReplyImpl::bytesAvailable() const
{
	return outcontent.size()-offset;
}

qint64 WobScgiNetworkReplyImpl::size()const
{
	return outcontent.size();
}

void WobScgiNetworkReplyImpl::abort(){QNetworkReply::close();}
void WobScgiNetworkReplyImpl::close(){QNetworkReply::close();}

void WobScgiNetworkReplyImpl::ignoreSslErrors()
{
	QSslSocket *ssock=qobject_cast<QSslSocket*>(sock);
	if(ssock)ssock->ignoreSslErrors();
}

void WobScgiNetworkReplyImpl::startwrite()
{
	//generate basic headers
	QByteArray head="CONTENT_LENGTH";
	head.append('\0').append(QByteArray::number(incontent.size())).append('\0');
	head.append("SCGI").append('\0').append("1").append('\0');
	head.append("REQUEST_METHOD").append('\0').append(op).append('\0');
	const QUrl url=request().url();
	QByteArray uri=url.toString(QUrl::RemoveScheme|QUrl::RemoveAuthority|QUrl::EncodeSpaces|QUrl::RemoveFragment).toLatin1();
	head.append("REQUEST_URI").append('\0').append(uri).append('\0');
	head.append("HTTP_HOST").append('\0').append(url.host().toUtf8()).append('\0');
	//generate remaining headers
	const QList<QByteArray>&hnames=request().rawHeaderList();
	for(int i=0;i<hnames.size();i++){
		const QByteArray hname=hnames[i].toUpper().replace('-','_');
		if(hname=="CONTENT_LENGTH" || hname=="SCGI" ||
		   hname=="REQUEST_METHOD"||hname=="REQUEST_URI" || hname=="HOST")
			continue;
		head.append("HTTP_"+hname).append('\0');
		head.append(request().rawHeader(hnames[i])).append('\0');
	}
	//generate netstring from headers
	head.prepend(QByteArray::number(head.size())+":");
	head.append(",");
	//send it
	sock->write(head);
	sock->write(incontent);
}

void WobScgiNetworkReplyImpl::sockread()
{
	qint64 avl=sock->bytesAvailable();
	if(avl<1)return;
	if(readMode==HeadRead){
		inhead+=sock->read(avl);
		//check for body
		int pos=inhead.indexOf("\r\n\r\n");
		if(pos<0)pos=inhead.indexOf("\n\n");
		if(pos<0)return;
		//ok split it
		int cn=0;
		while(pos<inhead.size() && cn<2 && (inhead[pos]=='\r' || inhead[pos]=='\n')){
			if(inhead[pos]=='\n')cn++;
			pos++;
		}
		outcontent=inhead.mid(pos);
		inhead=inhead.left(pos).trimmed();
		//switch mode
		readMode=ContentRead;
		//parse headers
		const QList<QByteArray>heads=inhead.split('\n');
		for(int i=0;i<heads.size();i++){
			const QByteArray head=heads[i].trimmed();
			pos=head.indexOf(':');
			if(pos<0)continue;
			const QString hname=head.left(pos).trimmed();
			const QString hval=head.mid(pos+1).trimmed();
			if(hname.toLower()=="status"){
				setAttribute(QNetworkRequest::HttpStatusCodeAttribute, hval.left(3).toInt());
				setAttribute(QNetworkRequest::HttpReasonPhraseAttribute, hval.mid(3).trimmed());
			}else
				setRawHeader(hname.toLatin1(),hval.toLatin1());
		}
	}else{
		outcontent+=sock->read(avl);
		emit downloadProgress(incontent.size(),header(QNetworkRequest::ContentLengthHeader).toLongLong());
	}
	//are we done yet?
	if(readMode==ContentRead){
		qint64 len=header(QNetworkRequest::ContentLengthHeader).toInt();
		if(len>0 && outcontent.size()>=len){
			finishUp();
		}
	}
}

void WobScgiNetworkReplyImpl::sockabort()
{
	//received a socketclosed signal
	//just in case: read remainder
	sockread();
	//now go in peace
	sock->close();
	finishUp();
}

static inline QNetworkReply::NetworkError translateError(QAbstractSocket::SocketError err)
{
	switch(err){
		case QAbstractSocket::ConnectionRefusedError:
			return QNetworkReply::ConnectionRefusedError;
		case QAbstractSocket::RemoteHostClosedError:
				return QNetworkReply::RemoteHostClosedError;
		case QAbstractSocket::HostNotFoundError:
				return QNetworkReply::HostNotFoundError;

		case QAbstractSocket::ProxyAuthenticationRequiredError:
			return QNetworkReply::ProxyAuthenticationRequiredError;

		case QAbstractSocket::ProxyConnectionRefusedError:
			return QNetworkReply::ProxyConnectionRefusedError;
		case QAbstractSocket::ProxyConnectionClosedError:
			return QNetworkReply::ProxyConnectionClosedError;
		case QAbstractSocket::ProxyConnectionTimeoutError:
			return QNetworkReply::ProxyTimeoutError;
		case QAbstractSocket::ProxyNotFoundError:
			return QNetworkReply::ProxyNotFoundError;
		case QAbstractSocket::ProxyProtocolError:
			return QNetworkReply::UnknownProxyError;

		case QAbstractSocket::SslHandshakeFailedError:
		case QAbstractSocket::SslInternalError:
		case QAbstractSocket::SslInvalidUserDataError:
			return QNetworkReply::SslHandshakeFailedError;

		default:
			return QNetworkReply::UnknownNetworkError;
	}
}

void WobScgiNetworkReplyImpl::sockerror(QAbstractSocket::SocketError err)
{
	if(err==QAbstractSocket::RemoteHostClosedError && readMode==ContentRead){
		finishUp();
		return;
	}
	qDebug()<<"WobScgiNetworkReplyImpl received network socket error"<<(int)err;
	emit errorOccurred(translateError(err));
}

static inline QNetworkReply::NetworkError translateError(QLocalSocket::LocalSocketError err)
{
	return translateError((QAbstractSocket::SocketError)err);
}

void WobScgiNetworkReplyImpl::lsockerror(QLocalSocket::LocalSocketError err)
{
	if(err==QLocalSocket::PeerClosedError && readMode==ContentRead){
		qint64 len=header(QNetworkRequest::ContentLengthHeader).toLongLong();
		if(len<=0)setRawHeader("Content-Length",QByteArray::number(outcontent.size()));
		finishUp();
		return;
	}
	qDebug()<<"WobScgiNetworkReplyImpl received local socket error"<<(int)err;
	emit errorOccurred(translateError(err));
}

void WobScgiNetworkReplyImpl::finishUp()
{
	if(isFinished())return;
	setFinished(true);
	emit finished();
}
