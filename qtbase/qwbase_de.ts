<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de_DE">
<context>
    <name>WTransaction_Private</name>
    <message>
        <location filename="src/transaction.cpp" line="89"/>
        <source>interface not found</source>
        <translation>Verbindung nicht gefunden</translation>
    </message>
    <message>
        <location filename="src/transaction.cpp" line="155"/>
        <source>Web Request timed out.</source>
        <translation>Web-Anfrage ist Ergebnislos abgelaufen.</translation>
    </message>
    <message>
        <location filename="src/transaction.cpp" line="173"/>
        <source>HTTP Error, return code %1 %2</source>
        <translation>HTTP Fehler, Code %1 &quot;%2&quot;</translation>
    </message>
</context>
</TS>
