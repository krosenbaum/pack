# Copyright (C) 2009-2013 by Konrad Rosenbaum <konrad@silmor.de>
# protected under the GNU LGPL version 3 or at your option any newer.
# See COPYING.LGPL file that comes with this distribution.

TEMPLATE = lib
TARGET = qwbase
DESTDIR = $$PWD
CONFIG += dll create_prl separate_debug_info hide_symbols
QT += xml network
QT -= gui
OBJECTS_DIR = .ctmp
MOC_DIR = .ctmp
RCC_DIR = .ctmp

#FIXME: eliminate this dependency
equals(QT_MAJOR_VERSION, 6): QT += core5compat

#no version info yet, TODO: fixme
VERSION =

#make sure symbols are exported:
DEFINES += WOLF_BASE_EXPORT=Q_DECL_EXPORT

HEADERS += \
	include/nullable.h \
	include/helper.h \
	include/object.h \
	include/transaction.h \
	include/transaction_p.h \
	include/interface.h \
	include/server.h \
	src/server_p.h \
	src/wobnam.h \
	src/wobnam_p.h \
	src/helper_p.h

SOURCES += \
	src/object.cpp \
	src/helper.cpp \
	src/transaction.cpp \
	src/interface.cpp \
	src/server.cpp \
	src/wobnam.cpp

INCLUDEPATH += include src
DEPENDPATH += include src

*g++* { QMAKE_CXXFLAGS += -std=c++11 }

#Localization
TRANSLATIONS = qwbase_de.ts
