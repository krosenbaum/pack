#ifndef CLOCK_H
#define CLOCK_H

//This file declares the handlers for transactions.
//They are implemented in clock.cpp.

class CTGetTime;
class CTGetTimeOffsetUtc;
class CTExitServer;
void getTimeCall(CTGetTime*);
void getTimeCall2(CTGetTimeOffsetUtc*);
void shutdownServer(CTExitServer*);

#endif
