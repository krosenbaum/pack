#include <CIncludeAll>
#include <QCoreApplication>
#include <QDateTime>
#include <QDebug>
#include <QStringList>
#include <QTimer>
#include <WServer>
#include "clock.h"


///The document used if all else fails...
static const QString rootdoc=
"<h1>Clock Example</h1>\n"
"Point your client to subdirectory .../clock<p>\n"
"Go to .../debug for the debugging reflector...\n";

///global web machine interface
CInterface ifc;


///helper function for handlers below, converts QDataTime to COTime
static COTime toCOTime(const QDateTime&now)
{
	COTime tm;
	tm.setyear(now.date().year());
	tm.setmonth(now.date().month());
	tm.setmonthName(now.date().toString("MMMM"));
	tm.setday(now.date().day());
	tm.setdayOfWeek(now.date().toString("dddd"));
	tm.sethour(now.time().hour());
	tm.setminute(now.time().minute());
	tm.setsecond(now.time().second());
	return tm;
}

///implements the GetTime transaction
void getTimeCall(CTGetTime* trans)
{
	QDateTime now=QDateTime::currentDateTime();
	trans->settime(toCOTime(now));
}

///implements the GetTimeOffsetUtc transaction
void getTimeCall2(CTGetTimeOffsetUtc* trans)
{
	QDateTime now=QDateTime::currentDateTime().toUTC();
	now.addSecs(trans->getminutesFromUtc()*60);
	trans->settime(toCOTime(now));
}

///owner object for all servers, when it is deleted all servers will shut down
QObject*owner=0;
///handler for the ShutdownServer transaction
void shutdownServer(CTExitServer* trans)
{
	//check whether the user said "please"
	if(trans->getmagicWord().trimmed().toLower()==QString("please")){
		trans->setshuttingdown(true);
		if(owner!=0)
			QTimer::singleShot(1,owner,SLOT(deleteLater()));
		QTimer::singleShot(100,QCoreApplication::instance(),SLOT(quit()));
	}else{
		trans->setshuttingdown(false);
	}
}

///main function
int main(int argc,char**argv)
{
	//central application object
	QCoreApplication app(argc,argv);
	owner=new QObject;
	//parse command line arguments
	QStringList args=app.arguments();
	bool haveserv=false;
	for(int i=0;i<args.size();i++){
		QStringList sl=args[i].split("=");
		if(sl.size()!=2)continue;
		WServer*server=0;
		//TCP server
		if(sl[0]=="-net"){
			sl=sl[1].split(",");
			//default is localhost
			QHostAddress addr("127.0.0.1");
			//get the host
			if(sl.size()>1){
				addr=QHostAddress(sl[0]);
				if(addr.isNull()){
					qDebug()<<"Invalid IP address"<<sl[0]<<"- aborting.";
					return 1;
				}
			}
			bool ok;
			//get the port
			int port=sl.last().toInt(&ok);
			if(!ok){
				qDebug()<<"not a port:"<<sl.last()<<"- aborting.";
				return 1;
			}
			if(port<0||port>65535){
				qDebug()<<"illegal port"<<port<<"- aborting.";
				return 1;
			}
			qDebug()<<"initializing network SCGI host"<<addr.toString()<<"on port"<<port;
			server=new WServer(addr,port,owner);
		}else
		//AF_UNIX server
		if(sl[0]=="-local"){
			qDebug()<<"initializing local SCGI server"<<sl[1];
			server=new WServer(sl[1],owner);
		}else continue;
		//do we have a server?
		if(server!=0){
			//if so: check whether it was successfully initialized
			if(!server->isActive()){
				delete server;
				qDebug()<<"unable to initialize server";
				continue;
			}
			//remember there is one
			haveserv=true;
			//register /debug URL - simple reflector
			server->enableDebugUrl();
			//register fall back document to everything below / that does not have something else
			server->registerStatic("/",rootdoc);
			//register actual server side interface to /clock
			server->registerInterface("/clock",&ifc);
		}
	}
	//check there is a working server
	if(!haveserv){
		qDebug()<<"Use -net=IPaddr,port, -net=port, or -local=file to configure the SCGI server.";
		return 1;
	}
	//enter event loop and wait for requests...
	return app.exec();
}