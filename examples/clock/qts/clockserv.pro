TEMPLATE = app
TARGET = clockserv
QT += network xml
QT -= gui
CONFIG += debug link_prl
LIBS += -L../../../qtbase -lqwbase
INCLUDEPATH += ../../../qtbase/include

OBJECTS_DIR = .ctmp
MOC_DIR = .ctmp
RCC_DIR = .ctmp

HEADERS += clock.h
SOURCES += clock.cpp

include(wob/wob.pri)

*g++* { QMAKE_CXXFLAGS += -std=c++11 }
