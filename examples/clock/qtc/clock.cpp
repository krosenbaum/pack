#include "clock.h"
#include <CIncludeAll>
#include <QApplication>
#include <QDateTime>
#include <QDebug>
#include <QFormLayout>
#include <QInputDialog>
#include <QLabel>
#include <QLineEdit>
#include <QMessageBox>
#include <QPushButton>
#include <QSpinBox>
#include <WInterface>

///the URL of the server
QString url;
///global interface to the server
CInterface ifc;

ClockDisplay::ClockDisplay()
{
	QFormLayout*fl;
	setLayout(fl=new QFormLayout);
	fl->addRow("Server URL:",new QLabel(url));
	QPushButton*p;
	fl->addRow("Query:",p=new QPushButton("Get Server Local Time"));
	connect(p,SIGNAL(clicked()),this,SLOT(askTime()));
	fl->addRow("Offset from Utc (min):",offs=new QSpinBox);
	offs->setRange(-14*60,14*60);
	fl->addRow("Query:",p=new QPushButton("Get UTC/offset Time"));
	connect(p,SIGNAL(clicked()),this,SLOT(askOffsetTime()));
	fl->addRow("Time of Query:",qtime=new QLabel);
	fl->addRow("Server Reported Time:",stime=new QLabel);
	fl->addRow("Shut Down",p=new QPushButton("Attempt to shut down Server"));
	connect(p,SIGNAL(clicked()),this,SLOT(shutdownServer()));
}

void ClockDisplay::askTime()
{
	CTGetTime gt=ifc.queryGetTime();
	displayTime(gt.gettime(),gt.hasError());
}

void ClockDisplay::askOffsetTime()
{
	CTGetTimeOffsetUtc gt=ifc.queryGetTimeOffsetUtc(offs->value());
	displayTime(gt.gettime(),gt.hasError());
}

void ClockDisplay::shutdownServer()
{
	//ask for the magic word, needed for shutting down
	QString s=QInputDialog::getText(this,"Magic Word","Please enter the magic word ('please'):");
	if(s=="")return;
	//query the server
	CTExitServer es=ifc.queryExitServer(s);
	//display result
	if(es.hasError() || es.getshuttingdown().isNull() || es.getshuttingdown().value()==false)
		QMessageBox::warning(this,"Error","Unable to shut down server.");
	else
		QMessageBox::information(this,"Success","The server is shutting down.");
}


void ClockDisplay::displayTime(const COTime& tm,bool hasError)
{
	//display current local time for comparison
	qtime->setText(QDateTime::currentDateTime().toString());
	//was there an error?
	if(hasError){
		stime->setText("#ERROR");
		return;
	}
	//display server time
	QString tx=QString("%1-%2-%3 %4:%5:%6")
		.arg((int)tm.year(),4,10,(QChar)'0')
		.arg((int)tm.month(),2,10,(QChar)'0')
		.arg((int)tm.day(),2,10,(QChar)'0')
		.arg((int)tm.hour(),2,10,(QChar)'0')
		.arg((int)tm.minute(),2,10,(QChar)'0')
		.arg((int)tm.second(),2,10,(QChar)'0') ;
	stime->setText(tx);

}


int main(int argc,char**argv)
{
	//central application object
	QApplication app(argc,argv);
	//parse arguments, we expect the URL...
	QStringList args=app.arguments();
	if(args.size()<2){
		qDebug()<<"Usage:"<<args[0].toLatin1().data()<<"url...";
		return 1;
	}
	url=args[1];
	//initialize the interface, set URL and debug mode
	ifc.setUrl(url);
	ifc.setLogLevel(WInterface::LogDetailed);
	qDebug()<<"Using URL"<<ifc.url().toString();
	//init display
	ClockDisplay cd;
	cd.show();
	//enter event loop
	return app.exec();
}
