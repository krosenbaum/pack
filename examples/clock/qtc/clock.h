#include <QDialog>

class QSpinBox;
class COTime;
class QLineEdit;
class QLabel;
///The display class for the clock client.
class ClockDisplay:public QDialog
{
	Q_OBJECT
	public:
		///instantiates the display
		ClockDisplay();
	public slots:
		///ask for the server time
		void askTime();
		///ask for server time as a time with specific offset from UTC
		void askOffsetTime();
		///attempt to shut the server down
		void shutdownServer();
		///helper: show the time we got from the server
		void displayTime(const COTime&,bool);
	private:
		QLabel*qtime,*stime;
		QSpinBox*offs;
};
