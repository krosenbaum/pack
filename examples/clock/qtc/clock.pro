TEMPLATE = app
TARGET = clock
CONFIG += debug link_prl
QT += xml network
contains(QT_MAJOR_VERSION,"5") {QT+=widgets}
LIBS += -L../../../qtbase -lqwbase
INCLUDEPATH += ../../../qtbase/include

OBJECTS_DIR = .ctmp
MOC_DIR = .ctmp
RCC_DIR = .ctmp

SOURCES += clock.cpp
HEADERS += clock.h

include(wob/wob.pri)

*g++* { QMAKE_CXXFLAGS += -std=c++11 }
