#######################################################
# Main Makefile for PACK
#
# see README for details
#######################################################

QMAKE = qmake
QCONFIG = CONFIG+=release
DOXYGEN = doxygen
MKDIR = mkdir -p
COPY = cp -d --remove-destination
COPY_R = $(COPY) -r
LRELEASE = lrelease

prefix=/usr/local
libprefix=$(prefix)/lib
binprefix=$(prefix)/bin
docprefix=$(prefix)/share/doc/
phpprefix=$(prefix)/share/pack/php
incprefix=$(prefix)/include

all: prewoc woc qtbase doc

.PHONY: prewoc woc qtbase doc clean install install-woc install-qtbase install-phpbase install-doc

qrcgen:
	cd woc/qrcgen && $(QMAKE) && $(MAKE)

vinfo/staticVersion.h prewoc: qrcgen
	cd woc && $(QMAKE) -o Makefile.prewoc CONFIG+=prewoc
	$(MAKE) -C woc -f Makefile.prewoc
	cd woc && $(abspath woc/prewoc) :/version.wolf

woc: vinfo/staticVersion.h qrcgen
	cd woc && $(QMAKE) CONFIG+=woc
	$(MAKE) -C woc

qtbase: woc
	cd qtbase && $(QMAKE)
	$(MAKE) -C qtbase
	cd qtbase && $(LRELEASE) wbase.pro

doc:
	cd woc && $(DOXYGEN)
	cd qtbase && $(DOXYGEN)
	cd phpbase && $(DOXYGEN)

clean:
	-rm -rf woc/woc woc/woc.exe woc/woc.app woc/prewoc* woc/.ctmp woc/.ptmp
	-rm -rf vinfo
	-rm -rf qtbase/.ctmp qtbase/libqwbase*
	-rm -rf doc/phpbase doc/qtbase doc/woc
	-rm -rf cgi2scgi/cgi2scgi cgi2scgi/cgi2scgi.exe cgi2scgi/cgi2scgi.app

install: install-woc install-qtbase-dev install-phpbase install-doc

install-woc: woc
	$(MKDIR) $(binprefix)
	$(COPY) woc/woc $(binprefix)
	strip $(binprefix)/woc

install-qtbase: qtbase
	$(MKDIR) $(libprefix)
	$(COPY) qtbase/libqwbase* $(libprefix)
	$(COPY) qtbase/*.qm $(libprefix)

install-qtbase-dev: install-qtbase
	$(MKDIR) $(incprefix)
	$(COPY) qtbase/include/* $(incprefix)

install-phpbase:
	$(MKDIR) $(phpprefix)
	$(COPY) phpbase/*.php $(phpprefix)

install-doc: doc
	$(MKDIR) $(docprefix)
	$(COPY_R) doc/*  $(docprefix)
