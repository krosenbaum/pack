<?php
// Copyright (C) 2009-2021 by Konrad Rosenbaum <konrad@silmor.de>
// protected under the GNU LGPL version 3 or at your option any newer.
// See COPYING.LGPL file that comes with this distribution.
//
/**MySQL adaptation of DB-Engine*/
class MysqlEngine extends DbEngine
{
    /**prefix for table names*/
    protected $prefix="";

    private $user;
    private $server;
    private $pass;
    private $dbhdl=false;
    private $dbname="";
    private $engine="InnoDB";
    private $charset="utf8";
    private $collate="utf8_bin";
    private $intrans=false;
    //used for table creation to work around mysql bugs
    private $tableappend;

    /**initialize driver*/
    public function __construct($server,$user,$pass)
    {
        $this->user=$user;
        $this->server=$server;
        $this->pass=$pass;
    }

    /**make sure it shuts down*/
    public function __destruct()
    {
        //fail on the side of caution: roll back unless already released
        if($this->intrans)
            @$this->rollbackTransaction();
    }

    /**set a table-name prefix for the database*/
    public function setPrefix($pre)
    {
        $this->prefix=$pre;
    }

    /**set the name of the database to be used*/
    public function setDbName($dbn)
    {
        $this->dbname=$dbn;
    }

    ///return the name of the database
    public function dbName()
    {
        return $this->dbname;
    }

    /**set the name of the storage engine to be used on DB creation*/
    public function setStorageEngine($e)
    {
        $this->engine=$e;
    }

    /**set the default charset and collation for tables and connections
    \param cs the character set - like 'latin1' or 'utf8', this must be a charset supported by MySQL, the default is 'utf8'
    \param col the collation to be used, the default is to use the corresponding binary collation (if you specify an empty string the default of MySQL will be used, which is a case independent comparison!)
    */
    public function setCharacterSet($cs,$col="%_bin")
    {
        $this->charset=$cs;
        $this->collate=str_replace('%',$cs,$col);
    }

    public function tryConnect()
    {
        //connect
        $this->dbhdl=mysqli_connect($this->server,$this->user,$this->pass,$this->dbname);
        if($this->dbhdl===false)
            die("Unable to connect to database system. Giving up.");
        //select Unicode or whatever charset is configured
        $cs=$this->escapeString($this->charset);
        $col=$this->escapeString($this->collate);
        $q="SET NAMES ".$cs;
        if($this->collate!="")$q.=" COLLATE ".$col;
        if(mysqli_query($this->dbhdl,$q)===false)
            die("Cannot set DB character set to ".$cs." and collation to ".$col.", aborting.");
        //make sure the DB is transaction safe
        if(mysqli_query($this->dbhdl,"SET SESSION TRANSACTION ISOLATION LEVEL SERIALIZABLE")===false)
            die("Cannot make this database transaction safe, aborting");
    }

    ///check whether the DB object is connected to an actual database
    public function isConnected()
    {
        return $this->dbhdl!==false;
    }

    public function hasTable($tnm)
    {
        if(!WobSchema::hasTable($tnm))return false;
        //$res=mysqli_query($this->dbhdl,"select * from ".$this->tableName($tnm)." where 1=2");
        $res=mysqli_query($this->dbhdl,"select * from information_schema.tables where table_schema=".$this->escapeString($this->dbName()).
            " and table_name=".$this->escapeString($this->tableName($tnm)));
        if($res===false)return false;
        $ret=mysqli_num_rows($res)>0;
        mysqli_free_result($res);
        return $ret;
    }

    public function beginTransaction()
    {
        WobTransaction::debug("DB Begin Transaction",WobTransaction::DebugDbTransaction);
        $this->intrans=mysqli_query($this->dbhdl,$this->sqlBeginTransaction());
        return $this->intrans;
    }

    public function commitTransaction()
    {
        if(!$this->intrans)return false;
        WobTransaction::debug("DB Commit Transaction",WobTransaction::DebugDbTransaction);
        //otherwise commit
        $this->intrans=false;
        return mysqli_query($this->dbhdl,$this->sqlCommitTransaction());
    }

    public function rollbackTransaction()
    {
        $this->intrans=false;
        WobTransaction::debug("DB RollBack Transaction",WobTransaction::DebugDbTransaction);
        //don't ask, just do (also catches implicit transactions)
        return mysqli_query($this->dbhdl,$this->sqlRollbackTransaction());
    }

    protected function lockDB($wl)
    {
        $req="SET autocommit = 0 ; LOCK TABLES ";
        $i=0;
        foreach(WobSchema::backupTables() as $tab){
            if($i)$req.=",";
            $i++;
            $req.=$this->tableName($tab);
            if($wl)$req.=" WRITE";
            else $req.=" READ";
        }
        WobTransaction::debug("DB Lock: ".$req,WobTransaction::DebugDbQuery);
        $res=mysqli_query($this->dbhdl,$req);
        if($res===false){
            WobTransaction::debug("MySQL error: ".$this->dbhdl->error."\n   Query: ".$req,WobTransaction::DebugDbError);
            return false;
        }
    }

    protected function unlockDB()
    {
        WobTransaction::debug("DB Full Unlock",WobTransaction::DebugDbQuery);
        mysqli_query($this->dbhdl,"UNLOCK TABLES");
    }

    public function sqlBeginTransaction(){return "BEGIN";}

    public function sqlCommitTransaction(){return "COMMIT";}

    public function sqlRollbackTransaction(){return "ROLLBACK";}


    public function select($table,$cols,$where="",$orderby="")
    {
        $query=$this->sqlSelect($table,$cols,$where,$orderby);
        if($this->transmode)$query.=" FOR UPDATE";
        $res=mysqli_query($this->dbhdl,$query);
        WobTransaction::debug("DB SELECT: ".$query."\n".($res===false?"failed":"successful"),WobTransaction::DebugDbQuery);
        if($res===false){
            WobTransaction::debug("MySQL error: ".$this->dbhdl->error."\n   Query: ".$query,WobTransaction::DebugDbError);
            return false;
        }
        $nr=mysqli_num_rows($res);
        $ret=array();
        for($i=0;$i<$nr;$i++){
            $ret[]=mysqli_fetch_array($res,MYSQLI_BOTH);
        }
        mysqli_free_result($res);
        return $ret;
    }

    protected function createTable($tn,$t)
    {
        $sql=$this->sqlCreateTable($tn,$t);
        WobTransaction::debug("DB create table: ".$sql,WobTransaction::DebugDbQuery);
        return mysqli_query($this->dbhdl,$sql);
    }

    protected function sqlCreateTable($tn,$t)
    {
        $this->tableappend="";
        $sql=parent::sqlCreateTable($tn,$t)." engine=".$this->engine." default charset=".$this->charset;
        if($this->collate!="")$sql.=" collate=".$this->collate;
// 		print("<pre>\n$sql\n</pre>\n");
        return $sql;
    }

    protected function createTableExtras($tablename,$table)
    {
        //warning: this is rather dangerous, but safe at the moment since there is only
        // one user of this function
        return $this->tableappend;
    }

    protected function upgradeTable($tablename,$doexec)
    {
        $dbtn=$this->tableName($tablename);
        $dbname=$this->dbName();
        //get schema
        $tschema=WobSchema::tableDefinition($tablename);
        if($tschema===false){
            print("--  internal error: table does not exist in schema\n");
            return;
        }
        //get columns from DB
        $res=mysqli_query($this->dbhdl,"select * from information_schema.columns where table_schema=".$this->escapeString($dbname).
            " and table_name=".$this->escapeString($dbtn)." order by ordinal_position");
        if($res===false){
            print("<font color=\"red\">-- Internal error while querying table:".$this->lastError()."</font>\n");
            return;
        }
        $dbcols=mysqli_fetch_all($res,MYSQLI_ASSOC);
        mysqli_free_result($res);
        //go through columns
        foreach($tschema as $cname=>$cdef){
            if(substr($cname,0,1)==":"){
                print("-- <font color=\"red\">special column ".$cname." IGNORING FOR NOW</font>\n");
                continue;
            }
            print("--  column ".$cname);
            // check column exists
            $ccol=array_values(array_filter($dbcols,function($col)use($cname){return $col["COLUMN_NAME"]==$cname;}));
            //print_r($ccol);
            if(count($ccol)<1){
                print(" ... does not exist\n");
                //create
                $stmt="ALTER TABLE ".$dbtn." ADD COLUMN ".$this->sqlCreateColumn($tablename,$cname,$cdef);
                print("<font color=\"green\">".$stmt.";</font>\n");
                if($doexec){
                    if(mysqli_query($this->dbhdl,$stmt)===false)
                        die("Unable to create column!");
                }
            }else{
                print(" ... exists, checking:\n");
                //check type is ok
                if($this->validateColumnType($cdef,$ccol[0]))
                    print("--   type ok\n");
                else{
                    print("--   adjusting type\n");
                }
            }
        }
    }

    private function validateColumnType($schemadef,$dbdef)
    {
        // check type
        $schematype=strtolower($this->dataType($schemadef[0]));
        $dbtype=strtolower($dbdef["DATA_TYPE"]);
        if($dbtype=="varchar")
            $dbtype=strtolower($dbdef["COLUMN_TYPE"]);
        if(stristr($dbdef["EXTRA"],"auto_increment")!="")
            $dbtype.=" auto_increment";
        if($schematype=="boolean")$schematype="tinyint";
        if($schematype!=$dbtype){
            print("--    <font color=\"orange\">(type mismatch - schema: $schematype db: $dbtype)</font>\n");
            return false;
        }else
            return true;
        // check null
        $schemanull=!in_array("notnull",$dbdef);
        $dbnull=$dbdef["IS_NULLABLE"]=="YES";
        return $schemanull==$dbnull;
    }

    public function tableName($tn)
    {
        return $this->prefix.$tn;
    }

    protected function dataType($type)
    {
        if($type=="int32"||$type=="enum"||$type=="enum32")return "INT";
        if($type=="int64"||$type=="enum64")return "BIGINT";
        if($type=="seq32")return "INT AUTO_INCREMENT";
        if($type=="seq64")return "BIGINT AUTO_INCREMENT";
        if($type=="text")return "TEXT";
        if($type=="blob")return "MEDIUMBLOB"; //max.16MB of blob
        $tpa=explode(":",$type);
        if($tpa[0]=="string"){
            if(isset($tpa[1]))
                return "VARCHAR(".$tpa[1].")";
            else
                return "VARCHAR(255)";
        }
        //fallback to SQL standard
        return parent::dataType($type);
    }

    protected function columnFlag($flag,$col,$table,$cflags)
    {
        //MySQL does not mark columns for indexing directly, instead the index is appended to the table definition
        if($flag=="index" && ($cflags&self::COLUMN_CREATE_INDEX)){
            $this->tableappend.=", INDEX(".$col.")";
            return "";
        }
        //MySQL::InnoDB is buggy, it accepts references only as appendix
        if($cflags&self::COLUMN_CREATE_FKEY){
            $tpa=explode(":",$flag);
            if($tpa[0]=="foreignkey"){
                if(count($tpa)<3)
                    return false;
                $this->tableappend.=", FOREIGN KEY (".$col.") REFERENCES ".$this->tableName($tpa[1])."($tpa[2])";
                return "";
            }
        }
        //fallback to SQL standard
        return parent::columnFlag($flag,$col,$table,$cflags);
    }

    public function insert($table,array $values)
    {
        $this->transmode=true;
        $sql=$this->sqlInsert($table,$values);
        $res=mysqli_query($this->dbhdl,$sql);
        WobTransaction::debug("DB Insert: ".$sql."\n".($res===false?"failed":"successful"),WobTransaction::DebugDbQuery);
        if($res===false){
            WobTransaction::debug("MySQL error: ".$this->dbhdl->error."\n   Query: ".$sql,WobTransaction::DebugDbError);
            return false;
        }
        $seq=WobSchema::hasSequence($table);
        if($seq!==false){
            if(isset($values[$seq]))return $values[$seq];
            $res=mysqli_query($this->dbhdl,"select LAST_INSERT_ID()");
            if(mysqli_num_rows($res)>0){
                $row=mysqli_fetch_array($res);
                $ret=$row[0];
            }else{
                $ret=true;
            }
            mysqli_free_result($res);
            return $ret;
        }else{
            return true;
        }
    }

    public function update($table,array $values,$where)
    {
        $this->transmode=true;
        $sql=$this->sqlUpdate($table,$values,$where);
        $res=mysqli_query($this->dbhdl,$sql);
        WobTransaction::debug("DB Update: ".$sql."\n".($res===false?"failed":"successful"),WobTransaction::DebugDbQuery);
        if($res!==false)return mysqli_affected_rows($this->dbhdl);
        else{
            WobTransaction::debug("MySQL error: ".$this->dbhdl->error."\n   Query: ".$sql,WobTransaction::DebugDbError);
            return false;
        }
    }

    public function deleteRows($table,$where)
    {
        $this->transmode=true;
        $sql=$this->sqlDelete($table,$where);
        $b=mysqli_query($this->dbhdl,$sql);
        WobTransaction::debug("DB Delete Rows: ".$sql."\n".($b===false?"failed":"successful"),WobTransaction::DebugDbQuery);
        if($b!==false)return mysqli_affected_rows($this->dbhdl);
        else{
            WobTransaction::debug("MySQL error: ".$this->dbhdl->error."\n   Query: ".$sql,WobTransaction::DebugDbError);
            return false;
        }
    }

    public function lastError()
    {
        return mysqli_error($this->dbhdl);
    }

    /**escapes strings; it uses mysqli_escape_string and encloses the value in ''*/
    public function escapeString($s)
    {
        if($s === false||$s===null) return "NULL";
        return "'".mysqli_real_escape_string($this->dbhdl,$s)."'";
    }

    /**escapes blobs; it uses mysqli_escape_string and encloses the value in '' - blobs are binary strings in MySQL*/
    public function escapeBlob($s)
    {
        if($s === false||$s===null) return "NULL";
        return "'".mysqli_real_escape_string($this->dbhdl,$s)."'";
    }

    /**escapes booleans, overwrites the orignal to use "0" and "1" instead of "FALSE" and "TRUE"*/
    public function escapeBool($b)
    {
        $r=DbEngine::escapeBool($b);
        if($r=="TRUE")return "1";
        if($r=="FALSE")return "0";
        return $r;
    }
};


//this function is often missing
if(!function_exists('mysqli_fetch_all')){
    function mysqli_fetch_all($result,$rtype=MYSQLI_NUM)
    {
        $ret=array();
        mysqli_data_seek($result,0);
        while(($tmp=mysqli_fetch_array($result, $rtype))!==null)$ret[]=$tmp;
        return $ret;
    }
}

//EOF
return
?>
