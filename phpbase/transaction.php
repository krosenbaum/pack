<?php
// Copyright (C) 2009-2021 by Konrad Rosenbaum <konrad@silmor.de>
// protected under the GNU LGPL version 3 or at your option any newer.
// See COPYING.LGPL file that comes with this distribution.
//

/* TRANSLATOR WobTransactionBase */

/**Ancestor of all Transactions*/
class WobTransactionBase {
    protected $ainput;
    protected $tinput;
    protected $aoutput;
    protected $toutput;
    protected $headers=array();
    static protected $debugstr="";
    static protected $debuglev=0;

    ///no debugging (used only in setDebugLevel())
    const DebugNone             = 0;
    ///activate all debug (used only in setDebugLevel())
    const DebugAll              = 0x0ffffff;
    ///flag: output the debug string immediately (risks losing headers, used only in debug())
    const DebugUrgent           = 0x1000000;
    ///debug database errors
    const DebugDbError          = 0x0000001;
    ///debug database queries when they are executed
    const DebugDbQuery          = 0x0000002;
    ///debug database statements when they are constructed
    const DebugDbStatement      = 0x0000004;
    ///debug database transaction start/end
    const DebugDbTransaction    = 0x0000008;
    ///output all database related actions (used only in setDebugLevel())
    const DebugDbAll            = 0x00000ff;
    ///debug transaction related actions
    const DebugTransactions     = 0x0000100;
    ///debug transaction related errors
    const DebugTransactError    = 0x0000200;
    ///miscellaneous debug output (default level)
    const DebugMisc             = 0x0800000;
    ///activate error logging only (used only in setDebugLevel())
    const DebugError            = self::DebugDbError | self::DebugTransactError;

    //these encoding constants must match the WocProcessor::MessageEncoding enum
    /**encode messages according to PACK standard - with minimal XML levels*/
    const WobEncoding=0;
    /**encode messages in a mode compatible with SOAP 1.2, which is much less effective than WobEncoding*/
    const Soap12Encoding=1;

    protected static $running="";
    protected static $instance=null;

    protected function __construct()
    {
        self::$instance=$this;
    }

    /**returns the name of the currently running transaction*/
    static public function getExecutingName(){return self::$running;}

    ///returns the currently running instance of a transaction
    static public function getInstance(){return self::$instance;}

    ///set the debug level of the server
    static public function setDebugLevel($level)
    {
        self::$debuglev=$level & self::DebugAll;
    }

    ///inserts a comment as debug statement
    static public function debug($str,$level=self::DebugMisc)
    {
        if((self::$debuglev & $level) == 0)return;

        if(self::$debugstr!="")self::$debugstr.="\n\n";
        self::$debugstr.=$str;

        if($level & self::DebugUrgent)self::printDebug();
    }

    ///print debug comment now (called from xmlToString() and TransactionError
    static public function printDebug()
    {
        if(self::$debugstr == "")return;
        error_log(self::$debugstr);
        print(self::getDebug());
    }

    ///return debug comment now (called from xmlToString() and TransactionError
    static public function getDebug()
    {
        if(self::$debugstr == "")return "";
        $ret="\n<!-- ".htmlentities(self::$debugstr)." -->\n";
        self::$debugstr="";
        return $ret;
    }

    /**Wob message encoding: called to determine the correct transaction, aborts the script if there is none.*/
    static public function getTransactionNameWob()
    {
        global $_SERVER;
        if($_SERVER["REQUEST_METHOD"] != "POST"){
            throw new WobWobTransactionError(tr("Request is not a POST Request, Aborting."),"non-post");
            exit();
        }
        if(!isset($_SERVER["HTTP_X_WOBREQUEST"])){
            throw new WobWobTransactionError(tr("Request is not a Wob Request, Aborting."),"non-wob");
            exit();
        }
        self::$running=$_SERVER["HTTP_X_WOBREQUEST"];
        return self::$running;
    }
    /**called to determine the correct transaction, aborts the script if there is none.*/
    static public function getTransactionNameSoap()
    {
        global $_SERVER;
        $this->headers=array();
        if($_SERVER["REQUEST_METHOD"] != "POST"){
            throw new WobTransactionError($this,tr("Request is not a POST Request, Aborting."),"non-post");
            exit();
        }
        if($this->messageEncoding()==self::WobEncoding){
            if(!isset($_SERVER["HTTP_X_WOBREQUEST"])){
                throw new WobTransactionError($this,tr("Request is not a Wob Request, Aborting."),"non-wob");
                exit();
            }
            self::$running=$_SERVER["HTTP_X_WOBREQUEST"];
            foreach(array_keys($_SERVER) as $k){
                if(substr($k,0,11)=="HTTP_X_WOB_")
                    $this->headers[substr($k,11)]=$_SERVER[$k];
            }
        }else
        if($this->messageEncoding()==self::Soap12Encoding){
            //parse soapAction
            if(!isset($_SERVER["HTTP_SOAPACTION"])){
                throw new WobTransactionError($this,tr("Wob SOAP mode, missing SOAPAction header."),"soap");
                exit();
            }
            //parse SOAP headers
            $sact=explode(":",$_SERVER["HTTP_SOAPACTION"]);
            if(count($sact)<2){
                throw new WobTransactionError($this,tr("Wob SOAP mode, invalid SOAPAction header."),"soap");
                exit();
            }
            self::$running=$sact[count($sact)-1];
            //parse SOAP headers
            //TODO
        }
        return self::$running;
    }
    /**called to determine the session id or other headers*/
    public function getHeader($hd)
    {
        $idx=strtoupper($hd);
// 		echo "gethd ".$hd."\n";
        if(isset($this->headers[$idx]))return $this->headers[$idx];
        else return "";
    }

    /**retrieves the XML data from the request and parses headers*/
    protected function getRequestXml()
    {
        /*low level XML parsing*/
        global $HTTP_RAW_POST_DATA;
        if(isset($HTTP_RAW_POST_DATA))$txt=$HTTP_RAW_POST_DATA;else $txt="";
        if($txt=="")$txt=file_get_contents("php://input");
        $xml=new DOMDocument;
        if(!$xml->loadXML($txt))$this->xmlParserError();
        if($this->messageEncoding()==self::WobEncoding){
            foreach(array_keys($_SERVER) as $k){
                if(substr($k,0,11)=="HTTP_X_WOB_"){
                    $this->headers[substr($k,11)]=$_SERVER[$k];
// 					echo("new hd ".substr($k,11)."\n");
                }
            }
        }else{
            $root=$xml->documentElement;
            //TODO: parse header element
        }
        return $xml;
    }
    /**called if the transaction is not known. aborts the script.*/
    static public function noSuchTransaction()
    {
        $this->abortTransaction();
        throw new WobTransactionError($this,tr("Request is not known, Aborting."),"non-wob");
    }

    /**called if authentication fails*/
    public function notAuthenticated(){
        $this->abortTransaction();
        throw new WobTransactionError($this,tr("User is not authenticated or does not have permission to execute this request, Aborting."),"auth");
    }

    /**called if XML parsing fails*/
    public function xmlParserError(){
        $this->abortTransaction();
        throw new WobTransactionError($this,tr("Error while parsing request XML, Aborting."),"xml");
    }

    /**called for generic exception handling*/
    public function handleException($ex){
        //log details
        try{
            $msg="WobTransactionBase is handling an exception of type ".get_class($ex)." code ".$ex->getCode()." in file "
                .$ex->getFile()." line ".$ex->getLine().":\n".$ex->getTraceAsString();
            error_log($msg);
        }catch(Exception $e){ /* oops. */ }
        //throw on, if it already is internal
        if(is_a($ex,"WobTransactionError"))
            throw $ex;
        $this->abortTransaction();
        throw new WobTransactionError($this,$ex->getMessage(),"exception");
    }

    /**called to abort a transactions flow
    \param $type \b optional defines the source of the error (should be only one word, defaults to "server")
    \param $text the human readable text returned to the client
    */
    public function abortWithError($text,$type="server"){
        $this->abortTransaction();
        throw new WobTransactionError($this,$text,$type);
    }

    /**called internally if a transaction is not implemented*/
    protected function abortNotImplemented()
    {
        $this->abortWithError(tr("Transaction not implemented."));
    }

    /**stub: overwrite this to implement a real transaction start action (eg. sending the DB a "BEGIN TRANSACTION" statement); the $updating parameter is set to true for transactions marked as updating*/
    protected function startTransaction($updating){}
    /**stub: overwrite this to implement a real transaction commit action (eg. sending the DB a "COMMIT TRANSACTION" statement)*/
    protected function commitTransaction(){}
    /**stub: overwrite this to implement a real transaction abort action (eg. sending the DB a "ROLLBACK TRANSACTION" statement)*/
    protected function abortTransaction(){}

    /**stub: returns whether the user is authenticated, overwrite if you want to use authenticated or authorized transactions*/
    protected function isAuthenticated(){return false;}
    /**stub: returns whether the user is authorized to run a specific transaction, overwrite if you want to use authorized transactions*/
    protected function isAuthorized($transactioName){return false;}
    /**stub: returns the name of the user (default returns empty string)*/
    protected function userName(){return "";}

    /**internal: returns an initialized XML array ("doc"=>DomDocument, "root"=>transaction rool DomElement)*/
    protected function xmlCreate($elem){
        $r=array();
        $r["doc"]=new DOMDocument;
        $r["root"]=$r["doc"]->createElementNS($this->xmlProjectNamespace(),$elem);
        return $r;
    }

    /**internal: converts XML array to string representation*/
    protected function xmlToString($xml){
        if($this->messageEncoding()==self::WobEncoding){
            //simply put it in
            $xml["doc"]->appendChild($xml["root"]);
        }else{
            $soapNS=$this->xmlSoap12Namespace();
            $env=$xml["doc"]->createElementNS($soapNS,"SOAP-ENV:Envelope");
            //TODO: construct header
            //create body
            $body=$xml["doc"]->createElementNS($soapNS,"SOAP-ENV:Body");
            $env->appendChild($body);
            //put envelope into document
            $xml["doc"]->appendChild($env);
        }
        return $xml["doc"]->saveXml() . self::getDebug();
    }
};

//EOF
return
?>
