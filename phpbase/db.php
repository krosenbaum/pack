<?php
// Copyright (C) 2009-2015 by Konrad Rosenbaum <konrad@silmor.de>
// protected under the GNU LGPL version 3 or at your option any newer.
// See COPYING.LGPL file that comes with this distribution.
//
/**Abstract base class for database engines*/
abstract class DbEngine
{
	/**passcode for admin.php*/
	private $adminpass=false;
	private $adminuser=false;
	
	/**transaction RO/RW mode: false=assume readonly, true=assume inserts/updates*/
	protected $transmode=false;
	
	//public function __construct();
	
	/**connect to the database, must be overwritten by DB driver*/
	public abstract function tryConnect();

	///check whether the DB object is connected to an actual database
	public abstract function isConnected();
	
	/**set the admin passcode; used by the config.php file if admin access is allowed*/
	public function setAdminPassCode($u,$p)
	{
		$this->adminuser=$u;
		$this->adminpass=$p;
	}
	
	/**check admin credentials*/
	public function checkAdmin()
	{
		global $_SERVER;
		if(!$this->canAdministrate())return false;
		if(!isset($_SERVER["PHP_AUTH_USER"]) || !isset($_SERVER["PHP_AUTH_PW"])){
			return false;
		}
		return $_SERVER["PHP_AUTH_USER"]==$this->adminuser && $_SERVER["PHP_AUTH_PW"]==$this->adminpass;
	}
	
	/**returns whether a passcode is known and admin.php may be used*/
	public function canAdministrate()
	{
		return $this->adminpass!==false && $this->adminuser!==false;
	}
	
	/**returns the version of the DB layout that is required by this version of Magic Smoke*/
	public function needVersion()
	{
		return WobSchema::version();
	}
	
	/**returns whether the table exists; must be implemented by driver*/
	public abstract function hasTable($tablename);

	/**begins a transaction; must be implemented by driver; use sqlBeginTransaction to create the SQL statement!*/
	public abstract function beginTransaction();
	
	/**ends a transaction successfully; must be implemented by driver; returns true on success; use sqlCommitTransaction to create the SQL statement!*/
	public abstract function commitTransaction();
	
	/**ends a transaction with a rollback; must be implemented by driver; returns true on success; use sqlRollbackTransaction to create the SQL statement!*/
	public abstract function rollbackTransaction();
	
	/**locks the database - only used by the backup functions; if necessary should also start a transaction; default just starts a transaction*/
	protected function lockDB($writelock){$this->beginTransaction();}
	
	/**unlocks the database - only used by the backup functions; if necessary should also commit a transaction; default just commits*/
	protected function unlockDB(){$this->commitTransaction();}
	
	/**gets some data from the database; $table is the name of the table, $cols is the list of columns to return or "*" for all, $where is the where clause of the SQL-statement, $orderby may contain additional ORDER BY or GROUP BY clauses; returns array of rows, which are in *_fetch_array format; returns false on error; use sqlSelect to create the SQL statement!; make sure that NULL values are returned as PHP value null (most DB drivers already do this)*/
	public abstract function select($table,$cols,$where="",$orderby="");
	
	/**insert values into a table; returns false on failure, the new primary key if a sequence was set, true otherwise; use sqlInsert to create the SQL statement!*/
	public abstract function insert($table,array $values);
	
	/**update database values; returns how many rows have been changed or false for failure; use sqlUpdate to create the SQL statement!*/
	public abstract function update($table,array $values,$where);
	
	/**delete database values; returns the amount of rows deleted or false if an error occurred; use sqlDelete to create the SQL statement!*/
	public abstract function deleteRows($table,$where);

	///returns the name of the database the instance is connected to
	public abstract function dbName();
	
	/**creates a table; the argument is an array of the form "col-name" => array("col-type", "flags"...); use sqlCreateTable() etc. to create the actual statement*/
	protected abstract function createTable($tablename,$table);
	
	/**transform an internally used table name to the actual table name in the DB; the default implementation returns exactly what it gets*/
	public function tableName($tname){return $tname;}
	
	/**returns the correct type name for the required abstract data type;
	types that must be understood are: int32 (INTEGER), int64 (LONG INTEGER), seq32 (auto-incrementing int), seq64, bool (boolean), string:$length (text up to 255 chars, length is optional, default is 255; VARCHAR($length)), text (unlimited text)*/
	protected function dataType($type)
	{
		if($type=="int32"||$type=="enum"||$type=="enum32")return "INTEGER";
		if($type=="int64"||$type=="enum64")return "LONG INTEGER";
		if($type=="bool")return "BOOLEAN";
		$tpa=explode(":",$type);
		if($tpa[0]=="string"){
			if(isset($tpa[1]))
				return "VARCHAR(".$tpa[1].")";
			else
				return "VARCHAR(255)";
		}
		return false;
	}
	
	/**returns the correct name/coding of a flag:
	primarykey, notnull, unique (implies notnull), foreignkey:$table:$col, defaultint:$val, defaultstr:$val, index*/
	protected function columnFlag($flag,$col,$table,$cflags)
	{
		if($cflags&self::COLUMN_CREATE_PKEY){
			if($flag=="primarykey")return "PRIMARY KEY";
		}
		if($cflags&self::COLUMN_CREATE_NULL){
			if($flag=="null")return "NULL";
			if($flag=="notnull")return "NOT NULL";
			if($flag=="unique")return "NOT NULL UNIQUE";
		}
		if($cflags&self::COLUMN_CREATE_INDEX){
			if($flag=="index")return "INDEX";
		}
		$tpa=explode(":",$flag);
		if($cflags&self::COLUMN_CREATE_FKEY){
			if($tpa[0]=="foreignkey"){
				if(count($tpa)<3)
					return false;
				return "REFERENCES ".$this->tableName($tpa[1])."($tpa[2])";
			}
		}
		if($cflags&self::COLUMN_CREATE_DEFAULT){
			if($tpa[0]=="defaultint"){
				if(count($tpa)<2)
					return "DEFAULT NULL";
				return "DEFAULT ".$this->escapeInt($tpa[1]);
			}
			if($tpa[0]=="defaultstr"){
				if(count($tpa)<2)
					return "DEFAULT NULL";
				return "DEFAULT ".$this->escapeString($tpa[1]);
			}
			if($tpa[0]=="defaultbool"){
				if(count($tpa)<2)
					return "DEFAULT NULL";
				return "DEFAULT ".$this->escapeBool($tpa[1]);
			}
			if($tpa[0]=="default"){
				if(count($tpa)<2)
					return "DEFAULT NULL";
				return "DEFAULT ".$this->escapeColumn($table,$col,$tpa[1]);
			}
		}
	}
	
	/**creates a SQL92 statement for creating a table; overwrite this to implement DB specific syntax*/
	protected function sqlCreateTable($tablename,$table)
	{
		$ret="CREATE TABLE ".$this->tableName($tablename)." (";
		$cm="";
		reset($table);
		while(list($col,$def)=each($table)){
			$ret.=$cm;$cm=",";
			$ret.=$this->sqlCreateColumn($tablename,$col,$def);
		}
		$ret.=$this->createTableExtras($tablename,$table);
		$ret.=")";
		WobTransaction::debug("Create Table Stmt: ".$ret,WobTransaction::DebugDbStatement);
		return $ret;
	}

	const COLUMN_CREATE_TYPE = 1;
	const COLUMN_CREATE_NULL = 2;
	const COLUMN_CREATE_PKEY = 4;
	const COLUMN_CREATE_FKEY = 8;
	const COLUMN_CREATE_KEY = 12;
	const COLUMN_CREATE_DEFAULT = 16;
	const COLUMN_CREATE_INDEX = 32;
	const COLUMN_CREATE_CONSTRAINT = 64;
	const COLUMN_CREATE_ALL = 0xffff;

	///creates SQL92 part of a statement for creating a single column
	protected function sqlCreateColumn($tablename,$columnname,$columndef,$flags=self::COLUMN_CREATE_ALL)
	{
		$ret="";
		//check whether this is a special column
		if(substr($columnname,0,1)==":"){
			if($columnname==":primarykey"){
				if($flags & self::COLUMN_CREATE_PKEY)
					$ret.=$this->sqlCreateTablePrimaryKey($columndef);
			}else if($columnname==":uniqueconstraint"){
				if($flags & self::COLUMN_CREATE_CONSTRAINT)
					$ret.=$this->sqlCreateTableUniqueConstraint($columndef);
			}else die("Unknown special column ".$columnname." while creating table ".$tablename);
		}else{
			//column name
			$ret.=$columnname." ";
			//get type
			if($flags & self::COLUMN_CREATE_TYPE)
				$ret.=$this->dataType($columndef[0])." ";
			//get flags
			for($i=0;$i<count($columndef);$i++)
				$ret.=$this->columnFlag($columndef[$i],$columnname,$tablename,$flags)." ";
		}
		return $ret;
	}
	
	/**This function can be used to amend the column definitions of a table; if overwritten it must return a string; overwrite this to implement DB specific syntax*/
	protected function createTableExtras($tablename,$table)
	{
		return "";
	}
	
	/**creates primary key statement for sqlCreateTable; overwrite this to implement DB specific syntax*/
	protected function sqlCreateTablePrimaryKey(array $cols)
	{
		$ret="PRIMARY KEY(";
		for($i=0;$i<count($cols);$i++){
			if($i>0)$ret.=",";
			$ret.=$cols[$i];
		}
		$ret.=")";
		return $ret;
	}

	///creates the complex unique constraints; overwrite this to implement DB specific syntax
	protected function sqlCreateTableUniqueConstraint(array $constraints)
	{
		$ret="";
		for($i=0;$i<count($constraints);$i++){
			if($i>0)$ret.=",";
			$ret.="UNIQUE (".$constraints[$i].")";
		}
		return $ret;
	}
	
	/**creates a SQL92 statement for selects; overwrite this to implement DB specific syntax; the concrete DB implementation should append "for update" to the select statement if $this->transmode is true and the DB supports it*/
	public function sqlSelect($table,$cols,$where,$orderby)
	{
		$query="SELECT $cols FROM ".$this->tableName($table);
		if($where!="")$query.=" WHERE ".$where;
		if($orderby!="")$query.=" ".$orderby;
		WobTransaction::debug("DB Select Stmt: ".$query,WobTransaction::DebugDbStatement);
		return $query;
	}
	
	/**creates a SQL92 statement for inserts; overwrite this to implement DB specific syntax*/
	protected function sqlInsert($table,array $values)
	{
		$ret="INSERT INTO ".$this->tableName($table)." (";
		reset($values);
		$cm="";
		$val=") VALUES (";
		while(list($k,$v)=each($values)){
			//make sure the column exists, ignore the riff-raff
			if(!WobSchema::tableHasColumn($table,$k))continue;
			$ret.=$cm;$val.=$cm;$cm=",";
			//append column name
			$ret.=$k;
			//append value
			if(WobSchema::isIntColumn($table,$k))
				$val.=$this->escapeInt($v);
			else
			if(WobSchema::isStringColumn($table,$k))
				$val.=$this->escapeString($v);
			else
			if(WobSchema::isBlobColumn($table,$k))
				$val.=$this->escapeBlob($v);
			else
			if(WobSchema::isBoolColumn($table,$k))
				$val.=$this->escapeBool($v);
			else
				//don't know how to escape it...
				$val.="NULL";
		}
		$ret.=$val.")";
		WobTransaction::debug("DB Insert Stmt: ".$ret,WobTransaction::DebugDbStatement);
		return $ret;
	}
	
	/**creates a SQL92 statement for deletes; overwrite this to implement DB specific syntax*/
	protected function sqlDelete($table,$where)
	{
		$ret="DELETE FROM ".$this->tableName($table)." WHERE ".$where;
		WobTransaction::debug("DB Delete Stmt: ".$ret,WobTransaction::DebugDbStatement);
		return $ret;
	}
	
	/**creates a SQL92 statement for updates; overwrite this to implement DB specific syntax*/
	protected function sqlUpdate($table,array $values,$where)
	{
		$ret="UPDATE ".$this->tableName($table)." SET ";
		reset($values);
		$cm="";
		while(list($k,$v)=each($values)){
			//make sure the column exists, ignore the riff-raff
			if(!WobSchema::tableHasColumn($table,$k))continue;
			$ret.=$cm;$cm=",";
			//append column name
			$ret.=$k."=";
			//append value
			if(WobSchema::isIntColumn($table,$k))
				$ret.=$this->escapeInt($v);
			else
			if(WobSchema::isStringColumn($table,$k))
				$ret.=$this->escapeString($v);
			else
			if(WobSchema::isBlobColumn($table,$k))
				$ret.=$this->escapeBlob($v);
			else
			if(WobSchema::isBoolColumn($table,$k))
				$ret.=$this->escapeBool($v);
			else
				//don't know how to escape it...
				$ret.="NULL";
		}
		$ret.=" WHERE ".$where;
		WobTransaction::debug("DB Update Stmt: ".$ret,WobTransaction::DebugDbStatement);
		return $ret;
	}
	
	/**returns the SQL92 statement for beginning a transaction; overwrite this to implement DB specific syntax*/
	public function sqlBeginTransaction()
	{
		return "BEGIN TRANSACTION";
	}
	
	/**returns the SQL92 statement for committing a transaction; overwrite this to implement DB specific syntax*/
	public function sqlCommitTransaction()
	{
		return "COMMIT TRANSACTION";
	}
	
	/**returns the SQL92 statement for rolling a transaction back; overwrite this to implement DB specific syntax*/
	public function sqlRollbackTransaction()
	{
		return "ROLLBACK TRANSACTION";
	}
	
	/**escapes integers; the default implementation just makes sure it is an int (false and null are translated to NULL)*/
	public function escapeInt($i)
	{
		if($i === false || $i === null)return "NULL";
		return round($i + 0);
	}
	
	/**escapes a list of integers; uses escapeInt for each element; automatically adds parentheses*/
	public function escapeIntList(array $il)
	{
		if(count($il)==0)return "(NULL)";
		$r="(";
		$b=false;
		foreach($il as $i){
			if($b)$r.=",";
			else $b=true;
			$r.=$this->escapeInt($i);
		}
		$r.=")";
		return $r;
	}
	
	/**escapes a list of strings; uses escapeInt for each element; automatically adds parentheses*/
	public function escapeStringList(array $il)
	{
		if(count($il)==0)return "(NULL)";
		$r="(";
		$b=false;
		foreach($il as $i){
			if($b)$r.=",";
			else $b=true;
			$r.=$this->escapeString($i);
		}
		$r.=")";
		return $r;
	}

	/**escapes a list of values for a specific column; uses escapeInt for each element; automatically adds parentheses*/
	public function escapeListColumn(array $il,$table,$col)
	{
		if(count($il)==0)return "(NULL)";
		$r="(";
		$b=false;
		foreach($il as $i){
			if($b)$r.=",";
			else $b=true;
			$r.=$this->escapeColumn($table,$col,$i);
		}
		$r.=")";
		return $r;
	}

	/**escapes strings; the default uses addslashes and encloses the value in ''; it is recommended to overwrite this with the proper escaping procedure for the target DB (false and null are translated to NULL)*/
	public function escapeString($s)
	{
		if($s === false || $s === null) return "NULL";
		return "'".addslashes($s)."'";
	}
	
	/**escapes blobs; the default uses addslashes and encloses the value in ''; it is recommended to overwrite this with the proper escaping procedure for the target DB*/
	public function escapeBlob($s)
	{
		if($s === false || $s===null) return "NULL";
		return "'".addslashes($s)."'";
	}
	
	/**escapes a boolean value; the default translates 0, '0', 'f', 'false', 'n', 'no', false to FALSE and numerics !=0, 't', 'true', 'y', 'yes', true to TRUE; any other value (incl. the PHP constant null) is translated to NULL*/
	public function escapeBool($s)
	{
		if($s===null)return "NULL";
		if(is_numeric($s)){
			if(($s+0)!=0)return "TRUE";
			else return "FALSE";
		}
		if($s===false)return "FALSE";
		if($s===true)return "TRUE";
		switch(strtolower($s)){
			case "t":case "true":case "yes":case "y":
				return "TRUE";
			case "f":case "false":case "n":case "no":
				return "FALSE";
			default:
				return "NULL";
		}
	}
	
	/**generic escape routine: queries the schema for the correct escape mechanism and then returns the appropriately escaped value*/
	public function escapeColumn($table,$col,$val)
	{
		if(WobSchema::isIntColumn($table,$col))return $this->escapeInt($val);
		if(WobSchema::isStringColumn($table,$col))return $this->escapeString($val);
		if(WobSchema::isBlobColumn($table,$col))return $this->escapeBlob($val);
		if(WobSchema::isBoolColumn($table,$col))return $this->escapeBool($val);
		//fallback: NULL
		return "NULL";
	}

	/**returns a configuration setting*/
	public function getConfig($key)
	{
		$mar=$this->select(WobSchema::configTable(),WobSchema::configValueColumn(),WobSchema::configKeyColumn()."=".$this->escapeString($key));
		if(count($mar)>0)return $mar[0][0];
		return false;
	}
	
	/**sets a config setting*/
	public function setConfig($key,$val)
	{
		$this->beginTransaction();
		$ctable=WobSchema::configTable();
		$ckey=WobSchema::configKeyColumn();
		$cval=WobSchema::configValueColumn();
		$mar=$this->select($ctable,$cval,$ckey."=".$this->escapeString($key));
		if(count($mar)>0)$this->update($ctable,array($cval=>$val),$key."=".$this->escapeString($key));
		else $this->insert($ctable,array($ckey=>$key,$cval=>$val));
		$this->commitTransaction();
	}
	
	/**tries to find out whether the connected DB version is usable*/
	public function canUseDb($checkVersion=true)
	{
		if(!$this->isConnected())
			return false;
		if(!$this->hasTable(WobSchema::configTable()))
			return false;
		return !$checkVersion || $this->getConfig(WobSchema::configVersionRow())==$this->needVersion();
	}
	
	/**creates the database, used by admin.php only!!*/
	public function createDb()
	{
		$this->beginTransaction();
		//iterate DB schema and create tables
		$tabs=WobSchema::tableNames();
		for($i=0;$i<count($tabs);$i++){
			//create table
			if(!$this->createTable($tabs[$i],WobSchema::tableDefinition($tabs[$i]))){
				print("DB Error while creating ".$tabs[$i].": ".$this->lastError()."<p>\n");
				print("Last statement was: ".$this->sqlCreateTable($tabs[$i],WobSchema::tableDefinition($tabs[$i]))."<p>\n");
				$this->rollbackTransaction();
				die("Unable to create database.");
			}
			//insert defaults
			foreach(WobSchema::tableDefaults($tabs[$i]) as $def){
				$this->insert($tabs[$i],$def);
			}
		}
		//close transaction
		$this->commitTransaction();
	}
	
	/**shows how the database would be created, used by admin.php only!!*/
	public function showCreateDb()
	{
		print("<h3>Database Creation SQL Script</h3>\n<pre>\n");
		print(htmlentities($this->sqlBeginTransaction()).";\n");
		//iterate DB schema and create tables
		$tabs=WobSchema::tableNames();
		for($i=0;$i<count($tabs);$i++){
			//create table
			print(htmlentities($this->sqlCreateTable($tabs[$i],WobSchema::tableDefinition($tabs[$i]))).";\n");
			//insert defaults
			foreach(WobSchema::tableDefaults($tabs[$i]) as $def){
				print(htmlentities($this->sqlInsert($tabs[$i],$def)).";\n");
			}
		}
		//close transaction
		print(htmlentities($this->sqlCommitTransaction()).";\n</pre>\n");
	}

	///Upgrades the database, used by admin.php
	public function upgradeDb($doexec=true)
	{
		print("<h3>Database Upgrade SQL Script</h3>\n");
		if($doexec)
			print("The following commands are being executed:<p>\n");
		else
			print("Please copy these commands into your database client to do a manual upgrade:<p>\n");
		print("<pre>\n");
		//iterate DB schema and check/create tables
		$tabs=WobSchema::tableNames();
		for($i=0;$i<count($tabs);$i++){
			print("-- checking table ".$tabs[$i]);
			if($this->hasTable($tabs[$i])){
				print(" ... exists:\n");
				$this->upgradeTable($tabs[$i],$doexec);
			}else{
				print(" ... does not exist, creating:\n");
				print("<font color=\"green\">".htmlentities($this->sqlCreateTable($tabs[$i],WobSchema::tableDefinition($tabs[$i]))).";</font>\n");
				//create table
				if($doexec){
					if(!$this->createTable($tabs[$i],WobSchema::tableDefinition($tabs[$i]))){
						print("<font color=\"red\">-- DB Error while creating ".$tabs[$i].": ".$this->lastError()."\n");
						print("-- Last statement was: ".$this->sqlCreateTable($tabs[$i],WobSchema::tableDefinition($tabs[$i]))."\n");
						$this->rollbackTransaction();
						die("</pre><p>Unable to upgrade database.");
					}
				}
				//insert defaults
				foreach(WobSchema::tableDefaults($tabs[$i]) as $def){
					print("<font color=\"green\">".htmlentities($this->sqlInsert($tabs[$i],$def)).";</font>\n");
					if($doexec)
						$this->insert($tabs[$i],$def);
				}
			}
		}
		//change version
		$ctable=WobSchema::configTable();
		$cupdate=array(WobSchema::configValueColumn() => $this->needVersion());
		$cwhere=WobSchema::configKeyColumn()."=". $this->escapeString(WobSchema::configVersionRow());
		print("-- update version:\n<font color=\"green\">".$this->sqlUpdate($ctable,$cupdate,$cwhere)."</font>\n");
		if($doexec)
			$this->update($ctable,$cupdate,$cwhere);
		print("\n</pre>\nDone.\n");
	}

	///helper for upgradeDb -> upgrades a single table
	protected abstract function upgradeTable($tablename,$doexec);
	
	/**returns the error string of the last operation*/
	public abstract function lastError();
	
	/**returns whether the result value is NULL; the default interprets only the special value null as NULL*/
	public function isNull($val)
	{
		if($val===null)return true;
		else return false;
	}
	
	/**returns whether the DB driver currently believes to be in RW transaction mode*/
	public function transactionIsUpdating()
	{
		return $this->transmode;
	}
	
	/**set the transaction mode: true if it is an updating transaction*/
	public function setTransactionUpdating($tm)
	{
		$this->transmode=$tm!=false;
	}
	
	/**helper: encode backup data for transport*/
	private function escapeBackup($tab,$col,$val)
	{
		//check for NULL
		if($this->isNull($val))return "NULL NULL";
		//get type
		if(WobSchema::isIntColumn($tab,$col))return "int ".($val+0);
		if(WobSchema::isBoolColumn($tab,$col)){
			if($val)return "bool 1";
			else return "bool 0";
		}
		//string and blob are base64 encoded
		return "str ".base64_encode($val);
	}
	
	/**dump a backup (returned as string)*/
	public function dumpBackup()
	{
		//make sure nobody else can disturb us (read lock)
		$this->lockDB(false);
		//dump basic stuff
		$ret="startbackup\n";
		$ret.="backupversion 0\n";
		$ret.="dbversion ".WobSchema::version()."\n";
		//go through backup tables
		foreach(WobSchema::backupTables() as $tab){
			$ret.="table ".$tab."\n";
			//get columns
			$cols=WobSchema::tableColumns($tab);
			//go through rows
			$res=$this->select($tab,"*","1=1");
			foreach($res as $row){
				foreach($cols as $col){
					if(!array_key_exists($col,$row))continue;
					$val=$row[$col];
					$ret.="value $col ".$this->escapeBackup($tab,$col,$val)."\n";
				}
				$ret.="insert\n";
			}
		}
		$ret.="endofbackup\n";
		//release lock & commit
		$this->unlockDB();
		
		return $ret;
	}

	/**explore the backup settings and database and return an index to the current data as structured text;
	 each line has the following format:
	 S
	 T tablename [min max [groupSize]]
	 V base64value
	 E

	 -> S/E mark the start/end of the data
	 -> T is a table description line
	 -> min/max are minimal/maximal integer values of the backup key, if both are "-" then it is a string column
	 -> groupSize is the recommended size of backup groups (in different key values) for this table
	 -> V is a single value of the backup key if the preceding table has a string backup key
	 */
	public function exploreBackup()
	{
		$ret="S\n";
		foreach(WobSchema::backupTables() as $tab){
			$ret+="T $tab";
			// get key and min/max/size
			$col=WobSchema::backupKeyForTable($tab);
			if($col!=""){
				if(WobSchema::isIntColumn($tab,$col)){
					$res=$this->select($tab,"min($col) as min, max($col) as max");
					if($res!==false){
						$ret+=" ".$res[0]["min"]." ".$res[0]["max"];
					}else
						$ret+=" ? ?";
				}else
					$ret+=" - -";
				$size=WobSchema::backupGroupSizeForTable($tab);
				if($size>0)
					$ret+=" $size";
			}
			$ret+="\n";
			if($col!="" && WobSchema::isStringColumn($tab,$col)){
				$res=$this->select($tab,$col);
				foreach($res as $r)
					$ret+="V ".base64_encode($r[$col])."\n";
			}
		}
		$ret+="E\n";
		return $ret;
	}
	
	/**helper: decode backup data from transport format*/
	private function unescapeBackup($fmt,$val)
	{
		switch($fmt){
			case "NULL":return null;
			case "int":return $val+0;
			case "str":return base64_decode($val);
			case "bool":return $val+0;
			default:
				print("Warning: encountered unknown data encoding \"".htmlspecialchars($fmt)."\". Using NULL instead.<br>\n");
				return null;
		}
	}
	
	/**synchronize all sequences with current data*/
	public function syncSequences()
	{
		foreach(WobSchema::tableNames() as $table)
			if(WobSchema::hasSequence($table)!==false)
				$this->syncSequence($table);
	}
	
	/**synchronize the sequence of one database table; overwrite to actually do something*/
	public function syncSequence($table){}
	
	/**helper: inserts data from backup*/
	private function backupInsert($table,$data,$overwrite)
	{
		//get primary keys
		$pk=WobSchema::primaryKeyColumns($table);
		//scan whether data is existent
		$pks="";$q="";
		foreach($pk as $p){
			if($pks!="")$pks.=",";
			$pks.=$p;
			if($q!="")$q.=" AND ";
// 			print("pk $p ");
			$q.=$p."=".$this->escapeColumn($table,$p,$data[$p]);
		}
// 		print("pk=$pks q=$q<br>");
		$res=$this->select($table,$pks,$q);
		if(count($res)>0){
			if($overwrite){
				$b=$this->update($table,$data,$q);
				print("update data: ".($b===false?"failed":"success")."<br/>\n");
				if($b===false)print("<font color=\"red\">Error: ".htmlentities($this->lastError())."</font><br/>\n");
			}else{
				print("ignoring existing row<br>\n");
			}
		}else{
			$b=$this->insert($table,$data);
			print("insert data: ".($b===false?"failed":"success")."<br/>\n");
			if($b===false)print("<font color=\"red\">Error: ".htmlentities($this->lastError())."</font><br/>\n");
		}
	}
	
	/**called from admin.php: restore a backup*/
	public function restoreData($file,$overwrite)
	{
		//prep
		$this->lockDB(true);
		$data=array();
		$table="";
		//open file
		$fd=fopen($file,"r");
		while(!feof($fd)){
			$line=trim(fgets($fd));
			if($line=="")continue;
			$cmd=explode(" ",$line);
			switch($cmd[0]){
				case "startbackup":
				case "backupversion":
				case "dbversion":
					/*currently ignored, maybe we do something with it later*/
					break;
				case "endofbackup":
					$this->syncSequences();
					print("Reached End of Backup.<br>\n");
					break 2;
				case "table":
					$data=array();
					$table=$cmd[1];
					if(!$this->hasTable($table)){
						print("Switching to non-existing table ".htmlspecialchars($table)." - ignoring data that follows.<br>\n");
						$table="";
					}else
						print("Switching to table ".htmlspecialchars($table)."<br>\n");
					break;
				case "value":
					if(!isset($cmd[3]))$cmd[3]='';
					$data[$cmd[1]]=$this->unescapeBackup($cmd[2],$cmd[3]);
					break;
				case "insert":
					if($table==""){
						print("Warning: insert on non-existing table, ignoring data.<br>\n");
					}else{
						$this->backupInsert($table,$data,$overwrite);
					}
					$data=array();
					break;
				default:
					print("Warning: unknown statement \"".htmlspecialchars($cmd[0])."\" in backup file. Ignoring it.<br>\n");
					break;
			}
		}
		fclose($fd);
		$this->unlockDB();
	}
};

//EOF
return
?>
