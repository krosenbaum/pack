<?php
// Copyright (C) 2009-2011 by Konrad Rosenbaum <konrad@silmor.de>
// protected under the GNU LGPL version 3 or at your option any newer.
// See COPYING.LGPL file that comes with this distribution.
//

class WObject {
	protected function __construct(){}

	///base for deserialization
	static protected function fromXmlHelper($data,$xml,$elem){}
	
	///base for serialization
	protected function toXmlHelper($xml,$root){}
	
	///base for property array
	public function propertyArray(){return array();}
	
	/**helper function that returns only the elements directly beneith the one given as $root*/
	public static function elementsByTagName($root,$tag)
	{
		$list=array();
		foreach($root->childNodes as $node){
			if($node->nodeType == XML_ELEMENT_NODE)
				if($node->tagName == $tag)
					$list[]=$node;
		}
		return $list;
	}
	
	/**helper function for classy objects that calls getPropertyArray if available*/
	protected static function objectToArray($obj)
	{
		if(!is_subclass_of($obj,"WObject"))return null;
		if(!method_exists($obj,"propertyArray"))return null;
		return $obj->propertyArray();
	}
	
	/**helper function for XML'ized objects that transforms XML boolean to PHP boolean*/
	public static function fromXmlBoolean($str)
	{
		$s=strtolower(trim($str));
		if(is_numeric($s))return ($s+0)!=0;
		if($s=="true" || $s=="yes")return true;
		else false;
	}
	/**helper function for XML'ized objects: it returns whether the string is a valid boolean*/
	public static function isXmlBoolean($str)
	{
		$s=strtolower(trim($str));
		if(is_numeric($s))return true;
		if($s=="true" || $s=="yes" || $s=="false" || $s=="no")return true;
		else false;
	}
};

//EOF
return
?>
