<?php
// Copyright (C) 2009-2011 by Konrad Rosenbaum <konrad@silmor.de>
// protected under the GNU LGPL version 3 or at your option any newer.
// See COPYING.LGPL file that comes with this distribution.
//

/**registers a class to be loaded automatically*/
function wob_autoclass($cname,$fname)
{
	global $WOB_AUTOCLASSREG;
	if(is_string($cname) && is_string($fname)){
		$WOB_AUTOCLASSREG[$cname]=$fname;
		return true;
	}else
		return false;
}

$d=dirname(__FILE__);
wob_autoclass("DbEngine",$d.'/db.php');
wob_autoclass("MysqlEngine",$d.'/db_mysql.php');
wob_autoclass("PGsqlEngine",$d.'/db_pgsql.php');
wob_autoclass("WobTable", $d."/table.php");
wob_autoclass("WobSchemaBase",$d."/schema.php");
wob_autoclass("WobTransactionBase",$d."/transaction.php");
wob_autoclass("WobXmlException",$d."/exception.php");
wob_autoclass("WobTransactionError",$d."/exception.php");
wob_autoclass("WobWobTransactionError",$d."/exception.php");
wob_autoclass("WobSoapTransactionError",$d."/exception.php");
wob_autoclass("WObject",$d."/object.php");
//load the linguist dummies, since we use them quite often
include_once($d."/tr.php");
unset($d);

function wob_autoload($cname)
{
	global $WOB_AUTOCLASSREG;
	if(isset($WOB_AUTOCLASSREG[$cname])){
		require_once $WOB_AUTOCLASSREG[$cname];
		return true;
	}else{
		return false;
	}
}
spl_autoload_register('wob_autoload');

//EOF
return
?>
