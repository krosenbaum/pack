<?php
// Copyright (C) 2009-2011 by Konrad Rosenbaum <konrad@silmor.de>
// protected under the GNU LGPL version 3 or at your option any newer.
// See COPYING.LGPL file that comes with this distribution.
//

/** tr() assumes the context is set with a C++ class statement (won't find any here)
or with a special comment, please include / * TRANSLATOR php:: * / in all files using it */
function tr($str,$comment=""){return $str;}

/** translate() also takes a context*/
function translate($context,$str,$comment=""){return $str;}


/**XML translation: & -> &amp;, < -> &lt; etc.pp.*/
function xq($str){return htmlspecialchars($str,ENT_NOQUOTES,"UTF-8");}

//EOF
return
?>
