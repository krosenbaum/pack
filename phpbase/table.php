<?php
// Copyright (C) 2009-2011 by Konrad Rosenbaum <konrad@silmor.de>
// protected under the GNU LGPL version 3 or at your option any newer.
// See COPYING.LGPL file that comes with this distribution.
//

/**thrown by WobTable if a property change is out of range*/
class ValueOutOfRange extends Exception
{
	public function __construct($tab,$col,$val)
	{
		$this->message="Value of table '$tab' column '$col' is out of range. Value '$val' not allowed.";
	}
};

/**parent class of all tables*/
abstract class WobTable
{
	protected $data;
	protected $cdata;
	private $isfromdb;
	private $table;

	/**constructs a basic table*/
	protected function __construct(array $data,$isfromdb,$table)
	{
		$this->data=array();
		if($isfromdb)$this->data=$data;
		else
		foreach($data as $k=>$d){
			//silently ignore garbage
			if(!$this->hasProperty($k))continue;
			//verify non-garbage
			$vm="verifyValue".$k;
			if(method_exists($this,$vm))
			if(!$this->$vm($d))
				throw ValueOutOfRange($this->table,$k,$d);
			//set
			$this->data[$k]=$d;
		}
		$this->isfromdb=$isfromdb;
		$this->table=$table;
		$this->cdata=array();
		$this->resetAudit();
	}

	/**set properties*/
	public function __set($name,$value)
	{
		//check for property
		if(!$this->hasProperty($name)){
			$trace=debug_backtrace();
			trigger_error("Trying to set undefined property ".$name." in ".$trace[0]['file']." on line ".$trace[0]['line'],E_USER_NOTICE);
		}
		//verify value (TODO: what about NULL?)
		$vm="verifyValue".$name;
		if(method_exists($this,$vm))
			if(!$this->$vm($value))
				throw ValueOutOfRange($this->table,$name,$value);
		//set
		$this->cdata[$name]=$value;
	}

	/**set multiple properties*/
	public function setFromArray(array $data)
	{
		foreach($data as $key=>$value){
			if($this->hasProperty($key))
				$this->__set($key,$value);
		}
	}

	/**reverts changes to the property*/
	public function revert($name)
	{
		if(!$this->hasProperty($name)){
			$trace=debug_backtrace();
			trigger_error("Trying to revert undefined property ".$name." in ".$trace[0]['file']." on line ".$trace[0]['line'],E_USER_NOTICE);
		}
		if(array_key_exists($name,$this->cdata))unset($this->cdata[$name]);
	}

	/**reverts all changes to properties*/
	public function revertAll()
	{
		$this->cdata=array();
	}

	/**returns the name of the table*/
	public function tableName(){return $this->table;}

	/**returns whether the table contains a specific column*/
	public function hasColumn($c)
	{
		return WobSchema::tableHasColumn($this->table,$name);
	}

	/**overridden by woc, returns true if the property exists*/
	abstract public function hasProperty($c);

	/**returns whether this is an auditable table*/
	public function isAuditable(){return false;}

	/**overridden by woc, if this an auditable table; used in insert and update*/
	protected function createAudit(){}

	/**returns the where clause to find this instance (via primary key columns)*/
	public function where()
	{
		global $db;
		$r="";
		$pk=WobSchema::primaryKeyColumns($this->table);
		foreach($pk as $c){
            if(!array_key_exists($c,$this->data))continue;
			if($r!="")$r.=" AND ";
			$r.=$c."=".$db->escapeColumn($this->table,$c,$this->data[$c]);
		}
		return $r;
	}

	/**returns the property/column*/
	public function __get($name)
	{
		//verify name
		if(!$this->hasProperty($name)){
			$trace=debug_backtrace();
			trigger_error("Accessing undefined property ".$name." in ".$trace[0]['file']." on line ".$trace[0]['line'],E_USER_NOTICE);
		}
		//return value or null
		if(array_key_exists($name,$this->cdata))return $this->cdata[$name];
		if(array_key_exists($name,$this->data))return $this->data[$name];
		else return null;
	}

	/**checks whether a column exists*/
	public function __isset($name)
	{
		global $db;
		//verify name and return true on existence AND notnull
		if(array_key_exists($name,$this->cdata))return isset($this->cdata[$name]);
		if(array_key_exists($name,$this->data))return isset($this->data[$name]);
		else return false;
	}

	/**unsets column-properties to NULL*/
	public function __unset($name)
	{
		//reset to null
		if($this->hasProperty($name))$this->cdata[$name]=null;
	}

	/**returns whether any property has changed since the last DB sync*/
	public function isChanged()
	{
		return count($this->cdata)>0;
	}

	/**returns whether a specific column has changed since the last DB sync*/
	public function isColumnChanged($c)
	{
		return array_key_exists($c,$this->cdata);
	}

	/**insert the object under a new primary key value into the DB (implicitly calls newKey); returns true on success*/
	public function insert()
	{
		global $db;
		$this->isfromdb=false;
		//create new key
		$this->newKey();
		//now insert
		$data=array();
		foreach($this->data as $k=>$d)$data[$k]=$d;
		foreach($this->cdata as $k=>$d)$data[$k]=$d;
		$r=$db->insert($this->table,$data);
		if($r===false)return false;
		$this->isfromdb=true;
		$this->data=$data;
		$this->cdata=array();
		//assign primary key if sequence (otherwise newKey has done it)
		$seq=WobSchema::hasSequence($this->table);
		if($seq!==false)
			$this->data[$seq]=$r;
		//create audit data
		$this->createAudit();
		//return success
		return true;
	}

	/**generate a new primary key value for insert and marks the object as not yet in the DB; the default sets the primary key to NULL if it is a sequence; call the original first if you overwrite it*/
	public function newKey()
	{
		$this->isfromdb=false;
		$this->resetAudit();
		$pk=WobSchema::hasSequence($this->table);
		if($pk!==false){
			if(array_key_exists($pk,$this->data))unset($this->data[$pk]);
			if(array_key_exists($pk,$this->cdata))unset($this->cdata[$pk]);
		}
	}

	/**updates the object in the database; returns true on success; fails if it did not come from the DB - use insertOrUpdate in this case; succeeds without asking the database if nothing has changed*/
	public function update()
	{
		if(!$this->isfromdb)return false;
		if(count($this->cdata)==0)return true;
		global $db;
		$succ=$db->update($this->table,$this->cdata,$this->where())!==false;
		if($succ){
			foreach($this->cdata as $k=>$d)$this->data[$k]=$d;
			$this->cdata=array();
			$this->createAudit();
		}
		return $succ;
	}

	/**updates existing object in the database or inserts it if it does not exist in the DB yet*/
	public function insertOrUpdate()
	{
		global $db;
		if($this->isfromdb)return $this->update();
		else {
			if(count($db->select($this->table,"*",$this->where()))>0){
				$this->isfromdb=true;
				$this->cdata=$this->data;
				return $this->update();
			}else
				return $this->insert();
		}
	}

	/**deletes this instance from the database; returns true if it actually executed*/
	public function deleteFromDb()
	{
		if(!$this->isfromdb)return false;
		global $db;
		//obliterate audit data
		if($this->isAuditable())
			$db->deleteRows($this->table."_audit",$this->where());
		//delete data
		$db->deleteRows($this->table,$this->where());
		//mark as outsider
		$this->isfromdb=false;
		$this->resetAudit();
		return true;
	}

	///overridden in auditable implementations: resets the audit state
	protected function resetAudit()
	{
	}
};

//EOF
return
?>
