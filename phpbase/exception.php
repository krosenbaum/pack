<?php
// Copyright (C) 2009-2011 by Konrad Rosenbaum <konrad@silmor.de>
// protected under the GNU LGPL version 3 or at your option any newer.
// See COPYING.LGPL file that comes with this distribution.
//

class WobXmlException extends Exception {};

/**general Transaction Error - children of this class are thrown whenever a transaction fails for any reason*/
class TransactionError extends Exception
{
	protected $etype="_system";
	protected $estr="";
	protected $trans=null;
	protected $mode=WobTransactionBase::WobEncoding;
	
	public function __construct($errorString,$errorType="_system")
	{
		parent::__construct($errorString);
		$this->etype=$errorType;
		$this->estr=$errorString;
	}
	
	/**used by machine oriented transaction to print the proper XML representation of the error*/
	public function printXml()
	{
		switch($this->mode){
			case WobTransactionBase::Soap12Encoding:$this->printXmlSchema();break;
			default: $this->printXmlWob();break;
		}
		//print debug stuff (if anything left)
		WobTransaction::printDebug();
	}
	
	/**used by machine oriented transaction to print the proper XML representation of the error in Wob Encoding*/
	protected function printXmlWob()
	{
		header("X-WobResponse-Status: Error");
		$xml=new DOMDocument;
		if($this->trans===null)
			$ns="ns:wob-error";
		else
			$ns=$this->trans->xmlPackNamespace();
		$root=$xml->createElementNS($ns,"WobError",$this->estr);
		$root->setAttribute("type",$this->etype);
		$xml->appendChild($root);
		print($xml->saveXml());
	}
	
	/**returns the error type*/
	public function errorType(){return $this->etype;}
	
	/**returns the error string*/
	public function errorString(){return $this->estr;}
	
};

/**general Wob Error - thrown whenever a transaction fails for any reason and the transaction is already known*/
class WobTransactionError extends TransactionError
{
	public function __construct(WobTransactionBase $trans,$errorString,$errorType="_system")
	{
		parent::__construct($errorString,$errorType);
		$this->trans=$trans;
		if($trans!==null){
			$this->mode=$trans->messageEncoding();
		}
	}
};

/**specific Wob Error - thrown where we know we use Wob protocol*/
class WobWobTransactionError extends TransactionError
{
	public function __construct($errorString,$errorType="_system")
	{
		parent::__construct($errorString,$errorType);
		$this->mode=WobTransactionBase::WobEncoding;
	}
};

/**specific Wob Error - thrown where we know we use SOAP protocol*/
class WobSoapTransactionError extends TransactionError
{
	public function __construct($errorString,$errorType="_system")
	{
		parent::__construct($errorString,$errorType);
		$this->mode=WobTransactionBase::Soap12Encoding;
	}
};

//EOF
return
?>
