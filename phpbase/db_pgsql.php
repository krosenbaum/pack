<?php
// Copyright (C) 2009-2015 by Konrad Rosenbaum <konrad@silmor.de>
// protected under the GNU LGPL version 3 or at your option any newer.
// See COPYING.LGPL file that comes with this distribution.
//
/**MySQL adaptation of DB-Engine*/
class PGsqlEngine extends DbEngine
{
	/**prefix for table names*/
	protected $prefix="";

	private $connstr;
	private $dbhdl=false;
	private $charset="UTF8";
	private $intrans=false;
	private $do_debug=false;
	private $do_abort=false;

	/**initialize driver*/
	public function __construct($connstring)
	{
		$this->connstr=$connstring;
	}

	/**make sure it shuts down*/
	public function __destruct()
	{
		//fail on the side of caution: roll back unless already released
		if($this->intrans)
			@$this->rollbackTransaction();
	}

	/**set a table-name prefix for the database*/
	public function setPrefix($pre)
	{
		$this->prefix=$pre;
	}

	/**set the default charset for tables and connections*/
	public function setCharacterSet($c)
	{
		$this->charset=$c;
	}

	public function tryConnect()
	{
		//connect
		$this->dbhdl=pg_connect($this->connstr);
		if($this->dbhdl===false)
			die("Unable to connect to database system. Giving up.");
		//select Unicode; TODO: fix it to be configurable
		if(pg_set_client_encoding($this->charset)!=0)
			die("Cannot set character set to ".$this->charset.", aborting.");
		//make sure the DB is transaction safe
		if(@pg_query($this->dbhdl,"SET SESSION CHARACTERISTICS AS TRANSACTION ISOLATION LEVEL SERIALIZABLE")===false)
			die("Cannot make this database transaction safe, aborting");
	}

	///check whether the DB object is connected to an actual database
	public function isConnected()
	{
		return $this->dbhdl!==false;
	}

	public function dbName()
	{
		return pg_dbname($this->dbhdl);
	}

	public function hasTable($tnm)
	{
		if(!WobSchema::hasTable($tnm))return false;
		$res=@pg_query($this->dbhdl,"select * from ".$this->tableName($tnm)." where 1=2");
		if($res===false){
			$this->db_debug_error("has table",$tnm);
			return false;
		}
		pg_free_result($res);
		return true;
	}
	public function beginTransaction()
	{
		$res=@pg_query($this->dbhdl,$this->sqlBeginTransaction());
		if($res===false){
			$this->db_debug_error("begin transaction");
			$this->intrans=false;
		}else{
			$this->intrans=pg_result_status($res)==PGSQL_COMMAND_OK;
			pg_free_result($res);
		}
		return $this->intrans;
	}

	public function commitTransaction()
	{
		if(!$this->intrans)return false;
		//otherwise commit
		$this->intrans=false;
		$res=@pg_query($this->dbhdl,$this->sqlCommitTransaction());
		if($res!==false){
			pg_free_result($res);
			return true;
		}else{
			$this->db_debug_error("commit");
			return false;
		}
	}

	public function rollbackTransaction()
	{
		$this->intrans=false;
		//don't ask, just do (also catches implicit transactions)
		$res=@pg_query($this->dbhdl,$this->sqlRollbackTransaction());
		if($res!==false){
			pg_free_result($res);
		}else
			$this->db_debug_error("rollback");
	}

	protected function lockDB($wl)
	{
		if(!$this->intrans)$this->beginTransaction();
		$req="LOCK TABLE ";
		$i=0;
		foreach(WobSchema::backupTables() as $tab){
			if($i)$req.=",";
			$i++;
			$req.=$this->tableName($tab);
		}
		$req.=" IN ACCESS EXCLUSIVE MODE";
		$res=@pg_query($this->dbhdl,$req);
		if($res!==false){
			pg_free_result($res);
		}else
			$this->db_debug_error("lock",$req);
	}

	public function sqlBeginTransaction(){return "BEGIN";}

	public function sqlCommitTransaction(){return "COMMIT";}

	public function sqlRollbackTransaction(){return "ROLLBACK";}


	public function select($table,$cols,$where="",$orderby="")
	{
		//actual select
		$query=$this->sqlSelect($table,$cols,$where,$orderby);
		//if we are in write mode and there is no grouping
		//(no grouping is a PGSql limitation)
		//then append "FOR UPDATE" to tell the DB we will be writing
		//currently turned off, there are too many don'ts
// 		if($this->transmode && $orderby=="")
// 			$query.=" FOR UPDATE";
		$res=@pg_query($this->dbhdl,$query);
		if($res===false){
			$this->db_debug_error("select",$query);
			return false;
		}
		//get column names and types
		$nf=pg_num_fields($res);
		$fnames=array();
		for($i=0;$i<$nf;$i++)
			$fnames[$i]=pg_field_name($res,$i);
		//convert data
		$nr=pg_num_rows($res);
		$ret=array();
		for($i=0;$i<$nr;$i++){
			$row=pg_fetch_row($res);
			$crw=array();
			for($j=0;$j<$nf;$j++){
				$fn=$fnames[$j];
				if(pg_field_is_null($res,$i,$j))
					$crw[$j]=null;
				else
				if(WobSchema::isIntColumn($table,$fn))
					$crw[$j]=$row[$j]+0;
				else
				if(WobSchema::isBlobColumn($table,$fn))
					$crw[$j]=pg_unescape_bytea($row[$j]);
				else
				if(WobSchema::isBoolColumn($table,$fn))
					$crw[$j]=$this->unescapeBool($row[$j]);
				else //any other type: hope string is ok
					$crw[$j]=$row[$j];
				//copy to named index
				$crw[$fn]=$crw[$j];
			}
			$ret[]=$crw;
		}
		//free up resources and return
		pg_free_result($res);
		return $ret;
	}

	protected function createTable($tn,$t)
	{
		$sql=$this->sqlCreateTable($tn,$t);
		$res=@pg_query($this->dbhdl,$sql);
		if($res!==false){
			$this->db_debug_error("create Table",$sql);
			pg_free_result($res);
			return true;
		}else
			return false;
	}

	protected function upgradeTable($tablename,$doexec)
	{
		$dbtn=$this->tableName($tablename);
		$dbname=$this->dbName();
		//get schema
		$tschema=WobSchema::tableDefinition($tablename);
		if($tschema===false){
			print("--  internal error: table does not exist in schema\n");
			return;
		}
		//get columns from DB
		$res=@pg_query($this->dbhdl,"select * from information_schema.columns where table_catalog=".$this->escapeString($dbname).
			" and table_name=".$this->escapeString($dbtn)." order by ordinal_position");
		if($res===false){
			print("<font color=\"red\">-- Internal error while querying table:".$this->lastError()."</font>\n");
			return;
		}
		$dbcols=pg_fetch_all($res);
		pg_free_result($res);
		//print_r($dbcols);
		//go through columns
		foreach($tschema as $cname=>$cdef){
			if(substr($cname,0,1)==":"){
				print("-- <font color=\"red\">special column ".$cname." IGNORING FOR NOW</font>\n");
				continue;
			}
			print("--  column ".$cname);
			// check column exists
			$ccol=array_values(array_filter($dbcols,function($col)use($cname){return $col["column_name"]==$cname;}));
			//print_r($ccol);
			if(count($ccol)<1){
				print(" ... does not exist\n");
				//create
				$stmt="ALTER TABLE ".$dbtn." ADD COLUMN ".$this->sqlCreateColumn($tablename,$cname,$cdef);
				print("<font color=\"green\">".$stmt.";</font>\n");
				if($doexec){
					if(@pg_query($this->dbhdl,$stmt)===false)
						die("Unable to create column!");
				}
			}else{
				print(" ... exists, checking:\n");
				//check type is ok
				if($this->validateColumnType($cdef,$ccol[0]))
					print("--   type ok\n");
				else{
					print("--   adjusting type\n");
				}
			}
		}
	}

	private function validateColumnType($schemadef,$dbdef)
	{
		//var_dump($dbdef);
		// check type
		$schematype=strtolower($this->dataType($schemadef[0]));
		$dbtype=strtolower($dbdef["data_type"]);
		if($dbtype=="character varying"){
			$dbtype="varchar";
			if($dbdef["character_maximum_length"]!=null)
				$dbtype.="(".$dbdef["character_maximum_length"].")";
		}
		if($schematype=="serial" || $schematype=="bigserial"){
			if(substr($dbdef["column_default"],0,8)=="nextval("){
				if($dbtype=="integer")$dbtype="serial";
				if($dbtype=="bigint")$dbtype="bigserial";
			}
		}
		if($schematype!=$dbtype){
			print("--    <font color=\"orange\">(type mismatch - schema: $schematype db: $dbtype)</font>\n");
			return false;
		}else
			return true;
		// check null
		$schemanull=!in_array("notnull",$dbdef);
		$dbnull=$dbdef["IS_NULLABLE"]=="YES";
		return $schemanull==$dbnull;
	}

	protected function sqlCreateTable($tn,$t)
	{
		//use standard SQL
		$ret=parent::sqlCreateTable($tn,$t);
		//append indizes
		foreach($t as $col=>$def){
			if(in_array("index",$def)){
				$ftn=$this->tableName($tn);
				$ret.=";CREATE INDEX ".$ftn."_".$col."_idx ON "
				     .$ftn."(".$col.")";
			}
		}
		return $ret;
	}

	public function tableName($tn)
	{
		return $this->prefix.$tn;
	}

	protected function dataType($type)
	{
		if($type=="int32"||$type=="enum"||$type=="enum32")return "INTEGER";
		if($type=="int64"||$type=="enum64")return "BIGINT";
		if($type=="seq32")return "SERIAL";
		if($type=="seq64")return "BIGSERIAL";
		if($type=="text")return "TEXT";
		if($type=="blob")return "BYTEA"; //max.4GB of blob
		$tpa=explode(":",$type);
		if($tpa[0]=="string"){
			if(isset($tpa[1]))
				return "VARCHAR(".$tpa[1].")";
			else
				return "VARCHAR";
		}
		//fallback to SQL standard
		return parent::dataType($type);
	}

	protected function columnFlag($flag,$col,$table,$cflags)
	{
		//PostgreSQL does not mark columns for indexing directly
		if($flag=="index"){
			return "";
		}
		//fallback to SQL standard
		return parent::columnFlag($flag,$col,$table,$cflags);
	}

	public function insert($table,array $values)
	{
		$this->transmode=true;
		$sql=$this->sqlInsert($table,$values);
		$res=@pg_query($this->dbhdl,$sql);
		if($res===false){
			$this->db_debug_error("insert",$sql);
			return false;
		}
		pg_free_result($res);
		$seq=WobSchema::hasSequence($table);
		if($seq!==false){
			if(isset($values[$seq]))return $values[$seq];
			$res=@pg_query($this->dbhdl,"select lastval()");
			if(pg_num_rows($res)>0){
				$row=pg_fetch_array($res);
				$ret=$row[0];
			}else{
				$ret=true;
			}
			pg_free_result($res);
			return $ret;
		}else{
			return true;
		}
	}

	public function update($table,array $values,$where)
	{
		$this->transmode=true;
		$sql=$this->sqlUpdate($table,$values,$where);
		$res=@pg_query($this->dbhdl,$sql);
		if($res!==false){
			$ret=pg_affected_rows($res);
			pg_free_result($res);
			return $ret;
		}else{
			$this->db_debug_error("update",$sql);
			return false;
		}
	}

	public function deleteRows($table,$where)
	{
		$this->transmode=true;
		$sql=$this->sqlDelete($table,$where);
		$res=@pg_query($this->dbhdl,$sql);
		if($res!==false){
			$ret=pg_affected_rows($res);
			pg_free_result($res);
			return $ret;
		}else{
			$this->db_debug_error("delete",$sql);
			return false;
		}
	}

	public function lastError()
	{
		return pg_last_error($this->dbhdl);
	}

	/**escapes strings; it uses mysqli_escape_string and encloses the value in ''*/
	public function escapeString($s)
	{
		if($s === false||$s===null) return "NULL";
		return "'".pg_escape_string($this->dbhdl,$s)."'";
	}

	/**escapes blobs; it uses mysqli_escape_string and encloses the value in '' - blobs are binary strings in MySQL*/
	public function escapeBlob($s)
	{
		if($s === false||$s===null) return "NULL";
		return "'".pg_escape_bytea($this->dbhdl,$s)."'";
	}

	/**internal: unescape a postgres bool towards PHP bool*/
	protected function unescapeBool($b)
	{
		if($b===null)return null;
		if(is_bool($b))return $b;
		if(is_numeric($b))return ($b+0)!=0;
		$b=strtolower($b);
		if($b=="t" || $b=="true" || $b=="yes" || $b=="y" || $b=="on")return true;
		else return false;
	}

	/**synchronize the sequence of one database table*/
	public function syncSequence($table)
	{
		$tab=$this->tableName($table);
		$col=WobSchema::hasSequence($table);
		if($col===false)return;
		$seq=$tab."_".$col."_seq";
		$q="select setval('".$seq."',(select max(".$col.") from ".$tab."))";
// 		print($q."<br/>\n");
		$res=@pg_query($this->dbhdl,$q);
		if($res===false){
			$this->db_debug_error("sequence sync",$q);
			return false;
		}
		pg_free_result($res);
		return true;
	}

	/**development helper: activate debug mode*/
	public function setDebugMode($b=true,$a=false)
	{
		$this->do_debug=$b;
		$this->do_abort=$b && $a;
	}

	/**internal helper: print debug message as XML comment, if activated*/
	private function db_debug_error($what,$detail=false)
	{
		if($this->do_debug){
			print("<!-- Error while doing '".$what."' on DB: ".xq($this->lastError())." -->\n");
			if($detail!==false)
				print("<!-- details: ".xq($detail)." -->\n");
		}
		if($this->do_abort)
			die("giving up");
	}
};

//EOF
return
?>
