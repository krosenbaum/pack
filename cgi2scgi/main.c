/* ***************************************************************************
 CGI to SCGI Bridge

 (c) Konrad Rosenbaum, 2010

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program (file COPYING.GPL).
    If not, see <http://www.gnu.org/licenses/>.

******************************************************************************/

#include <errno.h>
#include <fcntl.h>
#include <netdb.h>
#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/un.h>
#include <unistd.h>

extern char**environ;

#define KB *1024
#define MB KB KB

char buf[1 MB];
int scfd=-1;


#define ERROR \
 "Status: 500 CGI Bridge Error\r\n" \
 "Content-Type: text/html\r\n" \
 "\r\n" \
 "CGI Bridge not working correctly.\r\n"

#ifdef DEBUG
#define dprint(args...) fprintf(stderr,args)
#else
#define dprint(args...)
#endif

void encodeEnviron()
{
	char *evc,**evn;
	char buf2[1024];
	int pos=0;
	memset(buf,0,sizeof(buf));
	//encode SCGI
	strcpy(buf,"SCGI");pos=5;
	strcpy(buf+pos,"1");pos+=2;
	//encode content length
	evc=getenv("CONTENT_LENGTH");
	memcpy(buf+pos,"CONTENT_LENGTH",15);pos+=15;
	if(evc){
		memcpy(buf+pos,evc,strlen(evc)+1);
		pos+=strlen(evc)+1;
	}else{
		memcpy(buf+pos,"0",2);
		pos+=2;
	}
	//encode variables
	for(evn=environ;*evn;evn++){
		int l1,l2,i;
		if(strncmp(*evn,"CONTENT_LENGTH=",15)==0)continue;
		if(strncmp(*evn,"SCGI=",5)==0)continue;
		if(strlen(*evn)>=sizeof(buf2))continue;
		l1=strlen(*evn)+1;
		if((l1+pos)>sizeof(buf))break;
		strncpy(buf2,*evn,sizeof(buf2));
		l2=0;
		for(i=0;i<strlen(buf2);i++){
			if(buf2[i]=='='){
				buf2[i]=0;
				l2=i;
				break;
			}
		}
		if(l2<=0)continue;
		memcpy(buf+pos,buf2,l1);pos+=l1;
	}
	sprintf(buf2,"%i:",pos);
	write(scfd,buf2,strlen(buf2));
	write(scfd,buf,pos);
	write(scfd,",",1);
}

void relayBody()
{
	//get content length
	char*cle,*ep;
	int cl,l;
	cle=getenv("CONTENT_LENGTH");
	if(cle==0)return;
	cl=strtol(cle,&ep,10);
	if(*ep!=0)return;
	while(cl>0){
		if(cl>sizeof(buf))l=sizeof(buf);else l=cl;
		l=read(STDIN_FILENO,buf,l);
		write(scfd,buf,l);
		cl-=l;
	}
}

void relayResponse()
{
	fd_set rd,ex;
	int maxfd=scfd;
	int rfd;
	if(STDOUT_FILENO>maxfd)maxfd=STDOUT_FILENO;
	maxfd++;
	do{
		FD_ZERO(&rd);
		FD_ZERO(&ex);
		FD_SET(STDOUT_FILENO,&ex);
		FD_SET(scfd,&ex);
		FD_SET(scfd,&rd);
		rfd=select(maxfd,&rd,0,&ex,0);
		if(rfd<0){
			int err=errno;
			if(err==EINTR)continue;
			fprintf(stderr,"Error during relay of response: %s\n",strerror(err));
			return;
		}
		if(rfd==0)continue;
		if(FD_ISSET(scfd,&ex) || FD_ISSET(STDOUT_FILENO,&ex)){
			fprintf(stderr,"Exception during response relay.\n");
			return;
		}
		if(FD_ISSET(scfd,&rd)){
			int r;
			r=read(scfd,buf,sizeof(buf));
			if(r>0)write(STDOUT_FILENO,buf,r);
			else {
				fprintf(stderr,"End of input.\n");
				return;
			}
		}
	}while(1);
}

char*scgifile=0;
char*scgihost=0;
char*scgiport=0;

#ifndef UNIX_PATH_MAX
#define UNIX_PATH_MAX 108
#endif

void openFile()
{
	struct sockaddr_un sa;
	if(scgifile==0)return;
	dprint("Attempting to connect to file socket %s\n",scgifile);
	if(strlen(scgifile)>=UNIX_PATH_MAX)return;
	scfd=socket(AF_UNIX,SOCK_STREAM,0);
	memset(&sa,0,sizeof(sa));
	sa.sun_family=AF_UNIX;
	strncpy(sa.sun_path,scgifile,UNIX_PATH_MAX);
	if(connect(scfd,(struct sockaddr*)&sa,sizeof(sa))!=0){
		dprint("Unable to connect to %s: %s\n",scgifile,strerror(errno));
		scfd=-1;
	}else{
		dprint("Connected to file socket %s\n",scgifile);
	}
}

void openNet()
{
	int sfd,port;
	char*ep;
	struct addrinfo *ai,*rai;
	//sanity checks and get port
	if(scgihost==0 || scgiport==0)return;
	dprint("Attempting to connect to host %s port %s\n",scgihost,scgiport);
	port=strtol(scgiport,&ep,10);
	if(*ep!=0)return;
	if(port<=0 || port > 0xffff)return;
	//resolve name
	if(getaddrinfo(scgihost,0,0,&ai)!=0)return;
	rai=ai;
	if(ai==0)return;
	//get matching socket
	for(;ai;ai=ai->ai_next){
		//get a socket
		if(ai->ai_family!=AF_INET && ai->ai_family!=AF_INET6)continue;
		sfd=socket(ai->ai_family,SOCK_STREAM,0);
		if(sfd<0)continue;
		//IPv4?
		if(ai->ai_family==AF_INET){
			struct sockaddr_in sa;
			memset(&sa,0,sizeof(sa));
			sa.sin_family=AF_INET;
			sa.sin_port=htons(port);
			memcpy(&sa.sin_addr, &((struct sockaddr_in*)(ai->ai_addr))->sin_addr, sizeof(sa.sin_addr));
			if(connect(sfd,(struct sockaddr*)&sa,sizeof(sa))==0){
				scfd=sfd;
				dprint("Successfully connected.\n");
				break;
			}else
				close(sfd);
		}else{
			//IPv6
			struct sockaddr_in6 sa;
			memset(&sa,0,sizeof(sa));
			sa.sin6_family=AF_INET6;
			sa.sin6_port=htons(port);
			memcpy(&sa.sin6_addr, &((struct sockaddr_in6*)(ai->ai_addr))->sin6_addr, sizeof(sa.sin6_addr));
			if(connect(sfd,(struct sockaddr*)&sa,sizeof(sa))==0){
				scfd=sfd;
				dprint("Successfully connected.\n");
				break;
			}else
				close(sfd);
		}
	}
	//free info
	freeaddrinfo(rai);
}

int main(int argc,char**argv)
{
	//check argument list size
	if(argc>3){
		printf(ERROR);
		printf("Unable to decide where to connect to.\r\n");
		fprintf(stderr,"Error: usage %s [socket] | [host port]\n",*argv);
		fprintf(stderr,"Alternatively instead of arguments use environment variables.\n"); fprintf(stderr,"  socket or $SCGI_FILE - use a unix local file\n");
		fprintf(stderr,"  host/port or $SCGI_HOST/$SCGI_PORT - use network connection\n");
		return 1;
	}
	//check environment
	scgifile=getenv("SCGI_FILE");
	scgihost=getenv("SCGI_HOST");
	scgiport=getenv("SCGI_PORT");
	//overide env by args
	if(argc==2){
		scgifile=argv[1];
	}
	if(argc==3){
		scgihost=argv[1];
		scgiport=argv[2];
	}
	//get connection
	openFile();
	if(scfd<0)openNet();
	if(scfd<0){
		printf(ERROR);
		printf("SCGI Server unreachable.\r\n");
		fprintf(stderr,"Error: cannot open socket\n");
		return 1;
	}
	//send request
	dprint("Encoding environment...\n");
	encodeEnviron();
	dprint("Relaying request body...\n");
	relayBody();
	//relay response
	dprint("Relaying response...\n");
	relayResponse();
	
	return 0;
}