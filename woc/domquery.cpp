// Copyright (C) 2009-2011 by Konrad Rosenbaum <konrad@silmor.de>
// protected under the GNU GPL version 3 or at your option any newer.
// See COPYING.GPL file that comes with this distribution.
//

#include "domquery.h"


MDomQuery::MDomQuery(const QDomDocument&start,QString path)
{
	construct(start.documentElement(),path);
}

MDomQuery::MDomQuery(const QDomElement&start,QString path)
{
	construct(start,path);
}

void MDomQuery::construct(const QDomElement&start,QString path)
{
	//split query
	QStringList ql=path.split("/",Qt::SkipEmptyParts);
	//determine start mode and initialize list
	MDomNodeList ndlst;
	if(path.startsWith("//")){
		QDomDocument doc=start.ownerDocument();
		if(ql.size()<1){
			qDebug("MDomQuery: // query must not be empty!");
			return;
		}
		if(ql[0]=="*" || ql[0]=="@*"){
			qDebug("MDomQuery: // query must not start with wildcard * or @*.");
			return;
		}
		if(ql[0].startsWith("@")){
			qDebug("MDomQuery: cannot start with attributes in a // query.");
			return;
		}else{
			ndlst=doc.elementsByTagName(ql[0]);
			ql.removeFirst();
		}
	}else
	if(path.startsWith("/")){
		QDomElement root=start.ownerDocument().documentElement();
		if(ql.size()<1){
			//hmm, guess what the user really wanted (assume "/ *" )
			m_result<<root;
			return;
		}
		if(ql[0].startsWith("@")){
			qDebug("MDomQuery: cannot start with attributes in a / query.");
			return;
		}
		if(ql[0]=="*"){
			//any root
			ndlst<<root;
		}else{
			//specific root
			if(root.tagName()==ql[0])ndlst<<root;
		}
		//remove root from query, then go on below
		ql.removeFirst();
	}else{
		//query starts at current element
		ndlst<<start;
	}
	//iteratively go through path and eliminate non-matches
	while(ql.size()>0){
		MDomNodeList lst2;
		//parse pattern
		if(ql[0].startsWith("@")){
			//attribute handling...
			if(ql.size()>1){
				qDebug("MDomQuery: cannot sub-parse attributes.");
				return;
			}
			if(ql[0]=="@*"){
				//get all attributes of all current elements
				for(int i=0;i<ndlst.size();i++)
					lst2+=ndlst[i].toElement().attributes();
			}else{
				//get specific attributes
				for(int i=0;i<ndlst.size();i++){
					QDomElement el=ndlst[i].toElement();
					QString nm=ql[0].mid(1);
					if(el.hasAttribute(nm))
						lst2<<el.attributeNode(nm);
				}
			}
		}else{
			//element handling
			QString nm=ql[0];
			if(nm=="*"){
				//copy any element
				for(int i=0;i<ndlst.size();i++){
					QDomNodeList nl=ndlst[i].childNodes();
					for(int j=0;j<nl.size();j++){
						QDomElement el=nl.at(j).toElement();
						if(!el.isNull())
							lst2<<el;
					}
				}
			}else{
				//search for specific elements
				for(int i=0;i<ndlst.size();i++)
					lst2+=ndlst[i].toElement().elementsByTagName(nm);
			}
		}
		//next recursion:
		ql.removeFirst();
		ndlst=lst2;
	}
	m_result=ndlst;
}

QString MDomQuery::toString()const
{
	QString ret;
	for(int i=0;i<m_result.size();i++){
		if(ret!="")ret+=" ";
		if(m_result[i].isElement())
			ret+=m_result[i].toElement().text();
		else
			ret+=m_result[i].nodeValue();
	}
	return ret;
}

QStringList MDomQuery::toStringList()const
{
	QStringList ret;
	for(int i=0;i<m_result.size();i++)
		if(m_result[i].isElement())
			ret<<m_result[i].toElement().text();
		else
			ret<<m_result[i].nodeValue();
	return ret;
}
