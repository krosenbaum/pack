// Copyright (C) 2012 by Konrad Rosenbaum <konrad@silmor.de>
// protected under the GNU GPL version 3 or at your option any newer.
// See COPYING.GPL file that comes with this distribution.
//

#ifndef WOC_DOXYOUT_H
#define WOC_DOXYOUT_H
#include <QStringList>

inline QString doxyFormat(const QString&in,int level=0)
{
	//split to lines
	QStringList inl=in.trimmed().split('\n');
	QString pre,ret;
	//create indent and comment prefix
	for(int i=0;i<level;i++)pre+='\t';
	pre+="/// ";
	//add comment
	foreach(QString line,inl){
		ret+=pre;
		ret+=line;
		ret+="\n";
	}
	//done
	return ret;
}

inline QString doxyFormat(const QStringList&inl,int level=0)
{
	QString ret;
	foreach(QString in,inl){
		in=in.trimmed();
		if(in.isEmpty())continue;
		if(!ret.isEmpty())ret+=doxyFormat("",level);
		ret+=doxyFormat(in,level);
	}
	return ret;
}

#endif
