// Copyright (C) 2009-2011 by Konrad Rosenbaum <konrad@silmor.de>
// protected under the GNU GPL version 3 or at your option any newer.
// See COPYING.GPL file that comes with this distribution.
//

#include "htmlout.h"

#include <QDir>
#include <QDomElement>

WocHtmlOut::WocHtmlOut(QDomElement&el)
{
	qDebug("Info: creating Html Output Generator.");
	WocProcessor*woc=WocProcessor::instance();
	m_basedir=woc->baseDir()+"/"+el.attribute("sourceDir",".");
	m_subdir=el.attribute("subDir","htmlwob");
	bool clean=str2bool(el.attribute("clean","0"));
	//cleanup directory (remove normal files, assume remainder is harmless)
	QDir d(m_basedir+"/"+m_subdir);
	if(d.exists() && clean){
		QStringList ent=d.entryList(QDir::Files);
		for(int i=0;i<ent.size();i++)
			d.remove(ent[i]);
	}
	//get/create directory
	if(!d.exists())QDir(".").mkpath(m_basedir+"/"+m_subdir);

	//create index file
	m_index.setFileName(m_basedir+"/"+m_subdir+"/index.html");
	if(!m_index.open(QIODevice::WriteOnly|QIODevice::Truncate)){
		qDebug("Error: cannot create HTML index file %s.",m_index.fileName().toLatin1().data());
		emit errorFound();
		return;
	}
	m_index.write(QByteArray("<html>\n<head>\n"));
	m_index.write(QByteArray("<title>Project Index</title>\n"));

	m_index.write(QByteArray("</head><body>\n"));
	//write project info
	QString inf="<h1>Project "+woc->projectName()+"</h1>\n";
	inf+="Human Readable Version: "+woc->verHR()+"<br/>";
	inf+="Communication Layer Version: "+woc->verComm()+"<br/>";
	inf+="Minimum Compatible Version: "+woc->verNeedComm()+"<p/>";

	QMap<QString,QString> vinfo=woc->versionInfo();
	inf+="Repository Type: "+vinfo.value("System")+"<br/>";
	inf+="Repository URL: "+vinfo.value("RootURL")+"<br/>";
	inf+="Repository Path: "+vinfo.value("Path")+"<br/>";
	inf+="Author: "+vinfo.value("Author")+"<br/>";
	inf+="Time Stamp: "+vinfo.value("Time")+"<br/>";
	inf+="Revision: "+vinfo.value("Number")+"<p/>";

	inf+="Database Instance Object: "+woc->dbInst()+"<br/>";
	inf+="Database Schema Object: "+woc->dbSchema()+"<br/>";
	inf+="Database default access mode: "+QString(woc->dbUpdatingDefault()?"updating":"reading")+"<br/>";
	inf+="Database Schema Version: "+woc->dbVersion()+"<p/>";

	m_index.write(inf.toLatin1());

	//write global docu
	QStringList dcs=woc->docStrings();
	for(int i=0;i<dcs.size();i++){
		inf="<p>"+dcs[i]+"</p>\n";
		m_index.write(inf.toLatin1());
	}
}

WocHtmlOut::~WocHtmlOut(){}

void WocHtmlOut::finalize()
{
	//TODO: write index table content
	m_index.write(QByteArray("<h1>Index</h1>\n"));
	m_index.write(QByteArray("<table><tr><td><h2>Classes</h2></td><td><h2>Transactions</td><td><h2>Tables</h2></td></tr>\n"));
	m_index.write(QByteArray("<tr><td valign=\"top\"><ul>\n"));
	QStringList sl=WocProcessor::instance()->classNames();
	QString s;
	std::sort(sl.begin(),sl.end());
	for(int i=0;i<sl.size();i++)
		s+="<li><a href=\"class-"+sl[i]+".html\">"+sl[i]+"</a></li>\n";
	m_index.write(s.toLatin1());

	m_index.write(QByteArray("</ul></td><td valign=\"top\"><ul>"));
	sl=WocProcessor::instance()->transactionNames();
	s="";
	std::sort(sl.begin(),sl.end());
	for(int i=0;i<sl.size();i++)
		s+="<li><a href=\"trn-"+sl[i]+".html\">"+sl[i]+"</a></li>\n";
	m_index.write(s.toLatin1());

	m_index.write(QByteArray("</ul></td><td valign=\"top\"><ul>"));
	sl=WocProcessor::instance()->tableNames();
	s="";
	std::sort(sl.begin(),sl.end());
	for(int i=0;i<sl.size();i++)
		s+="<li><a href=\"table-"+sl[i]+".html\">"+sl[i]+"</a></li>\n";
	m_index.write(s.toLatin1());

	m_index.write(QByteArray("</ul></td></tr></table>\n"));

	m_index.write(QByteArray("\n</body></html>\n"));
	m_index.close();
}

void WocHtmlOut::newTable(const WocTable&tbl)
{
	QString cn=tbl.name();
	QFile htm(m_basedir+"/"+m_subdir+"/table-"+cn+".html");
	if(!htm.open(QIODevice::WriteOnly|QIODevice::Truncate)){
	   	qDebug("Error: cannot create html file for table %s.",cn.toLatin1().data());
	   	emit errorFound();
	   	return;
	}
	//lead in
	htm.write(QString("<html><title>Table "+cn+"</title><body>\n").toLatin1());

	QString hcd;
	//table declaration
	hcd+="<h1>Table "+cn+"</h1>\n";

	if(tbl.isAuditable()){
		hcd+="<p>This table is audited, see <a href=\"table-"+tbl.name()+"_audit.html\">"
			+tbl.name()+"_audit</a> for details.</p>\n";
	}

	QStringList td=tbl.docStrings();
	for(int i=0;i<td.size();i++)
		hcd+="<p>"+td[i]+"</p>\n";

	hcd+="<table frame=\"1\" border=\"1\"><tr><td><b>Column Name</b></td><td><b>Type</b></td><td><b>Properties</b></td><td><b>Docu</b></td></tr>\n";
	QStringList cl=tbl.columns();
	for(int i=0;i<cl.size();i++){
		hcd+="<tr><td>"+cl[i]+"</td><td>"+tbl.columnType(cl[i])+"</td><td>";
		if(tbl.columnIsPrimary(cl[i]))
			hcd+="Primary-Key ";
		if(tbl.columnIsNull(cl[i]))
			hcd+="NULL-able ";
		if(tbl.columnHasDefault(cl[i]))
			hcd+="default=\""+tbl.columnDefault(cl[i])+"\" ";
		if(tbl.columnIsForeign(cl[i])){
			QStringList cf=tbl.columnForeign(cl[i]).split(":");
			hcd+="Foreign-Key=<a href=\"table-"+cf[0]+".html\">"+cf[0]+"("+cf[1]+")</a> ";
		}
		if(tbl.columnIsIndexed(cl[i]))
			hcd+="Indexed ";
		if(tbl.columnIsUnique(cl[i]))
			hcd+="Unique";
		hcd+="</td><td>"+tbl.columnDoc(cl[i])+"</td></tr>\n";
	}
	hcd+="</table>\n";

	//enums
	for(int i=0;i<cl.size();i++){
		QList<WocEnum >lst=tbl.columnEnums(cl[i]);
		if(lst.size()>0){
			hcd+="<h2>Enum for column "+cl[i]+"</h2>\n<ul>\n";
			for(int j=0;j<lst.size();j++){
				hcd+="<li>"+lst[j].name+"="+QString::number(lst[j].val);
				if(lst[j].doc!="")hcd+="<br/>"+lst[j].doc;
				hcd+="</li>\n";
			}
			hcd+="</ul>\n";
		}
	}

	//foreign getters
	cl=tbl.foreigns();
	if(cl.size()>0){
		hcd+="<h2>Foreign Getters</h2>\n<ul>";
		for(int i=0;i<cl.size();i++){
			hcd+="<li>"+cl[i];
			QString s=tbl.foreignDoc(cl[i]);
			if(s!="")hcd+="<br/>"+s;
			hcd+="</li>\n";
		}
	}

	//presets
	QList<QMap<QString,QString> >pre=tbl.presets();
	if(pre.size()>0){
		hcd+="<h2>Presets</h2>\n<table frame=\"1\" border=\"1\">\n<tr>";
		cl=tbl.columns();
		for(int i=0;i<cl.size();i++)
			hcd+="<td><b>"+cl[i]+"</b></td>";
		hcd+="</tr>\n";
		for(int i=0;i<pre.size();i++){
			hcd+="<tr>";
			for(int j=0;j<cl.size();j++){
				hcd+="<td>";
				if(pre[i].contains(cl[j]))
					hcd+=pre[i][cl[j]];
				hcd+="</td>";
			}
			hcd+="</tr>\n";
		}
		hcd+="</table>\n";
	}

	hcd+="</body></html>\n";
	htm.write(hcd.toLatin1());
}

void WocHtmlOut::newClass(const WocClass&cls)
{
	QString cn=cls.name();
	QFile htm(m_basedir+"/"+m_subdir+"/class-"+cn+".html");
	if(!htm.open(QIODevice::WriteOnly|QIODevice::Truncate)){
	   	qDebug("Error: cannot create html file for class %s.",cn.toLatin1().data());
	   	emit errorFound();
	   	return;
	}
	//lead in
	htm.write(QString("<html><title>Class "+cn+"</title><body>\n").toLatin1());

	QString hcd;
	//class declaration
	hcd+="<h1>";
	if(cls.isAbstract(""))hcd+="Abstract ";
	hcd+="Class "+cn+"</h1>\n";

	//conditional abstract
	QStringList ab=cls.abstractLangs();
	if(ab.size()>0){
		hcd+="<p>The class is conditionally abstract in: ";
		for(int i=0;i<ab.size();i++){
			if(i)hcd+=", ";
			hcd+=ab[i];
		}
		hcd+="</p>\n";
	}

	//docu
	QStringList doc=cls.docStrings();
	for(int i=0;i<doc.size();i++)
		hcd+="<p>"+doc[i]+"</p>\n";
	htm.write(hcd.toLatin1());

	//enums
	classEnums(cls,htm);

	//properties
	classProperties(cls,htm);

	//mappings
	classMappings(cls,htm);

	//lead out
	htm.write(QByteArray("</body></html>\n"));
}

void WocHtmlOut::classEnums(const WocClass&cls,QFile&hdr)
{
	QStringList k=cls.enumTypes();
	if(k.size()==0)return;
	QString hcd="<h2>Enums</h2>\n";
	for(int i=0;i<k.size();i++){
		//type
		hcd+="<h3>enum "+k[i]+"</h3>\n";
		hcd+="<p>"+cls.enumDoc(k[i])+"</p>\n";
		hcd+="<table frame=\"1\" border=\"1\">";
		hcd+="<tr><td><b>Symbol</b></td><td><b>Value</b></td><td><b>Docu</b></td></tr>\n";
		QList<WocEnum>ev=cls.enumValues(k[i]);
		for(int j=0;j<ev.size();j++){
			hcd+="<tr><td>"+ev[j].name+"</td><td>"+QString::number(ev[j].val)
			   +"</td><td>"+ev[j].doc+"</td></tr>\n";
		}
		hcd+="</table>\n";
	}
	hdr.write(hcd.toLatin1());
}

void WocHtmlOut::classProperties(const WocClass&cls,QFile&hdr)
{
	QStringList k=cls.propertyNames();
	if(k.size()==0)return;
	QString hcd;
	//declare members
	hcd="<h2>Properties</h2>\n<ul>\n";
	for(int i=0;i<k.size();i++){
		hcd+="<li>"+k[i]+" (";
		if(cls.propertyIsObject(k[i]))
			hcd+="<a href=\"class-"+cls.propertyPlainType(k[i])+".html\">";
		hcd+=cls.propertyType(k[i]);
		if(cls.propertyIsObject(k[i]))
			hcd+="</a>";
		hcd+=")";
		QString d=cls.propDoc(k[i]);
		if(d!="")hcd+="<br/>"+d;
		hcd+="</li>\n";
	}
	hcd+="</ul>\n";
	//write
	hdr.write(hcd.toLatin1());
}

void WocHtmlOut::classMappings(const WocClass&cls,QFile&hdr)
{
	QStringList k=cls.mappingTables();
	if(k.size()==0)return;
	QString hcd;
	for(int i=0;i<k.size();i++){
		hcd+="<h2>Mapping for Table <a href=\"table-"+k[i]+".html\">"+k[i]+"</a></h2>\n";
		hcd+="<table frame=\"1\" border=\"1\">\n";
		hcd+="<tr><td><b>Property</b></td><td><b>Column</b></td></tr>\n";
		QMap<QString,QString> map=cls.mapping(k[i]);
		QStringList k2=map.keys();
		for(int j=0;j<k2.size();j++){
			hcd+="<tr><td>"+k2[j]+"</td><td>"+map[k2[j]]+"</td></tr>\n";
		}
		hcd+="</table>\n";
	}

	hdr.write(hcd.toLatin1());
}

void WocHtmlOut::newTransaction(const WocTransaction&trn)
{
	QString cn=trn.name();
	QFile htm(m_basedir+"/"+m_subdir+"/trn-"+cn+".html");
	if(!htm.open(QIODevice::WriteOnly|QIODevice::Truncate)){
	   	qDebug("Error: cannot create HTML file for transaction %s.",cn.toLatin1().data());
	   	emit errorFound();
	   	return;
	}
	//basics
	QStringList in=trn.inputNames();
	QStringList out=trn.outputNames();
	QStringList doc=trn.docStrings();
	//lead in
	QString hcd;
	hcd="<html><title>Transaction "+cn+"</title><body>\n<h1>Transaction "+cn+"</h1>\n";
	//auth mode
	hcd+="<p>Authentication mode: ";
	switch(trn.authMode()){
		case WocTransaction::Checked:hcd+="Checked (known user, must have the privilege)";break;
		case WocTransaction::Auth:hcd+="Authenticated (known user, any/no privileges)";break;
		case WocTransaction::Open:hcd+="Open (unauthenticated, any user)";break;
		default:hcd+="Ooops. Unknown Mode.";break;
	}
	hcd+="</p>\n";
	hcd+="<p>Database access mode: "+QString(trn.isDbUpdating()?"updating":"reading")+"</p>\n";
	//docu
	for(int i=0;i<doc.size();i++)
		hcd+="<p>"+doc[i]+"</p>\n";
	//in/out
	hcd+="<h2>Inputs:</h2>\n<ul>\n";
	for(int i=0;i<in.size();i++){
		hcd+="<li>"+in[i]+": ";
		QString t=trn.inputType(in[i]);
		if(trn.isObjectType(t))
			hcd+="<a href=\"class-"+trn.plainType(t)+".html\">";
		hcd+=t;
		if(trn.isObjectType(t))
			hcd+="</a>";
		//add docu
		t=trn.inputDoc(in[i]);
		if(t!="")hcd+="<br/>"+t;
		hcd+="</li>\n";
	}
	hcd+="</ul>\n<h2>Outputs:</h2>\n<ul>\n";
	for(int i=0;i<out.size();i++){
		hcd+="<li>"+out[i]+": ";
		QString t=trn.outputType(out[i]);
		if(trn.isObjectType(t))
			hcd+="<a href=\"class-"+trn.plainType(t)+".html\">";
		hcd+=t;
		if(trn.isObjectType(t))
			hcd+="</a>";
		//add docu
		t=trn.outputDoc(out[i]);
		if(t!="")hcd+="<br/>"+t;
		hcd+="</li>\n";
	}
	hcd+="</ul>\n";
	//privileges
	QStringList pri=trn.privileges();
	if(pri.size()){
		hcd+="<h2>Privileges</h2>\n<ul>\n";
		for(int i=0;i<pri.size();i++){
			hcd+="<li>"+pri[i];
			QString d=trn.privilegeDoc(pri[i]);
			if(d!="")
				hcd+="<br/>"+d;
			hcd+="</li>\n";
		}
		hcd+="</ul>\n";
	}
	hcd+="</body></html>\n";
	htm.write(hcd.toLatin1());
}
