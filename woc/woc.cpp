// Copyright (C) 2009-2011 by Konrad Rosenbaum <konrad@silmor.de>
// protected under the GNU GPL version 3 or at your option any newer.
// See COPYING.GPL file that comes with this distribution.
//

/** \mainpage

Woc is the central pre-processor that turns WOLF-Files into source code. It is organized in a hierarchy of classes: a frontend parser that reads in WOLF files and several backends that generate the target code.

The front-end WOLF parser is in the "proc" directory. Its main class is WocProcessor.

The HTML back-end is located in the WocHtmlOut class.

The SOAP back-end is in the "soap" directory with the WocSoapOut class as the main output class.

The Qt back-end is in the "qt" directory with WocQtOut, WocQtClientOut, and WocQtServerOut as the main output classes.

The PHP back-end is in the "php" directory with WocPHPOut, WocPHPClientOut, and WocPHPServerOut as the main output classes.

The generic pattern based back-end is in the "generic" directory with default patterns in the "pattern directory.
*/

#include <QCoreApplication>
#include <QStringList>
#include <QDebug>

#include "processor.h"
#include "genproc.h"

int main(int argc,char**argv)
{
	QCoreApplication app(argc,argv);
	//Initializations
	InitializeGeneric();
	//get arguments
	QStringList args=app.arguments();
	args.removeFirst();
	//process files
	WocProcessor proc;
	for(QString arg:args)
		if(arg.startsWith("-pattern="))
			InitializeGeneric(arg.mid(9));
		else
		if(!proc.processFile(arg)){
			qDebug()<<"Error: invalid argument"<<arg<<"- aborting scan.";
			return 1;
		}
	//call finalizer
	proc.finalize();
	qDebug("done.");
	//return success
	return 0;
}
