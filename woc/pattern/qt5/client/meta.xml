<?xml version="1.0" encoding="utf-8" ?>
<!-- Main definition:
    lang: programming language and variant
    tag: XML tag used in WOLF files -->
<Meta lang="qt/client" tag="Qt5ClientOutput">
    
    <!-- include other files with common definitions -->
    <!-- Include file="../meta-qt-common.xml" / -->
    
    <!-- Syntax definitions for pattern files:
        comment: lines that are discarded by the parser
        section-start: lines that start a new section of the pattern file
        variable-marker: start and end marker of variables in pattern files, default is { and }
    
        note: patterns in this file always use { and } for variables, regardless of the variable-marker attribute
    -->
    <Syntax comment="//==" section-start="//--" variable-marker="${ }" />
        
    <!-- Features are configurable in WOLF files in the XML tag for this output
        name: the XML attribute name for the feature
        type: string or boolean for the feature
        default: default value if the attribute is missing
    -->
    <Feature name="allClassPrefix" type="string" default="C" />
    <Feature name="classPrefix" type="string" default="Obj" />
    <Feature name="transactionPrefix" type="string" default="Trn" />
    <Feature name="interfaceName" type="string" default="Interface" />
    <Feature name="enableExport" type="boolean" default="yes" />

    <!-- files that are being generated through this definition:
        type: the trigger for creating and closing a file, e.g. type="class" means the file is generated on class.new and closed on class.close
        tag: identifying name for the file inside the generator - the combination of type and tag must be unique among files
        name: pattern for the file name that is being generated
        pattern: name of the pattern file to be interpreted
    
        A File rule generates at maximum one file at the same time, but it may generate several files consecutively. For this
        the name attribute must use variables to generate different file names.
    -->
    <File type="class" tag="include" name="{class.classname}" pattern="class-include"/>
    <File type="class" tag="header" name="src{class.classname}.h" pattern="class.h"/>
    <File type="class" tag="source" name="src{class.classname}.cpp" pattern="class.cpp"/>
    <File type="transaction" tag="include" name="{transaction.classname}" pattern="transaction-include"/>
    <!-- file transaction header src*.h
    file transaction source src*.cpp -->
    <!-- file interface source src*.cpp -->
    <File type="interface" tag="header" name="src{interface.classname}.h" pattern="interface.h"/>
    <File type="interface" tag="include" name="{allClassPrefix}Interface" pattern="interface-include"/>
    <File type="version" tag="header" name="staticVersion.h" pattern="staticVersion.h"/>
    <File type="project" tag="pri" name="{project.wobDir}.pri" pattern="wob.pri" />
    <File type="project" tag="includeAll" name="{allClassPrefix}IncludeAll" pattern="includeAll.h"/>

    <!-- type mappings, class/transaction and db to C++/Qt -->
    <!-- simple class and transaction types -->
    <Type config="string" map="QString"/>
    <Type config="astring" map="QString"/>
    <Type config="text" map="QString"/>
    <Type config="int" map="qint64"/>
    <Type config="int32" map="qint32"/>
    <Type config="int64" map="qint64"/>
    <Type config="bool" map="bool"/>
    <!-- DB types (string with length: use * as a pattern placeholder) -->
    <Type config="seq" map="qint64"/>
    <Type config="seq32" map="qint32"/>
    <Type config="seq64" map="qint64"/>
    <Type config="enum" map="qint64"/>
    <Type config="enum32" map="qint32"/>
    <Type config="enum64" map="qint64"/>
    <Type config="blob" map="QByteArray"/>
    <Type config="string:*" map="QString"/>
    <!-- special class/transaction types: 
    List: the WOLF type behind "List:" is sent through the mapper recursively and * in the map pattern is replaced with this result
    Object/Enum: * will be replaced with the original WOLF type name, you can use complete expressions -->
    <Type config="List:*" map="QList&lt;*>"/>
    <Type config="Object" map="{allClassPrefix}{classPrefix}*"/>
    <Type config="Enum" map="*"/>
    
    <!-- pre-calculated variables
        variables are calculated before any files are generated or patterns are applied
    
        trigger: trigger on which the calculation is executed
        deleteOn: trigger on which the variables are deleted again
    
        name: name of the variable
    
        value: value for the variable, may contain {var-references} itself
         or
        if: boolean expression, & and | are allowed as boolean operators, currently no parentheses
        valueTrue: value of the new variable if the expression evaluates to true
        valueFalse: value of the new variable if the expression evaluates to false
    -->
    <Precalc trigger="project.new">
        <Variable name="exportmacro" value="WOBGEN_{project.name|toUpper}__WOB_EXPORT"/>
        <Variable name="exportSymbol" if="{enableExport}" valueTrue="Q_DECL_IMPORT" valueFalse=""/>
        <Variable name="defineExportMacro" value="#ifndef {exportmacro}&#x0a;#define {exportmacro} {exportSymbol}&#x0a;#endif" />
    </Precalc>

    <!-- recalculation of variables on triggers -->
    <Precalc trigger="class.new" deleteOn="class.close">
        <Variable name="class.classnamenoabstract" value="{allClassPrefix}{classPrefix}{class.name}"/>
        <Variable name="class.classname" value="{class.classnamenoabstract}{class.abstract|if(Abstract)}"/>
        <Variable name="class.baseclassname" value="{allClassPrefix}{classPrefix}{class.base}"/>
        <Variable name="includeGuard" value="WOBGEN_{class.classname}"/>
    </Precalc>
    <Precalc trigger="transaction.new" deleteOn="transaction.close">
        <Variable name="transaction.classname" value="{allClassPrefix}{transactionPrefix}{transaction.name}"/>
        <Variable name="includeGuard" value="WOBGEN_{transaction.classname}"/>
    </Precalc>
    <Precalc trigger="interface.new" deleteOn="interface.close">
        <Variable name="interface.classname" value="{allClassPrefix}{interfaceName}"/>
        <Variable name="includeGuard" value="WOBGEN_{interface.classname|toUpper}_H"/>
    </Precalc>
    
</Meta>
