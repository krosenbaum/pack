// Copyright (C) 2009-2011 by Konrad Rosenbaum <konrad@silmor.de>
// protected under the GNU GPL version 3 or at your option any newer.
// See COPYING.GPL file that comes with this distribution.
//

#ifndef WOC_PROCESSOR_TRANS_H
#define WOC_PROCESSOR_TRANS_H

#include <QList>
#include <QMap>
#include <QObject>
#include <QPair>
#include <QStringList>

/**internal representation of a transaction*/
class WocTransaction
{
	public:
		/**initializes a transaction from XML*/
		WocTransaction(const QDomElement&);
		/**returns whether parsing it was successful*/
		bool isValid()const{return m_valid;}

		/**returns the name of the transaction*/
		QString name()const{return m_name;}

		/**returns whether an input variable exists*/
		bool hasInput(QString v)const;
		/**returns the names of all inputs in the order of definition*/
		QStringList inputNames()const;
		/**returns the type of an input variable*/
		QString inputType(QString)const;

		/**returns whether an output variable exists*/
		bool hasOutput(QString v)const;
		/**returns the names of all outputs in the order of definition*/
		QStringList outputNames()const;
		/**returns the type of an output variable*/
		QString outputType(QString)const;

		/**returns whether a specific language binding exists for a call*/
		bool hasCall(QString c)const{return m_call.contains(c);}
		/**returns the called function*/
		QString callFunction(QString c)const{return m_call[c].first;}
		/**returns the include file of the called function*/
		QString callInclude(QString c)const{return m_call[c].second.trimmed();}

		/**authentication mode*/
		enum AuthMode {
			/**default: need a valid session and the privilege*/
			Checked,
			/**only need to be authenticated, every valid user can do it*/
			Auth,
			/**available even to anonymous/unauthenticated users*/
			Open
		};
		AuthMode authMode()const{return m_mode;}

		///Log Mode: signals what kind of logging is forbidden
		enum NoLogMode {
                        ///default: all logging is allowed
                        LogAll=0,
                        ///logging the request is forbidden
                        NoLogRequest=1,
                        ///logging the response is forbidden
                        NoLogResponse=2,
                        ///logging any data is forbidden
                        NoLogAny=3
                };
                NoLogMode logMode()const{return m_logmode;}

		/**returns true if the type given is a list*/
		static bool isListType(QString t){return t.startsWith("List:");}
		/**returns the type without list or xml qualifiers*/
		static QString plainType(QString t){
			if(t.startsWith("List:"))t=t.mid(5);
			QStringList l=t.split("/",Qt::SkipEmptyParts);
			if(l.size()>0)t=l[0];else t="unknown";
			return t;
		}
		/**returns the XML serializer for Object types*/
		QString typeSerializer(QString t)const{
			QStringList l=t.split("/",Qt::SkipEmptyParts);
			if(l.size()>1)return l[1];
			else return "";
		}
		/**returns true if the type is integer*/
		static bool isIntType(QString t){QString pt=plainType(t);return pt=="int" || pt=="int32" || pt=="int64";}
		/**returns true if the type is boolean*/
		static bool isBoolType(QString t){return plainType(t)=="bool";}
		/**returns true if the type is a string*/
		static bool isStringType(QString t){QString p=plainType(t);return p=="astring"||p=="string";}
		/**returns true if the type is a blob*/
		static bool isBlobType(QString t){QString p=plainType(t);return p=="blob";}
		/**returns true if the type is to be encoded as attribute*/
		static bool isAttributeType(QString t){return t=="astring"||t=="int"||t=="int32"||t=="int64"||t=="bool";}
		/**returns true if the type is to be encoded as element*/
		static bool isElementType(QString t){return !isAttributeType(t);}
		/**return true if the type is an object type*/
		static bool isObjectType(QString t){QString p=plainType(t);return p!="astring"&&p!="string"&&p!="int"&&p!="int32"&&p!="int64"&&p!="bool"&&p!="blob";}

		/**return the documentation of the transaction*/
		QStringList docStrings()const{return m_docstrings;}
		/**return docu of input element*/
		QString inputDoc(QString v)const
		{if(m_indoc.contains(v))return m_indoc[v];else return "";}
		/**return docu of output element*/
		QString outputDoc(QString v)const
		{if(m_outdoc.contains(v))return m_outdoc[v];else return "";}
		/**return docu of a privilege*/
		QString privilegeDoc(QString p)const
		{if(m_privdoc.contains(p))return m_privdoc[p];else return "";}

		/**return privileges that exist inside this transaction*/
		QStringList privileges()const{return m_privileges;}

		/**returns whether this transaction is considered to update the database*/
		bool isDbUpdating()const{return m_update;}
	private:
		QString m_name;
		bool m_valid,m_update;
		AuthMode m_mode;
                NoLogMode m_logmode;
		QMap<QString,QPair<QString,QString> > m_call;
		QList<QPair<QString,QString> >m_input,m_output;
		QStringList m_privileges;
		//docu
		QStringList m_docstrings;
		QMap<QString,QString>m_indoc,m_outdoc,m_privdoc;
};

#endif
