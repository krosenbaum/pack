// Copyright (C) 2009-2011 by Konrad Rosenbaum <konrad@silmor.de>
// protected under the GNU GPL version 3 or at your option any newer.
// See COPYING.GPL file that comes with this distribution.
//

#include "processor.h"

#include "qtout.h"
#ifndef COMPILE_PREWOC
#include "phpout.h"
#include "htmlout.h"
#include "schemaout.h"
#include "soapout.h"
#endif
#include "genproc.h"

#include "domquery.h"

#include <QCoreApplication>
#include <QDateTime>
#include <QDebug>
#include <QDir>
#include <QDomDocument>
#include <QDomElement>
#include <QProcess>
#include <QUrl>

#include <stdlib.h>
#include <algorithm>

QList<QDomElement>elementsByTagName(const QDomElement&root,QString tag)
{
	QDomNodeList cn=root.childNodes();
	QList<QDomElement>ret;
	for(int i=0;i<cn.size();i++){
		QDomElement e=cn.at(i).toElement();
		if(e.isNull())continue;
		if(e.tagName()!=tag)continue;
		ret<<e;
	}
	return ret;
}


WocProcessor* WocProcessor::inst=0;

WocProcessor::WocProcessor()
{
	m_baseDir=QDir(".").absolutePath();
	m_wobDir=".";
	m_verComm="0";
	m_verNeedComm="0";
	m_verHR="0.0";
	m_svnExe="svn";
	m_gitExe="git";
	m_verSys=QStringList()<<"none";
	m_dbInst="dbInst";
	m_dbSchema="dbSchema";
	m_dbUpd=false;
	m_error=false;
	m_projname="WobProject";
	m_encmode=WobEncoding;
	m_auth=UnknownAuth;
	parseNamespaces();

	inst=this;
}

bool WocProcessor::processFile(QString fn)
{
	//open file
	QFile fd(fn);
	if(!fd.open(QIODevice::ReadOnly)){
		qDebug("Error: cannot read file %s",fn.toLocal8Bit().data());
		return false;
	}
	//get XML content
	QDomDocument doc;
	QString err;int eln,ecl;
	if(!doc.setContent(&fd,&err,&eln,&ecl)){
		qDebug("Error: XML parser error in file %s line %i column %i: %s",fn.toLocal8Bit().data(),eln,ecl,err.toLocal8Bit().data());
		fd.close();
		return false;
	}
	fd.close();
	QDomElement root=doc.documentElement();
	if(root.isNull() || root.tagName()!="Wolf"){
		qDebug("Error: XML File %s is not a Wolf.",fn.toLocal8Bit().data());
		return false;
	}
	//go on, process file
	qDebug("Info: processing file %s...",fn.toLocal8Bit().data());
	m_error=false;
	QDomNodeList nl=root.childNodes();
	for(int i=0;i<nl.size();i++){
		if(!nl.at(i).isElement())continue;
		QDomElement el=nl.at(i).toElement();
		if(el.isNull())continue;
		QString tn=el.tagName();
		if(tn=="Class"){
			WocClass cls(el);
			if(cls.isValid()){
				m_classes.append(cls);
				emit newClass(cls);
				if(m_error)return false;
			}else
				return false;
		}else
		if(tn=="Table"){
			WocTable tbl(el);
			if(tbl.isValid()){
				m_tables.append(tbl);
				emit newTable(tbl);
				if(m_error)return false;
				if(tbl.isAuditable()){
					WocTable atbl=tbl.auditTable();
					m_tables.append(atbl);
					emit newTable(atbl);
					if(m_error)return false;
				}
			}else
				return false;
		}else
		if(tn=="Transaction"){
			WocTransaction trn(el);
			if(trn.isValid()){
				m_transactions.append(trn);
				emit newTransaction(trn);
				if(m_error)return false;
			}else
				return false;
		}else
		if(tn=="Project"){
			if(el.hasAttribute("baseDir"))
				m_baseDir=el.attribute("baseDir");
			if(m_baseDir=="::woc::"){
				m_baseDir=QCoreApplication::applicationDirPath();
				qDebug()<<"Info: special value '::woc::' given as base dir, correcting it to"<<m_baseDir;
			}
			if(el.hasAttribute("wobDir"))
				m_wobDir=el.attribute("wobDir");
			if(el.hasAttribute("name"))
				m_projname=el.attribute("name");
			m_xmlNS=el.attribute("xml-namespace");
			QString enc=el.attribute("encoding","wob").toLower();
			if(enc=="soap"||enc=="soap12")m_encmode=Soap12Encoding;
			else m_encmode=WobEncoding;
			if(m_xmlNS=="")m_xmlNS="ns:"+QUrl::toPercentEncoding(m_projname);
			m_auth=str2AuthMode(el.attribute("auth","none"));
			if(m_auth==UnknownAuth){
				errorFound();
				qDebug("Error: unknown authentication mode %s", el.attribute("auth").toLatin1().data());
				return false;
			}
		}else
		if(tn=="Include"){
			if(!processFile(m_baseDir+"/"+m_wobDir+"/"+el.attribute("file")))
				return false;
		}else
		if(tn=="DataBase"){
			if(el.hasAttribute("instance"))
				m_dbInst=el.attribute("instance","dbInst");
			if(el.hasAttribute("schema"))
				m_dbSchema=el.attribute("schema","dbSchema");
			if(el.hasAttribute("version"))
				m_dbVer=el.attribute("version");
			if(el.hasAttribute("defaultUpdating"))
				m_dbUpd=str2bool(el.attribute("defaultUpdating"));
			if(el.hasAttribute("configTable"))
				m_dbConfigTable=el.attribute(("configTable"));
			if(el.hasAttribute("configKeyColumn"))
				m_dbConfigKey=el.attribute(("configKeyColumn"));
			if(el.hasAttribute("configValueColumn"))
				m_dbConfigVal=el.attribute(("configValueColumn"));
			if(el.hasAttribute("versionRow"))
				m_dbVersionRow=el.attribute(("versionRow"));
			QList<QDomElement> nl=elementsByTagName(el,"AuditTables");
			for(int i=0;i<nl.size();i++)
				WocTable::parseAuditStatic(nl.at(i).toElement());
		}else
		if(tn=="QtClientOutput"){
			new WocQtClientOut(el);
			if(m_error)return false;
		}else
#ifndef COMPILE_PREWOC
		if(tn=="QtServerOutput"){
			new WocQtServerOut(el);
			if(m_error)return false;
		}else
		if(tn=="PHPClientOutput"){
			new WocPHPClientOut(el);
			if(m_error)return false;
		}else
		if(tn=="PHPServerOutput"){
			new WocPHPServerOut(el);
			if(m_error)return false;
		}else
		if(tn=="HtmlOutput"){
			new WocHtmlOut(el);
			if(m_error)return false;
		}else
		if(tn=="SchemaOutput"){
			new WocSchemaOut(el);
			if(m_error)return false;
		}else
		if(tn=="SoapOutput"){
			new WocSoapOut(el);
			if(m_error)return false;
		}else
#endif
		if(tn=="Version"){
			if(el.hasAttribute("system"))
				m_verSys=el.attribute("system").split(" ",Qt::SkipEmptyParts);
			if(el.hasAttribute("comm"))
				m_verComm=el.attribute("comm");
			if(el.hasAttribute("needcomm"))
				m_verNeedComm=el.attribute("needcomm");
			if(el.hasAttribute("humanReadable"))
				m_verHR=el.attribute("humanReadable");
			if(el.hasAttribute("svnTarget"))//obsolete
				m_verTarget=el.attribute("svnTarget");
			if(el.hasAttribute("target"))
				m_verTarget=el.attribute("target");
			if(el.hasAttribute("svnExe"))
				m_svnExe=el.attribute("svnExe");
			if(el.hasAttribute("gitExe"))
				m_gitExe=el.attribute("gitExe");
			bool succ=false;
			if(m_verSys.contains("svn")||m_verSys.contains("subversion"))
				succ=callSvn();
			if(!succ && m_verSys.contains("git"))
				succ=callGit();
			if(!succ && m_verSys.contains("none"))
				succ=callNoVer();
			if(!succ){
				qDebug()<<"Error: unsuccessful in gathering version info. Tried:"<<m_verSys;
				qDebug()<<"Info: try to add 'none' (adds dummy data if nothing else is available) to ensure it runs.";
				return false;
			}
			m_verInfo.insert("Comm",m_verComm);
			m_verInfo.insert("NeedComm",m_verNeedComm);
			m_verInfo.insert("HR",m_verHR);
			m_verInfo.insert("GenTime",QDateTime::currentDateTime().toString(Qt::ISODate));
		}else
		if(tn=="Doc"){
			QString s=el.text().trimmed();
			if(s!="")m_docstrings<<s;
		}else{
            if(!CreateGenericOutput(tn,el)){
                //completely unknown tag?
                qDebug("Error: file %s has unknown element '%s' at line %i column %i", fn.toLocal8Bit().data(), tn.toLocal8Bit().data(), el.lineNumber(), el.columnNumber());
                return false;
            }
            //error during initialization?
            if(m_error)return false;
		}
	}
	//TODO: verify classes
	//TODO: verify transactions

	//return success
	return true;
}

bool WocProcessor::callSvn()
{
	QString svnRev,svnUrl,svnRoot,svnMod,svnAuth,svnTime;
	//svn info
	QString svntgt=m_baseDir+"/"+m_verTarget;
	QProcess svn;
	svn.setProcessChannelMode(QProcess::MergedChannels);
	svn.setWorkingDirectory(svntgt);
	svn.start(m_svnExe,QStringList()<<"info"<<"-R"<<"--xml"<<"--non-interactive"<<".");
	svn.waitForFinished();
	if(svn.exitCode()!=0){
		qDebug("Warning: error while calling svn info.");
		return false;
	}else{
		QDomDocument doc;
		if(!doc.setContent(svn.readAllStandardOutput())){
			qDebug("Warning: unable to parse output of svn info.");
			return false;
		}
		//find last changed rev
		QStringList rl=MDomQuery(doc,"/*/entry/commit/@revision");
		int lrev=0;
		for(int i=0;i<rl.size();i++){
			int r=rl[i].toInt();
			if(r>lrev)lrev=r;
		}
		//parse it: revisions of updates
		rl=MDomQuery(doc,"/*/entry/@revision");
		int minv=-1,maxv=-1;
		QDateTime mint,maxt;
		if(rl.size()==0){
			svnRev="unknown";
		}else{
			minv=maxv=rl[0].toInt();
			for(int i=1;i<rl.size();i++){
				int cv=rl[i].toInt();
				if(cv<minv)minv=cv;
				if(cv>maxv)maxv=cv;
			}
			//compare to last change revision; what counts is last change
			if(maxv>lrev)maxv=lrev;
			if(minv>maxv)minv=maxv;
			//remember
			if(minv==maxv)
				svnRev=QString::number(maxv);
			else
				svnRev=QString::number(minv)+"-"+QString::number(maxv);
		}
		//get authors
		rl=MDomQuery(doc,"/*/entry/commit/author");
#if QT_VERSION_MAJOR > 5
        std::sort(rl.begin(),rl.end());
        rl.resize(std::unique(rl.begin(),rl.end())-rl.begin());
#else
		rl=rl.toSet().toList();//reduce
		qSort(rl);
#endif
		for(int i=0;i<rl.size();i++){
			if(i)svnAuth+=", ";
			svnAuth+=rl[i];
		}
		//get time of min and max versions
		MDomNodeList nl=MDomQuery(doc,"/*/entry/commit");
		for(int i=0;i<nl.size();i++){
			//check revision
			bool ok;
			int rev=nl[i].toElement().attribute("revision").toInt(&ok);
			if(ok && rev==minv)
				mint=QDateTime::fromString( MDomQuery(nl[i].toElement(), "date").toStringList().value(0), Qt::ISODate);
			if(ok && rev==maxv)
				maxt=QDateTime::fromString( MDomQuery(nl[i].toElement(), "date").toStringList().value(0), Qt::ISODate);
		}
		if(mint==maxt)svnTime=mint.toString();
		else{
			if(mint.isValid())svnTime=mint.toString();
			if(mint.isValid() && maxt.isValid())svnTime+=" -- ";
			if(maxt.isValid())svnTime+=maxt.toString();
		}
// 		qDebug()<<"Info: SVN found dates"<<mint<<maxt<<svnTime;
		//parse for repository path
		nl=MDomQuery(doc,"/*/entry");
		for(int i=0;i<nl.size();i++){
			QDomElement el=nl.at(i).toElement();
			if(el.isNull())continue;
			if(el.attribute("path")=="."){
				rl=MDomQuery(el,"url");
				if(rl.size()>0)svnUrl=rl[0];
				rl=MDomQuery(el,"repository/root");
				if(rl.size()>0)svnRoot=rl[0];
				break;//we can stop now
			}
		}
	}
	//svn status
	svn.start(m_svnExe,QStringList()<<"status"<<"-v"<<"--xml"<<"--non-interactive"<<".");
	svn.waitForFinished();
	if(svn.exitCode()!=0){
		qDebug("Warning: error while calling svn status.");
		svnMod="uncertain";
		return false;
	}else{
		QDomDocument doc;
		if(!doc.setContent(svn.readAllStandardOutput())){
			qDebug("Warning: unable to parse output of svn status.");
			svnMod="uncertain";
		}else{
			//get modified status
			QStringList rl=MDomQuery(doc,"/status/target/entry/wc-status/@item");
			bool ismod=false;
			for(int i=0;i<rl.size();i++){
				if(rl[i]=="unversioned" || rl[i]=="normal")continue;
				ismod=true;
				break;
			}
			if(ismod)svnMod="modified";
			else svnMod="unmodified";
		}
	}
	//remember info data
	m_verInfo.insert("System","svn");
	m_verInfo.insert("RootURL",svnRoot);
	m_verInfo.insert("Path",svnUrl.mid(svnRoot.size()));
	m_verInfo.insert("Number",svnRev);
	m_verInfo.insert("Time",svnTime);
	m_verInfo.insert("Author",svnAuth);
	m_verInfo.insert("LocallyModified",svnMod);
	qDebug()<<"Info: SVN version info:"<<m_verInfo;
	return true;
}
/// internal helper class for the procGit parser routine
class GP{
	public:
		GP(const QString&s){par<<s;}
		GP(const GP&g):par(g){}
		GP(){}
		GP&operator+=(const QString&s){par<<s;return *this;}
		GP&operator+=(const QStringList&s){par<<s;return *this;}

		const GP operator+(const QString&s)const{return GP(*this)+=s;}
		const GP operator+(const QStringList&s)const{return GP(*this)+=s;}
		const GP operator+(const GP&g)const{return GP(*this)+=g.par;}

		operator QStringList()const{return par;}
	private:
		QStringList par;
};
const GP gp;
static bool procGit(const QStringList&param,QStringList&ret,const QString&tgt,const QString&git)
{
	QProcess proc;
	proc.setProcessChannelMode(QProcess::MergedChannels);
	proc.setWorkingDirectory(tgt);
	proc.start(git,param);
	proc.waitForFinished();
	if(proc.exitCode()!=0){
		qDebug("Warning: error while calling git %s.",param.value(0).toLatin1().data());
		return false;
	}
	ret=QString::fromLocal8Bit(proc.readAllStandardOutput()).split('\n');
	return true;
}

bool WocProcessor::callGit()
{
	QString tgt=m_baseDir+"/"+m_verTarget;
	QStringList res;
	//parse  branch info  and get current branch
	QString lbranch,lrev;
	if(!procGit(gp+"branch"+"-v",res,tgt,m_gitExe))return false;
	foreach(QString line,res){
		line=line.trimmed();
		if(line.size()<3)continue;
		if(line[0]=='*'){
			QStringList lst=line.split(' ',Qt::SkipEmptyParts);
			if(lst.size()<3)continue;
			//check for detached checkout
			if(lst[1]=="(no"&&lst[2]=="branch)"){
				if(lst.size()>3)lrev=lst[3];
				break;
			}
			//remember
			lbranch=lst[1];
			lrev=lst[2];
			break;
		}
	}
	//low level version info
	if(!procGit(gp+"log"+"-1"+"--format=raw",res,tgt,m_gitExe))return false;
	QString rev,date;
	foreach(QString line,res){
		QStringList lst=line.split(" ",Qt::SkipEmptyParts);
		if(lst.size()<2)continue;
		if(lst[0]=="commit")rev=lst[1];else
		if(lst[0]=="author"){
			//this contains the author, mail and commit time
			if(lst.size()<4)continue;
			//get the time
			QString tz=lst[lst.size()-1];
			date=lst[lst.size()-2];
            QDateTime tm=QDateTime::fromSecsSinceEpoch(date.toLongLong()).toUTC();
			if(tz.size()==5){
				int f=0;
				if(tz[0]=='+')f=1;else
				if(tz[0]=='-')f=-1;
				tm=tm.addSecs(tz.mid(1,2).toInt(0,10)*3600*f+
					tz.mid(3,2).toInt(0,10)*60*f);
			}else tz="";
			date=tm.toString("yyyy-MM-dd hh:mm:ss ")+tz;
			m_verInfo.insert("Time",date);
			//get all components of the author
			// remove "author" and time stamp
			lst.takeFirst();lst.takeLast();lst.takeLast();
			// reassemble
			QString auth;
			foreach(QString s,lst)auth+=s+" ";
			m_verInfo.insert("Author",auth.trimmed());
		}
	}
	if(rev.isEmpty())rev=lrev;
	//get remote info
	QString remote,remurl;
	if(!lbranch.isEmpty() && procGit(gp+"config"+"-l",res,tgt,m_gitExe)){
		//find the remote of the local branch
		QString key="branch."+lbranch+".remote";
		foreach(QString line,res){
			QStringList lst=line.trimmed().split("=");
			if(lst.size()!=2)continue;
			if(lst[0]==key){
				remote=lst[1];
				break;
			}
		}
		key="remote."+remote+".url";
		if(!remote.isEmpty())
		foreach(QString line,res){
			QStringList lst=line.trimmed().split("=");
			if(lst.size()!=2)continue;
			if(lst[0]==key){
				remurl=lst[1];
				break;
			}
		}
		if(!remurl.isEmpty())
			m_verInfo.insert("RootURL",remurl);
	}
	//find path
	QDir tdir(QDir(tgt).absolutePath());
	QString path;
	do{
		if(tdir.entryList(QDir::Dirs|QDir::AllDirs|QDir::Hidden|QDir::System).contains(".git")){
			break;
		}else{
			path=tdir.dirName()+(path.isEmpty()?"":"/"+path);
		}
	}while(tdir.cdUp());
	//fallback URL for non-tracking branches
	if(remurl.isEmpty()){
		m_verInfo.insert("RootURL",QUrl::fromLocalFile(tdir.absolutePath()).toString());
	}
	//modification state
	if(!procGit(gp+"status"+"-s"+".",res,tgt,m_gitExe))return false;
	int mctr=0;
	foreach(QString line,res){
		if(!line.trimmed().isEmpty())mctr++;
	}
	//done: add to version info
	m_verInfo.insert("System","git");
	m_verInfo.insert("Path",path);
	m_verInfo.insert("Number",rev);
	m_verInfo.insert("LocallyModified",mctr?"modified":"unmodified");
	return true;
}

static inline QString getLocalUser()
{
	//try Unix variable
        char*s=getenv("USER");
        if(s)return QString(s);
        //try Windows next
        s=getenv("USERNAME");
	if(s)return QString(s);
	//nothing
	return QString();
}

bool WocProcessor::callNoVer()
{
	qDebug()<<"Warning: using the no-version-system fallback, generating dummy infos.";
	m_verInfo.insert("System","none");
	m_verInfo.insert("LocallyModified","local");
	m_verInfo.insert("RootURL","file://");
	m_verInfo.insert("Path",QDir(m_baseDir+"/"+m_verTarget).absolutePath());
	m_verInfo.insert("Time",QDateTime::currentDateTime().toString());
	m_verInfo.insert("Author",getLocalUser());
	return true;
}

void WocProcessor::finalize()
{
	emit sfinalize();
}

bool WocProcessor::hasTable(QString n)const
{
	for(int i=0;i<m_tables.size();i++)
		if(m_tables[i].name()==n)return true;
	return false;
}

WocTable WocProcessor::table(QString n)const
{
	for(int i=0;i<m_tables.size();i++)
		if(m_tables[i].name()==n)return m_tables[i];
	return WocTable();
}

bool WocProcessor::hasClass(QString n)const
{
	for(int i=0;i<m_classes.size();i++)
		if(m_classes[i].name()==n)return true;
	return false;
}

void WocProcessor::errorFound()
{
	m_error=true;
}

QStringList WocProcessor::transactionNames()const
{
	QStringList l;
	for(int i=0;i<m_transactions.size();i++)
		l<<m_transactions[i].name();
	return l;
}

QStringList WocProcessor::classNames()const
{
	QStringList l;
	for(int i=0;i<m_classes.size();i++)
		l<<m_classes[i].name();
	return l;
}

QStringList WocProcessor::tableNames()const
{
	QStringList l;
	for(int i=0;i<m_tables.size();i++)
		l<<m_tables[i].name();
	return l;
}

QStringList WocProcessor::privilegeNames()const
{
	QStringList l;
	for(int i=0;i<m_transactions.size();i++){
		QString cn=m_transactions[i].name();
		QStringList priv=m_transactions[i].privileges();
		for(int j=0;j<priv.size();j++)
			l<<(cn+":"+priv[j]);
	}
	return l;
}

static inline QString getXmlNamespace(QString f)
{
	QFile fd(f);
	if(!fd.open(QIODevice::ReadOnly))
		return QString();
	QDomDocument doc;
	if(!doc.setContent(&fd))
		return QString();
	QDomElement root=doc.documentElement();
	return root.attribute("targetNamespace");
}

void WocProcessor::parseNamespaces()
{
	m_xmlPackNS=getXmlNamespace(":/wob-base.xsd");
	m_xmlSchemaNS=getXmlNamespace(":/schema.xsd");
	m_xmlSoap12NS=getXmlNamespace(":/soap12.xsd");
	m_xmlXmlNS=getXmlNamespace(":/xml.xsd");
}

WocProcessor::AuthMode WocProcessor::str2AuthMode(QString s)
{
	if(s=="none")return NoAuth;
	if(s=="session")return SessionAuth;
	if(s=="basic")return BasicAuth;
	return UnknownAuth;
}


/******************************************************************************
 * WocOutput
 ******************************************************************************/

WocOutput::WocOutput()
{
	connect(WocProcessor::instance(),SIGNAL(sfinalize()),this,SLOT(finalize()));
	connect(WocProcessor::instance(),SIGNAL(newClass(const WocClass&)),this,SLOT(newClass(const WocClass&)));
	connect(WocProcessor::instance(),SIGNAL(newTable(const WocTable&)),this,SLOT(newTable(const WocTable&)));
	connect(WocProcessor::instance(),SIGNAL(newTransaction(const WocTransaction&)),this,SLOT(newTransaction(const WocTransaction&)));
	connect(this,SIGNAL(errorFound()),WocProcessor::instance(),SLOT(errorFound()));
}

WocOutput::~WocOutput(){}
