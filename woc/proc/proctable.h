// Copyright (C) 2009-2018 by Konrad Rosenbaum <konrad@silmor.de>
// protected under the GNU GPL version 3 or at your option any newer.
// See COPYING.GPL file that comes with this distribution.
//

#ifndef WOC_PROCESSOR_TABLE_H
#define WOC_PROCESSOR_TABLE_H

#include <QList>
#include <QMap>
#include <QObject>
#include <QPair>
#include <QStringList>

class QDomElement;
class WocEnum;

/**stores the internal representation of a database table and its abstraction class*/
class WocTable
{
	public:
		/**initializes an invalid table*/
		WocTable();
		/**initializes a table from XML*/
		WocTable(const QDomElement&);
		
		/**returns whether this instance is valid, ie. whether parsing was successful*/
		bool isValid()const{return m_valid;}
		
		/**returns the table name*/
		QString name()const{return m_name;}
		/**returns the parent class of the table class - default: WobTable*/
		QString baseClass()const{return m_base;}
		
		/**returns whether the table is marked for backup*/
		bool inBackup()const{return m_backup;}
		///returns which column the table uses for backup grouping
		QString backupKey()const{return m_backupkey;}
		///returns the default group size for backup or <=0 if default is to be used
		int backupGroupSize()const{return m_backupsize;}

		/**returns whether the table has a column with this name*/
		bool hasColumn(QString)const;
		/**returns a list of all defined column names*/
		QStringList columns()const;
		/**returns the list of all primary key columns*/
		QStringList primaryColumns()const;
		/**returns the data type of the column*/
		QString columnType(QString)const;
		/**returns whether the column allows NULLs*/
		bool columnIsNull(QString)const;
		/**returns whether the column is part of the primary key*/
		bool columnIsPrimary(QString)const;
		/**returns whether the column has a default*/
		bool columnHasDefault(QString)const;
		/**returns the default value of the column (empty string if there is none)*/
		QString columnDefault(QString)const;
		/**returns whether the column is a foreign key*/
		bool columnIsForeign(QString)const;
		/**returns the foreign key reference of the column in the format table:column*/
		QString columnForeign(QString)const;
		/**returns whether the column has an index*/
		bool columnIsIndexed(QString)const;
		/**returns whether the column has a unique constraint*/
		bool columnIsUnique(QString)const;
		/**returns enum definitions of the column - each pair contains the symbolic name in first and the assigned integer value in second*/
		QList<WocEnum> columnEnums(QString)const;
		/**returns the insert call of a column for a specific language; empty string if there is none*/
		QString columnCall(QString col,QString lang)const;
        ///returns true if the column name is for an audit column
        bool columnIsAudit(QString col)const{return col=="auditid" || auditColumns().contains(col);}
        ///returns true if the column has a string type
        bool columnIsString(QString col)const{const QString t=columnType(col);return t=="string"||t=="text"||t.startsWith("string:");}
        ///returns true if the column has an int type
        bool columnIsInt(QString col)const{const QString t=columnType(col);return t.startsWith("int")||t.startsWith("seq")||t.startsWith("enum");}
        ///returns true if the column has a blob type
        bool columnIsBlob(QString col)const{return columnType(col)=="blob";}
        
		/**returns all enum definitions of the table; see also columnEnums */
		QList<WocEnum> getEnums()const;
		
		/**returns a list of all foreign definitions - methods that return data from other tables*/
		QStringList foreigns()const;
		/**returns the definition of a specific foreign table query method*/
		QString foreignQuery(QString)const;
		/**returns whether a foreign table query method exists*/
		bool haveForeign(QString)const;
		
		/**returns a list of all preset values (to be generated when the DB is created);
		each entry in the list is a dictionary with the column name as key and the intended preset value as value - each entry of the list is one DB row, each key-value-pair in the map is one preset value in that row
		\obsolete This method is obsolete, use presetList() */
		QT_DEPRECATED QList<QMap<QString,QString> > presets()const{return m_presets;}
		///Preset value
		struct Preset {
            QString column,value,call;
            Preset(){}
            Preset(QString col,QString val,QString ca):column(col),value(val),call(ca){}
            Preset(const Preset&)=default;
            Preset(Preset&&)=default;
            Preset& operator=(const Preset&)=default;
            Preset& operator=(Preset&&)=default;
        };
		/**returns a list of all preset values (to be generated when the DB is created);
        each entry in the list is a dictionary with the column name as key and the intended preset value as value - each entry of the list is one DB row, each key-value-pair in the map is one preset value in that row*/
        QList<QMap<QString,Preset>> presetList()const{return m_presets2;}
		
		/**parses the static part of auditing*/
		static void parseAuditStatic(const QDomElement&);
		/**returns whether the table is auditable*/
		bool isAuditable()const{return m_audit;}
		/**creates and returns the table instance that represents the audit table*/
		WocTable auditTable()const;
		/**returns the names of audit columns (except auditid)*/
		QStringList auditColumns()const;
        ///returns the name of the corresponding audit table
        QString auditTableName()const{if(m_audit)return m_name+"_audit";else return QString();}

		///returns all complex Unique constraints (those not defined for a single column)
		QStringList uniqueConstraints()const{return m_uniquecols;}
		
		/**returns table documentation*/
		QStringList docStrings()const{return m_docstrings;}
		/**returns column documentation*/
		QString columnDoc(QString c)const;
		/**returns foreign getter documentation*/
		QString foreignDoc(QString c)const
		{if(m_fordocs.contains(c))return m_fordocs[c];else return "";}
		
	private:
		bool m_valid,m_backup,m_audit;
		QString m_name,m_base,m_backupkey;
		///holds data for a DB table column
		struct s_col {
			QString name,type,foreign,defaultval,doc;
			bool isnull,isprime,isindex,isunique;
			QList<WocEnum>enumvals;
			QMap<QString,QString>methodcalls;
		};
		QList<s_col>m_columns,m_auditcolumns;
		static QList<s_col>m_staticauditcolumns;
		QList<QPair<QString,QString> >m_foreign;
		QList<QMap<QString,QString> >m_presets;
        QList<QMap<QString,Preset>> m_presets2;
		int m_backupsize=-1;
		
		QStringList m_docstrings,m_uniquecols;
		QMap<QString,QString>m_fordocs;
		
		//helper method: parses a single column element
		static QPair<bool,s_col> parseColumn(const QDomElement&,QString);
};


#endif
