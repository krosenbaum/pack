// Copyright (C) 2009-2011 by Konrad Rosenbaum <konrad@silmor.de>
// protected under the GNU GPL version 3 or at your option any newer.
// See COPYING.GPL file that comes with this distribution.
//

#ifndef WOC_PROCESSOR_H
#define WOC_PROCESSOR_H

#include <QList>
#include <QMap>
#include <QObject>
#include <QPair>
#include <QStringList>

class QDomElement;

QList<QDomElement>elementsByTagName(const QDomElement&,QString);

inline bool str2bool(QString s)
{
	bool b;
	int i=s.toInt(&b,0);
	if(b)return i?true:false;
	s=s.toLower();
	if(s=="yes"||s=="y"||s=="on"||s=="true"||s=="t")return true;
	return false;
}

#include "procclass.h"
#include "proctable.h"
#include "proctrans.h"


/**base class of all output generators*/
class WocOutput:public QObject
{
	Q_OBJECT
	public:
		/**the constructor should set up and initialize the environment of the generator*/
		WocOutput();
		/**currently there is no guarantee that the destructor is ever called.*/
		virtual ~WocOutput();
		
	protected slots:
		/**called whenever the parser finds a new class in the XML input; some references might not exist yet*/
		virtual void newClass(const WocClass&)=0;
		/**called whenever the parser finds a new table in the XML input; the parser guarantees that tables it depends on already exist (it will throw an error otherwise)*/
		virtual void newTable(const WocTable&)=0;
		/**called whenever the parser finds a new transaction; the parser guarantees that all referenced types exist (it will throw an error otherwise)*/
		virtual void newTransaction(const WocTransaction&)=0;
		/**called when the parsing is complete: it should clean up the generators environment; it may not be called if woc stops with an error*/
		virtual void finalize()=0;
	signals:
		void errorFound();
};

/**central processing singleton*/
class WocProcessor:public QObject
{
	Q_OBJECT
	public:
		WocProcessor();
		
		/**called from main loop to parse a file*/
		bool processFile(QString);
		/**called from main loop to finalize its work*/
		void finalize();
		
		/**returns the instance of the processor (if it exists yet)*/
		static WocProcessor* instance(){return inst;}
		
		/**returns the base directory of the project (all other pathes are relative to it)*/
		QString baseDir()const{return m_baseDir;}
		/**returns the directory where WOLFs are found, should normally not be used outside this class*/
		QString wobDir()const{return m_wobDir;}
		/**returns the project name (default="WobProject")*/
		QString projectName()const{return m_projname;}
		/**returns the current communication protocol version*/
		QString verComm()const{return m_verComm;}
		/**returns the communication protocol version that is at least needed to be compatible*/
		QString verNeedComm()const{return m_verNeedComm;}
		/**returns a human readable version string*/
		QString verHR()const{return m_verHR;}
		/**returns version information about the project*/
		QMap<QString,QString> versionInfo()const{return m_verInfo;}
		/**returns the variable name that will contain the database driver instance*/
		QString dbInst()const{return m_dbInst;}
		/**returns the variable name that will contain the database schema object*/
		QString dbSchema()const{return m_dbSchema;}
		/**returns the database schema version*/
		QString dbVersion()const{return m_dbVer;}
		///returns the config table name
		QString dbConfigTable()const{return m_dbConfigTable;}
		///returns the config key column
		QString dbConfigKeyColumn()const{return m_dbConfigKey;}
		///returns the config value column
		QString dbConfigValueColumn()const{return m_dbConfigVal;}
		///returns the row containing the DB version in the config table
		QString dbVersionRow()const{return m_dbVersionRow;}
		
		/**returns the XML namespace of the project*/
		QString xmlProjectNamespace()const{return m_xmlNS;}
		/**returns the XML namespace for Wob base elements*/
		QString xmlPackNamespace()const{return m_xmlPackNS;}
		/**returns the XML namespace for SOAP 1.2 elements*/
		QString xmlSoap12Namespace()const{return m_xmlSoap12NS;}
		/**returns the XML namespace for Schema itself*/
		QString xmlSchemaNamespace()const{return m_xmlSchemaNS;}
		/**returns the XML namespace for XML base elements*/
		QString xmlXmlNamespace()const{return m_xmlXmlNS;}
		
		/**describes the way message are encoded in transport*/
		enum MessageEncoding{
			/**encode as standard WOB transactions*/
			WobEncoding=0,
			/**encode as SOAP 1.2 */
			Soap12Encoding=1,
			/**encode according to the current SOAP standard*/
			SoapEncoding=1,
			/** \private helper value: used in some parts of the code to simply query the processor for the encoding, never used in the processor itself*/
			DefaultEncoding=-1
		};
		/**returns the message encoding mode*/
		MessageEncoding messageEncoding()const{return m_encmode;}
		
		/**describes the authentication mode of the project*/
		enum AuthMode {
			/**no authentication*/
			NoAuth=0,
			/**session ID authentication*/
			SessionAuth=1,
			/**basic user/password authentication*/
			BasicAuth=2,
			/** \private helper value: unknown authentication type, signifies an error condition*/
			UnknownAuth=-1
		};
		/**returns the authentication mode*/
		AuthMode authMode()const{return m_auth;}
		
		/**returns whether a table exists*/
		bool hasTable(QString)const;
		/**returns the requested table*/
		WocTable table(QString)const;
		
		/**returns whether a class exists*/
		bool hasClass(QString)const;
		
		/**returns a list of transaction names*/
		QStringList transactionNames()const;
		/**returns a list of class names*/
		QStringList classNames()const;
		/**returns a list of table names*/
		QStringList tableNames()const;
		
		/**returns global docu*/
		QStringList docStrings()const{return m_docstrings;}
		
		/**returns the qualified names of all privileges*/
		QStringList privilegeNames()const;
		
		/**returns the default for the database "updating" attribute*/
		bool dbUpdatingDefault()const{return m_dbUpd;}
	signals:
		void sfinalize();
		void newClass(const WocClass&);
		void newTable(const WocTable&);
		void newTransaction(const WocTransaction&);
	public slots:
		void errorFound();
	private:
		QString m_baseDir,m_wobDir,m_verComm,m_verNeedComm,m_verHR,m_projname;
		QString m_verTarget,m_svnExe,m_gitExe;
		QMap<QString,QString>m_verInfo;
		QStringList m_verSys;
		QString m_dbInst,m_dbSchema,m_dbVer,m_dbConfigTable,m_dbConfigKey,m_dbConfigVal,m_dbVersionRow;
		QString m_xmlNS,m_xmlPackNS,m_xmlSoap12NS,m_xmlSchemaNS,m_xmlXmlNS;
		QStringList m_docstrings;
		bool m_error,m_dbUpd;
		MessageEncoding m_encmode;
		AuthMode m_auth;
		
		QList<WocTable> m_tables;
		QList<WocClass> m_classes;
		QList<WocTransaction> m_transactions;
		
		static WocProcessor*inst;
		
		/**helper: calls SVN and parses its output*/
		bool callSvn();
		///helper: calls GIT and parses its output
		bool callGit();
		///helper: fallback for version control check
		bool callNoVer();
		
		/**helper: looks up some schema files and finds the corresponding namespaces*/
		void parseNamespaces();
		
		/**helper: converts Project/auth attribute to enum*/
		AuthMode str2AuthMode(QString);
};


#endif
