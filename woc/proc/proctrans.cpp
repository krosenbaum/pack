// Copyright (C) 2009-2011 by Konrad Rosenbaum <konrad@silmor.de>
// protected under the GNU GPL version 3 or at your option any newer.
// See COPYING.GPL file that comes with this distribution.
//

#include "processor.h"

#include "qtout.h"
#ifndef COMPILE_PREWOC
#include "phpout.h"
#include "htmlout.h"
#endif

#include "domquery.h"

#include <QDir>
#include <QDomDocument>
#include <QDomElement>
#include <QProcess>
#include <QRegExp>

/******************************************************************************
 * WocTransaction
 ******************************************************************************/

WocTransaction::WocTransaction(const QDomElement&root)
{
	m_valid=true;
	m_mode=Checked;
        m_logmode=LogAll;
	QRegExp rval("[a-z][a-z0-9_]*",Qt::CaseInsensitive);
	//get basics
	m_name=root.attribute("name");
	if(!rval.exactMatch(m_name)){
		qDebug("Error: invalid transaction name %s.",m_name.toLatin1().data());
		m_valid=false;
		return;
	}
	qDebug("Info: parsing transaction %s",m_name.toLatin1().data());
	if(root.hasAttribute("mode")){
		QString m=root.attribute("mode").toLower();
		if(m=="checked")m_mode=Checked;else
		if(m=="auth")m_mode=Auth;else
		if(m=="open")m_mode=Open;
		else{
			qDebug("Error: invalid transaction mode %s in transaction %s, must be checked, auth, or open.",m.toLatin1().data(),m_name.toLatin1().data());
			m_valid=false;
			return;
		}
	}
	if(root.hasAttribute("nolog")){
                QStringList nl=root.attribute("nolog").toLower().split(",");
                if(nl.contains("request")||nl.contains("req"))m_logmode=NoLogRequest;
                if(nl.contains("response")||nl.contains("rsp"))
                        m_logmode=NoLogMode(m_logmode|NoLogResponse);
                if(nl.contains("all")||nl.contains("any"))
                        m_logmode=NoLogAny;
        }
	if(root.hasAttribute("updating"))
		m_update=str2bool(root.attribute("updating"));
	else
		m_update=WocProcessor::instance()->dbUpdatingDefault();
	//input tag
	QList<QDomElement> nl=elementsByTagName(root,"Input");
	for(int i=0;i<nl.size();i++){
		QDomElement el=nl.at(i).toElement();
		if(el.isNull())continue;
		//variables
		QList<QDomElement> nl2=elementsByTagName(el,"Var");
		for(int j=0;j<nl2.size();j++){
			QDomElement el2=nl2.at(j).toElement();
			if(el2.isNull())continue;
			QString nm=el2.attribute("name");
			if(!rval.exactMatch(nm)){
				qDebug("Error: invalid input variable %s in transaction %s.",nm.toLatin1().data(),m_name.toLatin1().data());
				m_valid=false;
				return;
			}
			if(hasInput(nm)){
				qDebug("Error: double definition of input variable %s in transaction %s.",nm.toLatin1().data(),m_name.toLatin1().data());
				m_valid=false;
				return;
			}
			QString tp=el2.attribute("type");
			//TODO: validate type
			m_input.append(QPair<QString,QString>(nm,tp));
			//docu
			QString s=el2.text().trimmed();
			if(s!="")m_indoc.insert(nm,s);
		}
	}
	//call tag
	nl=elementsByTagName(root,"Call");
	for(int j=0;j<nl.size();j++){
		QDomElement el=nl.at(j).toElement();
		if(el.isNull())continue;
		QString nm=el.attribute("lang");
		if(hasCall(nm)){
			qDebug("Error: double definition of input variable %s in transaction %s.",nm.toLatin1().data(),m_name.toLatin1().data());
			m_valid=false;
			return;
		}
		QString mt=el.attribute("method");
		QString inc=el.attribute("include");
		m_call.insert(nm,QPair<QString,QString>(mt,inc));
		if(!nm.contains("/")){
			m_call.insert(nm+"/client",QPair<QString,QString>(mt,inc));
			m_call.insert(nm+"/server",QPair<QString,QString>(mt,inc));
		}
	}
	//output
	nl=elementsByTagName(root,"Output");
	for(int i=0;i<nl.size();i++){
		QDomElement el=nl.at(i).toElement();
		if(el.isNull())continue;
		//variables
		QList<QDomElement> nl2=elementsByTagName(el,"Var");
		for(int j=0;j<nl2.size();j++){
			QDomElement el2=nl2.at(j).toElement();
			if(el2.isNull())continue;
			QString nm=el2.attribute("name");
			if(!rval.exactMatch(nm)){
				qDebug("Error: invalid output variable %s in transaction %s.",nm.toLatin1().data(),m_name.toLatin1().data());
				m_valid=false;
				return;
			}
			if(hasOutput(nm)){
				qDebug("Error: double definition of output variable %s in transaction %s.",nm.toLatin1().data(),m_name.toLatin1().data());
				m_valid=false;
				return;
			}
			QString tp=el2.attribute("type");
			//TODO: validate type
			m_output.append(QPair<QString,QString>(nm,tp));
			//docu
			QString s=el2.text().trimmed();
			if(s!="")m_outdoc.insert(nm,s);
		}
	}
	//docu
	nl=elementsByTagName(root,"Doc");
	for(int i=0;i<nl.size();i++){
		QString s=nl.at(i).toElement().text().trimmed();
		if(s!="")m_docstrings<<s;
	}
	//privileges
	nl=elementsByTagName(root,"Privilege");
	for(int i=0;i<nl.size();i++){
		QString s=nl.at(i).toElement().attribute("name").trimmed();
		if(s!="")m_privileges<<s;
		m_privdoc.insert(s,nl.at(i).toElement().text().trimmed());
	}
}

bool WocTransaction::hasInput(QString v)const
{
	for(int i=0;i<m_input.size();i++)
		if(m_input[i].first==v)return true;
	return false;
}

QStringList WocTransaction::inputNames()const
{
	QStringList r;
	for(int i=0;i<m_input.size();i++)
		r<<m_input[i].first;
	return r;
}

QString WocTransaction::inputType(QString v)const
{
	for(int i=0;i<m_input.size();i++)
		if(m_input[i].first==v)return m_input[i].second;
	return "";
}

bool WocTransaction::hasOutput(QString v)const
{
	for(int i=0;i<m_output.size();i++)
		if(m_output[i].first==v)return true;
	return false;
}

QStringList WocTransaction::outputNames()const
{
	QStringList r;
	for(int i=0;i<m_output.size();i++)
		r<<m_output[i].first;
	return r;
}

QString WocTransaction::outputType(QString v)const
{
	for(int i=0;i<m_output.size();i++)
		if(m_output[i].first==v)return m_output[i].second;
	return "";
}
