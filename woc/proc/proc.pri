SOURCES+= \
	$$PWD/processor.cpp \
	$$PWD/procclass.cpp \
	$$PWD/proctrans.cpp \
	$$PWD/proctable.cpp
HEADERS+= \
	$$PWD/processor.h \
	$$PWD/procclass.h \
	$$PWD/proctrans.h \
	$$PWD/proctable.h

CONFIG(prewoc, prewoc|woc){
	RESOURCES += $$PWD/version.qrc
}

INCLUDEPATH += $$PWD
