// Copyright (C) 2009-2011 by Konrad Rosenbaum <konrad@silmor.de>
// protected under the GNU GPL version 3 or at your option any newer.
// See COPYING.GPL file that comes with this distribution.
//

#include "processor.h"

#include "qtout.h"
#ifndef COMPILE_PREWOC
#include "phpout.h"
#include "htmlout.h"
#endif

#include "domquery.h"

#include <QDir>
#include <QDomDocument>
#include <QDomElement>
#include <QProcess>
#include <QRegExp>


/******************************************************************************
 * WocTable
 ******************************************************************************/

WocTable::WocTable()
{
	m_backup=m_valid=m_audit=false;
}

WocTable::WocTable(const QDomElement&tbl)
{
	m_valid=true;
	//parse XML
	m_name=tbl.attribute("name");
	WocProcessor*woc=WocProcessor::instance();
	QRegExp good("[a-z][a-z0-9_]*",Qt::CaseInsensitive);
	//check name syntax, check it does not exist yet
	if(woc->hasTable(m_name)){
		qDebug("Error: double definition of table %s.",m_name.toLatin1().data());
		m_valid=false;
		return;
	}
	if(!good.exactMatch(m_name)){
		qDebug("Error: table %s does not have a regular name.",m_name.toLatin1().data());
		m_valid=false;
		return;
	}
	m_backup=str2bool(tbl.attribute("backup","0"));
	m_backupkey=tbl.attribute("backupKey",QString());
	m_backupsize=tbl.attribute("backupSize","-1").toInt();
	m_base=tbl.attribute("base","WobTable");
	m_audit=str2bool(tbl.attribute("audit","0"));
	qDebug("Info: parsing table %s",m_name.toLatin1().data());
	//Columns
	QList<QDomElement> nl=elementsByTagName(tbl,"Column");
	for(int i=0;i<nl.size();i++){
		QDomElement el=nl.at(i).toElement();
		if(el.isNull())continue;
		QPair<bool,s_col> cl=parseColumn(el,m_name);
		if(!cl.first)return;
		if(hasColumn(cl.second.name)){
			qDebug("Error: double definition of column %s in table %s.",cl.second.name.toLatin1().data(),m_name.toLatin1().data());
			m_valid=false;
			return;
		}
		m_columns.append(cl.second);
	}
	//Audit Columns
	nl=elementsByTagName(tbl,"AuditColumn");
	for(int i=0;i<nl.size();i++){
		QDomElement el=nl.at(i).toElement();
		if(el.isNull())continue;
		QPair<bool,s_col> cl=parseColumn(el,m_name);
		if(!cl.first)return;
		m_auditcolumns.append(cl.second);
	}

	//Foreign getter methods
	nl=elementsByTagName(tbl,"Foreign");
	for(int i=0;i<nl.size();i++){
		QDomElement el=nl.at(i).toElement();
		if(el.isNull())continue;
		m_foreign.append(QPair<QString,QString>(el.attribute("method"),el.attribute("via")));
		//TODO: validate foreign getter
		//docu
		QString s=el.text().trimmed();
		if(s!="")m_fordocs.insert(el.attribute("method"),s);
	}

	//Presets
	nl=elementsByTagName(tbl,"Preset");
	for(int i=0;i<nl.size();i++){
		QDomElement el=nl.at(i).toElement();
		if(el.isNull())continue;
        QMap<QString,QString>ps;
        QMap<QString,Preset>ps2;
        QList<QDomElement> nl2=elementsByTagName(el,"V");
		for(int j=0;j<nl2.size();j++){
			QDomElement el2=nl2.at(j).toElement();
			if(el2.isNull())continue;
			if(!el2.hasAttribute("col") || (!el2.hasAttribute("val") && !el2.hasAttribute("code"))){
				qDebug("Error: table definition %s contains illegal preset.",m_name.toLatin1().data());
				m_valid=false;
				return;
			}
			QString nm=el2.attribute("col");
			if(!hasColumn(nm)){
				qDebug("Error: table definition %s contains preset for invalid column %s.",m_name.toLatin1().data(),nm.toLatin1().data());
				m_valid=false;
				return;
			}
			QString v;
			if(el2.hasAttribute("val"))v="\""+el2.attribute("val").replace("\"","\\\"")+"\"";
			else v=el2.attribute("code");
			ps.insert(nm,v);
            ps2.insert(nm,Preset(nm,el2.attribute("val"),el2.attribute("code")));
		}
		if(ps.size()>0)m_presets.append(ps);
        if(ps2.size()>0)m_presets2.append(ps2);
	}

	//Docu
	nl=elementsByTagName(tbl,"Doc");
	for(int i=0;i<nl.size();i++){
		QString s=nl.at(i).toElement().text().trimmed();
		if(s!="")m_docstrings<<s;
	}

	//Complex Unique cols
	nl=elementsByTagName(tbl,"Unique");
	for(int i=0;i<nl.size();i++){
		m_uniquecols<<nl[i].toElement().text().trimmed();
	}

	//sanity checks
	//check we have any columns at all
	if(m_columns.size()==0){
		qDebug("Error: table %s does not have any columns.",m_name.toLatin1().data());
		m_valid=false;
		return;
	}
	//check that we have a primary key
	if(primaryColumns().size()==0){
		qDebug("Warning: table %s does not have any primary key columns.",m_name.toLatin1().data());
	}
	//check that unique definitions are valid
	for(QString uniq:m_uniquecols){
		if(uniq.isEmpty()){
			qDebug("Error: table %s contains an empty Unique declaration.",m_name.toLatin1().data());
			m_valid=false;
			return;
		}
		for(QString col:uniq.split(',')){
			bool found=false;
			for(auto c:m_columns)
				if(c.name==col){
					found=true;
					break;
				}
			if(!found){
				qDebug("Error: table %s has Unique declaration that refers to unknown column %s.",m_name.toLatin1().data(),col.toLatin1().data());
				m_valid=false;
				return;
			}
		}
	}
}


QPair<bool,WocTable::s_col> WocTable::parseColumn(const QDomElement&el,QString m_name)
{
	s_col cl;
	QRegExp good("[a-z][a-z0-9_]*",Qt::CaseInsensitive);
	cl.name=el.attribute("name");
	//check name syntax, check it is not doubled
	if(!good.exactMatch(cl.name)){
		qDebug("Error: table %s column %s does not have a regular name.",m_name.toLatin1().data(),cl.name.toLatin1().data());
		return QPair<bool,s_col>(false,s_col());
	}
	cl.isprime=str2bool(el.attribute("primarykey","0"));
	cl.isunique=str2bool(el.attribute("unique","0"));
	cl.isindex=str2bool(el.attribute("index","0"));
	if(cl.isprime)cl.isnull=false;
	else cl.isnull=true;
	if(el.hasAttribute("null"))
		cl.isnull=str2bool(el.attribute("null"));
	else if(el.hasAttribute("notnull"))
		cl.isnull=!str2bool(el.attribute("notnull","0"));
	cl.type=el.attribute("type");
	//TODO: validate type
	cl.foreign=el.attribute("foreignkey");
	WocProcessor*woc=WocProcessor::instance();
	//check foreign key exists
	if(cl.foreign!=""){
		QStringList fgn=cl.foreign.split(":");
		if(fgn.size()!=2){
			qDebug("Error: table %s column %s: foreign key definition must use syntax 'table:column'",m_name.toLatin1().data(),cl.name.toLatin1().data());
			return QPair<bool,s_col>(false,s_col());
		}
		if(!woc->hasTable(fgn[0])){
			qDebug("Error: table %s column %s: foreign key target table %s does not exist",m_name.toLatin1().data(),cl.name.toLatin1().data(),fgn[0].toLatin1().data());
			return QPair<bool,s_col>(false,s_col());
		}
		if(!woc->table(fgn[0]).hasColumn(fgn[1])){
			qDebug("Error: table %s column %s: foreign key target table/column %s does not exist",m_name.toLatin1().data(),cl.name.toLatin1().data(),cl.foreign.toLatin1().data());
			return QPair<bool,s_col>(false,s_col());
		}
	}
	cl.defaultval=el.attribute("default");
	//TODO: validate default against type
	QList<QDomElement> nl2=elementsByTagName(el,"Value");
	int nxval=0;
	//enum values
	for(int j=0;j<nl2.size();j++){
		QDomElement el2=nl2.at(j).toElement();
		if(el2.isNull())continue;
		QString n=el2.attribute("name");
		if(n==""){
			qDebug("Warning: anonymous enum value in table %s column %s. Ignoring it.",m_name.toLatin1().data(),cl.name.toLatin1().data());
			continue;
		}
		nxval=el2.attribute("value",QString::number(nxval)).toInt(0,0);
		cl.enumvals.append(WocEnum(n,nxval,el2.text().trimmed()));
		nxval++;
	}
	//default calls
	nl2=elementsByTagName(el,"Call");
	for(int j=0;j<nl2.size();j++){
		QDomElement el2=nl2.at(j).toElement();
		if(el2.isNull())continue;
		QString lang=el2.attribute("lang");
		QString meth=el2.attribute("method","false");
		cl.methodcalls.insert(lang,meth);
		if(!lang.contains("/")){
			cl.methodcalls.insert(lang+"/client",meth);
			cl.methodcalls.insert(lang+"/server",meth);
		}
	}
	//docu
	QDomNodeList nl3=el.childNodes();
	for(int j=0;j<nl3.size();j++){
		QDomNode n=nl3.at(j);
		if(n.isText()||n.isCDATASection())cl.doc+=" "+n.nodeValue();
		else
		if(n.isElement()&&n.nodeName()=="Doc")cl.doc+=" "+n.toElement().text();
	}
	cl.doc=cl.doc.trimmed();

	return QPair<bool,s_col>(true,cl);
}

QStringList WocTable::columns()const
{
	QStringList r;
	for(int i=0;i<m_columns.size();i++)
		r<<m_columns[i].name;
	return r;
}

QStringList WocTable::auditColumns()const
{
	QStringList r;
	for(int i=0;i<m_staticauditcolumns.size();i++)
		r<<m_staticauditcolumns[i].name;
	for(int i=0;i<m_auditcolumns.size();i++)
		r<<m_auditcolumns[i].name;
	return r;
}

QStringList WocTable::primaryColumns()const
{
	QStringList r;
	for(int i=0;i<m_columns.size();i++)
		if(m_columns[i].isprime)
			r<<m_columns[i].name;
	return r;
}

QString WocTable::columnType(QString c)const
{
	for(int i=0;i<m_columns.size();i++)
		if(m_columns[i].name==c)
			return m_columns[i].type;
	return "";
}

bool WocTable::columnIsNull(QString c)const
{
	for(int i=0;i<m_columns.size();i++)
		if(m_columns[i].name==c)
			return m_columns[i].isnull;
	return false;
}

bool WocTable::columnIsPrimary(QString c)const
{
	for(int i=0;i<m_columns.size();i++)
		if(m_columns[i].name==c)
			return m_columns[i].isprime;
	return false;
}

bool WocTable::columnHasDefault(QString c)const
{
	for(int i=0;i<m_columns.size();i++)
		if(m_columns[i].name==c)
			return m_columns[i].defaultval!="";
	return false;
}

QString WocTable::columnDefault(QString c)const
{
	for(int i=0;i<m_columns.size();i++)
		if(m_columns[i].name==c)
			return m_columns[i].defaultval;
	return "";
}

bool WocTable::hasColumn(QString c)const
{
	for(int i=0;i<m_columns.size();i++)
		if(m_columns[i].name==c)
			return true;
	return false;
}

QList<WocEnum> WocTable::columnEnums(QString c)const
{
	for(int i=0;i<m_columns.size();i++)
		if(m_columns[i].name==c)
			return m_columns[i].enumvals;
	return QList<WocEnum>();
}

QList<WocEnum> WocTable::getEnums()const
{
	QList<WocEnum> r;
	for(int i=0;i<m_columns.size();i++)
		for(int j=0;j<m_columns[i].enumvals.size();j++)
			r.append(m_columns[i].enumvals[j]);
	return r;
}
bool WocTable::columnIsForeign(QString c)const
{
	for(int i=0;i<m_columns.size();i++)
		if(m_columns[i].name==c)
			return m_columns[i].foreign!="";
	return false;
}

QString WocTable::columnForeign(QString c)const
{
	for(int i=0;i<m_columns.size();i++)
		if(m_columns[i].name==c)
			return m_columns[i].foreign;
	return "";
}

bool WocTable::columnIsIndexed(QString c)const
{
	for(int i=0;i<m_columns.size();i++)
		if(m_columns[i].name==c)
			return m_columns[i].isindex;
	return false;
}

bool WocTable::columnIsUnique(QString c)const
{
	for(int i=0;i<m_columns.size();i++)
		if(m_columns[i].name==c)
			return m_columns[i].isunique;
	return false;
}

QString WocTable::columnDoc(QString c)const
{
	for(int i=0;i<m_columns.size();i++)
		if(m_columns[i].name==c)
			return m_columns[i].doc;
	return "";
}


QStringList WocTable::foreigns()const
{
	QStringList r;
	for(int i=0;i<m_foreign.size();i++)
		r<<m_foreign[i].first;
	return r;
}

QString WocTable::foreignQuery(QString f)const
{
	for(int i=0;i<m_foreign.size();i++)
		if(m_foreign[i].first==f)
			return m_foreign[i].second;
	return "";
}

bool WocTable::haveForeign(QString f)const
{
	for(int i=0;i<m_foreign.size();i++)
		if(m_foreign[i].first==f)
			return true;
	return false;
}

QString WocTable::columnCall(QString col,QString lang)const
{
	for(int i=0;i<m_columns.size();i++){
		s_col cl=m_columns[i];
		if(cl.name==col){
			if(!cl.methodcalls.contains(lang))return "";
			else return cl.methodcalls[lang];
		}
	}
	return "";
}

QList<WocTable::s_col>WocTable::m_staticauditcolumns;
void WocTable::parseAuditStatic(const QDomElement&el)
{
	QList<QDomElement> nl=elementsByTagName(el,"Column");
	for(int i=0;i<nl.size();i++){
		QDomElement el2=nl.at(i).toElement();
		if(el2.isNull())continue;
		QPair<bool,s_col>cl=parseColumn(el2,"(global audit settings)");
		if(!cl.first){
			WocProcessor::instance()->errorFound();
			return;
		}
		m_staticauditcolumns.append(cl.second);
	}
}

WocTable WocTable::auditTable()const
{
	WocTable adt;
	adt.m_valid=m_valid;
	adt.m_backup=m_backup;
	adt.m_audit=false;
	adt.m_name=auditTableName();//enhance the name
	adt.m_base="WobTable";//revert to default
	adt.m_foreign=m_foreign;
	adt.m_fordocs=m_fordocs;
	adt.m_docstrings=m_docstrings;
	//these stay empty: m_presets, m_auditcolumns

	//now for the complicated stuff
	//create primary key
	s_col cl;
	cl.name="auditid";cl.type="seq64";cl.doc="additional primary key for auditing";
	cl.isindex=cl.isunique=cl.isnull=false;cl.isprime=true;
	adt.m_columns.append(cl);
	//copy common columns
	adt.m_columns.append(m_staticauditcolumns);
	//copy normal columns
	for(int i=0;i<m_columns.size();i++){
		//get and reset
		cl=m_columns[i];
		cl.isprime=cl.isunique=false;
		cl.methodcalls.clear();
		if(cl.type.left(3)=="seq")cl.type="int"+cl.type.mid(3);
		//no foreign keys
		cl.foreign="";
		//add
		adt.m_columns.append(cl);
	}
	//copy local audit columns
	adt.m_columns.append(m_auditcolumns);

	//return result
	return adt;
}
