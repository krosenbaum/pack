// Copyright (C) 2009-2011 by Konrad Rosenbaum <konrad@silmor.de>
// protected under the GNU GPL version 3 or at your option any newer.
// See COPYING.GPL file that comes with this distribution.
//

#include "processor.h"

#include "qtout.h"
#ifndef COMPILE_PREWOC
#include "phpout.h"
#include "htmlout.h"
#endif

#include "domquery.h"

#include <QDir>
#include <QDomDocument>
#include <QDomElement>
#include <QProcess>
#include <QRegExp>

/******************************************************************************
 * WocClass
 ******************************************************************************/

WocClass::WocClass(const QDomElement&cls)
{
	//scan basics
	m_valid=true;
	m_abstract=str2bool(cls.attribute("abstract","0"));
	WocProcessor *woc=WocProcessor::instance();
	QRegExp symok("[a-z_][a-z0-9_]*",Qt::CaseInsensitive);
	m_name=cls.attribute("name");
	if(m_name==""){
		qDebug("Error: unnamed class at line %i column %i.",cls.lineNumber(),cls.columnNumber());
		m_valid=false;
		return;
	}
	if(woc->hasClass(m_name)){
		qDebug("Error: double definition of class %s at line %i column %i.",m_name.toLatin1().data(),cls.lineNumber(),cls.columnNumber());
		m_valid=false;
		return;
	}
	if(!symok.exactMatch(m_name)){
		qDebug("Error: Illegal class name %s.",m_name.toLatin1().data());
		m_valid=false;
		return;
	}
	qDebug("Info: parsing class %s",m_name.toLatin1().data());
	//scan inheritance
	if(cls.hasAttribute("base"))
		m_base.insert("wolf",cls.attribute("base"));
	QList<QDomElement> nl=elementsByTagName(cls,"Base");
	for(int i=0;i<nl.size();i++){
		if(!nl[i].hasAttribute("lang") || !nl[i].hasAttribute("class")){
			m_valid=false;
			return;
		}
		QString lang=nl[i].attribute("lang");
		QString cls=nl[i].attribute("class");
		if(!lang.contains("/")){
			m_base.insert(lang+"/client",cls);
			m_base.insert(lang+"/server",cls);
		}else
			m_base.insert(lang,cls);
	}
	//scan properties
	nl=elementsByTagName(cls,"Property");
	for(int i=0;i<nl.size();i++){
		QDomElement el=nl.at(i).toElement();
		if(el.isNull())continue;
		s_prop p;
		p.name=el.attribute("name");
		if(!symok.exactMatch(p.name)){
			qDebug("Error: Illegal property %s in class %s.",p.name.toLatin1().data(),m_name.toLatin1().data());
			m_valid=false;
			return;
		}
		if(hasProperty(p.name)){
			qDebug("Error: Double definition of property %s in class %s.",p.name.toLatin1().data(),m_name.toLatin1().data());
			m_valid=false;
			return;
		}
		p.type=el.attribute("type");
		p.isid=str2bool(el.attribute("id","0"));
		p.isabstract=str2bool(el.attribute("abstract","0"));
		m_props.append(p);
		//docu
		QString s=el.text().trimmed();
		if(s!="")m_propdoc.insert(p.name,s);
	}
	//scan enums
	nl=elementsByTagName(cls,"Enum");
	for(int i=0;i<nl.size();i++){
		QDomElement el=nl.at(i).toElement();
		if(el.isNull())continue;
		QString nm=el.attribute("name");
		if(!symok.exactMatch(nm)){
			qDebug("Error: Illegal enum type %s in class %s.",nm.toLatin1().data(),m_name.toLatin1().data());
			m_valid=false;
			return;
		}
		if(hasEnumType(nm)){
			qDebug("Error: Double definition of enum type %s in class %s.",nm.toLatin1().data(),m_name.toLatin1().data());
			m_valid=false;
			return;
		}
		QList<WocEnum>ev;
		//check whether there is a reference
		QString coldoc;
		if(el.hasAttribute("refColumn")){
			QStringList ref=el.attribute("refColumn").split(":");
			if(ref.size()!=2){
				qDebug("Error: illegal enum reference in class %s enum %s.",m_name.toLatin1().data(),nm.toLatin1().data());
				m_valid=false;
				return;
			}
			if(!woc->hasTable(ref[0])){
				qDebug("Error: enum reference in class %s enum %s points to non-existant table.",m_name.toLatin1().data(),nm.toLatin1().data());
				m_valid=false;
				return;
			}
			WocTable tab=woc->table(ref[0]);
			if(!tab.hasColumn(ref[1])){
				qDebug("Error: enum reference in class %s enum %s points to non-existant column.",m_name.toLatin1().data(),nm.toLatin1().data());
				m_valid=false;
				return;
			}
			ev=tab.columnEnums(ref[1]);
			coldoc+="Generated from table "+tab.name()+", column "+ref[1]+".\n\n";
			coldoc=tab.columnDoc(ref[1]);
			coldoc=coldoc.trimmed();
		}
		//scan values
		QList<QDomElement> nl2=elementsByTagName(el,"Value");
		int nxval=0;
		for(int j=0;j<nl2.size();j++){
			QDomElement el2=nl2.at(j).toElement();
			if(el2.isNull())continue;
			QString n=el2.attribute("name");
			if(n==""){
				qDebug("Warning: anonymous enum value in class %s enum %s. Ignoring it.",m_name.toLatin1().data(),nm.toLatin1().data());
				continue;
			}
			nxval=el2.attribute("value",QString::number(nxval)).toInt(0,0);
			ev.append(WocEnum(n,nxval,el2.text().trimmed()));
			nxval++;
			//TODO: check that value name does not exist yet
		}
		m_enumvals.insert(nm,ev);
		//scan docu
		QString doc;
		foreach(QDomElement el2,elementsByTagName(el,"Doc")){
			QString d=el2.text().trimmed();
			if(d.size()>0){
				if(doc.size()>0)doc+="\n\n";
				doc+=d;
			}
		}
		if(coldoc.size()>0){
			if(doc.size()>0)doc+="\n\n";
			doc+=coldoc;
		}
		m_enumdoc.insert(nm,doc);
	}
	//scan mappings
	nl=elementsByTagName(cls,"Mapping");
	for(int i=0;i<nl.size();i++){
		QDomElement el=nl.at(i).toElement();
		if(el.isNull())continue;
		QString name=el.attribute("table");
		if(!symok.exactMatch(name)){
			qDebug("Error: Illegal mapping %s in class %s.",name.toLatin1().data(),m_name.toLatin1().data());
			m_valid=false;
			return;
		}
		if(hasMapping(name)){
			qDebug("Error: Double definition of mapping %s in class %s.",name.toLatin1().data(),m_name.toLatin1().data());
			m_valid=false;
			return;
		}
		QList<s_map>map;
		QList<QDomElement> nl2=elementsByTagName(el,"Map");
		for(int j=0;j<nl2.size();j++){
			QDomElement el2=nl2.at(j).toElement();
			if(el2.isNull())continue;
			s_map sm;
			sm.column=el2.attribute("column");
			sm.property=el2.attribute("property");
			if(sm.property=="")sm.property=sm.column;
			if(sm.column=="")sm.column=sm.property;
			if(sm.column==""){
				qDebug("Warning: empty mapping in class %s mapping %s. Ignoring it.",m_name.toLatin1().data(),name.toLatin1().data());
				continue;
			}
			QList<QDomElement> nl3=elementsByTagName(el2,"Call");
			for(int k=0;k<nl3.size();k++){
				QDomElement el3=nl3.at(k).toElement();
				if(el3.isNull())continue;
				QString lang=el3.attribute("lang").trimmed();
				QString meth=el3.attribute("method","false");
				sm.method.insert(lang,meth);
				if(!lang.contains("/")){
					sm.method.insert(lang+"/client",meth);
					sm.method.insert(lang+"/server",meth);
				}
			}
			map.append(sm);
		}
		m_maps.insert(name,map);
	}
	//docu
	nl=elementsByTagName(cls,"Doc");
	for(int i=0;i<nl.size();i++){
		QString s=nl.at(i).toElement().text().trimmed();
		if(s!="")m_docstrings<<s;
	}
	//check abstraction
	if(!m_abstract && isAbstract("")){
		qDebug("Warning: class %s should be declared abstract.",m_name.toLatin1().data());
	}
	//conditional abstraction (must be after check or we'll get false positives)
	nl=elementsByTagName(cls,"Abstract");
	for(int i=0;i<nl.size();i++){
		QString s=nl.at(i).toElement().attribute("lang").trimmed();
		if(s!=""){
			m_cabstract<<s;
			if(!s.contains("/"))
				m_cabstract<<(s+"/client")<<(s+"/server");
		}
	}
}

bool WocClass::hasProperty(QString p)const
{
	for(int i=0;i<m_props.size();i++)
		if(m_props[i].name==p)return true;
	return false;
}

QStringList WocClass::propertyNames()const
{
	QStringList r;
	for(int i=0;i<m_props.size();i++)
		r<<m_props[i].name;
	return r;
}

QString WocClass::propertyType(QString p)const
{
	for(int i=0;i<m_props.size();i++)
		if(m_props[i].name==p)return m_props[i].type;
	return "";
}

bool WocClass::propertyIsIdentity(QString p)const
{
	for(int i=0;i<m_props.size();i++)
		if(m_props[i].name==p)return m_props[i].isid;
	return false;
}

bool WocClass::propertyIsAbstract(QString p)const
{
	for(int i=0;i<m_props.size();i++)
		if(m_props[i].name==p)return m_props[i].isabstract;
	return false;
}

bool WocClass::isAbstract(QString l)const
{
	if(m_cabstract.contains(l))return true;
	for(int i=0;i<m_props.size();i++)
		if(m_props[i].isabstract)return true;
	return m_abstract;
}

QString WocClass::propertyPlainType(QString p)const
{
	QString r=propertyType(p);
	if(r.startsWith("List:"))return r.mid(5);
	else return r;
}

const QStringList WocClass::attrtypes=QStringList()<<"astring"<<"int"<<"int32"<<"int64"<<"bool";
bool WocClass::propertyIsAttribute(QString p)const
{
	if(p.startsWith("List:"))return false;
	QString t=propertyPlainType(p);
	if(attrtypes.contains(t))return true;
	if(hasEnumType(t))return true;
	return false;
}

const QStringList WocClass::elemtypes=QStringList()<<"string"<<"blob";
bool WocClass::propertyIsSimpleElement(QString p) const
{
	QString t=propertyPlainType(p);
	return elemtypes.contains(t);
}

bool WocClass::propertyIsElement(QString p)const
{
	return !propertyIsAttribute(p);
}

bool WocClass::propertyIsObject(QString p)const
{
	QString t=propertyPlainType(p);
	//default types take precedence over classes
	if(attrtypes.contains(t))return false;
	if(elemtypes.contains(t))return false;
	//enums shadow classes
	if(hasEnumType(t))return false;
	//check that the class exists
	return WocProcessor::instance()->hasClass(t);
}

bool WocClass::propertyIsEnum(QString p)const
{
	QString t=propertyPlainType(p);
	//default types take precedence over enums
	if(attrtypes.contains(t))return false;
	if(elemtypes.contains(t))return false;
	//enums
	return hasEnumType(t);
}

bool WocClass::propertyIsList(QString p)const
{
	if(propertyType(p).startsWith("List:"))return true;
	else return false;
}

QMap<QString,QString> WocClass::mapping(QString m)const
{
	if(!m_maps.contains(m))return QMap<QString,QString>();
	QList<s_map> sml=m_maps[m];
	QMap<QString,QString>ret;
	for(int i=0;i<sml.size();i++)
		ret.insert(sml[i].property,sml[i].column);
	return ret;
}

QStringList WocClass::mappingProperties(QString m)const
{
	if(!m_maps.contains(m))return QStringList();
	QList<s_map> sml=m_maps[m];
	QStringList ret;
	for(int i=0;i<sml.size();i++)
		ret<<sml[i].property;
	return ret;
}

QString WocClass::mapMethod(QString table,QString property,QString lang)const
{
	if(!m_maps.contains(table))return "";
	QList<s_map> sml=m_maps[table];
	for(int i=0;i<sml.size();i++)
		if(sml[i].property==property){
			if(sml[i].method.contains(lang))return sml[i].method[lang];
			else return "";
		}
	return "";
}

QString WocClass::baseClass(QString lang, QString prefix) const
{
	if(m_base.contains("wolf"))
		return prefix+m_base["wolf"];
	if(m_base.contains(lang))
		return m_base[lang];
	return "WObject";
}

