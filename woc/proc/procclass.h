// Copyright (C) 2009-2011 by Konrad Rosenbaum <konrad@silmor.de>
// protected under the GNU GPL version 3 or at your option any newer.
// See COPYING.GPL file that comes with this distribution.
//

#ifndef WOC_PROCESSOR_CLASS_H
#define WOC_PROCESSOR_CLASS_H

#include <QList>
#include <QMap>
#include <QObject>
#include <QPair>
#include <QStringList>

class QDomElement;

/**helper structure to store enums in classes and tables*/
struct WocEnum {
	QString name,doc;
	int val;
	WocEnum(){val=0;}
	WocEnum(QString n,int v,QString d=""){name=n;val=v;doc=d;}
};

/**stores a communication class including serialization and deserialization information*/
class WocClass
{
	public:
		/**parses XML to create itself*/
		WocClass(const QDomElement&);
		
		/**returns whether parsing was successful and this instance represents a valid communication class*/
		bool isValid()const{return m_valid;}
		
		/**returns the class name*/
		QString name()const{return m_name;}
		/**returns the name of the class it is derived from - default: WObject
		\param lang the language for which to return the base class
		\param prefix the class prefix in the target language */
		QString baseClass(QString lang,QString prefix)const;
		
		/**returns true of it has a property with this name*/
		bool hasProperty(QString)const;
		/**returns a list of all property names*/
		QStringList propertyNames()const;
		/**returns the type string of the property, including "List:" if applicable*/
		QString propertyType(QString)const;
		/**returns the plain type string of the property without "List:"*/
		QString propertyPlainType(QString)const;
		/**returns whether this property identifies the object instance*/
		bool propertyIsIdentity(QString)const;
		/**returns whether this property is abstract*/
		bool propertyIsAbstract(QString)const;
		/**returns whether this property is serialized as XML attribute*/
		bool propertyIsAttribute(QString)const;
		/**returns whether this property is serialized as XML element*/
		bool propertyIsElement(QString)const;
		/**returns whether this property is serialized as XML element, but not a class*/
		bool propertyIsSimpleElement(QString)const;
		/**returns whether this property has an enum type*/
		bool propertyIsEnum(QString)const;
		/**returns whether the property is an object*/
		bool propertyIsObject(QString)const;
		/**returns whether the property is a list of values (false for scalars)*/
		bool propertyIsList(QString)const;
		/**returns whether the property type is integer*/
		bool propertyIsInt(QString p)const{QString pt=propertyPlainType(p);return pt=="int" || pt=="int32" || pt=="int64";}
		/**returns whether the property type is boolean*/
		bool propertyIsBool(QString p)const{return propertyPlainType(p)=="bool";}
		/**returns whether the property type is string*/
		bool propertyIsString(QString p)const{QString pt=propertyPlainType(p);return pt=="string"||pt=="astring";}
		/**returns whether the property type is blob*/
		bool propertyIsBlob(QString p)const{QString pt=propertyPlainType(p);return pt=="blob";}
		
		/**returns whether the class is abstract in the requested language (needs to be customized); it is automatically abstract if any property is abstract*/
		bool isAbstract(QString)const;
		/**returns the languages in which the class is conditionally abstract*/
		QStringList abstractLangs()const{return m_cabstract;}
		
		/**returns the names of all enum types of this class*/
		QStringList enumTypes()const{return m_enumvals.keys();}
		/**returns true if the given enum type exists in this class*/
		bool hasEnumType(QString t)const{return m_enumvals.contains(t);}
		/**returns a list of enum values as name-value pairs*/
		QList<WocEnum> enumValues(QString t)const{return m_enumvals[t];}
		
		/**returns true if the given mapping exists*/
		bool hasMapping(QString m)const{return m_maps.contains(m);}
		/**returns the names of all tables for which a mapping exists*/
		QStringList mappingTables()const{return m_maps.keys();}
		/**returns the (correctly ordered) properties for a mapping*/
		QStringList mappingProperties(QString)const;
		/**returns the specific mapping; map key=property, map value=column*/
		QMap<QString,QString> mapping(QString m)const;
		/**returns the method for a specific mapping or an empty string if it does not exist in the specified language*/
		QString mapMethod(QString table,QString property,QString lang)const;
		
		/**returns documentation for this class*/
		QStringList docStrings()const{return m_docstrings;}
		/**returns documentation for a property*/
		QString propDoc(QString p)const
		{if(m_propdoc.contains(p))return m_propdoc[p];else return "";}
		/**returns base documentation for enums*/
		QString enumDoc(QString e)const
		{if(m_enumdoc.contains(e))return m_enumdoc[e];else return QString();}
		
	private:
		///valid: parsing the WOLF succeeded
		bool m_valid;
		///abstract: the class is declared abstract (isAbstract may return true even if this is false)
		bool m_abstract;
		///cabstract: conditional abstract - just for one or two languages; abstract overrides cabstract!
		QStringList m_cabstract;
		///class name without prefix
		QString m_name;
		///name of parent class ("wolf" => the "base" attributes' content)
		QMap<QString,QString>m_base;
		///property info
		struct s_prop{
			QString name,type;
			bool isid,isabstract;
		};
		QList<s_prop> m_props;
		///mappings: "table-name" => List of ("column-name","property-name")
		struct s_map{
			QString column,property;
			QMap<QString,QString>method;//lang->method
		};
		QMap<QString,QList<s_map> >m_maps;
		///enum types: "type-name" => List of ("constant-name",int-constant-value)
		QMap<QString,QList<WocEnum> >m_enumvals;
		///enum types: "type-name" => "documentation"
		QMap<QString,QString>m_enumdoc;

		///docu of class itself
		QStringList m_docstrings;
		///docu of properties
		QMap<QString,QString>m_propdoc;
		
		//helper: contains predefined types sorted by serialization type
		static const QStringList attrtypes,elemtypes;
};

#endif
