# Copyright (C) 2009-2018 by Konrad Rosenbaum <konrad@silmor.de>
# protected under the GNU GPL version 3 or at your option any newer.
# See COPYING.GPL file that comes with this distribution.

TEMPLATE=app
QT-=gui
QT+=xml
CONFIG+=console
CONFIG-=app_bundle

#FIXME: eliminate Qt5 compat dependencies
equals(QT_MAJOR_VERSION, 6): QT+=core5compat

CONFIG(prewoc, prewoc|woc){
	DEFINES += NO_PACK_VERSION_H COMPILE_PREWOC
	TARGET = ../woc/prewoc
	MYTMPDIR = .ptmp
}else{
	DEFINES += COMPILE_WOC
	TARGET = ../woc/woc
	MYTMPDIR = .ctmp
}

#compilation output:
DESTDIR = ../woc
OBJECTS_DIR = $$MYTMPDIR
MOC_DIR = $$MYTMPDIR
RCC_DIR = $$MYTMPDIR


SOURCES+= \
	woc.cpp \
	mfile.cpp \
	domquery.cpp
HEADERS+= \
	mfile.h \
	domquery.h

include(proc/proc.pri)
include(qt/qt.pri)
include(generic/generic.pri)
CONFIG(prewoc, prewoc|woc){
}else{
	SOURCES += htmlout.cpp
	HEADERS += htmlout.h
	include(php/php.pri)
	include(soap/soap.pri)
}

GRESOURCES+=pattern
include (qrcgen/qrcgen.pri)

CONFIG += c++11

INCLUDEPATH += . ../vinfo ../qtbase/include

#make sure dependencies are found
DEPENDPATH += $$INCLUDEPATH
