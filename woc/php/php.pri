SOURCES+= \
	$$PWD/phpout.cpp \
	$$PWD/phpclass.cpp \
	$$PWD/phptrans.cpp \
	$$PWD/phpctrans.cpp \
	$$PWD/phpstrans.cpp \
	$$PWD/phpdb.cpp
HEADERS+= \
	$$PWD/phpout.h \
	$$PWD/phpdb.h \
	$$PWD/phpclass.h \
	$$PWD/phptrans.h \
	$$PWD/phpctrans.h \
	$$PWD/phpstrans.h

INCLUDEPATH += $$PWD
