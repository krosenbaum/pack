// Copyright (C) 2009-2014 by Konrad Rosenbaum <konrad@silmor.de>
// protected under the GNU GPL version 3 or at your option any newer.
// See COPYING.GPL file that comes with this distribution.
//

#ifndef WOC_PHP_CONST_H
#define WOC_PHP_CONST_H

static const QByteArray PHPSTART("<?php\n//BEGIN OF AUTOMATICALLY GENERATED FILE\n//PLEASE DO NOT CHANGE!\n\n");
static const QByteArray PHPEND("\n//END OF AUTOMATICALLY GENERATED FILE\nreturn;\n?>");

#ifndef WOC_PHP_NO_WRAP
#define m_basedir	m_parent->m_basedir
#define m_subdir	m_parent->m_subdir
#define m_fileext	m_parent->m_fileext
#define m_transbase	m_parent->m_transbase
#define m_loader	m_parent->m_loader
#define m_schema	m_parent->m_schema
#define m_transact	m_parent->m_transact
#define className	m_parent->className
#define abstractClassName m_parent->abstractClassName
#define trnClassName	m_parent->trnClassName
#define addLoad		m_parent->addLoad
#define m_lang		m_parent->m_lang
#endif

#endif
