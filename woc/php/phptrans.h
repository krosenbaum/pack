// Copyright (C) 2009-2011 by Konrad Rosenbaum <konrad@silmor.de>
// protected under the GNU GPL version 3 or at your option any newer.
// See COPYING.GPL file that comes with this distribution.
//

#ifndef WOC_PHPOUT_TRANS_H
#define WOC_PHPOUT_TRANS_H

#include "phpout.h"


/**generates output for a PHP server side*/
class WocPHPTransaction:public QObject
{
	Q_OBJECT
	public:
		/**initializes the output object*/
		WocPHPTransaction(WocPHPOut*);
		/**writes any last words after parsing finished*/
		virtual void finalize();
		/**creates a transaction*/
		virtual void newTransaction(const WocTransaction&);
	private:

		/**helper: create info functions (mainly version info) at the start*/
		virtual void transInfo();
		/**helper: create info functions (mainly version info) at the end*/
		virtual void transInfo2();
		
		/**helper: create transaction constructor*/
		virtual QString trnConstruct(const WocTransaction&);
		/**helper: create handlers*/
		virtual QString trnHandlers(const WocTransaction&)=0;
		/**helper: create getters and setters*/
		virtual QString trnGetSet(const WocTransaction&);
		/**helper: create privilege check code for web interface*/
		virtual QString trnPrivileges(const WocTransaction&);
	protected:
		WocPHPOut*m_parent;
	signals:
		void errorFound();
};

#endif
