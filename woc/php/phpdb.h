// Copyright (C) 2009-2011 by Konrad Rosenbaum <konrad@silmor.de>
// protected under the GNU GPL version 3 or at your option any newer.
// See COPYING.GPL file that comes with this distribution.
//

#ifndef WOC_PHPOUT_DB_H
#define WOC_PHPOUT_DB_H

#include "phpout.h"

/**generates output for a PHP server side*/
class WocPHPTable:public QObject
{
	Q_OBJECT
	public:
		/**initializes the output object*/
		WocPHPTable(WocPHPOut*);
		/**writes any last words after parsing finished*/
		virtual void finalize();
		/**creates a class*/
		virtual void newTable(const WocTable&);
	private:
		WocPHPOut*m_parent;
	signals:
		void errorFound();
};

#endif
