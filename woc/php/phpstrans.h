// Copyright (C) 2009-2011 by Konrad Rosenbaum <konrad@silmor.de>
// protected under the GNU GPL version 3 or at your option any newer.
// See COPYING.GPL file that comes with this distribution.
//

#ifndef WOC_PHPOUT_STRANS_H
#define WOC_PHPOUT_STRANS_H

#include "phptrans.h"


/**generates output for a PHP server side*/
class WocPHPServerTransaction:public WocPHPTransaction
{
	public:
		/**initializes the output object*/
		WocPHPServerTransaction(WocPHPOut*);
	private:
		/**helper: create handlers*/
		virtual QString trnHandlers(const WocTransaction&);
		/**helper: create transaction input parser*/
		QString trnInput(const WocTransaction&);
		/**helper: create transaction output serializer*/
		QString trnOutput(const WocTransaction&);
		/**helper: create direct execution code for web interface*/
		QString trnExecute(const WocTransaction&);
};

#endif
