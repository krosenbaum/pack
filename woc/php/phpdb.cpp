// Copyright (C) 2009-2011 by Konrad Rosenbaum <konrad@silmor.de>
// protected under the GNU GPL version 3 or at your option any newer.
// See COPYING.GPL file that comes with this distribution.
//

#include "phpout.h"
#include "phpdb.h"

#include <QDir>

#include "phpconst.h"

static const QByteArray SCHEMASTART("class WobSchema extends WobSchemaBase\n{\nfunction __construct(){\n\tparent::__construct();\n");
static const QByteArray SCHEMAEND("}};\n");

WocPHPTable::WocPHPTable(WocPHPOut* p)
	:m_parent(p)
{
	connect(this,SIGNAL(errorFound()),p,SIGNAL(errorFound()));

	//create schema file
	m_schema.setFileName(m_basedir+"/"+m_subdir+"/schema"+m_fileext);
	if(!m_schema.open(QIODevice::ReadWrite|QIODevice::Truncate)){
		qDebug("Error: PHP Server Generator - cannot create DB schema file.");
		emit errorFound();
		return;
	}
	m_schema.write(PHPSTART);
	m_schema.write(SCHEMASTART);
	m_schema.write(("\tself::$sversion=\""+WocProcessor::instance()->dbVersion()+"\";\n").toLatin1());
	m_schema.write(("\tself::$config[\"table\"]=\""+WocProcessor::instance()->dbConfigTable()+"\";\n").toLatin1());
	m_schema.write(("\tself::$config[\"key\"]=\""+WocProcessor::instance()->dbConfigKeyColumn()+"\";\n").toLatin1());
	m_schema.write(("\tself::$config[\"value\"]=\""+WocProcessor::instance()->dbConfigValueColumn()+"\";\n").toLatin1());
	m_schema.write(("\tself::$config[\"vrow\"]=\""+WocProcessor::instance()->dbVersionRow()+"\";\n").toLatin1());
	m_parent->addStaticLoad("WobSchema","schema");
}

void WocPHPTable::finalize()
{
	if(m_schema.isOpen()){
		m_schema.write(SCHEMAEND);
		m_schema.write(("$"+WocProcessor::instance()->dbSchema()+"=new WobSchema();\n").toLatin1());
		m_schema.write(PHPEND);
		m_schema.close();
	}
}

void WocPHPTable::newTable(const WocTable&tbl)
{
	if(!m_loader.isOpen())return;
	WocProcessor *woc=WocProcessor::instance();
	//create table file
	QString fn=m_subdir+"/wt_"+tbl.name()+m_fileext;
	QFile tf(m_basedir+"/"+fn);
	if(!tf.open(QIODevice::ReadWrite|QIODevice::Truncate)){
		qDebug("Error: cannot create PHP table file %s.",fn.toLatin1().data());
		emit errorFound();
		return;
	}
	tf.write(PHPSTART);
	QString code="/* TRANSLATOR WT"+tbl.name()+" */\nclass WT"+tbl.name()+" extends "+tbl.baseClass()+"\n{\n";
	//initializer
	code+="protected function __construct(array $data,$isfromdb){parent::__construct($data,$isfromdb,\""+tbl.name()+"\");}\n\n";

	//static new instance for insert only
	code+="public static function newRow(array $data=array()){return new WT"+tbl.name()+"($data,false);}\n\n";

	//static get instance
	QStringList cols=tbl.columns();
	QStringList pcols=tbl.primaryColumns();
	 //header
	code+="public static function getFromDB(";
	for(int i=0;i<pcols.size();i++){
		if(i)code+=",";
		code+="$"+pcols[i];
	}
	QString dbi="$"+woc->dbInst();
	 //DB query
	code+="){\n\tglobal "+dbi+";\n\t$res="+dbi+"->select(\""+tbl.name()+"\",\"*\",\"";
	for(int i=0;i<pcols.size();i++){
		if(i)code+=" AND ";
		code+=pcols[i]+"=\"."+dbi+"->escapeColumn(\""+tbl.name()+"\",\""+pcols[i]+"\",$"+pcols[i]+").\"";
	}
	code+="\");\n";
	 //check result
	code+="\tif($res===false)return false;\n\tif(count($res)<1)return false;\n\telse return new WT"+tbl.name()+"($res[0],true);\n}\n\n";

	//static get selection
	code+="public static function selectFromDB($where=\"\",$orderby=\"\"){\n\tglobal "+dbi+";\n\t$res="+dbi+"->select(\""+tbl.name()+"\",\"*\",$where,$orderby);\n\tif($res===false || count($res)<1)return array();\n\t";
	code+="$r=array();\n\tforeach($res as $row)\n\t\t$r[]=new WT"+tbl.name()+"($row,true);\n\treturn $r;\n}\n\n";

	//go through columns, generate specific code
	for(int i=0;i<cols.size();i++){
		//automatic resolution of internal foreign keys
		if(tbl.columnIsForeign(cols[i])){
			code+="public function getObjectFor"+cols[i]+"(){\n\tglobal "+dbi+";\n\treturn WT";
			QStringList foreign=tbl.columnForeign(cols[i]).split(":");
			code+=foreign[0]+"::selectFromDB(\""+foreign[1]+"=\"."+dbi+"->escapeColumn(\""+foreign[0]+"\",\""+foreign[1]+"\",$this->"+cols[i]+"));\n}\n\n";
		}
		//implement enum check for set method of enum columns
		if(tbl.columnType(cols[i]).startsWith("enum")){
			code+="protected function verifyValue"+cols[i]+"($v){if(false";
			QList<WocEnum>ens=tbl.columnEnums(cols[i]);
			QList<int>envs;
			for(int j=0;j<ens.size();j++){
				int v=ens[j].val;
				if(envs.contains(v))continue;
				envs.append(v);
				code+="||$v=="+QString::number(v);
			}
			code+=")return true;else return false;}\n\n";
		}
	}

	//reverse resolution of configured foreign keys
	QStringList fs=tbl.foreigns();
	for(int i=0;i<fs.size();i++){
		QString via=tbl.foreignQuery(fs[i]);
		//parse via
		QStringList v1=via.split("=");
		if(v1.size()!=2){
			qDebug("Warning: Foreign clause %s of table %s has illegal syntax. Should be foreigntable:column=localcolumn.",fs[i].toLatin1().data(),tbl.name().toLatin1().data());
			continue;
		}
		QString local=v1[1].trimmed();
		QStringList foreign=v1[0].split(":");
		if(foreign.size()!=2){
			qDebug("Warning: Foreign clause %s of table %s has illegal syntax. Should be foreigntable:column=localcolumn.",fs[i].toLatin1().data(),tbl.name().toLatin1().data());
			continue;
		}
		code+="public function "+fs[i]+"(){\n\tglobal "+dbi+";\n\treturn WT"+foreign[0]+"::selectFromDB(\""+foreign[1]+"=\"."+dbi+"->escapeColumn(\"";
		code+=foreign[0]+"\",\""+foreign[1]+"\",$this->"+local+"));\n}\n\n";
	}

	//create enum constants
	QList<WocEnum>ens=tbl.getEnums();
	for(int i=0;i<ens.size();i++){
		code+="const "+ens[i].name+"="+QString::number(ens[i].val)+";\n";
	}

	//hasproperty function
	code+="public function hasProperty($p){switch($p){\n";
	for(int i=0;i<cols.size();i++)
		code+="\tcase \""+cols[i]+"\":\n";
	QStringList aps=tbl.auditColumns();
	for(int i=0;i<aps.size();i++)
		code+="\tcase \""+aps[i]+"\":\n";
	code+="\t\treturn true;\n\tdefault:return false;}}\n";

	//create audit stuff
	if(tbl.isAuditable()){
		code+="public function isAuditable(){return true;}\n";
		code+="private $audittable=null;\n";
		code+="protected function createAudit(){\n";
		code+="\tif($this->audittable!==null)\n";
		code+="\t\t$this->audittable->setFromArray($this->data);\n";
		code+="\telse\n";
		code+="\t$this->audittable=WT"+tbl.name()+"_audit::newRow($this->data);\n";
		code+="\treturn $this->audittable->insert();\n}\n";
		code+="protected function resetAudit(){$this->audittable=null;}\n";
	}

	//create newKey function
	code+="public function newKey(){\n\tparent::newKey();\n";
	for(int i=0;i<cols.size();i++){
		QString c=tbl.columnCall(cols[i],m_lang);
		if(c=="")continue;
		code+="\t$this->cdata[\""+cols[i]+"\"]="+c+";\n";
	}
	code+="}\n";

	//write table class
	code+="};\n";
	tf.write(code.toLatin1());
	tf.write(PHPEND);
	tf.close();

	//extend schema file
	//column definitions
	code="\tself::$scheme[\""+tbl.name()+"\"]=array(";
	for(int i=0;i<cols.size();i++){
		if(i)code+=",";
		code+="\n\t\t\""+cols[i]+"\"=>array(\"";
		code+=tbl.columnType(cols[i])+"\"";
		if(!tbl.columnIsNull(cols[i]))code+=",\"notnull\"";
		if(tbl.columnIsForeign(cols[i]))code+=",\"foreignkey:"+tbl.columnForeign(cols[i])+"\"";
		if(pcols.size()<2 && tbl.columnIsPrimary(cols[i]))code+=",\"primarykey\"";
		if(tbl.columnHasDefault(cols[i]))code+=",\"default:"+tbl.columnDefault(cols[i])+"\"";
		if(tbl.columnIsIndexed(cols[i]))code+=",\"index\"";
		if(tbl.columnIsUnique(cols[i]))code+=",\"unique\"";
		code+=")";
	}
	if(pcols.size()>=2){
		code+=",\n\t\t\":primarykey\"=>array(";
		for(int i=0;i<pcols.size();i++){
			if(i)code+=",";
			code+="\""+pcols[i]+"\"";
		}
		code+=")";
	}
	QStringList ucols=tbl.uniqueConstraints();
	if(ucols.size()>0){
		code+=",\n\t\t\":uniqueconstraint\"=>array(";
		for(int i=0;i<ucols.size();i++){
			if(i)code+=",";
			code+="\""+ucols[i]+"\"";
		}
		code+=")";
	}
	code+="\n\t);\n";
	if(tbl.inBackup()){
		code+="\tself::$backup[]=\""+tbl.name()+"\";\n";
		code+="\tself::$backupCfg[\""+tbl.name()+"\"]=array(\"key\"=>\""+tbl.backupKey()+"\",\"size\"=>"+QString::number(tbl.backupGroupSize())+");\n";
	}
	//write presets
	QList<QMap<QString,QString> >presets=tbl.presets();
	if(presets.size()>0){
		code+="\tself::$preset[\""+tbl.name()+"\"]=array(";
		for(int i=0;i<presets.size();i++){
			if(i)code+=",";
			code+="\n\t\tarray(";
			QStringList k=presets[i].keys();
			for(int j=0;j<k.size();j++){
				if(j)code+=",";
				code+="\""+k[j]+"\"=>"+presets[i][k[j]];
			}
			code+=")";
		}
		code+="\n\t);\n";
	}

	//write
	m_schema.write(code.toLatin1());

	//create autoloading
	addLoad("WT"+tbl.name(),"wt_"+tbl.name());
}
