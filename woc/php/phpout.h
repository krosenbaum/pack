// Copyright (C) 2009-2011 by Konrad Rosenbaum <konrad@silmor.de>
// protected under the GNU GPL version 3 or at your option any newer.
// See COPYING.GPL file that comes with this distribution.
//

#ifndef WOC_PHPOUT_H
#define WOC_PHPOUT_H

#include <QFile>

#include "processor.h"

class QDomElement;

class WocPHPOut;
class WocPHPClass;
class WocPHPTable;
class WocPHPTransaction;
class WocPHPClientTransaction;
class WocPHPServerTransaction;

/**Abstract base class for generating output for PHP.*/
class WocPHPOut:public WocOutput
{
	public:
		/**initializes the output object*/
		WocPHPOut(const QDomElement&);
		
		virtual ~WocPHPOut()=0;
	protected:
		/**writes any last words after parsing finished*/
		virtual void finalize();
		/**creates a class*/
		virtual void newClass(const WocClass&);
		/**creates a table*/
		virtual void newTable(const WocTable&);
		/**creates a transaction*/
		virtual void newTransaction(const WocTransaction&);

		friend class WocPHPClass;
		friend class WocPHPTable;
		friend class WocPHPTransaction;
		friend class WocPHPClientTransaction;
		friend class WocPHPServerTransaction;
		
		QString m_basedir,m_subdir,m_fileext;
		QString m_transbase,m_lang;
		QFile m_loader,m_schema,m_transact;
		
		/**helper: adds a loader line for a class to autoload.php*/
		void addLoad(QString classname,QString filename);
		/**helper: adds an immediate loader line for a class to autoload.php*/
		void addStaticLoad(QString classname,QString filename);

		WocPHPClass*pclass;
		WocPHPTable*ptable;
		WocPHPTransaction*ptrans;
		
		///helper: returns the class prefix
		QString classPrefix(){return "WO";}
		/**helper: return the PHP-class-name of a WocClass*/
		QString className(const WocClass&c){return "WO"+c.name();}
		/**helper: return the PHP-class-name of a WocClass plus Abstract if it is abstract*/
		QString abstractClassName(const WocClass&c){return "WO"+c.name()+QString(c.isAbstract(m_lang)?"Abstract":"");}
		
		/**helper: returns the PHP-class-name for a WocTransaction*/
		QString trnClassName(const WocTransaction&t){return "WTr"+t.name();}
};

///specialization that creates a PHP server
class WocPHPServerOut:public WocPHPOut
{
	public:
		/**initializes the output object*/
		WocPHPServerOut(const QDomElement&);
};

///specialization that creates a PHP client
class WocPHPClientOut:public WocPHPOut
{
	public:
		/**initializes the output object*/
		WocPHPClientOut(const QDomElement&);
};

#endif
