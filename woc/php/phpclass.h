// Copyright (C) 2009-2011 by Konrad Rosenbaum <konrad@silmor.de>
// protected under the GNU GPL version 3 or at your option any newer.
// See COPYING.GPL file that comes with this distribution.
//

#ifndef WOC_PHPOUT_CLASS_H
#define WOC_PHPOUT_CLASS_H


class WocPHPOut;
class WocClass;

#include <QObject>

/**generates output for a PHP server side*/
class WocPHPClass:public QObject
{
	Q_OBJECT
	public:
		/**initializes the output object*/
		WocPHPClass(WocPHPOut*);
		/**writes any last words after parsing finished*/
		virtual void finalize();
		/**creates a class*/
		virtual void newClass(const WocClass&);

	protected:
		WocPHPOut*m_parent;
		
		/**helper: generates PHP code to transform a class property to XML*/
		QString propertyToXml(const WocClass&,QString);
		/**helper: generate class constructor*/
		QString classConstruct(const WocClass&);
		/**helper: generate class internal enums*/
		QString classEnums(const WocClass&);
		/**helper: generate class internal props*/
		QString classProperties(const WocClass&);
		/**helper: generate class internal serializers*/
		QString classSerializers(const WocClass&);
		/**helper: generate class internal deserializers*/
		QString classDeserializers(const WocClass&);
		/**helper: generate class internal mappers*/
		QString classMappings(const WocClass&);
		/**helper: generate property validator*/
		QString classPropertyValidator(const WocClass&,QString);
		/**helper: generate getters for list properties*/
		QString classPropertyListGetters(const WocClass&,QString);
		/**helper: generate setters for list properties*/
		QString classPropertyListSetters(const WocClass&,QString);
		/**helper: generate getters for scalar properties*/
		QString classPropertyScalarGetters(const WocClass&,QString);
		/**helper: generate setters for sclar properties*/
		QString classPropertyScalarSetters(const WocClass&,QString);
		/**helper: generate the property to array converter, eg. for renderers like for Twig*/
		QString classPropertiesList(const WocClass&);
	signals:
		void errorFound();
};

#endif
