// Copyright (C) 2009-2011 by Konrad Rosenbaum <konrad@silmor.de>
// protected under the GNU GPL version 3 or at your option any newer.
// See COPYING.GPL file that comes with this distribution.
//

#ifndef WOC_PHPOUT_CTRANS_H
#define WOC_PHPOUT_CTRANS_H

#include "phptrans.h"


/**generates output for a PHP server side*/
class WocPHPClientTransaction:public WocPHPTransaction
{
	public:
		/**initializes the output object*/
		WocPHPClientTransaction(WocPHPOut*);
	private:
		/**helper: create transaction constructor*/
		QString trnHandlers(const WocTransaction&);
		/**helper: create transaction input parser*/
		QString trnInput(const WocTransaction&);
		/**helper: create transaction output serializer*/
		QString trnOutput(const WocTransaction&);
};

#endif
