// Copyright (C) 2009-2011 by Konrad Rosenbaum <konrad@silmor.de>
// protected under the GNU GPL version 3 or at your option any newer.
// See COPYING.GPL file that comes with this distribution.
//

#include "phpout.h"
#include "phpdb.h"
#include "phpctrans.h"
#include "phpstrans.h"
#include "phpclass.h"

#include <QDir>
#include <QDomElement>

#define WOC_PHP_NO_WRAP
#include "phpconst.h"

static const QByteArray SCHEMASTART("class WobSchema extends WobSchemaBase\n{\nfunction __construct(){\n\tparent::__construct();\n");
static const QByteArray SCHEMAEND("}};\n");

WocPHPOut::~WocPHPOut(){}

WocPHPServerOut::WocPHPServerOut(const QDomElement&el)
	:WocPHPOut(el)
{
	m_lang="php/server";
	pclass=new WocPHPClass(this);
	ptable=new WocPHPTable(this);
	ptrans=new WocPHPServerTransaction(this);
}

WocPHPClientOut::WocPHPClientOut(const QDomElement& el)
	:WocPHPOut(el)
{
	m_lang="php/client";
	pclass=new WocPHPClass(this);
	ptrans=new WocPHPClientTransaction(this);
}


WocPHPOut::WocPHPOut(const QDomElement&el)
{
	pclass=0;ptable=0;ptrans=0;
	qDebug("Info: creating PHP Server Output Generator.");
	m_basedir=WocProcessor::instance()->baseDir()+"/"+el.attribute("sourceDir",".");
	m_subdir=el.attribute("subDir","phpwob");
	m_fileext=el.attribute("extension",".inc");
	m_transbase=el.attribute("transactionBase","WobTransaction");
	//cleanup directory (remove normal files, assume remainder is harmless)
	QDir d(m_basedir+"/"+m_subdir);
	if(d.exists() && str2bool(el.attribute("clean","0"))){
		QStringList ent=d.entryList(QDir::Files);
		for(int i=0;i<ent.size();i++)
			d.remove(ent[i]);
	}
	//get/create directory
	if(!d.exists())QDir(".").mkpath(m_basedir+"/"+m_subdir);
	//create loader
	m_loader.setFileName(m_basedir+"/"+m_subdir+"/autoload"+m_fileext);
	if(!m_loader.open(QIODevice::ReadWrite|QIODevice::Truncate)){
		qDebug("Error: PHP Server Generator - cannot create loader file, deactivating output.");
		emit errorFound();
		return;
	}
	m_loader.write(PHPSTART);
	m_loader.write(QString("$d=dirname(__FILE__);\n").toLatin1());
}


void WocPHPOut::finalize()
{
	if(pclass)pclass->finalize();
	if(ptable)ptable->finalize();
	if(ptrans)ptrans->finalize();
	if(m_loader.isOpen()){
		m_loader.write(PHPEND);
		m_loader.close();
	}
}


void WocPHPOut::addStaticLoad(QString cn,QString fn)
{
	Q_UNUSED(cn);
	QString ld="include($d.\"/"+fn+m_fileext+"\");\n";
	m_loader.write(ld.toLatin1());
}

void WocPHPOut::addLoad(QString cn,QString fn)
{
	QString ld="wob_autoclass(\""+cn+"\",$d.\"/"+fn+m_fileext+"\");\n";
	m_loader.write(ld.toLatin1());
}

void WocPHPOut::newTransaction(const WocTransaction&trn)
{
	if(ptrans)ptrans->newTransaction(trn);
}

void WocPHPOut::newClass(const WocClass& cls)
{
	if(pclass)pclass->newClass(cls);
}

void WocPHPOut::newTable(const WocTable& tbl)
{
	if(ptable)ptable->newTable(tbl);
}
