// Copyright (C) 2009-2011 by Konrad Rosenbaum <konrad@silmor.de>
// protected under the GNU GPL version 3 or at your option any newer.
// See COPYING.GPL file that comes with this distribution.
//

#include "phpout.h"
#include "phpstrans.h"

#include <QDir>
#include <QDomElement>

#include "phpconst.h"

WocPHPServerTransaction::WocPHPServerTransaction(WocPHPOut*p )
	:WocPHPTransaction(p)
{
}

QString WocPHPServerTransaction::trnHandlers(const WocTransaction&trn)
{
	WocProcessor*woc=WocProcessor::instance();
	//create file
	QString cn=trnClassName(trn);
	QString fn="wtr_"+trn.name();
	////
	//generate code
	QString code;
	//request handler:
	code="public function handleRequest(){\n";
	
	//parse headers, get XML
	code+="\t/*low level XML parsing*/\n";
	code+="\t$xml=$this->getRequestXml();\n";
	
	//get DB transaction started
	code+="\t$this->startTransaction("+QString(trn.isDbUpdating()?"true":"false")+");\n";
	
	//security handling
	switch(trn.authMode()){
		case WocTransaction::Checked:
			code+="\t/*security check: authenticated and authorized*/\n";
			code+="\tif(!$this->isAuthorized(\""+trn.name()+"\""+"))$this->notAuthenticated();\n";
			break;
		case WocTransaction::Auth:
			code+="\t/*security check: authenticated*/\n";
			code+="\tif(!$this->isAuthenticated())$this->notAuthenticated();\n";
			break;
		default:
			code+="\t/*no security check, open function*/\n";
			break;//none
	}
	
	//Wob Encoding: parse low level XML and header
	code+="\t/*get body element*/\n";
	code+="\t$root=$xml->documentElement;\n";
	if(woc->messageEncoding()==WocProcessor::Soap12Encoding){
		code+="\t$list=$root->getElementsByTagName(\"Body\");\n";
		code+="\tif($list->length<1)throw new WobSoapTransactionError(tr(\"SOAP: missing body\").\"soap\");\n";
		code+="\t$root=$list->item(0);\n";
		code+="\t$list=$root->childNodes;$root=false;\n";
		code+="\tforeach($list as $e){\n";
		code+="\t\tif($e->nodeType!=XML_ELEMENT_NODE)continue;\n";
		code+="\t\tif($e->localName==\"WobRequest-"+trn.name()+"\"){$root=$e;break}\n\t}\n";
		code+="\tif($root===false)throw new WobSoapTransactionError(tr(\"SOAP: missing message content\").\"soap\");\n";
	}
	
	//parse inputs
	code+=trnInput(trn);
	
	//call
	if(trn.hasCall(m_lang)){
		code+="\t/*call actual functionality:*/\n";
		code+="\ttry{"+trn.callFunction(m_lang)+"}catch(Exception $e){$this->handleException($e);}\n";
	}else{
		code+="\t/*normally here would be the PHP call, but it is missing from the config*/\n";
		code+="\t$this->abortNotImplemented();\nreturn;\n";
		qDebug("Warning: transaction %s does not have a PHP call!",trn.name().toLatin1().data());
	}
	
	//encode outputs/handle errors
	code+=trnOutput(trn);
	
	code+="\t$this->commitTransaction();\n}\n";
	
	//direct execution
	code+=trnExecute(trn).toLatin1();

	return code;
}

QString WocPHPServerTransaction::trnInput(const WocTransaction&trn)
{
	QString code="\t/*start of input parsing*/\n";
	code+="\ttry{\n";
	QStringList sl=trn.inputNames();
	for(int i=0;i<sl.size();i++){
		QString t=trn.inputType(sl[i]);
		if(trn.isAttributeType(t)){
			code+="\t\t$this->ainput[\""+sl[i]+"\"]=";
			if(trn.isBoolType(t))code+="WObject::fromXmlBoolean(";
			code+="$root->getAttribute(\""+sl[i]+"\")";
			if(trn.isIntType(t))code+="+0";
			if(trn.isBoolType(t))code+=")";
			code+=";\n";
		}else{
			if(trn.isListType(t)){
				QString pt=trn.plainType(t);
				code+="\t\tforeach(WObject::elementsByTagName($root,\""+sl[i]+"\") as $el){\n";
				if(trn.isObjectType(t)){
					code+="\t\t\t$this->ainput[\""+sl[i]+"\"][]=WO"+pt+"::fromXml($xml,$el);\n";
				}else if(trn.isBlobType(t)){
					code+="\t\t\t$this->ainput[\""+sl[i]+"\"][]=base64_decode($el->textContent);\n";
				}else{
					code+="\t\t\t$this->ainput[\""+sl[i]+"\"][]=";
					if(trn.isBoolType(t))code+="WObject::fromXmlBoolean(";
					code+="$el->textContent";
					if(trn.isIntType(t))code+="+0";
					if(trn.isBoolType(t))code+=")";
					code+=";\n";
				}
				code+="\t\t}\n";
			}else{
				code+="\t\tforeach(WObject::elementsByTagName($root,\""+sl[i]+"\") as $el){\n";
				if(trn.isObjectType(t)){
					code+="\t\t\t$this->ainput[\""+sl[i]+"\"]=WO"+t+"::fromXml($xml,$el);\n";
				}else if(trn.isBlobType(t)){
					code+="\t\t\t$this->ainput[\""+sl[i]+"\"]=base64_decode($el->textContent);\n";
				}else{
					code+="\t\t\t$this->ainput[\""+sl[i]+"\"]=$el->textContent";
					if(trn.isIntType(t))code+="+0";
					code+=";\n";
				}
				code+="\t\t}\n";
			}
		}
	}
	code+="\t}catch(Exception $e){$this->handleException($e);}\n";
	code+="\t/*end of input parsing*/\n";
	return code;
}

QString WocPHPServerTransaction::trnOutput(const WocTransaction&trn)
{
	QStringList sl=trn.outputNames();
	QString code="\t/*start of output encoding*/\n";
	code+="\ttry{\n\t\t$xmla=$this->xmlCreate(\"WobResponse-"+trn.name()+"\");\n";
	code+="\t\t$xml=$xmla['doc'];\n\t\t$root=$xmla['root'];\n";
	for(int i=0;i<sl.size();i++){
		QString t=trn.outputType(sl[i]);
		if(trn.isAttributeType(t)){
			code+="\t\t$root->setAttribute(\""+sl[i]+"\",$this->aoutput[\""+sl[i]+"\"]);\n";
		}else{
			if(trn.isListType(t)){
				if(trn.isObjectType(t)){
					code+="\t\tforeach($this->aoutput[\""+sl[i]+"\"] as $o)\n";
					code+="\t\t\tif(is_a($o,\"WO"+trn.plainType(t)+"\"))\n";
					code+="\t\t\t\t$root->appendChild($o->toXml"+trn.typeSerializer(t)+"($xml,\""+sl[i]+"\"));\n";
				}else if(trn.isBlobType(t)){
					code+="\t\tforeach($this->aoutput[\""+sl[i]+"\"] as $v)\n";
					code+="\t\t\t$root->appendChild($xml->createElement(\""+sl[i]+"\",base64_encode($v)));\n";
				}else{
					code+="\t\tforeach($this->aoutput[\""+sl[i]+"\"] as $v)\n";
					code+="\t\t\t$root->appendChild($xml->createElement(\""+sl[i]+"\",xq($v)));\n";
				}
			}else{
				if(trn.isObjectType(t)){
					code+="\t\t$o=$this->aoutput[\""+sl[i]+"\"];\n";
					code+="\t\tif(is_a($o,\"WO"+trn.plainType(t)+"\"))\n";
					code+="\t\t\t$root->appendChild($o->toXml"+trn.typeSerializer(t)+"($xml,\""+sl[i]+"\"));\n";
				}else if(trn.isBlobType(t)){
					code+="\t\t$root->appendChild($xml->createElement(\""+sl[i]+"\",base64_encode($this->aoutput[\""+sl[i]+"\"])));\n";
				}else{
					code+="\t\t$root->appendChild($xml->createElement(\""+sl[i]+"\",xq($this->aoutput[\""+sl[i]+"\"])));\n";
				}
			}
		}
	}
	code+="\t\theader(\"X-WobResponse-Status: Ok\");\n";
	code+="\t\tprint($this->xmlToString($xmla));\n";
	code+="\t}catch(Exception $e){$this->handleException($e);}\n";
	code+="\t/*end of output*/\n";
	return code;
}

QString WocPHPServerTransaction::trnExecute(const WocTransaction&trn)
{
	//the execute function is split into a static and non-static part,
	//since the code from the wolf file assumes to be inside the instance
	QStringList in=trn.inputNames();
	//static wrapper for execution
	QString code="static public function execute(";
	for(int i=0;i<in.size();i++){
		if(i)code+=",";
		code+="$"+in[i];
	}
	code+=")\n{\n\t$inst=new "+trnClassName(trn)+";\n";
	code+="\t$inst->do_execute(";
	for(int i=0;i<in.size();i++){
		if(i)code+=",";
		code+="$"+in[i];
	}
	code+=");\n";
	code+="\treturn $inst;\n}\n";
	//actual non-static implementation
	code+="private function do_execute(";
	for(int i=0;i<in.size();i++){
		if(i)code+=",";
		code+="$"+in[i];
	}
	code+=")\n{\n";
	for(int i=0;i<in.size();i++)
		code+="\t$this->ainput[\""+in[i]+"\"]=$"+in[i]+";\n";
	code+="\tself::$running=\""+trn.name()+"\";\n";
	code+="\ttry{"+trn.callFunction(m_lang)+"}\n\tcatch(Exception $ex){\n";
	code+="\t\tself::$running=\"\";\n\t\t$this->handleException($ex);\n\t}\n";
	code+="\tself::$running=\"\";\n}\n";
	return code;
}
