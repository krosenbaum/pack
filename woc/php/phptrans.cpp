// Copyright (C) 2009-2011 by Konrad Rosenbaum <konrad@silmor.de>
// protected under the GNU GPL version 3 or at your option any newer.
// See COPYING.GPL file that comes with this distribution.
//

#include "phpout.h"
#include "phpctrans.h"

#include <QDir>
#include <QDomElement>

#include "phpconst.h"

static const QByteArray TRANSACTCLASS("class WobTransaction extends WobTransactionBase\n{\n");
static const QByteArray TRANSACTSTART("  static public function handle(){\n    try{switch(self::getTransactionName()){\n");
static const QByteArray TRANSACTEND("\tdefault:WobTransactionBase::noSuchTransaction();break;\n    }}catch(TransactionError $er){$er->printXml();}\n  }\n");

WocPHPTransaction::WocPHPTransaction(WocPHPOut*p ): QObject(p)
{
	m_parent=p;
	connect(this,SIGNAL(errorFound()),p,SIGNAL(errorFound()));
	//create Transaction file
	m_transact.setFileName(m_basedir+"/"+m_subdir+"/transaction"+m_fileext);
	if(!m_transact.open(QIODevice::ReadWrite|QIODevice::Truncate)){
		qDebug("Error: PHP Server Generator - cannot create transaction processor file.");
		emit errorFound();
		return;
	}
	m_transact.write(PHPSTART);
	m_transact.write(TRANSACTCLASS);
	transInfo();
	m_transact.write(TRANSACTSTART);
	addLoad("WobTransaction","transaction");
}

void WocPHPTransaction::transInfo()
{
	WocProcessor*woc=WocProcessor::instance();
	QString code;
	code+="  static public function commVersion(){return \""+woc->verComm()+"\";}\n";
	code+="  static public function needCommVersion(){return \""+woc->verNeedComm()+"\";}\n";
	code+="  static public function version(){return \""+woc->verHR()+"\";}\n";
#warning "TODO: add version info to PHP generator"
// 	code+="  static public function svnVersion(){return \""+woc->svnRevision()+"\";}\n";
// 	code+="  static public function svnRepositoryRoot(){return \""+woc->svnRepositoryRoot()+"\";}\n";
// 	code+="  static public function svnRepositoryUrl(){return \""+woc->svnRepositoryUrl()+"\";}\n";
	code+="\n";
	code+="  static public function xmlProjectNamespace(){return \""+woc->xmlProjectNamespace()+"\";}\n";
	code+="  static public function xmlPackNamespace(){return \""+woc->xmlPackNamespace()+"\";}\n";
	code+="  static public function xmlSoap12Namespace(){return \""+woc->xmlSoap12Namespace()+"\";}\n";
	code+="  static public function xmlSchemaNamespace(){return \""+woc->xmlSchemaNamespace()+"\";}\n";
	code+="  static public function xmlXmlNamespace(){return \""+woc->xmlXmlNamespace()+"\";}\n";
	code+="\n";
	code+="  static public function messageEncoding(){return "+ 
		QString::number((int)woc->messageEncoding())+";}\n";
	code+="\n";
	code+="  static public function getTransactionName(){return parent::getTransactionName";
	switch(woc->messageEncoding()){
		case WocProcessor::Soap12Encoding:code+="Soap12";break;
		default:code+="Wob";break;
	}
	code+="();}\n\n";
	m_transact.write(code.toLatin1());
	
}

void WocPHPTransaction::transInfo2()
{
	WocProcessor*woc=WocProcessor::instance();
	//transaction names
	QString code="  static public function transactionNames(){\n\treturn array(";
	QStringList tns=woc->transactionNames();
	for(int i=0;i<tns.size();i++){
		if(i)code+=",";
		code+="\n\t\ttranslate(\"_TransactionNames\",\""+tns[i]+"\")";
	}
	code+=");\n  }\n";
	//privilege names
	code+="static public function privilegeNames(){\n\treturn array(";
	QStringList priv=woc->privilegeNames();
	for(int i=0;i<priv.size();i++){
		if(i)code+=",\n\t\t";else code+="\n\t\t";
		code+="translate(\"_PrivilegeNames\",\""+priv[i]+"\")";
	}
	code+="\n\t);\n}\n";
	
	m_transact.write(code.toLatin1());
}

void WocPHPTransaction::finalize()
{
	if(m_transact.isOpen()){
		m_transact.write(TRANSACTEND);
		transInfo2();
		m_transact.write("};\n");
		m_transact.write(PHPEND);
		m_transact.close();
	}
}


void WocPHPTransaction::newTransaction(const WocTransaction&trn)
{
	//create file
	QString cn=trnClassName(trn);
	QString fn="wtr_"+trn.name();
	addLoad(cn,fn);
	fn=m_subdir+"/"+fn+m_fileext;
	QFile tf(m_basedir+"/"+fn);
	if(!tf.open(QIODevice::ReadWrite|QIODevice::Truncate)){
		qDebug("Error: cannot create PHP object file %s.",fn.toLatin1().data());
		emit errorFound();
		return;
	}
	tf.write(PHPSTART);
	
	////
	//generate code
	QString code="/* TRANSLATOR "+cn+" */\nclass "+cn+" extends "+m_transbase+"{\n";
	//constructor
	code+=trnConstruct(trn);
	tf.write(code.toLatin1());
	
	//request handler:
	tf.write(trnHandlers(trn).toLatin1());

	//getters/setters
	tf.write(trnGetSet(trn).toLatin1());
	
	//privileges
	tf.write(trnPrivileges(trn).toLatin1());
	
	//end
	code="\n//end of class\n}\n";
	tf.write(code.toLatin1());
	tf.write(PHPEND);
	tf.close();
	
	////
	//generate loader code
	code="\tcase \""+trn.name()+"\":$trn=new "+cn+";$trn->handleRequest();break;\n";
	m_transact.write(code.toLatin1());
}

QString WocPHPTransaction::trnConstruct(const WocTransaction&trn)
{
	QString code="public function __construct(){\n\tparent::__construct();\n";
	code+="\t$this->ainput=array(";
	QStringList sl=trn.inputNames();
	for(int i=0;i<sl.size();i++){
		if(i)code+=",";
		code+="\""+sl[i]+"\"=>";
		if(trn.isListType(trn.inputType(sl[i])))code+="array()";
		else code+="false";
	}
	code+=");\n";
	code+="\t$this->tinput=array(";
	for(int i=0;i<sl.size();i++){
		if(i)code+=",";
		code+="\""+sl[i]+"\"=>\"";
		code+=trn.inputType(sl[i]);
		code+="\"";
	}
	code+=");\n";
	
	
	code+="\t$this->aoutput=array(";
	sl=trn.outputNames();
	for(int i=0;i<sl.size();i++){
		if(i)code+=",";
		code+="\""+sl[i]+"\"=>";
		if(trn.isListType(trn.outputType(sl[i])))code+="array()";
		else code+="false";
	}
	code+=");\n";
	code+="\t$this->toutput=array(";
	for(int i=0;i<sl.size();i++){
		if(i)code+=",";
		code+="\""+sl[i]+"\"=>\"";
		code+=trn.outputType(sl[i]);
		code+="\"";
	}
	code+=");\n}\n";
	return code;
}

QString WocPHPTransaction::trnGetSet(const WocTransaction&trn)
{
	QString code;
	//getters
	QStringList sl=trn.inputNames();
	for(int i=0;i<sl.size();i++){
		code+="public function get"+sl[i]+"(){return $this->ainput[\""+sl[i]+"\"];}\n";
	}
	//setters
	sl=trn.outputNames();
	for(int i=0;i<sl.size();i++){
		code+="public function result"+sl[i]+"(){return $this->aoutput[\""+sl[i]+"\"];}\n";
		QString add;
		QString t=trn.outputType(sl[i]);
		code+="public function set"+sl[i]+"($v){\n";
		if(trn.isListType(t)){
			add="public function add"+sl[i]+"($vv){\n";
			code+="\t$this->aoutput[\""+sl[i]+"\"]=array();\n";
			code+="\tforeach($v as $vv){\n";
			if(trn.isIntType(t)){
				code+="\t\tif(is_numeric($vv))$this->aoutput[\""+sl[i]+"\"][]=$vv+0;\n";
				add+="\tif(is_numeric($vv))$this->aoutput[\""+sl[i]+"\"][]=$vv+0;\n";
			}else
			if(trn.isBoolType(t)){
				code+="\t\tif(is_bool($vv))$this->aoutput[\""+sl[i]+"\"][]=$vv!=false;\n";
				add+="\tif(is_bool($vv))$this->aoutput[\""+sl[i]+"\"][]=$vv!=false;\n";
			}else
			if(trn.isObjectType(t)){
				code+="\t\tif(is_a($vv,\"WO"+trn.plainType(t)+"\"))$this->aoutput[\""+sl[i]+"\"][]=$vv;\n";
				add+="\tif(is_a($vv,\"WO"+trn.plainType(t)+"\"))$this->aoutput[\""+sl[i]+"\"][]=$vv;\n";
			}else{
				code+="\t\t$this->aoutput[\""+sl[i]+"\"][]=\"\".$vv;\n";
				add+="\t$this->aoutput[\""+sl[i]+"\"][]=\"\".$vv;\n";
			}
			code+="\t}\n";
			add+="}\n";
		}else{
			if(trn.isIntType(t)){
				code+="\tif(is_numeric($v))$this->aoutput[\""+sl[i]+"\"]=$v+0;\n";
			}else
			if(trn.isBoolType(t)){
				code+="\tif(is_bool($v))$this->aoutput[\""+sl[i]+"\"]=$v!=false;\n";
			}else
			if(trn.isObjectType(t)){
				code+="\tif(is_a($v,\"WO"+trn.plainType(t)+"\"))$this->aoutput[\""+sl[i]+"\"]=$v;\n";
			}else{
				code+="\t$this->aoutput[\""+sl[i]+"\"]=\"\".$v;\n";
			}
		}
		code+="}\n"+add;
	}
	
	
	return code;
}

QString WocPHPTransaction::trnPrivileges(const WocTransaction&trn)
{
	//privilege inventory
	QString code;
	code+="static public function privileges(){\n\treturn array(";
	QString cn=trnClassName(trn);
	QStringList priv=trn.privileges();
	for(int i=0;i<priv.size();i++){
		if(i)code+=",\n\t\t";else code+="\n\t\t";
		code+="\""+priv[i]+"\"";
	}
	code+="\n\t);\n}\n";
	//constants for use by custom code
	for(int i=0;i<priv.size();i++)
		code+="const Priv_"+priv[i]+"=\""+priv[i]+"\";\n";
	//check method
	code+="public function havePrivilege($priv){\n";
	code+="\tif(!in_array($priv,self::privileges()))return false;\n";
	code+="\treturn $this->isAuthorized(\""+trn.name()+":\".$priv);\n}\n";
	
	return code;
}
