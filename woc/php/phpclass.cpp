// Copyright (C) 2009-2011 by Konrad Rosenbaum <konrad@silmor.de>
// protected under the GNU GPL version 3 or at your option any newer.
// See COPYING.GPL file that comes with this distribution.
//

#include "phpout.h"
#include "phpclass.h"

#include <QDir>
#include <QDomElement>

#include "phpconst.h"

WocPHPClass::WocPHPClass(WocPHPOut* p)
{
	m_parent=p;
	connect(this,SIGNAL(errorFound()),p,SIGNAL(errorFound()));
}

void WocPHPClass::finalize()
{
}

void WocPHPClass::newClass(const WocClass&cls)
{
	//cover basics
	QString cn=className(cls);
	QString cna=abstractClassName(cls);
	QString fn="wo_"+cls.name();
	addLoad(cna,fn);
	fn=m_subdir+"/"+fn+m_fileext;
	QFile tf(m_basedir+"/"+fn);
	if(!tf.open(QIODevice::ReadWrite|QIODevice::Truncate)){
		qDebug("Error: cannot create PHP object file %s.",fn.toLatin1().data());
		emit errorFound();
		return;
	}
	tf.write(PHPSTART);
	
	////
	//generate code
	QString code="/* TRANSLATOR "+cna+" */\nclass "+cna+" extends "+
		cls.baseClass(m_lang,m_parent->classPrefix())+
		"{\n\n";
	tf.write(code.toLatin1());
	
	//property declaration and constructor
	tf.write(classConstruct(cls).toLatin1());
	
	//enums
	tf.write(classEnums(cls).toLatin1());
	
	//properties
	tf.write(classProperties(cls).toLatin1());
	
	//mappings
	tf.write(classMappings(cls).toLatin1());
	
	//serializers
	tf.write(classSerializers(cls).toLatin1());
	
	//de-serializer
	tf.write(classDeserializers(cls).toLatin1());
	
	//prop to array conversion
	tf.write(classPropertiesList(cls).toLatin1());
	
	//end of class
	code="\n//end of class\n};\n";
	tf.write(code.toLatin1());
	
	tf.write(PHPEND);
	tf.close();
}

QString WocPHPClass::classConstruct(const WocClass&cls)
{
	QString code;
	QStringList k=cls.propertyNames();
	for(int i=0;i<k.size();i++){
		code+="protected $prop_"+k[i];
		if(!cls.propertyIsList(k[i]))code+="=null";
		code+=";\n";
	}
	code+="public function __construct()\n{\n\tparent::__construct();\n";
	for(int i=0;i<k.size();i++)
		if(cls.propertyIsList(k[i]))
			code+="\t$this->prop_"+k[i]+"=array();\n";
	code+="}\n";
	return code;
}

QString WocPHPClass::classEnums(const WocClass&cls)
{
	QString code;
	QStringList k=cls.enumTypes();
	for(int i=0;i<k.size();i++){
		code+="//enum "+k[i]+"\n";
		QList<WocEnum>ev=cls.enumValues(k[i]);
		for(int j=0;j<ev.size();j++)
			code+="const "+ev[j].name+"="+QString::number(ev[j].val)+";\n";
	}
	return code;
}

QString WocPHPClass::classProperties(const WocClass&cls)
{
	QString code;
	QStringList k=cls.propertyNames();
	for(int i=0;i<k.size();i++){
		code+="\n";
		//generate validator
		code+=classPropertyValidator(cls,k[i]);
		//generic getter
		code+="public function get"+k[i]+"(){return $this->prop_"+k[i]+";}\n";
		//is it a list?
		if(cls.propertyIsList(k[i])){
			//lists...
			//getters
			code+=classPropertyListGetters(cls,k[i]);
			//setters
			code+=classPropertyListSetters(cls,k[i]);
		}else{
			//non-lists...
			//getters
			code+=classPropertyScalarGetters(cls,k[i]);
			//setter
			code+=classPropertyScalarSetters(cls,k[i]);
		}
	}
	
	return code;
}

QString WocPHPClass::classPropertiesList(const WocClass&cls)
{
	QString code;
	QStringList k=cls.propertyNames();
	code+="public function propertyArray(){\n\treturn array_merge(parent::propertyArray(),array(";
	for(int i=0;i<k.size();i++){
		if(i)code+=",";
		code+="\n\t\t\""+k[i]+"\"=>";
		//is it a list?
		if(cls.propertyIsList(k[i])){
			//lists...
			//object?
			if(cls.propertyIsObject(k[i]))
				code+="array_map(\"WObject::objectToArray\",$this->prop_"+k[i]+")";
			else
				code+="$this->prop_"+k[i];
		}else{
			//non-lists...
			//object?
			if(cls.propertyIsObject(k[i]))
				code+="WObject::objectToArray($this->prop_"+k[i]+")";
			else
				code+="$this->prop_"+k[i];
		}
	}
	code+="));\n}\n";
	
	return code;
}

QString WocPHPClass::classPropertyValidator(const WocClass&cls,QString prop)
{
	QString code;
	code+="static public function validate"+prop+"($value){\n";
	if(cls.propertyIsEnum(prop)){
		QList<WocEnum>ev=cls.enumValues(cls.propertyPlainType(prop));
		code+="\tif(is_numeric($value)){\n\t\t$value=$value+0;\n";
		for(int j=0;j<ev.size();j++){
			code+="\t\tif($value=="+QString::number(ev[j].val)+")return true;\n";
		}
		code+="\t\treturn false;\n\t}else{\n";
		for(int j=0;j<ev.size();j++){
			code+="\t\tif($value==\""+ev[j].name+"\")return true;\n";
		}
		code+="\t\treturn false;\n\t}\n";
	}else
	if(cls.propertyIsInt(prop))
		code+="\treturn is_numeric($value);\n";
	else
	if(cls.propertyIsString(prop)||cls.propertyIsBlob(prop))
		code+="\treturn true;\n";//TODO: special handling for astring
	else
	if(cls.propertyIsBool(prop))
		code+="\treturn is_bool($value);\n";
	else
	if(cls.propertyIsObject(prop))
		code+="\treturn is_a($value,\"WO"+cls.propertyPlainType(prop)+"\");\n";
	else{
		qDebug("Warning: unable to generate validator for class %s property %s: unknown type.",cls.name().toLatin1().data(),prop.toLatin1().data());
		code+="\treturn false;\n";
	}
	code+="}\n";
	
	return code;
}

QString WocPHPClass::classPropertyListGetters(const WocClass&cls,QString prop)
{
	QString code;
	if(cls.propertyIsString(prop))
		code+="public function getstrlist_"+prop+"(){return $this->prop_"+prop+";}\n";
	else
	if(cls.propertyIsBlob(prop)){
		code+="public function getstrlist_"+prop+"(){\n\t$ret=array();\n";
		code+="\tforeach($this->prop_"+prop+" as $p)$ret[]=base64_encode($this->prop_"+prop+");";
		code+="\treturn $ret;\n}\n";
	}else
	if(cls.propertyIsInt(prop)){
		code+="public function getstrlist_"+prop+"(){\n";
		code+="\t$ret=array();\n\tforeach($this->prop_"+prop+" as $p)$ret[]=\"\".$p;\n";
		code+="\treturn $ret;\n}\n";
	}else
	if(cls.propertyIsBool(prop)){
		code+="public function getstrlist_"+prop+"(){\n";
		code+="\t$ret=array();\n\tforeach($this->prop_"+prop+" as $p)$ret[]=$p?\"true\":\"false\";\n";
		code+="\treturn $ret;\n}\n";
	}else
	if(cls.propertyIsEnum(prop)){
		code+="public function getstrlist_"+prop+"(){\n";
		code+="\t$ret=array();\n";
		code+="\tforeach($this->prop_"+prop+" as $p)switch($p){\n";
		QList<WocEnum> ev=cls.enumValues(cls.propertyPlainType(prop));
		for(int j=0;j<ev.size();j++){
			code+="\t\tcase "+QString::number(ev[j].val)+":$ret[]=\""+ev[j].name+"\";break;\n";
		}
		code+="\t\tdefault:$ret[]=null;break;\n\t}\n\treturn $ret;\n}\n";
	}
	
	return code;
}

QString WocPHPClass::classPropertyListSetters(const WocClass&cls,QString prop)
{
	QString code;
	code+="public function clear_"+prop+"(){$this->prop_"+prop+"=array();}\n";
	QString acode;//body of add_ function, see below
	code+="public function set"+prop+"(array $values){\n";
	if(cls.propertyIsEnum(prop)){
		QList<WocEnum>ev=cls.enumValues(cls.propertyPlainType(prop));
		code+="\t$prop=array();\n";
		code+="\tforeach($values as $value){\n";
		code+="\t\tif(is_numeric($value)){\n\t\t\t$value=$value+0;\n";
		acode+="\tif(is_numeric($value)){\n\t\t$value=$value+0;\n";
		for(int j=0;j<ev.size();j++){
			code+="\t\t\tif($value=="+QString::number(ev[j].val)+"){\n";
			code+="\t\t\t\t$prop[]="+QString::number(ev[j].val)+";\n";
			code+="\t\t\t}else\n";
			acode+="\t\tif($value=="+QString::number(ev[j].val)+"){\n";
			acode+="\t\t\t$this->prop_"+prop+"[]="+QString::number(ev[j].val)+";\n";
			acode+="\t\t\treturn true;\n\t\t}\n";
		}
		code+="\t\t\treturn false;\n\t\t}else{\n";
		acode+="\t\treturn false;\n\t}else{\n";
		for(int j=0;j<ev.size();j++){
			code+="\t\t\tif($value==\""+ev[j].name+"\"){\n";
			code+="\t\t\t\t$prop[]="+QString::number(ev[j].val)+";\n";
			code+="\n\t\t\t}else\n";
			acode+="\t\tif($value==\""+ev[j].name+"\"){\n";
			acode+="\t\t\t$this->prop_"+prop+"[]="+QString::number(ev[j].val)+";\n";
			acode+="\t\t\treturn true;\n\t\t}\n";
		}
		code+="\t\t\treturn false;\n\t\t}\n\t}\n";
		code+="\t$this->prop_"+prop+"=$prop;\n";
		code+="\treturn true;\n";
		acode+="\t\treturn false;\n\t}\n";
	}else
	if(cls.propertyIsInt(prop)){
		code+="\t$prop=array();\n\tforeach($values as $value)\n";
		code+="\t\tif(is_numeric($value)){\n\t\t\t$prop[]=0+$value;\n\t\t}else return false;\n";
		code+="\t$this->prop_"+prop+"=$prop;\n\treturn true;\n";
		acode+="\tif(is_numeric($value)){\n";
		acode+="\t\t$this->prop_"+prop+"=0+$value;\n\t\treturn true;\n\t}else return false;\n";
	}else
	if(cls.propertyIsBool(prop)){
		code+="\t$prop=array();\n\tforeach($values as $value)\n";
		code+="\t\tif(is_bool($value))$prop[]=$value!=false;else\n";
		code+="\t\tif(WObject::isXmlBoolean($value))$prop[]=WObject::fromXmlBoolean($value);else\n";
		code+="\t\treturn false;\n";
		code+="\t$this->prop_"+prop+"=$prop;\n\treturn true;\n";
		acode+="\tif(is_bool($value)){\n";
		acode+="\t\t$this->prop_"+prop+"=$value!=false;\n\t\treturn true;\n\t}else return false;\n";
	}else
	if(cls.propertyIsString(prop)||cls.propertyIsBlob(prop)){
		code+="\t$prop=array();\n\tforeach($values as $value)$prop[]=\"\".$value;\n";
		code+="\t$this->prop_"+prop+"=$prop;\n\treturn true;\n";
		//TODO: special handling for astring
		acode+="\t$this->prop_"+prop+"[]=\"\".$value;\n\treturn true;\n";
	}else
	if(cls.propertyIsObject(prop)){
		code+="\t$prop=array();\n\tforeach($values as $value)\n";
		code+="\t\tif(is_a($value,\"WO"+cls.propertyPlainType(prop)+"\"))\n";
		code+="\t\t\t$prop[]=$value;\n";
		code+="\t\telse return false;\n";
		code+="\t$this->prop_"+prop+"=$prop;\n";
		code+="\treturn true;\n";
		acode+="\tif(is_a($value,\"WO"+cls.propertyPlainType(prop)+"\")){\n";
		acode+="\t\t$this->prop_"+prop+"[]=$value;\n";
		acode+="\t\treturn true;\n";
		acode+="\t}else return false;\n";
	}else{
		qDebug("Warning: unable to generate setter for class %s property %s: unknown type.",cls.name().toLatin1().data(),prop.toLatin1().data());
		code+="\treturn false;\n";
	}
	code+="}\n";
	code+="public function add_"+prop+"($value){\n"+acode+"}\n";
	
	return code;
}

QString WocPHPClass::classPropertyScalarGetters(const WocClass&cls,QString prop)
{
	QString code;
	if(cls.propertyIsString(prop))
		code+="public function getstr_"+prop+"(){return $this->prop_"+prop+";}\n";
	else
	if(cls.propertyIsBlob(prop))
		code+="public function getstr_"+prop+"(){return base64_encode($this->prop_"+prop+");}\n";
	else
	if(cls.propertyIsInt(prop))
		code+="public function getstr_"+prop+"(){return \"\".$this->prop_"+prop+";}\n";
	else
	if(cls.propertyIsBool(prop))
		code+="public function getstr_"+prop+"(){return $this->prop_"+prop+"?\"true\":\"false\";}\n";
	else
	if(cls.propertyIsEnum(prop)){
		code+="public function getstr_"+prop+"(){\n\tswitch($this->prop_"+prop+"){\n";
		QList<WocEnum> ev=cls.enumValues(cls.propertyPlainType(prop));
		for(int j=0;j<ev.size();j++){
			code+="\t\tcase "+QString::number(ev[j].val)+":return translate(\""+abstractClassName(cls)+"\",\""+ev[j].name+"\");\n";
		}
		code+="\t\tdefault:return null;\n\t}\n}\n";
	}
	
	return code;
}

QString WocPHPClass::classPropertyScalarSetters(const WocClass&cls,QString prop)
{
	QString code;
	code+="public function set"+prop+"($value){\n";
	if(cls.propertyIsEnum(prop)){
		QList<WocEnum>ev=cls.enumValues(cls.propertyPlainType(prop));
		code+="\tif(is_numeric($value)){\n\t\t$value=$value+0;\n";
		for(int j=0;j<ev.size();j++){
			code+="\t\tif($value=="+QString::number(ev[j].val)+"){\n";
			code+="\t\t\t$this->prop_"+prop+"="+QString::number(ev[j].val)+";\n";
			code+="\t\t\treturn true;\n\t\t}\n";
		}
		code+="\t\treturn false;\n\t}else{\n";
		for(int j=0;j<ev.size();j++){
			code+="\t\tif($value==\""+ev[j].name+"\"){\n";
			code+="\t\t\t$this->prop_"+prop+"="+QString::number(ev[j].val)+";\n";
			code+="\t\t\treturn true;\n\t\t}\n";
		}
		code+="\t\treturn false;\n\t}\n";
	}else
	if(cls.propertyIsInt(prop))
		code+="\tif(is_numeric($value)){\n\t\t$this->prop_"+prop+"=0+$value;\n\t\treturn true;\n\t}else return false;\n";
	else
	if(cls.propertyIsBool(prop)){
		code+="\tif(is_bool($value))$this->prop_"+prop+"=$value!=false;else\n";
		code+="\tif(WObject::isXmlBoolean($value))$this->prop_"+ prop+"=WObject::fromXmlBoolean($value);\n";
		code+="\telse return false;\n";
		code+="\treturn true;\n";
	}else
	if(cls.propertyIsString(prop)||cls.propertyIsBlob(prop))
		code+="\t$this->prop_"+prop+"=\"\".$value;\n\treturn true;\n";
		//TODO: special handling for astring
		//TODO: do something about Blob
	else
	if(cls.propertyIsObject(prop)){
		code+="\tif(is_a($value,\"WO"+cls.propertyPlainType(prop)+"\")){\n";
		code+="\t\t$this->prop_"+prop+"=$value;\n";
		code+="\t\treturn true;\n\t}\n";
	}else{
		qDebug("Warning: unable to generate setter for class %s property %s: unknown type.",cls.name().toLatin1().data(),prop.toLatin1().data());
		code+="\treturn false;\n";
	}
	code+="}\n";
	
	return code;
}

QString WocPHPClass::classSerializers(const WocClass&cls)
{
	QString code;
	//toString function (wraps toXml)
	code+="\npublic function toString(){\n\t$xml=new DomDocument;\n";
	code+="\t$xml->appendChild($this->toXml($xml));\n\treturn $xml->saveXml();\n}\n";
	//toXml function:
	code+="public function toXml($xml,$elementname=\""+cls.name()+"\"){\n";
	code+="\t$root=$xml->createElement($elementname);\n";
	code+="\t$this->toXmlHelper($xml,$root);\n";
	code+="\treturn $root;\n}\n";
	//toXmlHelper function:
	code+="protected function toXmlHelper($xml,$root){\n";
	code+="\tparent::toXmlHelper($xml,$root);\n";
	//add properties
	QStringList p=cls.propertyNames();
	for(int j=0;j<p.size();j++)
		code+=propertyToXml(cls,p[j]);
	code+="}\n";
	return code;
}

QString WocPHPClass::classDeserializers(const WocClass&cls)
{
	QString code;
	QStringList k;
	code+="\nstatic public function fromString($txt){\n\t$xml=new DomDocument;\n";
	code+="\tif(!$xml->loadXml(trim($txt)))";
	code+="\n\t\tthrow new WobXmlException(translate(\""+abstractClassName(cls)+"\",\"Unable to deserialize object of type "+className(cls)+": invalid XML.\"));";
	code+="\n\treturn self::fromXml($xml,$xml->documentElement);\n}\n";
	code+="static public function fromXml($xml,$elem){\n\t$data=new "+className(cls)+"();\n";
	code+="\tself::fromXmlHelper($data,$xml,$elem);\n\treturn $data;\n}\n";
	code+="static protected function fromXmlHelper($data,$xml,$elem){\n";
	code+="\tparent::fromXmlHelper($data,$xml,$elem);\n";
	k=cls.propertyNames();
	for(int i=0;i<k.size();i++){
		//scan properties
		if(cls.propertyIsList(k[i])){
			code+="\t$data->clear_"+k[i]+"();\n";
			code+="\tforeach(WObject::elementsByTagName($elem,\""+k[i]+"\") as $el){\n";
			if(cls.propertyIsObject(k[i])){
				code+="\t\t$data->add_"+k[i]+"(WO"+cls.propertyPlainType(k[i])+"::fromXml($xml,$el));\n";
			}else if(cls.propertyIsBlob(k[i])){
				code+="\t\t$data->add_"+k[i]+"(base64_decode($el->textContent));\n";
			}else{
				code+="\t\t$data->add_"+k[i]+"($el->textContent);\n";
			}
			code+="\t}\n";
		}else{
			if(cls.propertyIsObject(k[i])){
				code+="\tforeach(WObject::elementsByTagName($elem,\""+k[i]+"\") as $el){\n";
				code+="\t\t$data->set"+k[i]+"(WO"+cls.propertyPlainType(k[i])+"::fromXml($xml,$el));\n";
				code+="\t}\n";
			}else
			if(cls.propertyIsAttribute(k[i])){
				code+="\tif($elem->hasAttribute(\""+k[i]+"\"))\n";
				code+="\t\t$data->set"+k[i]+"($elem->getAttribute(\""+k[i]+"\"));\n";
			}else{
				code+="\tforeach(WObject::elementsByTagName($elem,\""+k[i]+"\") as $el){\n";
				if(cls.propertyIsBlob(k[i]))
					code+="\t\t$data->set"+k[i]+"(base64_decode($el->textContent));\n";
				else
					code+="\t\t$data->set"+k[i]+"($el->textContent);\n";
				code+="\t}\n";
			}
		}
	}
	code+="}\n";
	return code;
}

QString WocPHPClass::classMappings(const WocClass&cls)
{
	//don't do it for client mode (clients do not have a DB)
	if(m_parent->ptable==0)return QString();
	//implement mappings
	QString code;
	QStringList k=cls.mappingTables();
	for(int i=0;i<k.size();i++){
		WocTable tab=WocProcessor::instance()->table(k[i]);
		//single object mapping
		code+="\nstatic public function fromTable"+k[i]+"($table){\n";
		code+="\tif($table === false)return false;\n";
		code+="\t$data=new WO"+cls.name()+"();\n";
		QMap<QString,QString>map=cls.mapping(k[i]);
		QStringList mapk=cls.mappingProperties(k[i]);
		for(int j=0;j<mapk.size();j++){
			QString meth=cls.mapMethod(k[i],mapk[j],m_lang);
			if(meth!="")code+="\t$data->prop_"+mapk[j]+"="+meth+";\n";
			else code+="\t$data->prop_"+mapk[j]+"=$table->"+map[mapk[j]]+";\n";
		}
		code+="\treturn $data;\n}\n";
		//wrapper for multi-object mapping
		code+="static public function fromTableArray"+k[i]+"(array $table){\n\t$ret=array();\n";
		code+="\tfor($i=0;$i<count($table);$i++)$ret[]=self::fromTable"+k[i]+"($table[$i]);\n";
		code+="\treturn $ret;\n}\n";
		//reverse mapping
		code+="public function toTable"+k[i]+"(&$table){\n";
		for(int j=0;j<mapk.size();j++){
			//do not reverse translate method conversions
			QString meth=cls.mapMethod(k[i],mapk[j],m_lang).trimmed();
			if(meth!="")continue;
			//check that column exists
			if(tab.hasColumn(map[mapk[j]])){
				code+="\tif($this->prop_"+mapk[j]+"!==null && $table->"+map[mapk[j]]+"!==$this->prop_"+mapk[j]+")\n";
				code+="\t\t$table->"+map[mapk[j]]+"=$this->prop_"+mapk[j]+";\n";
			}
		}
		code+="\treturn $table;\n}\n";
	}
	
	return code;
}

QString WocPHPClass::propertyToXml(const WocClass&cls,QString sl)
{
	QString prop=sl.trimmed();
	//is it a list?
	if(cls.propertyIsList(prop)){
		//is it a class?
		if(cls.propertyIsObject(prop)){
			QString code="\tforeach($this->get"+prop+"() as $o)\n\t\t";
			code+="$root->appendChild($o->toXml($xml,\""+prop+"\"));\n";
			return code;
		}else{
			//there is no way to create lists of attributes, hence we always create elements
			QString code="\tforeach($this->getstrlist_"+prop+"() as $e)\n\t\t";
			code+="$root->appendChild($xml->createElement(\""+prop+"\",xq($e)));\n";
			return code;
		}
	}
	//non lists:
	QString code="\t$p=$this->get"+prop+"();\n";
	//is it an attribute?
	if(cls.propertyIsAttribute(prop))
		return code+"\tif($p!==null)$root->setAttribute(\""+prop+"\",$this->getstr_"+prop+"());\n";
	//is it an element?
	if(cls.propertyIsSimpleElement(prop))
		return code+"\tif($p!==null)$root->appendChild($xml->createElement(\""+prop+"\",xq($this->getstr_"+prop+"())));\n";
	//is it a class?
	if(cls.propertyIsObject(prop))
		return code+"\tif(is_a($p,\"WO"+cls.propertyPlainType(prop)+"\"))$root->appendChild($p->toXml($xml,\""+prop+"\"));\n";
	//anything else?
	qDebug("Warning: end of WocPHPClass::propertyToXml - this code should not be reachable.");
	return "//internal generator error!\n";
}
