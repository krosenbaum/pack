// Copyright (C) 2009-2011 by Konrad Rosenbaum <konrad@silmor.de>
// protected under the GNU GPL version 3 or at your option any newer.
// See COPYING.GPL file that comes with this distribution.
//

#include "mfile.h"

#include <QFileInfo>

//static 
QMap<QString,QStringList> MFile::touched;

MFile::MFile(QString n,QObject*parent):QFile(n+",new",parent),name(n){isopen=false;}
MFile::MFile(QObject *parent):QFile(parent){isopen=false;}
MFile::~MFile()
{
	if(isopen)close();
}

void MFile::close()
{
// 	qDebug("Info: closing %s",name.toAscii().data());
	//compare
	bool ident=false;
	if(isopen){
		QFile::close();
		QFile::open(QIODevice::ReadOnly);
		QByteArray here=readAll();
		QFile there(name);
		if(there.open(QIODevice::ReadOnly)){
			ident=(here == there.readAll());
			there.close();
		}
	}
	//actual close
	QFile::close();
	//move
	if(isopen){
		isopen=false;
		if(ident){
			//if identical: remove new version to preserve file time
			remove();
		}else{
			//if not identical: use new version, remove old one
			QFile(name).remove();
			QFile(name+",new").rename(name);
		}
	}
}

void MFile::setFileName(QString n)
{
	name=n;
	QFile::setFileName(n+",new");
}

bool MFile::open(QIODevice::OpenMode m)
{
	isopen=QFile::open(m);
	if(isopen){
		//remember it
		QFileInfo fi(name);
		QString p=fi.absolutePath();
		QString f=fi.fileName();
		if(!touched.contains(p))touched.insert(p,QStringList());
		if(!touched[p].contains(f))touched[p]<<f;
	}
	return isopen;
}

bool MFile::touchedFile(QString name)
{
	QFileInfo fi(name);
	QString p=fi.absolutePath();
	QString f=fi.fileName();
	if(!touched.contains(p))return false;
	return touched[p].contains(f);
}
