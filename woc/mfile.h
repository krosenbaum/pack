// Copyright (C) 2009-2011 by Konrad Rosenbaum <konrad@silmor.de>
// protected under the GNU GPL version 3 or at your option any newer.
// See COPYING.GPL file that comes with this distribution.
//

#ifndef WOC_MFILE_H
#define WOC_MFILE_H

#include <QFile>
#include <QMap>
#include <QStringList>

/**overwrites QFile to only generate a file if its new version differs from an existing one, it creates a temporary file with ",new" added to the file name; additionally it records all file names that it touches;
this is not a complete implementation - just enough for woc; it assumes that files are opened to be (re)created*/
class MFile:public QFile
{
	public:
		/**creates a new file (tentatively) with the given name (temporarily with ",new" appended)*/
		MFile(QString name,QObject*parent=0);
		/**creates a file object*/
		MFile(QObject *parent=0);
		/**implicitly closes the file*/
		~MFile();
		
		/**compares the old (if it exists) and new version of the file and overwrites the old one if there are differences*/
		virtual void close();
		/**opens the file, WriteOnly is assumed as mode*/
		virtual bool open(QIODevice::OpenMode m=QIODevice::WriteOnly);
		
		/**returns the name of the file (without ",new")*/
		QString fileName()const{return name;}
		/**sets the file name for the file to be written; must be called before it is opened*/
		void setFileName(QString n);
		
		/**returns true if that file name has been touched yet*/
		static bool touchedFile(QString);
	private:
		//flag to show whether file is open
		bool isopen;
		//original name of the file
		QString name;
		
		//stores touched files
		static QMap<QString,QStringList> touched;
};

#endif
