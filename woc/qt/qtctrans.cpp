// Copyright (C) 2009-2012 by Konrad Rosenbaum <konrad@silmor.de>
// protected under the GNU GPL version 3 or at your option any newer.
// See COPYING.GPL file that comes with this distribution.
//

#include "qtout.h"
#include "qtctrans.h"

#include <QDir>
#include <QDomElement>

#include "qtconst.h"
#include "doxyout.h"

WocQtClientTransaction::WocQtClientTransaction(WocQtOut*p)
	:WocQtTransaction(p)
{}
WocQtClientTransaction::~WocQtClientTransaction(){}

void WocQtClientTransaction::finalize()
{
	trnList();
}

///internal cache to hold the output for one transaction, a reference to this struct
///is handed arout WocQtClientTransaction
struct QtCTrans{
	///class name
	QString cn;
	///private (d-ptr) class name
	QString cnp;
	///default interface name, used as parameter in query
	QString defparm;
	//code store
	///part of the header: contains class decl
	QString hcd;
	///part of the header: #include's
	QString hdi;
	///private (d-ptr) class decl
	QString pcd;
	///cpp source file main content
	QString scd;
	///cpp source file #include's
	QString sri;
	//in/out params
	///input parameters of the transaction
	QStringList in;
	///output parameters of the transaction
	QStringList out;
	///input param list for function calls (type1 val1,type2 val2,...)
	QString inlist;
	///input param list, but call values only (val1,val2,...)
	QString clist;
	///input params (type+name) plus interface name
	QString xinlist;
	///input params (for calls) plus interface name
	QString xclist;
	///docu tags for input params
	QString indoc;
	///docu tag for the interface param of query(...)
	QString ifcdoc;
	///transaction itself, from the processor
	const WocTransaction&trn;
	///convenience constructor
	QtCTrans(const WocTransaction&t,QString cn_,QString cnp_):trn(t){
		in=trn.inputNames();
		out=trn.outputNames();
		cn=cn_;cnp=cnp_;
	}
};

void WocQtClientTransaction::newTransaction(const WocTransaction&trn)
{
	QString cn=m_parent->namePrefix()+"T"+trn.name();
	QString cnp=cn+"_Private";
	m_parent->addFile(cn);
	MFile hdr(m_parent->destinationPath()+"/src"+cn+".h");
	MFile src(m_parent->destinationPath()+"/src"+cn+".cpp");
	if(!hdr.open(QIODevice::WriteOnly|QIODevice::Truncate) ||
	   !src.open(QIODevice::WriteOnly|QIODevice::Truncate)){
	   	qDebug("Error: cannot create class files for transaction %s.",cn.toLatin1().data());
	   	emit errorFound();
	   	return;
	}
	//basics
	QtCTrans ct(trn,cn,cnp);
	initList(ct);
	//lead in
	hdr.write(QByteArray(HDRSTART).replace("%",cn.toLatin1()));
	src.write(QByteArray(SRCSTART).replace("%",cn.toLatin1()));
        hdr.write(m_parent->exportLines());
	
	//start constructing code
	
	//include section
	genInclude(ct);
	//start of class
	ct.hcd+="\nclass "+cnp+";\n";
	ct.hcd+="\n"+doxyFormat(trn.docStrings());
	ct.hcd+="class "+m_parent->exportSymbol()+" "+cn+":public "+m_parent->transactionBase()+"\n{\n  Q_OBJECT\n";
	ct.pcd+="\nclass "+cnp+":public WTransaction_PrivateBase\n{\n";
	
	//create properties
	genProperties(ct);
	
	//create constructors
	ct.defparm="=\""+WocProcessor::instance()->projectName()+"\"";
	genTors(ct);
	
	//query method implemented
	genQuery(ct);

	//create getters
	genGetters(ct);
	
        //create log control
        genLogCtrl(ct);
	
	//button class up
	ct.hcd+="};\n\n";
	ct.pcd+="};\n\n";
	
	//make meta object...
	ct.hcd+="Q_DECLARE_METATYPE("+cn+")\n";
	ct.scd+="static int mymetatypeid=qRegisterMetaType<"+cn+">();\n";
	
	//write code
	hdr.write(ct.hdi.toLatin1());
	hdr.write(ct.hcd.toLatin1());
	src.write(ct.sri.toLatin1());
	src.write(ct.pcd.toLatin1());
	src.write(ct.scd.toLatin1());
	
	//lead out
	hdr.write(QByteArray(HDREND).replace("%",cn.toLatin1()));
	src.write(QByteArray(SRCEND).replace("%",cn.toLatin1()));
	
	//memorize documentation for finalize
	s_transdoc td;
	td.tdoc=trn.docStrings();
	foreach(QString prv,trn.privileges())
		td.privdoc.insert(prv,trn.privilegeDoc(prv));
	m_transdoc.insert(trn.name(),td);
}

void WocQtClientTransaction::initList(QtCTrans& ct)
{
	for(int i=0;i<ct.in.size();i++){
		if(i){ct.inlist+=",";ct.clist+=",";}
		ct.inlist+="const "+m_parent->qttype(ct.trn,ct.in[i],WocQtOut::In)+"&a"+ct.in[i];
		ct.clist+="a"+ct.in[i];
		ct.indoc+="\t/// \\param a"+ct.in[i]+" ";
		ct.indoc+=ct.trn.inputDoc(ct.in[i]).replace('\n',' ').replace('\r',' ');
		ct.indoc+="\n";
	}
	ct.xinlist=ct.inlist;
	ct.xclist=ct.clist;
	if(ct.xinlist!="")ct.xinlist+=",";
	ct.xinlist+="QString iface";
	if(ct.xclist!="")ct.xclist+=",";
	ct.xclist+="iface";
	ct.ifcdoc="\t/// \\param iface ID of the interface that the transaction will be sent on\n";
}


void WocQtClientTransaction::genInclude(QtCTrans&ct)
{
	ct.hdi+="#include \""+m_parent->transactionBase()+"\"\n";
	ct.sri+="#include \"WTransaction_Private\"\n";
	ct.sri+="#include \"WInterface\"\n";
	ct.sri+="#include <QCoreApplication>\n\n";
	for(int i=0;i<ct.in.size();i++){
		QString tp=m_parent->qtobjtype(ct.trn,ct.in[i],WocQtOut::In);
		if(tp!="")ct.hdi+="#include <"+tp+">\n";
	}
	for(int i=0;i<ct.out.size();i++){
		QString tp=m_parent->qtobjtype(ct.trn,ct.out[i],WocQtOut::Out);
		if(tp!="")ct.hdi+="#include <"+tp+">\n";
	}
}

void WocQtClientTransaction::genProperties(QtCTrans&ct)
{
	ct.hcd+="  private:\n\t"+ct.cnp+"*p;\n\tfriend class "+ct.cnp+";\n";
	ct.pcd+="  protected:\n\tfriend class "+ct.cn+";\n";
	ct.pcd+="\t"+ct.cnp+"("+ct.cn+"*parent){parent->p=this;}\n";
	ct.pcd+="\tvoid attach("+ct.cn+"*parent){parent->p=this;WTransaction_PrivateBase::attach();}\n";
	ct.pcd+="\tvoid detach("+ct.cn+"*parent){parent->p=0;WTransaction_PrivateBase::detach();}\n";
	for(int i=0;i<ct.in.size();i++){
		ct.pcd+="\t"+m_parent->qttype(ct.trn,ct.in[i],WocQtOut::In)+"in_"+ct.in[i]+";\n";
	}
	for(int i=0;i<ct.out.size();i++)
		ct.pcd+="\t"+m_parent->qttype(ct.trn,ct.out[i],WocQtOut::Out)+"out_"+ct.out[i]+";\n";
}

void WocQtClientTransaction::genTors(QtCTrans&ct)
{
	//define parametric constructor
	ct.hcd+="  protected:\n";
	ct.hcd+="\t/// generates an instance from its properties\n";
	ct.hcd+=ct.indoc+ct.ifcdoc;
	ct.hcd+="\texplicit "+ct.cn+"("+ct.xinlist+");\n";
	//parametric constructor implementation
	ct.scd+=ct.cn+"::"+ct.cn+"("+ct.xinlist+")\n\t:"+m_parent->transactionBase()+"(iface)\n{\n";
	ct.scd+="\tnew "+ct.cnp+"(this);\n";
	for(int i=0;i<ct.in.size();i++){
		ct.scd+="\tp->in_"+ct.in[i]+"=a"+ct.in[i]+";\n";
	}
	ct.scd+="}\n\n";
	//decl default constructor
	ct.hcd+="  public:\n";
	ct.hcd+="\t/// default constructor: generates an invalid transaction\n";
	ct.hcd+="\t"+ct.cn+"();\n";
	ct.scd+=ct.cn+"::"+ct.cn+"()\n{\n\tnew "+ct.cnp+"(this);\n}\n\n";
	//decl copy constructor
	ct.hcd+="\t/// copy constructor: the two copies share their state\n";
	ct.hcd+="\t"+ct.cn+"(const "+ct.cn+"&);\n";
	//copy constructor implementation
	ct.scd+=ct.cn+"::"+ct.cn+"(const "+ct.cn+"&t)\n\t:"+m_parent->transactionBase()+"(t)\n{\n";
	ct.scd+="\tt.p->attach(this);\n";
	ct.scd+="}\n\n";
	//decl copy operator
	ct.hcd+="\t/// copy assignment: the copy shares the state of the original object\n";
	ct.hcd+="\t"+ct.cn+"& operator=(const "+ct.cn+"&);\n";
	//copy operator implemented
	ct.scd+=ct.cn+"& "+ct.cn+"::operator=(const "+ct.cn+"&t)\n{\n";
	ct.scd+="\tif(this == &t)return *this;\n";
	ct.scd+="\t"+m_parent->transactionBase()+"::operator=(t);\n";
	ct.scd+="\tp->detach(this);t.p->attach(this);\n";
	ct.scd+="\treturn *this;\n}\n\n";
	
	//destructor
	ct.hcd+="\t/// deletes this instance\n";
	ct.hcd+="\tvirtual ~"+ct.cn+"();\n";
	ct.scd+=ct.cn+"::~"+ct.cn+"()\n{\n\tp->detach(this);\n}\n\n";
}

void WocQtClientTransaction::genQuery(QtCTrans& ct)
{
	//global interface code
	m_parent->addIfaceHeaderPrefix("class "+ct.cn+";\n");
// 	m_parent->addIfaceImpl("#include \"src"+ct.cn+".h\"\n");
	m_parent->addIfaceHeaderClass("  /// convenience call to query "+ct.cn+" synchronously\n");
	m_parent->addIfaceHeaderClass("  "+ct.cn+" query"+ct.trn.name()+"("+ct.inlist+");\n");
	m_parent->addIfaceImpl(ct.cn+" "+ m_parent->ifaceClassName()+"::query"+ct.trn.name()+ "("+ct.inlist+ ")\n{return "+ct.cn+"::query("+ ct.clist+(ct.clist!=""?",":"")+"name());}\n\n");
	//query method decl
	ct.hcd+="  private:\n";
	ct.hcd+="\tvoid netquery();\n\tvoid asyncnetquery();\n";
	ct.hcd+="\tQByteArray encodeData();\n\tvoid decodeData(QByteArray);\n";
	ct.hcd+="  protected:\n\tvirtual void endQuery();\n";
	//sync query
	ct.scd+="void "+ct.cn+"::netquery()\n{\n";
	ct.scd+="\tWTransaction::Log log(this,\""+ct.trn.name()+"\");\n";
	ct.scd+="\tQByteArray enc=encodeData();\n";
	ct.scd+="\tif(enc.isEmpty()){\n\t\temit finished();\n\t\treturn;\n\t}\n";
	ct.scd+="\texecuteQuery(\""+ct.trn.name()+"\",enc);\n";
	ct.scd+="}\n";
	//async query method implemented
	ct.scd+="void "+ct.cn+"::asyncnetquery()\n{\n";
	ct.scd+="\tnew WTransaction::Log(this,\""+ct.trn.name()+"\");\n";
	ct.scd+="\tQByteArray enc=encodeData();\n";
	ct.scd+="\tif(enc.isEmpty()){\n\t\temit finished();\n\t\treturn;\n\t}\n";
	ct.scd+="\tstartQuery(\""+ct.trn.name()+"\",enc);\n";
	ct.scd+="}\n";
	//encode input
	ct.scd+="QByteArray "+ct.cn+"::encodeData()\n{\n";
	ct.scd+=trnInput(ct.trn);
	ct.scd+="}\n";
	//decode output
	ct.scd+="void "+ct.cn+"::decodeData(QByteArray rba)\n{\n";
	ct.scd+=trnOutput(ct.trn);
	ct.scd+="}\n";
	ct.scd+="void "+ct.cn+"::endQuery()\n{\n";
	ct.scd+="\tdecodeData(d->m_rspdata.trimmed());\n";
	ct.scd+="\temit finished();\n}\n";
	//create queries
	ct.hcd+="  public:\n";
	ct.hcd+="\t/// emits the query over the network and returns the finished transaction object\n";
	ct.hcd+=ct.indoc+ct.ifcdoc;
	ct.hcd+="\tstatic "+ct.cn+" query("+ct.xinlist+ct.defparm+");\n";
	ct.scd+=ct.cn+" "+ct.cn+"::query("+ct.xinlist+")\n{\n";
	ct.scd+="\t"+ct.cn;
	ct.scd+=" r("+ct.xclist+");\n";
	ct.scd+="\tr.netquery();\n\treturn r;\n}\n";
	ct.hcd+="\t/// emits the query over the network and returns the transaction object, use isFinished() to check for the transaction's state\n";
	ct.hcd+=ct.indoc+ct.ifcdoc;
	ct.hcd+="\tstatic "+ct.cn+" asyncQuery("+ct.xinlist+ct.defparm+");\n";
	ct.scd+=ct.cn+" "+ct.cn+"::asyncQuery("+ct.xinlist+")\n{\n";
	ct.scd+="\t"+ct.cn;
	ct.scd+=" r("+ct.xclist+");\n";
	ct.scd+="\tr.asyncnetquery();\n\treturn r;\n}\n";
}

void WocQtClientTransaction::genGetters(QtCTrans& ct)
{
	for(int i=0;i<ct.out.size();i++){
		QString tp=m_parent->qttype(ct.trn,ct.out[i],WocQtOut::Out);
		ct.hcd+=doxyFormat(ct.trn.outputDoc(ct.out[i]),1);
		ct.hcd+="\tQ_SLOT "+tp+" get"+ct.out[i]+"()const;\n";
		ct.scd+=tp+" "+ct.cn+"::get"+ct.out[i]+"()const{return p->out_"+ct.out[i]+";}\n";
	}
}

void WocQtClientTransaction::genLogCtrl(QtCTrans& ct)
{
        if(ct.trn.logMode()==WocTransaction::LogAll)return;
        ct.hcd+="  protected:\n";
        if(ct.trn.logMode()&WocTransaction::NoLogRequest)
                ct.hcd+="\tvirtual bool canLogRequest()const{return false;}\n";
        if(ct.trn.logMode()&WocTransaction::NoLogResponse)
                ct.hcd+="\tvirtual bool canLogResponse()const{return false;}\n";
}

QString WocQtClientTransaction::trnInput(const WocTransaction&trn)
{
	QString code;
	code+="\tWTransaction::LogWrap log(this,\""+trn.name()+"\");\n";
	code+="\tQDomDocument doc;QDomElement root=doc.createElement(\"WobRequest-"+trn.name()+"\");\n";
	code+="\tQDomElement tmp;\n";
	code+="\tWInterface *iface=WInterface::instance(d->m_iface);\n";
	code+="\tif(iface==0){\n";
	code+="\t\td->m_errtype=\"_iface\";d->m_errstr=\"interface not found\";\n";
	code+="\t\td->m_stage=Error;log.setError(d->m_errstr);\n\t\treturn QByteArray();\n\t}\n";
	code+="\t/*start of input encoding*/\n";
	QStringList sl=trn.inputNames();
	for(int i=0;i<sl.size();i++){
		QString t=trn.inputType(sl[i]);
		if(trn.isAttributeType(t)){
			code+="\troot.setAttribute(\""+sl[i]+"\",p->in_"+sl[i];
			if(trn.isBoolType(t))
				code+="?\"true\":\"false\"";
			code+=");\n";
		}else{
			if(trn.isListType(t)){
				QString pt=trn.plainType(t);
				code+="\tfor(int i=0;i<p->in_"+sl[i]+".size();i++){\n";
				if(trn.isObjectType(t)){
					code+="\t\ttmp=p->in_"+sl[i]+"[i].toXml(doc,\""+sl[i]+"\");\n";
				}else{
					code+="\t\ttmp=doc.createElement(\""+sl[i]+"\");\n";
					code+="\t\ttmp.appendChild(doc.createTextNode(";
					if(trn.isIntType(t))
						code+="QString::number(p->in_"+sl[i]+"[i])";
					else
					if(trn.isBoolType(t))
						code+="p->in_"+sl[i]+"[i]?\"true\":\"false\"";
					else
					if(trn.isBlobType(t))
						code+="p->in_"+sl[i]+".toBase64()";
					else
						code+="p->in_"+sl[i]+"[i]";
					code+="));\n";
				}
				code+="\t\troot.appendChild(tmp);\n";
				code+="\t}\n";
			}else{
				if(trn.isObjectType(t)){
					code+="\troot.appendChild(p->in_"+sl[i]+".toXml(doc,\""+sl[i]+"\"));\n";
				}else{
					code+="\ttmp=doc.createElement(\""+sl[i]+"\");\n";
					code+="\ttmp.appendChild(doc.createTextNode(";
					if(trn.isIntType(t))
						code+="QString::number(p->in_"+sl[i]+")";
					else
					if(trn.isBlobType(t))
						code+="p->in_"+sl[i]+".toBase64()";
					else
						code+="p->in_"+sl[i];
					code+="));\n\troot.appendChild(tmp);\n";
				}
			}
		}
	}
	code+="\t/*end of input encoding*/\n";
	code+="\tdoc.appendChild(root);\n";
	code+="\treturn doc.toByteArray();\n";
	return code;
}

QString WocQtClientTransaction::trnOutput(const WocTransaction&trn)
{
	QStringList sl=trn.outputNames();
	QString code;
	code+="\tWTransaction::LogWrap log(this,\""+trn.name()+"\");\n";
	code+="\t/*start of output decoding*/\n";
	//basic XML parsing
	code+="\tif(rba.isEmpty()){\n";
	code+="\t\td->m_stage=Error;d->m_errtype=\"_iface\";\n";
	code+="\t\td->m_errstr=QCoreApplication::translate(\"WobTransaction\",\"XML result parser error: empty response.\");\n";
	code+="\t\tlog.setError(d->m_errstr);\n\t\treturn;\n\t}\n";
	code+="\tQDomDocument doc;\n";
	code+="\tQString emsg;int eln,ecl;\n";
	code+="\tif(!doc.setContent(rba,&emsg,&eln,&ecl)){\n";
	code+="\t\td->m_stage=Error;d->m_errtype=\"_iface\";\n";
	code+="\t\td->m_errstr=QString(QCoreApplication::translate(\"WobTransaction\",\"XML result parser error line %1 col %2: %3\")).arg(eln).arg(ecl).arg(emsg);\n";
	code+="\t\tlog.setError(d->m_errstr);\n\t\treturn;\n\t}\n";
	code+="\tQDomElement root=doc.documentElement();\n";
	//decide where to go, error handling
	code+="\tif(d->m_wobstatus!=\"ok\"){\n";
	code+="\t\td->m_stage=Error;d->m_errtype=\"_server\";d->m_errstr=\"unknown server error\";\n";
	code+="\t\tQString terr=root.text().trimmed();\n";
	code+="\t\tif(terr==\"\"){\n\t\t\tlog.setError(d->m_errstr);\n\t\t\treturn;\n\t\t}\n";
	code+="\t\td->m_errtype=root.attribute(\"type\",\"_server\");\n";
	code+="\t\td->m_errstr=terr;\n\t\tlog.setError(d->m_errstr);\n\t\treturn;\n\t}\n";
	code+="\tQList<QDomElement> nl;\n";
	//parse parameters
	for(int i=0;i<sl.size();i++){
		QString t=trn.outputType(sl[i]);
		if(trn.isAttributeType(t)){
			code+="\tp->out_"+sl[i]+"=";
			if(trn.isBoolType(t))code+="str2bool(";
			code+="root.attribute(\""+sl[i]+"\")";
			if(trn.isIntType(t))code+=".toInt()";else
			if(trn.isBoolType(t))code+=")";
			code+=";\n";
		}else{
			code+="\tnl=elementsByTagName(root,\""+sl[i]+"\");\n";
			if(trn.isListType(t)){
				code+="\tfor(int i=0;i<nl.size();i++){\n";
				if(trn.isObjectType(t)){
					code+="\t\ttry{p->out_"+sl[i]+".append("+m_parent->qtobjtype(trn,sl[i],WocQtOut::Out)+"(nl.at(i).toElement()));}catch(WException e){d->m_stage=Error;d->m_errtype=e.component();d->m_errstr=e.error();log.setError(d->m_errstr);}\n";
				}else if(trn.isIntType(t)){
					code+="\t\tp->out_"+sl[i]+".append(nl.at(i).toElement().text().toInt());\n";
				}else if(trn.isBoolType(t)){
					code+="\t\tp->out_"+sl[i]+".append(str2bool(nl.at(i).toElement().text()));\n";
				}else if(trn.isBlobType(t)){
					code+="\t\tp->out_"+sl[i]+".append(QByteArray::fromBase64(nl.at(i).toElement().text().toLatin1()));\n";
				}else{//can only be string
					code+="\t\tp->out_"+sl[i]+".append(nl.at(i).toElement().text());\n";
				}
				code+="\t}\n";
			}else{
				code+="\tif(nl.size()>0){\n";
				if(trn.isObjectType(t)){
					code+="\t\ttry{p->out_"+sl[i]+"="+m_parent->qtobjtype(trn,sl[i],WocQtOut::Out)+"(nl.at(0).toElement());}catch(WException e){d->m_stage=Error;d->m_errtype=e.component();d->m_errstr=e.error();log.setError(d->m_errstr);}\n";
				}else if(trn.isBlobType(t)){
					code+="\t\tp->out_"+sl[i]+"=QByteArray::fromBase64(nl.at(0).toElement().text().toLatin1());\n";
				}else{//can only be string
					code+="\t\tp->out_"+sl[i]+"=nl.at(0).toElement().text();\n";
				}
				code+="\t}\n";
			}
		}
	}
	code+="\t/*end of output*/\n";
	return code;
}

void WocQtClientTransaction::trnList()
{
	QString code;
	//header
	code+="  Q_ENUMS(Right)\n";
	code+="  /// This enum represents transactions and the right to use them.\n";
	code+="  enum Right {\n    /// dummy as a fall back for no transaction\n    NoRight";
	QStringList r=WocProcessor::instance()->transactionNames();
	QStringList p=WocProcessor::instance()->privilegeNames();
	QStringList pp=p;
	for(int i=0;i<r.size();i++){
		code+=",\n";
		if(m_transdoc.contains(r[i]))
			code+=doxyFormat(m_transdoc[r[i]].tdoc,1);
		code+="\tR"+r[i];
	}
	for(int i=0;i<p.size();i++){
		QStringList pn=pp[i].split(':');
		code+=",\n";
		if(pn.size()==2 && m_transdoc.contains(pn[0]) && m_transdoc[pn[0]].privdoc.contains(pn[1]))
			code+=doxyFormat(m_transdoc[pn[0]].privdoc[pn[1]],1);
		//note: the replace call below has the side effect of being a permanent change,
		//this is on purpose, since the changed values are needed again below
		code+="\tP"+pp[i].replace(':',"_");
	}
	code+="\n  };\n";
	code+="  typedef QList<Right> RightList;\n";
	code+="  ///converts a right enum to its string representation\n";
	code+="  Q_INVOKABLE static QString rightToString(Right);\n";
	code+="  ///converts a right enum to its localized string representation\n";
	code+="  Q_INVOKABLE static QString rightToLocalString(Right);\n";
	code+="  ///converts a string to a matching right enum, returns NoRight if the string does not match\n";
	code+="  Q_INVOKABLE static Right stringToRight(QString);\n";
	code+="  ///converts a localized string to a matching right enum, returns NoRight if the string does not match\n";
	code+="  Q_INVOKABLE static QStringList allKnownRightsString();\n";
	code+="  ///returns a list of all known rights/transactions\n";
	code+="  Q_INVOKABLE static "+m_parent->ifaceClassName()+"::RightList allKnownRights();\n";
	m_parent->addIfaceHeaderClass(code);code.clear();
	
	code+="static int righttypeid=";
	code+="qRegisterMetaType<"+m_parent->ifaceClassName()+"::RightList>()+";
	code+="qRegisterMetaType<QList<"+m_parent->ifaceClassName()+"::RightList> >();\n";
	m_parent->addPostIface("Q_DECLARE_METATYPE("+m_parent->ifaceClassName()+"::RightList)\n");
	m_parent->addPostIface("Q_DECLARE_METATYPE(QList<"+ m_parent->ifaceClassName()+ "::RightList>)\n");
	code+="QString "+m_parent->ifaceClassName()+"::rightToString(Right r)\n{\n\tswitch(r){\n";
	for(int i=0;i<r.size();i++)
		code+="\t\tcase R"+r[i]+":return \""+r[i]+"\";\n";
	for(int i=0;i<p.size();i++)
		code+="\t\tcase P"+pp[i]+":return \""+p[i]+"\";\n";
	code+="\t\tdefault:return \"\";\n\t}\n}\n";
	code+="QString "+m_parent->ifaceClassName()+"::rightToLocalString(Right r)\n{\n\tswitch(r){\n";
	for(int i=0;i<r.size();i++)
		code+="\t\tcase R"+r[i]+":return tr(\""+r[i]+"\");\n";
	for(int i=0;i<p.size();i++)
		code+="\t\tcase P"+pp[i]+":return tr(\""+p[i]+"\");\n";
	code+="\t\tdefault:return \"\";\n\t}\n}\n";
	code+=m_parent->ifaceClassName()+"::Right "+m_parent->namePrefix()+ "Interface::stringToRight(QString s)\n{\n";
	for(int i=0;i<r.size();i++)
		code+="\tif(s==\""+r[i]+"\")return R"+r[i]+";else\n";
	for(int i=0;i<p.size();i++)
		code+="\tif(s==\""+p[i]+"\")return P"+pp[i]+";else\n";
	code+="\treturn NoRight;\n}\n";
	code+="QList<"+m_parent->ifaceClassName()+"::Right> "+m_parent->ifaceClassName()+"::allKnownRights()\n{\n";
	code+="\tQList<Right> ret;ret";
	for(int i=0;i<r.size();i++)
		code+="<<R"+r[i];
	for(int i=0;i<p.size();i++)
		code+="<<P"+pp[i];
	code+=";\n\treturn ret;\n}\n";
	code+="QStringList "+m_parent->ifaceClassName()+"::allKnownRightsString()\n{\n";
	code+="\tQStringList ret;ret";
	for(int i=0;i<r.size();i++)
		code+="<<\""+r[i]+"\"";
	for(int i=0;i<p.size();i++)
		code+="<<\""+p[i]+"\"";
	code+=";\n\treturn ret;\n}\n";
	m_parent->addIfaceImpl(code);
}
