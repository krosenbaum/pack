// Copyright (C) 2009-2011 by Konrad Rosenbaum <konrad@silmor.de>
// protected under the GNU GPL version 3 or at your option any newer.
// See COPYING.GPL file that comes with this distribution.
//

#ifndef WOC_QTOUT_CLASS_H
#define WOC_QTOUT_CLASS_H

#include "mfile.h"
#include <QObject>

class QDomElement;
class WocClass;
class WocQtOut;

///Specialization that outputs classes for WocQtOut and its sub-classes.
class WocQtClass:public QObject
{
	Q_OBJECT
	public:
		///constructs the class output for a specific Qt output
		explicit WocQtClass(WocQtOut*);
		~WocQtClass();
		///called when all parsing is done
		virtual void finalize();
		///generates the code of a specific class
		virtual void newClass(const WocClass&);
	private:
		WocQtOut*m_parent;
		QString m_aux;
		
		/**helper: generate enums for classes*/
		void classEnums(const WocClass&,MFile&,MFile&,QString);
		/**helper: generate properties*/
		void classProperties(const WocClass&,MFile&,MFile&);
		/**helper: generate constructors/deserializer/copiers*/
		void classDeserializer(const WocClass&,MFile&,MFile&,QString);
		/**helper: generate serializers*/
		void classSerializers(const WocClass&,MFile&,MFile&,QString);
	signals:
		///emitted if something is wrong, aborts the processor
		void errorFound();
};

#endif
