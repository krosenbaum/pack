// Copyright (C) 2009-2011 by Konrad Rosenbaum <konrad@silmor.de>
// protected under the GNU GPL version 3 or at your option any newer.
// See COPYING.GPL file that comes with this distribution.
//

#include "qtout.h"
#include "qtdb.h"

#include "qtconst.h"

WocQtTable::WocQtTable(WocQtOut*){}
WocQtTable::~WocQtTable(){}
void WocQtTable::finalize(){}
void WocQtTable::newTable(const WocTable&){}
