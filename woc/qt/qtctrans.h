// Copyright (C) 2009-2011 by Konrad Rosenbaum <konrad@silmor.de>
// protected under the GNU GPL version 3 or at your option any newer.
// See COPYING.GPL file that comes with this distribution.
//

#ifndef WOC_QTOUT_CTRANS_H
#define WOC_QTOUT_CTRANS_H

#include "qtout.h"

struct QtCTrans;
class WocQtOut;
class WocTransaction;
class WocClass;

///Generates Qt code for transactions, the target is the client.
///The generator is instantiated when the Qt output is instantiated and stays alive till the end of the input.
class WocQtClientTransaction:public WocQtTransaction
{
	public:
		///creates the generator
		explicit WocQtClientTransaction(WocQtOut*);
		///deletes the generator
		~WocQtClientTransaction();
		///called last when all parsing is done
		virtual void finalize();
		///called for each transaction encountered
		virtual void newTransaction(const WocTransaction&);
	private:
		/**helper generates the transaction input encoding*/
		QString trnInput(const WocTransaction&);
		/**helper generates the transaction output decoding*/
		QString trnOutput(const WocTransaction&);
		/**helper generates enums and strings for all transactions*/
		void trnList();
		///helper: generate include section
		void genInclude(QtCTrans&);
		///helper: generate properties
		void genProperties(QtCTrans&);
		///helper: generate con- and de-structors
		void genTors(QtCTrans&);
		///helper: generate query methods
		void genQuery(QtCTrans&);
		///helper: generate getter methods
		void genGetters(QtCTrans&);
                ///helper: generate log control
                void genLogCtrl(QtCTrans&);
		///helper: initialize data in the transaction wrapper
		void initList(QtCTrans&);
		
		///memory for transaction and privilege docu
		struct s_transdoc{
			///transaction docu
			QStringList tdoc;
			///docu for each privilege
			QMap<QString,QString>privdoc;
		};
		///cache for transaction documentation, used by finalize()
		QMap<QString,s_transdoc>m_transdoc;
};

#endif
