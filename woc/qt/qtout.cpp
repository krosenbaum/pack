// Copyright (C) 2009-2011 by Konrad Rosenbaum <konrad@silmor.de>
// protected under the GNU GPL version 3 or at your option any newer.
// See COPYING.GPL file that comes with this distribution.
//

#include "qtout.h"
#include "qtctrans.h"
#include "qtstrans.h"
#include "qtclass.h"
#include "qtdb.h"

#include <QDir>
#include <QDomElement>

#define QT_OUT_NO_WRAP
#include "qtconst.h"
#include "doxyout.h"

WocQtOut::WocQtOut(QDomElement&el)
{
	qclass=0;qtable=0;qtrans=0;

	qDebug("Info: creating %s Generator.",el.tagName().toLatin1().data());
	m_basedir=WocProcessor::instance()->baseDir()+"/"+el.attribute("sourceDir",".");
	m_subdir=el.attribute("subDir","qtwob");
	m_clean=str2bool(el.attribute("clean","0"));
	m_prefix=el.attribute("classPrefix","Wob");
	m_cprefix=el.attribute("shareObjects",m_prefix);
	m_transbase=el.attribute("transactionBase","WTransaction");
        m_export=str2bool(el.attribute("enableExport","0"));
	//get/create directory
	QDir d(m_basedir+"/"+m_subdir);
	if(!d.exists())QDir(".").mkpath(m_basedir+"/"+m_subdir);

	//create project file
	m_pri.setFileName(m_basedir+"/"+m_subdir+"/"+el.attribute("priInclude","qtwob.pri"));
	if(!m_pri.open(QIODevice::WriteOnly|QIODevice::Truncate)){
		qDebug("Error: cannot create Qt project file %s.",m_pri.fileName().toLatin1().data());
		emit errorFound();
		return;
	}
	m_pri.write(QByteArray("#AUTOMATICALLY GENERATED FILE - DONT CHANGE!\n"));
	m_pri.write(QByteArray("INCLUDEPATH += ")+m_subdir.toLatin1()+QByteArray("\n"));
        if(m_export)
                m_pri.write(QByteArray("DEFINES += ")+exportSymbol().toLatin1()+QByteArray("=Q_DECL_EXPORT\n"));

	//create interface class
	WocProcessor*woc=WocProcessor::instance();
	QString pn=woc->projectName();
	m_ifacecpp.setFileName(m_basedir+"/"+m_subdir+"/src"+ifaceClassName()+".cpp");
	if(!m_ifacecpp.open(QIODevice::WriteOnly|QIODevice::Truncate)){
		qDebug("Error: cannot create Qt interface file %s.",m_ifacecpp.fileName().toLatin1().data());
		emit errorFound();
		return;
	}
	m_ifacecpp.write(QByteArray(SRCSTART).replace('%',ifaceClassName().toUtf8()));
	m_iface.setFileName(m_basedir+"/"+m_subdir+"/src"+ifaceClassName()+".h");
	if(!m_iface.open(QIODevice::WriteOnly|QIODevice::Truncate)){
		qDebug("Error: cannot create Qt interface file %s.",m_iface.fileName().toLatin1().data());
		emit errorFound();
		return;
	}
	m_iface.write(QByteArray(HDRSTART).replace("%",m_prefix.toLatin1()+"INTERFACE_H"));
	m_iface.write(QByteArray("#include \"WInterface\"\n"));
	m_iface.write(QByteArray("#include <QStringList>\n"));
        m_iface.write(exportLines());
	addIfaceHeaderClass(doxyFormat(pn+" interface class."));
	addIfaceHeaderClass(QString("class "+exportSymbol()+" "+ifaceClassName()+":public WInterface\n{\n Q_OBJECT\n"));
	addIfaceHeaderClass(QString(" public:\n  "+ifaceClassName()+"(QString name=\""+pn+"\"):WInterface(name){}\n"));
	addIfaceHeaderClass("  /// convenience override: returns a pointer to an instance with the given name if it is of the correct type, otherwise nullptr\n");
	addIfaceHeaderClass("  static "+ifaceClassName()+"*instance(QString name=\""+pn+"\")\n\t{return qobject_cast<"+ifaceClassName()+"*>(WInterface::instance(name));}\n\n");
	//version info, static
	addIfaceHeaderClass("  /// returns version information of this interface\n");
	addIfaceHeaderClass("  Q_INVOKABLE static QString staticVersionInfo(WOb::VersionInfo);\n");
	m_ifacecpp.write(QByteArray("#include \""+m_prefix.toLatin1()+"IncludeAll\"\n"));
	m_ifacecpp.write(QByteArray("#include \"staticVersion.h\"\n"));
	m_ifacecpp.write(QString("QString "+ifaceClassName()+"::staticVersionInfo(WOb::VersionInfo vi){return WOCgenerated_versionInfo(vi);}\n").toLatin1());
	addIfaceHeaderClass("  /// returns version information about the WOC that created the interface class\n");
	addIfaceHeaderClass("  Q_INVOKABLE static QString staticWocVersionInfo(WOb::VersionInfo);\n");
	m_ifacecpp.write(QString("QString "+ifaceClassName()+"::staticWocVersionInfo(WOb::VersionInfo vi){return WOCcopied_versionInfo(vi);}\n").toLatin1());
	//version info, virtual
	addIfaceHeaderClass("  /// returns version information of this interface\n");
	addIfaceHeaderClass("  Q_INVOKABLE QString versionInfo(WOb::VersionInfo)const;\n");
	m_ifacecpp.write(QString("QString "+ifaceClassName()+"::versionInfo(WOb::VersionInfo vi)const{return WOCgenerated_versionInfo(vi);}\n").toLatin1());
	addIfaceHeaderClass("  /// returns version information about the WOC that created the interface class\n");
	addIfaceHeaderClass("  Q_INVOKABLE QString wocVersionInfo(WOb::VersionInfo)const;\n");
	m_ifacecpp.write(QString("QString "+ifaceClassName()+"::wocVersionInfo(WOb::VersionInfo vi)const{return WOCcopied_versionInfo(vi);}\n").toLatin1());

	//create all includer
	m_hdr.setFileName(m_basedir+"/"+m_subdir+"/"+m_prefix+"IncludeAll");
	if(!m_hdr.open(QIODevice::WriteOnly|QIODevice::Truncate)){
		qDebug("Error: cannot create Qt header file %s.",m_hdr.fileName().toLatin1().data());
		emit errorFound();
		return;
	}
	m_hdr.write(QByteArray(HDRSTART).replace("%",m_prefix.toLatin1()+"INCLUDE_H"));
}

WocQtOut::~WocQtOut(){}

void WocQtOut::finalize()
{
	//finish sub-classes
	if(qclass)qclass->finalize();
	if(qtable)qtable->finalize();
	if(qtrans)qtrans->finalize();
	//generate versionInfo
	initVersionH();
	//add interface class
	addFile(ifaceClassName().toLatin1());
	//finish sources
	m_ifacecpp.write(SRCEND);
	m_ifacecpp.close();
	m_iface.write(m_iface_prefix.toLatin1());
	m_iface.write(m_iface_class.toLatin1());
	m_iface.write(QByteArray("};\n"));
	m_iface.write(m_postiface.toLatin1());
	m_iface.write(HDREND);
	m_iface.close();
	m_pri.write(QByteArray("\n#END OF AUTOGENERATED PRI FILE\n"));
	m_pri.close();
	m_hdr.write(HDREND);
	m_hdr.close();
	//cleanup directory (remove untouched normal files, assume remainder is harmless)
	QDir d(m_basedir+"/"+m_subdir);
	if(m_clean){
		QStringList ent=d.entryList(QDir::Files);
		for(int i=0;i<ent.size();i++)
			if(!MFile::touchedFile(m_basedir+"/"+m_subdir+"/"+ent[i])){
				qDebug("Info: removing old file %s",ent[i].toLatin1().data());
				d.remove(ent[i]);
			}
	}
}

void WocQtOut::addPostIface(QString s)
{
	m_postiface+=s;
}


void WocQtOut::newTable(const WocTable&tbl)
{
	if(qtable)qtable->newTable(tbl);
}

void WocQtOut::newClass(const WocClass&cls)
{
	if(qclass)qclass->newClass(cls);
}

void WocQtOut::newTransaction(const WocTransaction&trn)
{
	if(qtrans)qtrans->newTransaction(trn);
}

void WocQtOut::addFile(QString bn)
{
	QString code="HEADERS+="+m_subdir+"/src"+bn+".h\n";
	code+="SOURCES+="+m_subdir+"/src"+bn+".cpp\n";
	m_pri.write(code.toLatin1());
	m_hdr.write(QByteArray("#include \"%\"\n").replace('%',bn.toLatin1()));
	MFile wrap(m_basedir+"/"+m_subdir+"/"+bn);
	wrap.open();
	wrap.write(QString("#include \"src"+bn+".h\"\n").toLatin1());
}

void WocQtOut::addFile(WocQtOut::FileType ft,QString fn)
{
	QString code;
	switch(ft){
		case Header:code="HEADERS+="+m_subdir+"/"+fn+"\n";break;
		case Source:code+="SOURCES+="+m_subdir+"/"+fn+"\n";break;
	}
	m_pri.write(code.toLatin1());
}

void WocQtOut::addProjectLine(QString ln)
{
	m_pri.write(QString(ln+"\n").toLatin1());
}


QString WocQtOut::qttype(const WocClass&cls,QString p,bool dolist)
{
	QString r;
	if(cls.propertyIsList(p)&&dolist)r="QList<";
	else r="Nullable<";
	if(cls.propertyIsString(p))r+="QString";else
	if(cls.propertyIsInt(p))r+="qint64";else
	if(cls.propertyIsBool(p))r+="bool";else
	if(cls.propertyIsBlob(p))r+="QByteArray";else
	if(cls.propertyIsObject(p))r+=m_cprefix+"O"+cls.propertyPlainType(p);
	else r+=cls.propertyPlainType(p);
	r+=">";
	return r;
}

QString WocQtOut::qttype(const WocTransaction&trn,QString v,InOut io)
{
	QString tp=io==In?trn.inputType(v):trn.outputType(v);
	QString r,e;
	if(tp.startsWith("List:")){
	        r="QList<";e=">";
	        tp=tp.mid(5);
	}else
	        if(io==Out){
	                r="Nullable<";
	                e=">";
	        }
	if(tp=="astring" || tp=="string")r+="QString";else
	if(tp=="int"||tp=="int32"||tp=="int64")r+="qint64";else
	if(tp=="blob")r+="QByteArray";else
	if(tp=="bool")r+="bool";else
	if(tp==""){
	        qDebug("Warning: the final type of property %s is empty!",v.toLatin1().data());
	        r+="void";
	}else r+=m_cprefix+"O"+tp.split("/",Qt::SkipEmptyParts).at(0);
	r+=e;r+=" ";
	return r;
}

QString WocQtOut::qtobjtype(const WocTransaction&trn,QString v,InOut io)
{
	QString tp=io==In?trn.inputType(v):trn.outputType(v);
	if(tp.startsWith("List:"))
	        tp=tp.mid(5);
	if(tp=="astring" || tp=="string" || tp=="int" || tp=="int32" || tp=="int64" || tp=="bool" || tp=="blob")
	        return "";
	else
	        return m_cprefix+"O"+tp;
}

WocQtClientOut::WocQtClientOut(QDomElement&el)
	:WocQtOut(el)
{
	m_lang="qt/client";
	if(namePrefix()==sharedPrefix())
		qclass=new WocQtClass(this);
	qtrans=new WocQtClientTransaction(this);
}

WocQtServerOut::WocQtServerOut(QDomElement& el)
	:WocQtOut(el)
{
	m_lang="qt/server";
	if(namePrefix()==sharedPrefix())
		qclass=new WocQtClass(this);
	qtrans=new WocQtServerTransaction(this);
	qtable=new WocQtTable(this);
}


WocQtTransaction::WocQtTransaction(WocQtOut*p)
	:m_parent(p)
{
	connect(this,SIGNAL(errorFound()),p,SIGNAL(errorFound()));
}

#ifndef NO_PACK_VERSION_H
#include "../../vinfo/staticVersion.h"
#endif

static inline QString escapeStr(QString s)
{
	return s.replace("\\","\\\\").replace("\"","\\\"").replace("\n","\\n");
}

void WocQtOut::initVersionH()
{
	//create file
	MFile vhf;
	vhf.setFileName(m_basedir+"/"+m_subdir+"/staticVersion.h");
	if(!vhf.open(QIODevice::WriteOnly|QIODevice::Truncate)){
		qDebug("Error: cannot create Qt version info file %s.",vhf.fileName().toLatin1().data());
		emit errorFound();
		return;
	}
	vhf.write(QByteArray(HDRSTART).replace("%",m_prefix.toLatin1()+"STATIC_VERSION_H"));
	//generate functions for project's own values
	QString data="#include <WOb>\n#include <QString>\n#include <QMap>\n\n"
		"static inline QString WOCgenerated_versionInfo(WOb::VersionInfo vi){\nswitch(vi){\n";
	QString data2;
	QMap<QString,QString> vm=WocProcessor::instance()->versionInfo();
	QStringList vi=vm.keys();
	for(int i=0;i<vi.size();i++){
		data+="  case WOb::Version"+vi[i]+":return \"";
		QString val=escapeStr(vm[vi[i]]);
		data+=val+"\";\n";
		data2+="  vi.insert(\""+escapeStr(vi[i]);
		data2+="\",\""+val+"\");\n";
	}
	data+="  default:return QString();\n}}\n";
	data+="static inline void WOCgenerated_versionInfo(QMap<QString,QString>&vi){\nvi.clear();\n";
	data+=data2+"}\n";
	vhf.write(data.toUtf8());
	//copy values of Woc
	data="static inline QString WOCcopied_versionInfo(WOb::VersionInfo vi){\nswitch(vi){\n";
	data2.clear();
#ifndef NO_PACK_VERSION_H
	WOCgenerated_versionInfo(vm);
	vi=vm.keys();
	for(int i=0;i<vi.size();i++){
		data+="  case WOb::Version"+vi[i]+":return \"";
		QString val=escapeStr(vm[vi[i]]);
		data+=val+"\";\n";
		data2+="  vi.insert(\""+escapeStr(vi[i]);
		data2+="\",\""+val+"\");\n";
	}
#endif
	data+="  default:return QString();\n}}\n";
	data+="static inline void WOCcopied_versionInfo(QMap<QString,QString>&vi){\nvi.clear();\n";
	data+=data2+"}\n";
	vhf.write(data.toUtf8());
	//close file
	vhf.write(QByteArray(HDREND));
}

void WocQtOut::addIfaceImpl ( const QString& s )
{
	m_ifacecpp.write(s.toLatin1());
}

QByteArray WocQtOut::exportLines() const
{
        if(!m_export)return QByteArray();
        QByteArray ret="\n#ifndef "+exportSymbol().toLatin1()+
                "\n#define "+exportSymbol().toLatin1()+" Q_DECL_IMPORT\n"
                "#endif\n\n";
        return ret;
}

QString WocQtOut::exportSymbol() const
{
        if(!m_export)return QString();
        if(m_exportsym.isEmpty()){
                m_exportsym="WOBGEN_";
                QString r=WocProcessor::instance()->projectName();
                r+="_"+m_subdir;
                for(auto c:r.toUpper()){
                        if(c>='A' && c<='Z')m_exportsym+=c;
                        else if(c=='_')m_exportsym+="__";
                        else m_exportsym+="_"+QString::number((unsigned char)c.toLatin1(),16);
                }
                m_exportsym+="_EXPORT";
        }
        return m_exportsym;
}
