// Copyright (C) 2009-2012 by Konrad Rosenbaum <konrad@silmor.de>
// protected under the GNU GPL version 3 or at your option any newer.
// See COPYING.GPL file that comes with this distribution.
//

#include "qtout.h"
#include "qtclass.h"

#include <QDir>
#include <QDomElement>

#include "qtconst.h"
#include "doxyout.h"

WocQtClass::WocQtClass(WocQtOut* p)
{
	m_parent=p;
	connect(this,SIGNAL(errorFound()),p,SIGNAL(errorFound()));
}

WocQtClass::~WocQtClass()
{
}

void WocQtClass::finalize()
{
}


void WocQtClass::newClass(const WocClass&cls)
{
	QString cn=m_parent->sharedPrefix()+"O"+cls.name();
	QString cna=cn;
	if(cls.isAbstract(m_parent->languageSpec()))cna+="Abstract";
	m_parent->addFile(cna);
	m_parent->addIfaceHeaderPrefix("class "+cn+";\n");
	if(cn!=cna)m_parent->addIfaceHeaderPrefix("class "+cna+";\n");
	MFile hdr(m_parent->destinationPath()+"/src"+cna+".h");
	MFile src(m_parent->destinationPath()+"/src"+cna+".cpp");
	if(!hdr.open(QIODevice::WriteOnly|QIODevice::Truncate) ||
	   !src.open(QIODevice::WriteOnly|QIODevice::Truncate)){
	   	qDebug("Error: cannot create class files for class %s.",cn.toLatin1().data());
	   	emit errorFound();
	   	return;
	}
	//lead in
	hdr.write(QByteArray(HDRSTART).replace("%",cna.toLatin1()));
	src.write(QByteArray(SRCSTART).replace("%",cna.toLatin1()));
        hdr.write(m_parent->exportLines());

	QString hcd;
	QString scd;
	//includes
	hcd="#include \""+cls.baseClass(m_parent->languageSpec(),m_parent->namePrefix()+"O")+ "\"\n#include <QCoreApplication>\n\n";
	QStringList k=cls.propertyNames();
	for(int i=0;i<k.size();i++)
		if(cls.propertyIsObject(k[i]))
			hcd+="#include \""+m_parent->namePrefix()+"O"+cls.propertyPlainType(k[i])+"\"\n";

	//class declaration
	QString cnb=cls.baseClass(m_parent->languageSpec(),m_parent->namePrefix()+"O");
	hcd+=doxyFormat(cls.docStrings());
	hcd+="class "+m_parent->exportSymbol()+" "+cna+":public "+cnb+"\n{\n  Q_GADGET\n";
	hdr.write(hcd.toLatin1());

	//enums
	classEnums(cls,hdr,src,cna);

	//properties
	classProperties(cls,hdr,src);

	//serializer
	classSerializers(cls,hdr,src,cna);

	//deserializer (constructor)
	classDeserializer(cls,hdr,src,cna);

	//meta types
	m_aux+="Q_DECLARE_METATYPE("+cna+")\n";
	m_aux+="Q_DECLARE_METATYPE(QList<"+cna+">)\n";
	m_aux+="Q_DECLARE_METATYPE(Nullable<"+cna+">)\n";
	scd="static int class_metatypeid=";
	scd+="\tqRegisterMetaType<"+cna+">()+";
	scd+="\tqRegisterMetaType<QList<"+cna+"> >()+";
	scd+="\tqRegisterMetaType<Nullable<"+cna+"> >();\n";
	scd+="static bool class_converter=QMetaType::registerConverter<Nullable<"+cna+">,"+cna+">([](const Nullable<"+cna+">&n){return n.value();})|\n";
	scd+="\tQMetaType::registerConverter<QList<"+cna+">,QVariantList>([](const QList<"+cna+">&n){QVariantList r;for(auto v:n)r<<QVariant::fromValue(v);return r;});\n";

	//end of class, aux stuff, meta types
	hdr.write(QByteArray("\n};\n"));
	hdr.write(m_aux.toLatin1());m_aux.clear();
	src.write(scd.toLatin1());

	//lead out
	hdr.write(QByteArray(HDREND).replace("%",cna.toLatin1()));
	src.write(QByteArray(SRCEND).replace("%",cna.toLatin1()));
}

void WocQtClass::classEnums(const WocClass&cls,MFile&hdr,MFile&src,QString cn)
{
	QStringList k=cls.enumTypes();
	if(k.size()==0)return;
	QString hcd="  public:\n";
	QString scd;
	for(int i=0;i<k.size();i++){
		//meta type
		hcd+="\tQ_ENUMS("+k[i]+");\n";
		m_aux+="Q_DECLARE_METATYPE("+cn+"::"+k[i]+")\n";
		m_aux+="Q_DECLARE_METATYPE(QList<"+cn+"::"+k[i]+">)\n";
		scd+="static int enum_"+k[i]+"_metatypeid=qRegisterMetaType<"+cn+"::"+k[i]+">();\n";
		scd+="static int enum_"+k[i]+"_metatypeid2=qRegisterMetaType<QList<"+cn+"::"+k[i]+"> >();\n";
		//type
		hcd+=doxyFormat(cls.enumDoc(k[i]),1);
		hcd+="\tenum "+k[i]+"{\n";
		QList<WocEnum>ev=cls.enumValues(k[i]);
		if(ev.size()<1){
			qDebug("Error: enums must have at least one member - class %s enum %s",cls.name().toLatin1().data(),k[i].toLatin1().data());
			emit errorFound();
			return;
		}
		for(int j=0;j<ev.size();j++){
			if(j)hcd+=",\n";
			hcd+=doxyFormat(ev[j].doc,2);
			hcd+="\t\t"+ev[j].name+"="+QString::number(ev[j].val);
		}
		hcd+="\n\t};\n";
		//string converters
		hcd+="\t/// Converts string into the corresponding enum "+k[i]+" value.\n";
		hcd+="\tstatic "+k[i]+" str2"+k[i]+"(QString,bool*ok=0);\n";
		hcd+="\t/// Converts enum "+k[i]+" value into the corresponding string.\n";
		hcd+="\tstatic QString "+k[i]+"2str("+k[i]+");\n";
		scd+=cn+"::"+k[i]+" "+cn+"::str2"+k[i]+"(QString s,bool*ok)\n{\n";
		scd+="\ts=s.toLower();if(ok)*ok=true;\n";
		for(int j=0;j<ev.size();j++){
			scd+="\tif(s==\""+ev[j].name.toLower()+"\")return "+ev[j].name+";\n";
		}
		scd+="\tif(ok)*ok=false;\n\treturn "+ev[0].name+";\n}\n";
		scd+="QString "+cn+"::"+k[i]+"2str("+k[i]+" e)\n{\n";
		for(int j=0;j<ev.size();j++){
			scd+="\tif(e=="+ev[j].name+")return \""+ev[j].name+"\";\n";
		}
		scd+="\treturn \"\";\n}\n";
		//localized string converters
		hcd+="\t/// Converts a localized string into the corresponding enum "+k[i]+" value.\n";
		hcd+="\tstatic "+k[i]+" locstr2"+k[i]+"(QString,bool*ok=0);\n";
		hcd+="\t/// Converts enum "+k[i]+" value into the corresponding localized string.\n";
		hcd+="\tstatic QString "+k[i]+"2locstr("+k[i]+");\n";
		scd+=cn+"::"+k[i]+" "+cn+"::locstr2"+k[i]+"(QString s,bool*ok)\n{\n";
		scd+="\ts=s.toLower();if(ok)*ok=true;\n";
		for(int j=0;j<ev.size();j++){
			scd+="\tif(s==QCoreApplication::translate(\""+cn+"\",\""+ev[j].name+"\").toLower())return "+ev[j].name+";\n";
		}
		scd+="\tif(ok)*ok=false;\n\treturn "+ev[0].name+";\n}\n";
		scd+="QString "+cn+"::"+k[i]+"2locstr("+k[i]+" e)\n{\n";
		for(int j=0;j<ev.size();j++){
			scd+="\tif(e=="+ev[j].name+")return QCoreApplication::translate(\""+cn+"\",\""+ev[j].name+"\");\n";
		}
		scd+="\treturn \"\";\n}\n";
	}
	hdr.write(hcd.toLatin1());
	src.write(scd.toLatin1());
}

void WocQtClass::classProperties(const WocClass&cls,MFile&hdr,MFile&)
{
	QStringList k=cls.propertyNames();
	if(k.size()==0)return;
	QString hcd,scd;
	//declare property
	for(int i=0;i<k.size();i++){
		hcd+=doxyFormat(cls.propDoc(k[i]),1);
		hcd+="\tQ_PROPERTY("+m_parent->qttype(cls,k[i])+" "+k[i]+" READ "+k[i]+" WRITE set"+k[i]+")\n";
	}
	//declare members
	hcd+="  protected:\n";
	for(int i=0;i<k.size();i++){
		hcd+="\t"+m_parent->qttype(cls,k[i])+" mp_"+k[i]+";\n";
	}
	//declare getters
	hcd+="  public:\n";
	for(int i=0;i<k.size();i++){
		hcd+=doxyFormat(cls.propDoc(k[i]),1);
		hcd+="\tvirtual "+m_parent->qttype(cls,k[i])+" "+k[i]+"()const{return mp_"+k[i]+";}\n";
	}

	//declare setters
	for(int i=0;i<k.size();i++){
		hcd+=doxyFormat(cls.propDoc(k[i]),1);
		hcd+="\tvirtual void set"+k[i]+"("+m_parent->qttype(cls,k[i])+" s){mp_"+k[i]+"=s;}\n";
		if(cls.propertyIsList(k[i])){
			hcd+="\tvirtual void clear"+k[i]+"(){mp_"+k[i]+".clear();}\n";
			hcd+="\tvirtual void add"+k[i]+"("+m_parent->qttype(cls,k[i],false)+" a){mp_"+k[i]+".append(a);}\n";
		}
	}
	//write
	hdr.write(hcd.toLatin1());
}

void WocQtClass::classDeserializer(const WocClass&cls,MFile&hdr,MFile&src,QString cn)
{
	QString hcd,scd;
	hcd="  public:\n";
	//this is needed to allow WObjects to be Nullable and some other operations...
	QString cnb=cls.baseClass(m_parent->languageSpec(),m_parent->namePrefix()+"O");
	hcd+="\t/// default constructor: constructs an invalid instance of "+cn+"\n";
	hcd+="\t"+cn+"():"+cnb+"(){}\n";

	//implement copiers
	QStringList k=cls.propertyNames();
	hcd+="\t/// copy constructor: creates a (deep) copy of the object\n";
	hcd+="\t"+cn+"(const "+cn+"&);\n";
	scd+=cn+"::"+cn+"(const "+cn+"&o)\n\t:"+cnb+"(o)\n{\n";
	for(int i=0;i<k.size();i++)
		scd+="\tmp_"+k[i]+"=o.mp_"+k[i]+";\n";
	scd+="}\n";
	hcd+="\t/// copy assignment: creates a (deep) copy of the object\n";
	hcd+="\t"+cn+"& operator=(const "+cn+"&);\n";
	scd+=cn+"& "+cn+"::operator=(const "+cn+"&o)\n{\n";
	scd+="\tif(this == &o)return *this;\n";
	scd+="\t"+cnb+"::operator=(o);\n";
	for(int i=0;i<k.size();i++)
		scd+="\tmp_"+k[i]+"=o.mp_"+k[i]+";\n";
	scd+="\treturn *this;\n}\n";

    //implement comparison
    hcd+="\t/// equality comparison operator\n";
    hcd+="\tbool operator==(const "+cn+"&)const;\n";
    hcd+="\t/// inequality comparison operator\n";
    hcd+="\tinline bool operator!=(const "+cn+"&o)const{return !operator==(o);}\n";
    scd+="bool "+cn+"::operator==(const "+cn+"&o)const\n{\n";
    scd+="\treturn "+cnb+"::operator==(o)";
    for(int i=0;i<k.size();i++)
        scd+=" && mp_"+k[i]+"==o.mp_"+k[i];
    scd+=";\n}\n";

	//implement deserializer (as constructor)
	hcd+="\t/// special constructor: create from the XML representation, deserializing the object\n";
	hcd+="\texplicit "+cn+"(const QDomElement&);\n";
	scd+=cn+"::"+cn+"(const QDomElement&root)\n\t:"+cnb+"(root)\n{\n";
	scd+="\tQList<QDomElement> nl;\n";
	for(int i=0;i<k.size();i++){
		if(cls.propertyIsList(k[i])){
			scd+="\tnl=elementsByTagName(root,\""+k[i]+"\");\n";
			scd+="\tfor(int i=0;i<nl.size();i++){\n\t\tQDomElement el=nl.at(i).toElement();\n";
			scd+="\t\tif(el.isNull())continue;\n";
			if(cls.propertyIsInt(k[i])){
				scd+="\t\tbool b;\n\t\tint ct=el.text().toInt(&b);\n";
				scd+="\t\tif(b)add"+k[i]+"(ct);\n";
				scd+="\t\telse throw WDeserializerException(QCoreApplication::translate(\"WobTransaction\",\"Class '%1' property '%2' is integer list, but non-integer was found.\").arg(\""+cn+"\").arg(\""+k[i]+"\"));\n";
			}else
			if(cls.propertyIsBool(k[i])){
				scd+="\t\tadd"+k[i]+"(str2bool(el.text()));\n";
			}else
			if(cls.propertyIsString(k[i])){
				scd+="\t\tadd"+k[i]+"(el.text());\n";
			}else
			if(cls.propertyIsBlob(k[i])){
				scd+="\t\tadd"+k[i]+"(QByteArray::fromBase64(el.text().toLatin1()));\n";
			}else
			if(cls.propertyIsEnum(k[i])){
				scd+="\t\tbool b;\n";
				QString pt=cls.propertyPlainType(k[i]);
				scd+="\t\t"+pt+" ct=str2"+pt+"(root.attribute(\""+k[i]+"\"),&b);\n";
				scd+="\t\tif(b)add"+k[i]+"(ct);\n";
				scd+="\t\telse throw WDeserializerException(QCoreApplication::translate(\"WobTransaction\",\"Class '%1' property '%2' is enum list, invalid value was found.\").arg(\""+cn+"\").arg(\""+k[i]+"\"));\n";
			}else
			if(cls.propertyIsObject(k[i])){
				scd+="\t\tadd"+k[i]+"("+m_parent->namePrefix()+ "O"+cls.propertyPlainType(k[i])+"(el));\n";
			}else{
				scd+="#error \"Internal Generator error.\"\n";
				qDebug("Error: unable to generate code for property %s of type %s.", k[i].toLatin1().data(),cls.propertyType(k[i]).toLatin1().data());
				emit errorFound();
				return;
			}
			scd+="\t}\n";
		}else{
			//non-list types
			//attribute stored types
			if(cls.propertyIsAttribute(k[i])){
				scd+="\tif(root.hasAttribute(\""+k[i]+"\")){\n";
				if(cls.propertyIsInt(k[i])){
					scd+="\t\tbool b;\n\t\tint ct=root.attribute(\""+k[i]+"\").toInt(&b);\n";
					scd+="\t\tif(b)set"+k[i]+"(ct);\n";
					scd+="\t\telse throw WDeserializerException(QCoreApplication::translate(\"WobTransaction\",\"Class '%1' property '%2' is integer, but non-integer was found.\").arg(\""+cn+"\").arg(\""+k[i]+"\"));\n";
				}else
				if(cls.propertyIsBool(k[i])){
					scd+="\t\tset"+k[i]+"(str2bool(root.attribute(\""+k[i]+"\")));\n";
				}else
				if(cls.propertyIsString(k[i])){
					scd+="\t\tset"+k[i]+"(root.attribute(\""+k[i]+"\"));\n";
				}else
				if(cls.propertyIsEnum(k[i])){
					scd+="\t\tbool b;\n";
					QString pt=cls.propertyPlainType(k[i]);
					scd+="\t\t"+pt+" ct=str2"+pt+"(root.attribute(\""+k[i]+"\"),&b);\n";
					scd+="\t\tif(b)set"+k[i]+"(ct);\n";
					scd+="\t\telse throw WDeserializerException(QCoreApplication::translate(\"WobTransaction\",\"Class '%1' property '%2' is enum, invalid value was found.\").arg(\""+cn+"\").arg(\""+k[i]+"\"));\n";
				}else{
					scd+="#error \"Internal Generator error.\"\n";
					qDebug("Error: unable to generate code for property %s of type %s.",k[i].toLatin1().data(),cls.propertyType(k[i]).toLatin1().data());
					emit errorFound();
					return;
				}
				scd+="\t}\n";
			}else{
				//element stored types...
				scd+="\tnl=elementsByTagName(root,\""+k[i]+"\");\n";
				scd+="\tif(nl.size()>0){\n";
				if(cls.propertyIsString(k[i])){
					scd+="\t\tset"+k[i]+"(nl.at(0).toElement().text());\n";
				}else
				if(cls.propertyIsBlob(k[i])){
					scd+="\t\tset"+k[i]+ "(QByteArray::fromBase64(nl.at(0).toElement().text().toLatin1()));\n";
				}else
				if(cls.propertyIsObject(k[i])){
					scd+="\t\tset"+k[i]+"("+ m_parent->namePrefix()+ "O"+cls.propertyPlainType(k[i])+ "(nl.at(0).toElement()));\n";
				}else{
					scd+="#error \"Internal Generator error.\"\n";
					qDebug("Error: unable to generate code for property %s of type %s.",k[i].toLatin1().data(),cls.propertyType(k[i]).toLatin1().data());
					emit errorFound();
					return;
				}
				scd+="\t}\n";
			}
		}
	}
	scd+="}\n";
        hcd+="\t///create "+cn+" from XML (inverse of toXml)\n";
        hcd+="\tstatic "+cn+" fromXml(const QDomElement&);\n";
        scd+=cn+" "+cn+"::fromXml(const QDomElement&root){return "+cn+"(root);}\n";
        hcd+="\t///create "+cn+" from XML formatted string (inverse of toString)\n";
        hcd+="\tstatic "+cn+" fromString(const QString&);\n";
        scd+=cn+" "+cn+"::fromString(const QString&txt)\n{\n\tQDomDocument doc;\n";
        scd+="\tif(!doc.setContent(txt))return "+cn+"();\n";
        scd+="\telse return "+cn+"(doc.documentElement());\n}\n";
	hcd+="\t/// destructor: deletes this copy of the object\n";
	hcd+="\tvirtual ~"+cn+"(){}\n";

	//write it...
	hdr.write(hcd.toLatin1());
	src.write(scd.toLatin1());
}

void WocQtClass::classSerializers(const WocClass&cls,MFile&hdr,MFile&src,QString cn)
{
	QString hcd="  public:\n";
	QString scd;
	hcd+="\t/// Serializes the object to XML and returns the string representation of that XML.\n";
	hcd+="\tQString toString();\n";
	scd+="QString "+cn+"::toString()\n{\n";
	scd+="\tQDomDocument doc;\n\tdoc.appendChild(toXml(doc));\n";
	scd+="\treturn doc.toString();\n}\n";
	hcd+="\t/// Transforms the object into its XML representation, the element node returned is not appended to the document - you have to do that yourself.\n";
	hcd+="\t/// \\param doc the DOM document node for which to generate the element\n";
	hcd+="\t/// \\param name the name to give the generated element, per default \""+cn+"\" is used\n";
	hcd+="\tQDomElement toXml(QDomDocument&doc,QString name=\""+cls.name()+"\");\n";
	hcd+="\t/// Serializes the object into the given element.\n";
	hcd+="\tvoid toXml(QDomDocument&,QDomElement&);\n";
	scd+="QDomElement "+cn+"::toXml(QDomDocument&doc,QString name)\n{\n";
	scd+="\tQDomElement r=doc.createElement(name);\n\ttoXml(doc,r);\n";
	scd+="\treturn r;\n}\n";
	scd+="void "+cn+"::toXml(QDomDocument&doc,QDomElement&r)\n{\n";
	scd+="\t"+cls.baseClass(m_parent->languageSpec(),m_parent->namePrefix()+"O")+"::toXml(doc,r);\n";
	QStringList p=cls.propertyNames();
	for(int j=0;j<p.size();j++){
		QStringList pv=p[j].split("/",Qt::SkipEmptyParts);
		if(pv.size()<1){
			qDebug("Error: encountered empty property while creating serializer for class %s.",cls.name().toLatin1().data());
			emit errorFound();
			return;
		}
		QString prop=pv[0];
		QString var;
		if(pv.size()>1)var=pv[1];
		//is it a list
		if(cls.propertyIsList(prop)){
			scd+="\tfor(int i=0;i<mp_"+prop+".size();i++){\n";
			if(cls.propertyIsObject(prop))
				scd+="\t\tr.appendChild(mp_"+prop+"[i].toXml"+var+"(doc,\""+prop+"\"));\n";
			else
			if(cls.propertyIsEnum(prop)){
				scd+="\t\tQDomElement el=doc.createElement(\""+prop+"\");\n";
				scd+="\t\tel.appendChild(doc.createTextNode("+cls.propertyPlainType(prop)+"2str(mp_"+prop+"[i])));\n";
				scd+="\t\tr.appendChild(el);\n";
			}else
			if(cls.propertyIsString(prop)){
				scd+="\t\tQDomElement el=doc.createElement(\""+prop+"\");\n";
				scd+="\t\tel.appendChild(doc.createTextNode(mp_"+prop+"[i]));\n";
				scd+="\t\tr.appendChild(el);\n";
			}else
			if(cls.propertyIsBool(prop)){
				scd+="\t\tQDomElement el=doc.createElement(\""+prop+"\");\n";
				scd+="\t\tel.appendChild(doc.createTextNode(mp_"+prop+"[i]?\"true\":\"false\"));\n";
				scd+="\t\tr.appendChild(el);\n";
			}else
			if(cls.propertyIsBlob(prop)){
				scd+="\t\tQDomElement el=doc.createElement(\""+prop+"\");\n";
				scd+="\t\tel.appendChild(doc.createTextNode(mp_"+prop+"[i].toBase64()));\n";
				scd+="\t\tr.appendChild(el);\n";
			}else
			if(cls.propertyIsInt(prop)){
				scd+="\t\tQDomElement el=doc.createElement(\""+prop+"\");\n";
				scd+="\t\tel.appendChild(doc.createTextNode(QString::number(mp_"+prop+"[i])));\n";
				scd+="\t\tr.appendChild(el);\n";
			}else{
				qDebug("Error: cannot generate serializer for class %s property %s.",cls.name().toLatin1().data(),prop.toLatin1().data());
				emit errorFound();
				return;
			}
			scd+="\t}\n";
		}else{
			//non lists
			scd+="\tif(!mp_"+prop+".isNull()){\n";
			if(cls.propertyIsAttribute(prop)){
				if(cls.propertyIsBool(prop))
					scd+="\t\tr.setAttribute(\""+prop+"\",mp_"+prop+".value()?\"true\":\"false\");\n";
				else
					scd+="\t\tr.setAttribute(\""+prop+"\",mp_"+prop+".value());\n";
			}else{
				if(cls.propertyIsObject(prop)){
					scd+="\t\tr.appendChild(mp_"+prop+".value().toXml"+var+"(doc,\""+prop+"\"));\n";
				}else
				if(cls.propertyIsString(prop)){
					scd+="\t\tQDomElement el=doc.createElement(\""+prop+"\");\n";
					scd+="\t\tel.appendChild(doc.createTextNode(mp_"+prop+".value()));\n";
					scd+="\t\tr.appendChild(el);\n";
				}else
				if(cls.propertyIsBlob(prop)){
					scd+="\t\tQDomElement el=doc.createElement(\""+prop+"\");\n";
					scd+="\t\tel.appendChild(doc.createTextNode(mp_"+prop+".value().toBase64()));\n";
					scd+="\t\tr.appendChild(el);\n";
				}else{
					qDebug("Error: cannot generate serializer for class %s property %s.",cls.name().toLatin1().data(),prop.toLatin1().data());
					emit errorFound();
					return;
				}
			}
			scd+="\t}\n";
		}
	}
	scd+="\n}\n";
	hdr.write(hcd.toLatin1());
	src.write(scd.toLatin1());
}
