SOURCES+= \
	$$PWD/qtout.cpp \
	$$PWD/qtclass.cpp \
	$$PWD/qtdb.cpp \
	$$PWD/qtctrans.cpp \
	$$PWD/qtstrans.cpp

HEADERS+= \
	$$PWD/qtout.h \
	$$PWD/qtclass.h \
	$$PWD/qtdb.h \
	$$PWD/qtctrans.h \
	$$PWD/qtstrans.cpp

INCLUDEPATH += $$PWD
