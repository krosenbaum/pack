// Copyright (C) 2009-2011 by Konrad Rosenbaum <konrad@silmor.de>
// protected under the GNU GPL version 3 or at your option any newer.
// See COPYING.GPL file that comes with this distribution.
//

#ifndef WOC_QTOUT_STRANS_H
#define WOC_QTOUT_STRANS_H

#include "qtout.h"

class QtSTrans;

///Specialization for generating transactions for a Qt server target, used by WocQtServerOut
class WocQtServerTransaction:public WocQtTransaction
{
	public:
		///generates the object for a specific output
		explicit WocQtServerTransaction(WocQtOut*);
		~WocQtServerTransaction();
		///called when all parsing is done
		virtual void finalize();
		///generates code for one transaction
		virtual void newTransaction(const WocTransaction&);
	private:
		/**helper generates the transaction input encoding*/
		QString trnInput(const WocTransaction&);
		/**helper generates the transaction output decoding*/
		QString trnOutput(const WocTransaction&);
		/**helper generates enums and strings for all transactions*/
		void trnList();
		///helper: generate include section
		void genInclude(QtSTrans&);
		///helper: generate properties
		void genProperties(QtSTrans&);
		///helper: generate con- and de-structors
		void genTors(QtSTrans&);
		///helper: generate query methods
		void genQuery(QtSTrans&);
		///helper: generate getter methods
		void genGetters(QtSTrans&);
		///helper: generate setter methods
		void genSetters(QtSTrans&);
                ///helper: generate log control
                void genLogCtrl(QtSTrans&);
};

#endif
