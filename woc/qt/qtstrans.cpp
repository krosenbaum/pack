// Copyright (C) 2009-2012 by Konrad Rosenbaum <konrad@silmor.de>
// protected under the GNU GPL version 3 or at your option any newer.
// See COPYING.GPL file that comes with this distribution.
//

#include "qtout.h"
#include "qtstrans.h"

#include "qtconst.h"

WocQtServerTransaction::WocQtServerTransaction(WocQtOut*p)
	:WocQtTransaction(p)
{
}
WocQtServerTransaction::~WocQtServerTransaction(){}

void WocQtServerTransaction::finalize()
{
	trnList();
}


///internal helper to hold the output for one transaction, this is handed around WocQtServerTransaction
struct QtSTrans{
	///class name
	QString cn;
	///private (d-ptr) class name
	QString cnp;
	///default interface name, used as a default parameter in query(...)
	QString defparm;
	//code store
	///part of the header: class decl
	QString hcd;
	///part of the header: #include's
	QString hdi;
	///private (d-ptr) class decl
	QString pcd;
	///part of cpp source: implementations
	QString scd;
	///cpp source: #include's
	QString sri;
	//in/out params
	///input parameters of the transaction
	QStringList in;
	///output parameters of the transaction
	QStringList out;
	///transaction itself, from the front-end
	const WocTransaction&trn;
	///convenience constructor
	QtSTrans(const WocTransaction&t,QString cn_,QString cnp_):trn(t){
		in=trn.inputNames();
		out=trn.outputNames();
		cn=cn_;cnp=cnp_;
	}
};

void WocQtServerTransaction::newTransaction(const WocTransaction&trn)
{
	QString cn=m_parent->namePrefix()+"T"+trn.name();
	QString cnp=cn+"_Private";
	m_parent->addFile(cn);
	MFile hdr(m_parent->destinationPath()+"/src"+cn+".h");
	MFile src(m_parent->destinationPath()+"/src"+cn+".cpp");
	if(!hdr.open(QIODevice::WriteOnly|QIODevice::Truncate) ||
	   !src.open(QIODevice::WriteOnly|QIODevice::Truncate)){
	   	qDebug("Error: cannot create class files for transaction %s.",cn.toLatin1().data());
	   	emit errorFound();
	   	return;
	}
	//basics
	QtSTrans ct(trn,cn,cnp);
	//lead in
	hdr.write(QByteArray(HDRSTART).replace("%",cn.toLatin1()));
	src.write(QByteArray(SRCSTART).replace("%",cn.toLatin1()));
        hdr.write(m_parent->exportLines());
	
	//start constructing code
	
	//include section
	genInclude(ct);
	//start of class
	ct.hcd+="\nclass "+cnp+";\n";
	ct.hcd+="\nclass "+m_parent->exportSymbol()+" "+cn+":public "+m_parent->transactionBase()+"\n{\n  Q_OBJECT\n";
	ct.pcd+="\nclass "+cnp+":public WTransaction_PrivateBase\n{\n";
	
	//create properties
	genProperties(ct);
	
	//create constructors
	ct.defparm="=\""+WocProcessor::instance()->projectName()+"\"";
	genTors(ct);
	
	//query method implemented
	genQuery(ct);

	//create getters
	genGetters(ct);
	genSetters(ct);
	
        //create log control
        genLogCtrl(ct);
	
	//button class up
	ct.hcd+="};\n\n";
	ct.pcd+="};\n\n";
	
	//make meta object...
	ct.hcd+="Q_DECLARE_METATYPE("+cn+")\n";
	ct.scd+="static int mymetatypeid=qRegisterMetaType<"+cn+">();\n";
	
	//write code
	hdr.write(ct.hdi.toLatin1());
	hdr.write(ct.hcd.toLatin1());
	src.write(ct.sri.toLatin1());
	src.write(ct.pcd.toLatin1());
	src.write(ct.scd.toLatin1());
	
	//lead out
	hdr.write(QByteArray(HDREND).replace("%",cn.toLatin1()));
	src.write(QByteArray(SRCEND).replace("%",cn.toLatin1()));
}

void WocQtServerTransaction::genInclude(QtSTrans&ct)
{
	ct.hdi+="#include \""+m_parent->transactionBase()+"\"\n";
	ct.sri+="#include \"WTransaction_Private\"\n";
	ct.sri+="#include \""+m_parent->ifaceClassName()+"\"\n";
	ct.sri+="#include <QCoreApplication>\n\n";
	for(int i=0;i<ct.in.size();i++){
		QString tp=m_parent->qtobjtype(ct.trn,ct.in[i],WocQtOut::In);
		if(tp!="")ct.hdi+="#include <"+tp+">\n";
	}
	for(int i=0;i<ct.out.size();i++){
		QString tp=m_parent->qtobjtype(ct.trn,ct.out[i],WocQtOut::Out);
		if(tp!="")ct.hdi+="#include <"+tp+">\n";
	}
}

void WocQtServerTransaction::genProperties(QtSTrans&ct)
{
	ct.hcd+="  private:\n\t"+ct.cnp+"*p;\n\tfriend class "+ct.cnp+";\n";
	ct.pcd+="  protected:\n\tfriend class "+ct.cn+";\n";
	ct.pcd+="\t"+ct.cnp+"("+ct.cn+"*parent){parent->p=this;}\n";
	ct.pcd+="\tvoid attach("+ct.cn+"*parent){parent->p=this;WTransaction_PrivateBase::attach();}\n";
	ct.pcd+="\tvoid detach("+ct.cn+"*parent){parent->p=0;WTransaction_PrivateBase::detach();}\n";
	for(int i=0;i<ct.in.size();i++){
		ct.pcd+="\t"+m_parent->qttype(ct.trn,ct.in[i],WocQtOut::In)+"in_"+ct.in[i]+";\n";
	}
	for(int i=0;i<ct.out.size();i++)
		ct.pcd+="\t"+m_parent->qttype(ct.trn,ct.out[i],WocQtOut::Out)+"out_"+ct.out[i]+";\n";
	ct.pcd+="\tWServerRequest request;\n";
}

void WocQtServerTransaction::genTors(QtSTrans&ct)
{
	//define parametric constructor
	ct.hcd+="  protected:\n";
	ct.hcd+="\tfriend class "+m_parent->ifaceClassName()+";\n";
	ct.hcd+="\texplicit "+ct.cn+"(QString,const WServerRequest&);\n";
	//parametric constructor implementation
	ct.scd+=ct.cn+"::"+ct.cn+"(QString iface,const WServerRequest&request)\n";
	ct.scd+="\t:"+m_parent->transactionBase()+"(iface)\n{\n";
	ct.scd+="\tnew "+ct.cnp+"(this);\n";
	ct.scd+="\tp->request=request;\n";
	ct.scd+="\tdecodeData(request.bodyData());\n";
	ct.scd+="}\n\n";
	//decl default constructor
	ct.hcd+="  public:\n";
	ct.hcd+="\t"+ct.cn+"();\n";
	ct.scd+=ct.cn+"::"+ct.cn+"()\n{\n\tnew "+ct.cnp+"(this);\n}\n\n";
	//decl copy constructor
	ct.hcd+="\t"+ct.cn+"(const "+ct.cn+"&);\n";
	//copy constructor implementation
	ct.scd+=ct.cn+"::"+ct.cn+"(const "+ct.cn+"&t)\n\t:"+m_parent->transactionBase()+"(t)\n{\n";
	ct.scd+="\tt.p->attach(this);\n";
	ct.scd+="}\n\n";
	//decl copy operator
	ct.hcd+="\t"+ct.cn+"& operator=(const "+ct.cn+"&);\n";
	//copy operator implemented
	ct.scd+=ct.cn+"& "+ct.cn+"::operator=(const "+ct.cn+"&t)\n{\n";
	ct.scd+="\tif(this==&t)return *this;\n";
	ct.scd+="\t"+m_parent->transactionBase()+"::operator=(t);\n";
	ct.scd+="\tp->detach(this);t.p->attach(this);\n";
	ct.scd+="\treturn *this;\n}\n\n";
	
	//destructor
	ct.hcd+="\t~"+ct.cn+"();\n";
	ct.scd+=ct.cn+"::~"+ct.cn+"()\n{\n\tp->detach(this);\n}\n\n";
}

void WocQtServerTransaction::genQuery(QtSTrans& ct)
{
	//method decl
	ct.hcd+="  private:\n";
	ct.hcd+="\tQByteArray encodeData();\n";
	ct.hcd+="\tvoid decodeData(QByteArray);\n";
	//encode input
	ct.scd+="QByteArray "+ct.cn+"::encodeData()\n{\n";
	ct.scd+=trnOutput(ct.trn);
	ct.scd+="}\n";
	//decode output
	ct.scd+="void "+ct.cn+"::decodeData(QByteArray rba)\n{\n";
	ct.scd+=trnInput(ct.trn);
	ct.scd+="}\n";
	//execution method
	ct.hdi+="class WServerRequest;class WServerReply;\n";
	ct.sri+="#include<WServerRequest>\n#include<WServerReply>\n";
	ct.hcd+="  protected:\n\tvoid executeLocal();\n";
	ct.hcd+="  public:\n\tWServerReply execute();\n";
	ct.scd+="WServerReply "+ct.cn+"::execute()\n{\n\texecuteLocal();\n";
	//encode reply
	ct.scd+="\tWServerReply reply;\n";
	ct.scd+="\treply.setStatus(200,\"Ok\");\n";
	ct.scd+="\tif(d->m_stage!=Error){\n";
	ct.scd+="\t\treply.setHeader(\"X-WobResponse-Status\",\"Ok\");\n";
	ct.scd+="\t\treply.setBody(encodeData());\n";
	ct.scd+="\t}else{\n";
	ct.scd+="\t\treply.setHeader(\"X-WobResponse-Status\",\"Error\");\n";
	ct.scd+="\t\treply.setBody(encodeError());\n\t}\n";
	ct.scd+="\treturn reply;\n}\n";
	//local part of executor
	ct.scd+="void "+ct.cn+"::executeLocal()\n{\n";
	//add auth check
	if(ct.trn.authMode()!=WocTransaction::Open){
		ct.scd+="\t"+m_parent->ifaceClassName()+" *iface="+m_parent->ifaceClassName()+"::instance(interface());\n";
		ct.scd+="\tif(iface==0){\n";
		ct.scd+="\t\td->m_stage=Error;d->m_errtype=\"_iface\";d->m_errstr=\"Interface not found.\";\n";
		ct.scd+="\t\treturn;\n\t}\n";
		ct.scd+="\tif(!iface->isAuthenticated(p->request)){\n";
		ct.scd+="\t\td->m_stage=Error;d->m_errtype=\"_auth\";d->m_errstr=\"Not authenticated.\";\n";
		ct.scd+="\t\treturn;\n\t}\n";
		if(ct.trn.authMode()==WocTransaction::Checked){
			ct.scd+="\tif(!iface->hasRight(p->request,"+m_parent->ifaceClassName()+"::R"+ct.trn.name()+")){\n";
			ct.scd+="\t\td->m_stage=Error;d->m_errtype=\"_auth\";d->m_errstr=\"User does not have access right.\";\n";
			ct.scd+="\t\treturn;\n\t}\n";
		}
	}
	//execute
	ct.scd+="\t"+ct.trn.callFunction("qt/server")+"\n";
	QString inc=ct.trn.callInclude("qt/server");
	if(inc!="")
		ct.sri+="#include \""+inc+"\"\n";
	ct.scd+="}\n";
	//set error method
	ct.hcd+="\tvoid setError(QString type,QString errorText);\n";
	ct.scd+="void "+ct.cn+"::setError(QString type,QString errorText)\n{\n";
	ct.scd+="\td->m_stage=Error;d->m_errtype=type;d->m_errstr=errorText;\n";
	ct.scd+="}\n";
}

void WocQtServerTransaction::genGetters(QtSTrans& ct)
{
	ct.hcd+="  public:\n";
	for(int i=0;i<ct.in.size();i++){
		QString tp=m_parent->qttype(ct.trn,ct.in[i],WocQtOut::In);
		ct.hcd+="\tQ_SLOT "+tp+" get"+ct.in[i]+"();\n";
		ct.scd+=tp+" "+ct.cn+"::get"+ct.in[i]+"(){return p->in_"+ct.in[i]+";}\n";
	}
}
void WocQtServerTransaction::genSetters(QtSTrans& ct)
{
	for(int i=0;i<ct.out.size();i++){
		QString tp=m_parent->qttype(ct.trn,ct.out[i],WocQtOut::Out);
		ct.hcd+="\tQ_SLOT void set"+ct.out[i]+"(const "+tp+"&);\n";
		ct.scd+="void "+ct.cn+"::set"+ct.out[i]+"(const "+tp+"&obj){p->out_"+ct.out[i]+"=obj;}\n";
	}
}

void WocQtServerTransaction::genLogCtrl(QtSTrans& ct)
{
        if(ct.trn.logMode()==WocTransaction::LogAll)return;
        ct.hcd+="  protected:\n";
        if(ct.trn.logMode()&WocTransaction::NoLogRequest)
                ct.hcd+="\tvirtual bool canLogRequest()const{return false;}\n";
        if(ct.trn.logMode()&WocTransaction::NoLogResponse)
                ct.hcd+="\tvirtual bool canLogResponse()const{return false;}\n";
}

QString WocQtServerTransaction::trnOutput(const WocTransaction&trn)
{
	QString code;
	code+="\tWTransaction::LogWrap log(this,\""+trn.name()+"\");\n";
	code+="\tQDomDocument doc;QDomElement root=doc.createElement(\"WobResponse-"+trn.name()+"\");\n";
	code+="\tQDomElement tmp;\n";
	code+="\t/*start of output encoding*/\n";
	QStringList sl=trn.outputNames();
	for(int i=0;i<sl.size();i++){
		QString t=trn.outputType(sl[i]);
		if(trn.isAttributeType(t)){
			code+="\troot.setAttribute(\""+sl[i]+"\",p->out_"+sl[i]+".value()";
			if(trn.isBoolType(t))
				code+="?\"true\":\"false\"";
			code+=");\n";
		}else{
			if(trn.isListType(t)){
				QString pt=trn.plainType(t);
				code+="\tfor(int i=0;i<p->out_"+sl[i]+".size();i++){\n";
				if(trn.isObjectType(t)){
					code+="\t\ttmp=p->out_"+sl[i]+"[i].toXml(doc,\""+sl[i]+"\");\n";
				}else{
					code+="\t\ttmp=doc.createElement(\""+sl[i]+"\");\n";
					code+="\t\ttmp.appendChild(doc.createTextNode(";
					if(trn.isIntType(t))
						code+="QString::number(p->out_"+sl[i]+"[i])";
					else
					if(trn.isBoolType(t))
						code+="p->out_"+sl[i]+"[i]?\"true\":\"false\"";
					else
					if(trn.isBlobType(t))
						code+="p->out_"+sl[i]+".toBase64()";
					else
						code+="p->out_"+sl[i]+"[i]";
					code+="));\n";
				}
				code+="\t\troot.appendChild(tmp);\n";
				code+="\t}\n";
			}else{
				if(trn.isObjectType(t)){
					code+="\troot.appendChild(p->out_"+sl[i]+".value().toXml(doc,\""+sl[i]+"\"));\n";
				}else{
					code+="\ttmp=doc.createElement(\""+sl[i]+"\");\n";
					code+="\ttmp.appendChild(doc.createTextNode(";
					if(trn.isIntType(t))
						code+="QString::number(p->out_"+sl[i]+".value())";
					else
					if(trn.isBlobType(t))
						code+="p->out_"+sl[i]+".value().toBase64()";
					else
						code+="p->out_"+sl[i]+".value()";
					code+="));\n\troot.appendChild(tmp);\n";
				}
			}
		}
	}
	code+="\t/*end of output encoding*/\n";
	code+="\tdoc.appendChild(root);\n";
	code+="\treturn doc.toByteArray();\n";
	return code;
}

QString WocQtServerTransaction::trnInput(const WocTransaction&trn)
{
	QStringList sl=trn.inputNames();
	QString code;
	code+="\tWTransaction::LogWrap log(this,\""+trn.name()+"\");\n";
	code+="\t/*start of input decoding*/\n";
	//basic XML parsing
	code+="\tif(rba.isEmpty()){\n";
	code+="\t\td->m_stage=Error;d->m_errtype=\"_iface\";\n";
	code+="\t\td->m_errstr=QCoreApplication::translate(\"WobTransaction\",\"XML request parser error: empty request.\");\n";
	code+="\t\tlog.setError(d->m_errstr);\n\t\treturn;\n\t}\n";
	code+="\tQDomDocument doc;\n";
	code+="\tQString emsg;int eln,ecl;\n";
	code+="\tif(!doc.setContent(rba,&emsg,&eln,&ecl)){\n";
	code+="\t\td->m_stage=Error;d->m_errtype=\"_iface\";\n";
	code+="\t\td->m_errstr=QString(QCoreApplication::translate(\"WobTransaction\",\"XML request parser error line %1 col %2: %3\")).arg(eln).arg(ecl).arg(emsg);\n";
	code+="\t\tlog.setError(d->m_errstr);\n\t\treturn;\n\t}\n";
	code+="\tQDomElement root=doc.documentElement();\n";
	//parse parameters
	code+="\tQList<QDomElement> nl;\n";
	for(int i=0;i<sl.size();i++){
		QString t=trn.inputType(sl[i]);
		if(trn.isAttributeType(t)){
			code+="\tp->in_"+sl[i]+"=";
			if(trn.isBoolType(t))code+="str2bool(";
			code+="root.attribute(\""+sl[i]+"\")";
			if(trn.isIntType(t))code+=".toInt()";else
			if(trn.isBoolType(t))code+=")";
			code+=";\n";
		}else{
			code+="\tnl=elementsByTagName(root,\""+sl[i]+"\");\n";
			if(trn.isListType(t)){
				code+="\tfor(int i=0;i<nl.size();i++){\n";
				if(trn.isObjectType(t)){
					code+="\t\ttry{p->in_"+sl[i]+".append("+m_parent->qtobjtype(trn,sl[i],WocQtOut::Out)+"(nl.at(i).toElement()));}catch(WException e){d->m_stage=Error;d->m_errtype=e.component();d->m_errstr=e.error();log.setError(d->m_errstr);}\n";
				}else if(trn.isIntType(t)){
					code+="\t\tp->in_"+sl[i]+".append(nl.at(i).toElement().text().toInt());\n";
				}else if(trn.isBoolType(t)){
					code+="\t\tp->in_"+sl[i]+".append(str2bool(nl.at(i).toElement().text()));\n";
				}else if(trn.isBlobType(t)){
					code+="\t\tp->in_"+sl[i]+".append(QByteArray::fromBase64(nl.at(i).toElement().text().toLatin1()));\n";
				}else{//can only be string
					code+="\t\tp->in_"+sl[i]+".append(nl.at(i).toElement().text());\n";
				}
				code+="\t}\n";
			}else{
				code+="\tif(nl.size()>0){\n";
				if(trn.isObjectType(t)){
					code+="\t\ttry{p->in_"+sl[i]+"="+m_parent->qtobjtype(trn,sl[i],WocQtOut::Out)+"(nl.at(0).toElement());}catch(WException e){d->m_stage=Error;d->m_errtype=e.component();d->m_errstr=e.error();log.setError(d->m_errstr);}\n";
				}else if(trn.isBlobType(t)){
					code+="\t\tp->in_"+sl[i]+"=QByteArray::fromBase64(nl.at(0).toElement().text().toLatin1());\n";
				}else{//can only be string
					code+="\t\tp->in_"+sl[i]+"=nl.at(0).toElement().text();\n";
				}
				code+="\t}\n";
			}
		}
	}
	code+="\t/*end of input*/\n";
	return code;
}

void WocQtServerTransaction::trnList()
{
	QString code;
	//header
	code+="  Q_ENUMS(Right)\n";
	code+="  enum Right {\n    NoRight";
	QStringList r=WocProcessor::instance()->transactionNames();
	QStringList p=WocProcessor::instance()->privilegeNames();
	QStringList pp=p;
	for(int i=0;i<r.size();i++)
		code+=",\n    R"+r[i];
	for(int i=0;i<p.size();i++)
		code+=",\n    P"+pp[i].replace(':',"_");
	code+="\n  };\n";
	code+="  typedef QList<Right> RightList;\n";
	code+="  Q_INVOKABLE static QString rightToString(Right);\n";
	code+="  Q_INVOKABLE static QString rightToLocalString(Right);\n";
	code+="  Q_INVOKABLE static Right stringToRight(QString);\n";
	code+="  Q_INVOKABLE static QStringList allKnownRightsString();\n";
	code+="  Q_INVOKABLE static "+m_parent->ifaceClassName()+"::RightList allKnownRights();\n";
	code+="  protected: WServerReply execute(const WServerRequest&);\n";

	//implement authenticator
	code+="public:\n";
	code+="  ///override to implement authentication checks\n";
	code+="  virtual bool isAuthenticated(const WServerRequest&){return false;}\n";
	code+="  ///override to implement the check for access rights\n";
	code+="  virtual bool hasRight(const WServerRequest&,Right){return false;}\n";

	//write header
	m_parent->addIfaceHeaderClass(code);code.clear();
	
	//register types
	code+="static int righttypeid=";
	code+="qRegisterMetaType<"+m_parent->ifaceClassName()+"::RightList>()+";
	code+="qRegisterMetaType<QList<"+m_parent->ifaceClassName()+"::RightList> >();\n";
	m_parent->addPostIface("Q_DECLARE_METATYPE("+m_parent->ifaceClassName()+"::RightList)\n");
	m_parent->addPostIface("Q_DECLARE_METATYPE(QList<"+ m_parent->ifaceClassName()+ "::RightList>)\n");
	
	//implement rights
	code+="QString "+m_parent->ifaceClassName()+"::rightToString(Right r)\n{\n\tswitch(r){\n";
	for(int i=0;i<r.size();i++)
		code+="\t\tcase R"+r[i]+":return \""+r[i]+"\";\n";
	for(int i=0;i<p.size();i++)
		code+="\t\tcase P"+pp[i]+":return \""+p[i]+"\";\n";
	code+="\t\tdefault:return \"\";\n\t}\n}\n";
	code+="QString "+m_parent->ifaceClassName()+"::rightToLocalString(Right r)\n{\n\tswitch(r){\n";
	for(int i=0;i<r.size();i++)
		code+="\t\tcase R"+r[i]+":return tr(\""+r[i]+"\");\n";
	for(int i=0;i<p.size();i++)
		code+="\t\tcase P"+pp[i]+":return tr(\""+p[i]+"\");\n";
	code+="\t\tdefault:return \"\";\n\t}\n}\n";
	code+=m_parent->ifaceClassName()+"::Right "+m_parent->namePrefix()+ "Interface::stringToRight(QString s)\n{\n";
	for(int i=0;i<r.size();i++)
		code+="\tif(s==\""+r[i]+"\")return R"+r[i]+";else\n";
	for(int i=0;i<p.size();i++)
		code+="\tif(s==\""+p[i]+"\")return P"+pp[i]+";else\n";
	code+="\treturn NoRight;\n}\n";
	code+="QList<"+m_parent->ifaceClassName()+"::Right> "+m_parent->ifaceClassName()+"::allKnownRights()\n{\n";
	code+="\tQList<Right> ret;ret";
	for(int i=0;i<r.size();i++)
		code+="<<R"+r[i];
	for(int i=0;i<p.size();i++)
		code+="<<P"+pp[i];
	code+=";\n\treturn ret;\n}\n";
	code+="QStringList "+m_parent->ifaceClassName()+"::allKnownRightsString()\n{\n";
	code+="\tQStringList ret;ret";
	for(int i=0;i<r.size();i++)
		code+="<<\""+r[i]+"\"";
	for(int i=0;i<p.size();i++)
		code+="<<\""+p[i]+"\"";
	code+=";\n\treturn ret;\n}\n";
	
	//implement handler
	code+="#include <WServerReply>\n#include <WServerRequest>\n";
	code+="WServerReply "+m_parent->ifaceClassName()+"::execute(const WServerRequest&request)\n";
	code+="{\n\tQString rq=request.header(\"X-WobRequest\");\n";
	for(int i=0;i<r.size();i++)
		code+="\tif(rq==\""+r[i]+"\")return "+m_parent->namePrefix()+ "T"+r[i]+"(name(),request).execute();\n";
	code+="\tWServerReply rp;rp.setStatus(200,\"Ok\");\n";
	code+="\trp.setHeader(\"X-WobResponse-Status\",\"Error\");\n";
	code+="\trp.setBody(QByteArray(\"<WobError type=\\\"_iface\\\">Unknown Transaction</WobError>\"));\n";
	code+="\treturn rp;\n}\n";
	
	//write to interface CPP
	m_parent->addIfaceImpl(code.toLatin1());
}
