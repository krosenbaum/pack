// Copyright (C) 2009-2011 by Konrad Rosenbaum <konrad@silmor.de>
// protected under the GNU GPL version 3 or at your option any newer.
// See COPYING.GPL file that comes with this distribution.
//

#ifndef WOC_QTOUT_H
#define WOC_QTOUT_H

#include "processor.h"

#include "mfile.h"

class QDomElement;

class WocQtOut;

///abstract base class for Qt transaction generator
class WocQtTransaction:public QObject
{
	Q_OBJECT
	public:
		///instantiates the transaction generator
		explicit WocQtTransaction(WocQtOut*);
		virtual ~WocQtTransaction(){}
		///abstract helper: called for each new transaction
		virtual void newTransaction(const WocTransaction&)=0;
		///abstract helper: called when parsing is about to finish
		virtual void finalize()=0;
	signals:
		///emitted if something goes wrong, this will cause the parser process to abort
		void errorFound();
	protected:
		WocQtOut*m_parent;
};

class WocQtClass;
class WocQtTable;

///Abstract base class for Qt client and server output.
///This class controls the main include files and uses helpers to generate
///transactions, classes and tables.
///
///It generates the interface class, a .pri-file and an include-all file.
class WocQtOut:public WocOutput
{
	public:
		///creates a Qt output object from the corresponding XML tag
		///that specifies what kind of output is desired.
		explicit WocQtOut(QDomElement&);
		///deletes the output object
		~WocQtOut()=0;
		
		///adds data to the prefix section of the interface class header,
		///this is for #include's and forward declarations
		void addIfaceHeaderPrefix(const QString&s){m_iface_prefix+=s;}
		///adds data to the class body of the interface class header,
		///this is for method declarations
		void addIfaceHeaderClass(const QString&s){m_iface_class+=s;}
		///adds code to the implementation file of the interface class
		void addIfaceImpl(const QString&);
		
	protected:
		///overloaded slot, called when parsing is finished
		virtual void finalize();
		///overloaded slot, called for each new class
		virtual void newClass(const WocClass&);
		///overloaded slot, called for each new table
		virtual void newTable(const WocTable&);
		///overloaded slot, called for each new transaction
		virtual void newTransaction(const WocTransaction&);
	
	public:
		///types of files generated
		enum FileType{
			Header,
			Source
		};
		
		/**helper: generate a proper Qt type for a property*/
		QString qttype(const WocClass&,QString,bool dolist=true);
		enum InOut{In,Out};
		/**helper: generate a proper QT type for a transaction variable*/
		QString qttype(const WocTransaction&,QString,InOut);
		/**helper: generate a proper QT type for a transaction variable, WO* only */
		QString qtobjtype(const WocTransaction&,QString,InOut);
		/**helper: adds a file to the project file
		\param basename the class name to add*/
		void addFile(QString basename);
		/**helper: adds a file to the project file
		\param fname the file name to add
		\param kind the type of file*/
		void addFile(FileType kind,QString fname);
		/**helper: add a line to the project file*/
		void addProjectLine(QString);
		/**helper: adds a line after the class definition of the interface class*/
		void addPostIface(QString);
		/**helper: returns the class name of the interface class*/
		QString ifaceClassName()const{return m_prefix+"Interface";}
		
		///returns the class name prefix configured for this output
		inline QString namePrefix()const{return m_prefix;}
		///returns the class name prefix for shared or unshared classes
		inline QString sharedPrefix()const{return m_cprefix;}
		///returns the language output spec
		inline QString languageSpec()const{return m_lang;}
		///returns the base directory of the project
		inline QString baseDir()const{return m_basedir;}
		///returns the sub directory relative to the project where to store files
		inline QString subDir()const{return m_subdir;}
		///returns the complete path for files, baseDir()+subDir()
		inline QString destinationPath()const{return m_basedir+"/"+m_subdir;}
		///returns the base class of all transactions
		inline QString transactionBase()const{return m_transbase;}
		
		///returns some lines of code to define an export symbol
		QByteArray exportLines()const;
                ///returns the name of the preprocessor symbol used for export
                QString exportSymbol()const;
		
	private:
		///internal: generate the static header for version info
		void initVersionH();
		
	private:
		QString m_basedir,m_subdir,m_prefix,m_cprefix,m_transbase,m_postiface;
                mutable QString m_exportsym;
		bool m_clean,m_export;
		MFile m_pri,m_iface,m_ifacecpp,m_hdr;
		QString m_iface_prefix,m_iface_class;
	protected:
		QString m_lang;
		WocQtClass*qclass;
		WocQtTable*qtable;
		WocQtTransaction*qtrans;
};

///specialization that generates adequate output for Qt clients
class WocQtClientOut:public WocQtOut
{
	public:
		explicit WocQtClientOut(QDomElement&);
};

///specialization that generates adequate output for Qt servers
class WocQtServerOut:public WocQtOut
{
	public:
		explicit WocQtServerOut(QDomElement&);
};

#endif
