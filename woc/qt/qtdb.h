// Copyright (C) 2009-2011 by Konrad Rosenbaum <konrad@silmor.de>
// protected under the GNU GPL version 3 or at your option any newer.
// See COPYING.GPL file that comes with this distribution.
//

#ifndef WOC_QTOUT_DB_H
#define WOC_QTOUT_DB_H

#include "processor.h"

#include "mfile.h"

class QDomElement;
class WocQtOut;

///generates code for DB table wrappers in Qt
///so far this class is non-functional
class WocQtTable
{
	public:
		///generates the table output as a helper to a specific output
		explicit WocQtTable(WocQtOut*);
		~WocQtTable();
		///called when all is done
		virtual void finalize();
		///called for every table to generate
		virtual void newTable(const WocTable&);
	private:
		WocQtOut*m_parent;
};

#endif
