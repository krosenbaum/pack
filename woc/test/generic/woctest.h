// Copyright (C) 2016 by Konrad Rosenbaum <konrad@silmor.de>
// protected under the GNU GPL version 3 or at your option any newer.
// See COPYING.GPL file that comes with this distribution.
//

#include <QObject>
#include "generic/genfile.h"
#include "generic/genout.h"

class WocTest:public QObject
{
    Q_OBJECT
private slots:
    void genVarTest();
    void genExprStringTest();
    void genExprBoolTest();
    void genFileTest();
};

class OutMock:public WocGenericOutBase
{
public:
    OutMock(){}
    QString outputBaseDir()const override{return ".";}
    QString outputSubDir()const override{return ".";}
    QString outputLanguage()const override{return "fake/client";}
    
    QString syntaxComment()const override{return "//==";}
    QString syntaxSection()const override{return "//--";}
    QString syntaxVariableStart()const override{return "{";}
    QString syntaxVariableEnd()const override{return "}";}
    QPair<QString,QString> syntaxVariable()const override{return QPair<QString,QString>("{","}");}
    
    QString outputTargetDir()const override{return ".";}
    
    QString mapType(QString n,const WocClass*context=nullptr)const override;
protected:
    virtual void finalize()override{}
    virtual void newClass(const WocClass&)override{}
    virtual void newTransaction(const WocTransaction&)override{}
    virtual void newTable(const WocTable&)override{}
};
