# Copyright (C) 2009-2018 by Konrad Rosenbaum <konrad@silmor.de>
# protected under the GNU GPL version 3 or at your option any newer.
# See COPYING.GPL file that comes with this distribution.

TEMPLATE=app
QT-=gui
QT+=xml testlib
CONFIG+=console debug
CONFIG-=app_bundle

DEFINES += COMPILE_WOC
TARGET = ../woctest_generic
MYTMPDIR = .ctmp

#compilation output:
OBJECTS_DIR = $$MYTMPDIR
MOC_DIR = $$MYTMPDIR
RCC_DIR = $$MYTMPDIR


SOURCES+= \
	../../mfile.cpp \
	../../domquery.cpp
HEADERS+= \
	../../mfile.h \
	../../domquery.h

SOURCES+= woctest.cpp
HEADERS+= woctest.h

include(../../proc/proc.pri)
include(../../generic/generic.pri)
GRESOURCES += testfiles
# TODO: remove
include(../../qt/qt.pri)
include(../../php/php.pri)
include(../../soap/soap.pri)
SOURCES+=../../htmlout.cpp
#END TODO

include(../../qrcgen/qrcgen.pri)

CONFIG += c++11

INCLUDEPATH += . ../../../vinfo ../../../qtbase/include ../..

#make sure dependencies are found
DEPENDPATH += $$INCLUDEPATH
