//== this pattern tests chains of conditional sections
//-- * test.close
results:
{#CHAIN1|trim}
{#CHAIN2|trim}
{#CHAIN3|trim}
{#CHAIN4|trim}
{#CHAIN5|trim}
{#CHAIN6A|trim}-{#CHAIN6B|trim}-{#CHAIN6C|trim}
{#CHAIN7A|trim}-{#CHAIN7B|trim}-{#CHAIN7C|trim}
{#UNCO|trim}
//==
//== Chain 1: simple if, one section only
//-- CHAIN1 run1 if({run|contains[1]})
C1
//==
//== Chain 2: two sections, first triggers
//-- CHAIN2 run2 if({run|isNotEmpty})
C2
//-- CHAIN2 run2 else
C2E
//== Chain 3: same trigger different chain
//-- CHAIN3 run2 if(true)
C3
//== Chain 3b: same trigger, same target, different chain
//-- CHAIN3 run2 if({run|isNotEmpty})
C3B
//== unconditional addition
//-- UNCO run2
U1
//==
//== Chain 4: 3 sections, last else triggers
//-- CHAIN4 run3 if({run|isEmpty})
C4E1
//-- CHAIN4 run3 else if({run|contains[55]})
C4E2
//-- CHAIN4 run3 else
C4
//==
//== Chain5: 3 sections, middle one triggers
//-- CHAIN5 run5 if({run|isEmpty})
C5E1
//-- CHAIN5 run5 else if({hello|toLower|contains[world]})
C5
//-- CHAIN5 run5 else
C5E2
//==
//== Chain 6: different targets for same chain
//-- CHAIN6A run6 if({run|isEmpty})
C6E1
//-- CHAIN6B run6 else if({hello|toLower|contains[world]})
C6
//-- CHAIN6C run6 else
C6E2
//==
//== Chain 6: different targets for same chain, all expressions true
//-- CHAIN7A run7 if({run|isNotEmpty})
C7
//-- CHAIN7B run7 else if({hello|toLower|contains[world]})
C7E1
//-- CHAIN7C run7 else if(true)
C7E2
//-- END ofFile
