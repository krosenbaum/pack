//== this pattern tests some basics
//== invisible line
//-- * test.new
{file}
//-- * test.close
{#OTHER|trim}
//== skipped line
{#OTHER|trim|oneLine(,)}
//-- OTHER run1
{run}
//-- OTHER run3
{run}
three
//-- * after
this line should NOT appear in the result: the file should be closed by now
//-- END ofFile
