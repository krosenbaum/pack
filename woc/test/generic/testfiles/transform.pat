//== this pattern tests all defined transformations
//-- * test.close
{var1|toUpper}
{hello|toLower}
{#XML|trim|xmlEncode}
{#URL|trim|urlEncode}
{#STR|trim|toCString}
{#MULTI|trim|oneLine}
{#MULTI|trim|oneLine(->)}
{#MULTI|trim|sort|oneLine}
{#MULTI|trim|unique|oneLine}
{#MULTI|trim|sort|unique|oneLine}

{#NONE|isEmpty|if(e)(n)}
{#NONE|isNotEmpty|if(e)(n)}
{#STR|isEmpty|if(e)(n)}
{#STR|isNotEmpty|if(e)(n)}
{#MULTI|contains(aaa)|if(ya)(na)}
{#MULTI|contains(aaa)|negate|if(ya)(na)}

{var2|replace(2)(two)}

{#TWOS|trim|prepend(//)}

{#TWOS|linetrim|indent(3)}
//-- XML run2
<{run} />
//-- URL run3
x://y@z
//-- STR run4
"{run}'x
//-- MULTI run1
aaa
{run}
//-- MULTI run2
{run}
//-- MULTI run3
{run}
//-- MULTI run4
{run}
//-- MULTI run5
{run}
//-- MULTI run7
aaa
aaa
//-- NONE run8

//-- TWOS run9
 one
 two
//-- END ofFile
