// Copyright (C) 2018 by Konrad Rosenbaum <konrad@silmor.de>
// protected under the GNU GPL version 3 or at your option any newer.
// See COPYING.GPL file that comes with this distribution.
//

#include <QtTest>
#include "woctest.h"

#include "generic/genvar.h"
#include "generic/genexpr.h"
#include "generic/genfile.h"

void WocTest::genVarTest()
{
    WocSimpleVariables sv1(nullptr);
    
    QVERIFY(sv1.setVariable("v1","val1"));
    QCOMPARE(sv1.getVariable("v1"),"val1");
    QCOMPARE(sv1.getVariable("xy"),"");
    
    QVERIFY(!sv1.setVariable("###","s"));
    QCOMPARE(sv1.getVariable("###"),"");
    QVERIFY(!sv1.setVariable("#v","s"));
    QCOMPARE(sv1.getVariable("#v"),"");
    
    WocSimpleVariables sv2(&sv1);
    QVERIFY(sv1.setVariable("v2","val2"));
    QVERIFY(sv2.setVariable("v2","valB"));
    QCOMPARE(sv1.getVariable("v2"),"val2");
    QCOMPARE(sv2.getVariable("v2"),"valB");
    QCOMPARE(sv2.getVariable("v1"),"val1");
    
    WocSectionVariables sec(&sv1);
    QVERIFY(sec.setVariable("#section","content"));
    QVERIFY(!sec.setVariable("normal","blah"));
    QCOMPARE(sec.getVariable("#section"),"content");
    QCOMPARE(sec.getVariable("v1"),"val1");
}

void WocTest::genExprStringTest()
{
    WocSimpleVariables sv(nullptr);
    QPair<QString,QString>mark1("{","}");
    sv.setVariable("mix","Hello World!");
    QCOMPARE(WocGenericExpression("{mix}",mark1,&sv).evaluateToString(),"Hello World!");
    QCOMPARE(WocGenericExpression("{mix|toLower}",mark1,&sv).evaluateToString(),"hello world!");
    QCOMPARE(WocGenericExpression("{mix|toUpper}",mark1,&sv).evaluateToString(),"HELLO WORLD!");
    QCOMPARE(WocGenericExpression("{mix|replace(l)(XX)}",mark1,&sv).evaluateToString(),"HeXXXXo WorXXd!");
    QCOMPARE(WocGenericExpression("{mix|replace(l)}",mark1,&sv).evaluateToString(),"Heo Word!");
    QCOMPARE(WocGenericExpression("{mix|replace[l][<((><]}",mark1,&sv).evaluateToString(),"He<((><<((><o Wor<((><d!");
    QCOMPARE(WocGenericExpression("{mix|replace(l)(xX)|toUpper}",mark1,&sv).evaluateToString(),"HEXXXXO WORXXD!");
    QCOMPARE(WocGenericExpression("I say '{mix}' to you.",mark1,&sv).evaluateToString(),"I say 'Hello World!' to you.");
    QCOMPARE(WocGenericExpression("{mix|contains(ell)}",mark1,&sv).evaluateToString(),"true");
    QCOMPARE(WocGenericExpression("{mix|contains(x)}",mark1,&sv).evaluateToString(),"false");
    
    sv.setVariable("enc","<jo at=\"tr\"/>");
    QCOMPARE(WocGenericExpression("**{enc|xmlEncode}**",mark1,&sv).evaluateToString(),"**&lt;jo at=&quot;tr&quot;/&gt;**");
    QCOMPARE(WocGenericExpression("**{enc|urlEncode}**",mark1,&sv).evaluateToString(),"**%3Cjo%20at%3D%22tr%22%2F%3E**");
    QCOMPARE(WocGenericExpression("**{enc|toCString}**",mark1,&sv).evaluateToString(),"**\"<jo at=\\\"tr\\\"/>\"**");
    
    sv.setVariable("ml","hello\nworld");
    QCOMPARE(WocGenericExpression("{ml}",mark1,&sv).evaluateToString(),"hello\nworld");
    QCOMPARE(WocGenericExpression("{ml|oneLine}",mark1,&sv).evaluateToString(),"hello world");
    QCOMPARE(WocGenericExpression("{ml|oneLine(,)}",mark1,&sv).evaluateToString(),"hello,world");
    QCOMPARE(WocGenericExpression("{ml|prepend(//)}",mark1,&sv).evaluateToString(),"//hello\n//world");
    QCOMPARE(WocGenericExpression("{ml|indent(3)}",mark1,&sv).evaluateToString(),"   hello\n   world");
    
    sv.setVariable("spc"," \t\n ");
    sv.setVariable("wspc"," space\t");
    QCOMPARE(WocGenericExpression("{mix|isEmpty}",mark1,&sv).evaluateToString(),"false");
    QCOMPARE(WocGenericExpression("{mix|isNotEmpty}",mark1,&sv).evaluateToString(),"true");
    QCOMPARE(WocGenericExpression("{spc|isEmpty}",mark1,&sv).evaluateToString(),"true");
    QCOMPARE(WocGenericExpression("{spc|isNotEmpty}",mark1,&sv).evaluateToString(),"false");
    QCOMPARE(WocGenericExpression("{wspc|isEmpty}",mark1,&sv).evaluateToString(),"false");
    QCOMPARE(WocGenericExpression("{wspc|isNotEmpty}",mark1,&sv).evaluateToString(),"true");
    QCOMPARE(WocGenericExpression("{wspc|trim}",mark1,&sv).evaluateToString(),"space");
    QCOMPARE(WocGenericExpression("{wspc}",mark1,&sv).evaluateToString()," space\t");
    
    sv.setVariable("t","on");
    sv.setVariable("f","no");
    QCOMPARE(WocGenericExpression("{t|negate}",mark1,&sv).evaluateToString(),"false");
    QCOMPARE(WocGenericExpression("{f|negate}",mark1,&sv).evaluateToString(),"true");
    QCOMPARE(WocGenericExpression("{t|if(yo)(nah)}",mark1,&sv).evaluateToString(),"yo");
    QCOMPARE(WocGenericExpression("{f|if(yo)(nah)}",mark1,&sv).evaluateToString(),"nah");
    QCOMPARE(WocGenericExpression("{t|if(yo)}",mark1,&sv).evaluateToString(),"yo");
    QCOMPARE(WocGenericExpression("{f|if(yo)}",mark1,&sv).evaluateToString(),"");

    QPair<QString,QString>mark2("$[",")>");
    QCOMPARE(WocGenericExpression("{wspc}$[mix)>{mix}",mark2,&sv).evaluateToString(),"{wspc}Hello World!{mix}");
    
    QCOMPARE(WocGenericExpression().evaluateToString(),"");
    QCOMPARE(WocGenericExpression("{mix}",mark1,nullptr).evaluateToString(),"");
}

void WocTest::genExprBoolTest()
{
    WocSimpleVariables sv(nullptr);
    QPair<QString,QString>mark1("{","}");
    sv.setVariable("mix","Hello World!");
    sv.setVariable("t","on");
    sv.setVariable("f","no");
    QCOMPARE(WocGenericExpression("{t}",mark1,&sv).evaluateToBool(),true);
    QCOMPARE(WocGenericExpression("{f}",mark1,&sv).evaluateToBool(),false);
    QCOMPARE(WocGenericExpression("yes",mark1,&sv).evaluateToBool(),true);
    QCOMPARE(WocGenericExpression("no",mark1,&sv).evaluateToBool(),false);
    QCOMPARE(WocGenericExpression("{t}||{f}",mark1,&sv).evaluateToBool(),true);
    QCOMPARE(WocGenericExpression("{t}&&{f}",mark1,&sv).evaluateToBool(),false);
    QCOMPARE(WocGenericExpression("{t}|| no",mark1,&sv).evaluateToBool(),true);
    QCOMPARE(WocGenericExpression("{t}&& yes",mark1,&sv).evaluateToBool(),true);
    QCOMPARE(WocGenericExpression("{t} && yes && {f|negate} || {f}",mark1,&sv).evaluateToBool(),true);

    QCOMPARE(WocGenericExpression("!{t}",mark1,&sv).evaluateToBool(),false);
    QCOMPARE(WocGenericExpression("! {t}",mark1,&sv).evaluateToBool(),false);
    QCOMPARE(WocGenericExpression("!{f}",mark1,&sv).evaluateToBool(),true);
    QCOMPARE(WocGenericExpression("! {f}",mark1,&sv).evaluateToBool(),true);
    QCOMPARE(WocGenericExpression("!!{f}",mark1,&sv).evaluateToBool(),false);
    QCOMPARE(WocGenericExpression("!!{t}",mark1,&sv).evaluateToBool(),true);

    QCOMPARE(WocGenericExpression("!{f} && {t} && !no",mark1,&sv).evaluateToBool(),true);
}

static inline QString getFile(QString fn)
{
    QFile fd(fn);
    if(!fd.open(QIODevice::ReadOnly))return QString();
    QString r=QString::fromUtf8(fd.readAll());
    fd.close();
    return r.trimmed();
}

void WocTest::genFileTest()
{
    //environment
    WocSimpleVariables sv(nullptr);
    for(int i=0;i<10;i++)sv.setVariable(QString("var%1").arg(i),QString("value_%1").arg(i));
    sv.setVariable("hello","Hello World!");
    OutMock om;
    //go through test files
    QDir d(":/testfiles");
    for(QString fn:d.entryList(QStringList()<<"*.pat")){
        qDebug()<<"Testing pattern file"<<fn;
        const QString base=QFileInfo(fn).baseName();
        //process file
        sv.setVariable("file",fn);
        sv.setVariable("base",base);
        WocGenericFile gf(&om,"test","testpat_{base}.res",":/testfiles/"+fn,&sv);
        gf.trigger("test.new");
        for(int i=0;i<10;i++){
            sv.setVariable("run",QString::number(i),"x");
            gf.trigger(QString("run%1").arg(i));
        }
        sv.deleteOldVariables("x");
        gf.trigger("test.close");
        gf.trigger("after");
        //get result
        QCOMPARE(getFile("testpat_"+base+".res"),getFile(":/testfiles/"+base+".res"));
        QFile::remove("testpat_"+base+".res");
    }
}

QString OutMock::mapType(QString n, const WocClass* context) const
{
    Q_UNUSED(context);
    return "/"+n+"/";
}


QTEST_MAIN(WocTest);
