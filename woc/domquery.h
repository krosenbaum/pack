// Copyright (C) 2009-2011 by Konrad Rosenbaum <konrad@silmor.de>
// protected under the GNU GPL version 3 or at your option any newer.
// See COPYING.GPL file that comes with this distribution.
//

#ifndef MAGICSMOKE_DOMQUERY_H
#define MAGICSMOKE_DOMQUERY_H


#include <QDomElement>
#include <QList>
#include <QStringList>

/**Helper class: more flexible version of QDomNodeList*/
class MDomNodeList:public QList<QDomNode>
{
	public:
		MDomNodeList(){}
		MDomNodeList(const MDomNodeList&l):QList<QDomNode>(l){}
		MDomNodeList(const QList<QDomNode>&l):QList<QDomNode>(l){}
		MDomNodeList(const QDomNodeList&l){for(int i=0;i<l.size();i++)append(l.at(i));}
		MDomNodeList(const QDomNamedNodeMap&l){for(int i=0;i<l.size();i++)append(l.item(i));}
		
		MDomNodeList& operator+=(const QDomNodeList&l)
		{for(int i=0;i<l.size();i++)append(l.at(i));return *this;}
		
		MDomNodeList& operator+=(const QDomNamedNodeMap&l)
		{for(int i=0;i<l.size();i++)append(l.item(i));return *this;}
};


/**DOM Query Class

It uses a simplified version of the XPath syntax: 

Namespaces/Prefixes are ignored. It is recommended to use patternist for cases in which namespaces matter.

The search path uses slash "/" to separate elements, the last item in the search path may start with "@" to match an attribute instead of an element. The special names "*" and "@*" may be used to match any element or attribute. If the search path starts with a slash "/", the search starts at the document root irrespective of what element was fed to the query object (the first item matched against the root element). If the search path starts with a double slash "//" the search considers all positions in the document as valid starting points (using all elements that match the first item to start the search, the first item must not be "*" in that case). If there is no slash at the start of the path the search begins exactly at the supplied element (the first item matched against child elements of the supplied element). Queries that start with a slash must search an element at the first position.

When casting to QString or QStringList the query object uses the method text() of the QDomElement to generate the content of the element. For attributes it simply returns the value.

Examples:
<table>
<tr><td>mouse</td><td>return all child elements of the current element that are called "mouse"</td></tr>
<tr><td>\@mouse</td><td>return all attribute nodes of the current element that are called "mouse"</td></tr>
<tr><td>*</td><td>returns all child elements of the current element</td></tr>
<tr><td>\@*</td><td>returns all attributes of the current element</td></tr>
<tr><td>/<!-- -->*</td><td>returns the root element of the document irrespective of name</td></tr>
<tr><td>/mouse</td><td>returns the root element of the document if it has the tag name "mouse" or an empty list</td></tr>
<tr><td>/<!-- -->*<!-- -->/mouse</td><td>returns all elements directly below the root element that are called "mouse"</td></tr>
<tr><td>//mouse</td><td>returns all elements in the document that are called "mouse"</td></tr>
<tr><td>mouse/\@trap/door</td><td>syntax error, attributes cannot have children</td></tr>
</table>
*/
class MDomQuery
{
	public:
		/**creates the query object and executes the query*/
		MDomQuery(const QDomElement&start,QString path);
		
		/**creates the query object and executes the query; this is equivalent to calling the query with the document element as starting point*/
		MDomQuery(const QDomDocument&start,QString path);
		
		/**returns the search result as a single string, if there were multiple matches, they are separated by spaces*/
		QString toString()const;
		/**returns the search result as a list of strings, one string element per match*/
		QStringList toStringList()const;
		/**returns the result as a list of DOM nodes*/
		MDomNodeList toNodeList()const{return m_result;}
		///returns the result as a list of DOM elements, filters out anything not an element
		QList<QDomElement> toElementList()const{QList<QDomElement>ret;for(auto n:m_result)if(n.isElement())ret<<n.toElement();return ret;}
		
		/**cast to QString: see toString()*/
		operator QString()const{return toString();}
		/**cast to QStringList: see toStringList()*/
		operator QStringList()const{return toStringList();}
		/**cast to DOM node list: see toNodeList()*/
		operator MDomNodeList()const{return m_result;}
	private:
		MDomNodeList m_result;
		
		//helper for constructor
		void construct(const QDomElement&start,QString path);
};

#endif
