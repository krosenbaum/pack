// Copyright (C) 2018 by Konrad Rosenbaum <konrad@silmor.de>
// protected under the GNU GPL version 3 or at your option any newer.
// See COPYING.GPL file that comes with this distribution.
//

#include "genexpr.h"
#include "genvar.h"

#include <QUrl>
#include <QDebug>

//helper to parse a single boolean
static inline bool str2bool_ext(QString s)
{
    s=s.toLower();
    //static strings
    if(s=="1"||s=="true"||s=="t"||s=="yes"||s=="y"||s=="on")return true;
    if(s=="0"||s=="false"||s=="f"||s=="no"||s=="n"||s=="off")return false;
    //int?
    bool ok=true;
    int i=s.toInt(&ok,0);
    if(ok){
        if(i>0)return true;
        if(i==0)return true;
        qDebug()<<"Warning: negative numbers are still false in bool.";
        return false;
    }
    //no match
    qDebug()<<"Warning: interpreting"<<s<<"as bool false.";
    return false;
}


// =======================================================================
// Expressions

WocGenericExpression::WocGenericExpression(QString expr, QPair<QString, QString> marker, WocGenericVariables* vars)
    :mvars(vars)
{
    //parser state
    int pos=0;
    bool isConst=true;
    //go through expr
    while(pos<expr.size()){
        if(isConst){//normal text mode -> find start of variable
            Part p;
            p.isConst=true;
            int next=expr.indexOf(marker.first,pos);
            if(next<0){
                p.textorvarname=expr.mid(pos);
                mparts<<p;
                break;
            }else{
                p.textorvarname=expr.mid(pos,next-pos);
                mparts<<p;
                pos=next+marker.first.size();
                isConst=false;
                continue;
            }
        }else{//variable mode -> find start of next const text
            Part p;
            p.isConst=false;
            int next=expr.indexOf(marker.second,pos);
            if(next<0){
                qDebug()<<"Warning: unterminated value expression at pos"<<pos<<"in expression"<<expr;
                next=expr.size();
            }
            //parse value expression
            QString subex=expr.mid(pos,next-pos);
            QStringList vex=subex.split("|",Qt::SkipEmptyParts);
            pos=next+marker.second.size();
            isConst=true;//prepare for next...
            if(vex.size()==0){
                qDebug()<<"Warning: empty value expression.";
                continue;
            }
            p.textorvarname=vex.takeFirst();
            //find correct type of parentheses
            QChar sc='(',ec=')';
            for(QChar c:subex)
                if(c=='('||c=='['||c=='{'||c=='<'){
                    sc=c;
                    if(c=='[')ec=']';else if(c=='{')ec='}';else if(c=='<')ec='>';
                    break;
                }
            //go through transformations
            for(QString trans:vex){
                //find parameters
                int sp=trans.indexOf(sc);
                if(sp<0){
                    p.transforms<<Transform(trans);
                    continue;
                }
                Transform t(trans.left(sp));
                trans=trans.mid(sp+1);
                //parse out params
                while(trans.size()>0){
                    sp=trans.indexOf(ec);
                    if(sp<0)break;
                    t.params<<trans.left(sp);
                    sp=trans.indexOf(sc,sp);
                    if(sp<0)break;
                    trans=trans.mid(sp+1);
                }
                //remember transformation
                p.transforms<<t;
            }
            //remember part
            mparts<<p;
        }
    }
}

QString WocGenericExpression::evaluateToString()const
{
    QString ret;
    for(const Part&part:mparts){
        if(part.isConst){
            ret+=part.textorvarname;
            continue;
        }
        QString val=mvars?mvars->getVariable(part.textorvarname):QString();
        for(const Transform&trans:part.transforms){
            val=transform(val,trans.funcName,trans.params);
        }
        ret+=val;
    }

    return ret;
}

bool WocGenericExpression::evaluateToBool()const
{
    QString value=evaluateToString().trimmed();
    //parse again
    QStringList bex;
    QString cur;
    bool isOp=false;
    for(QChar c:value){
        if(c=='!'){
            bex<<cur.trimmed();
            bex<<"!";
            cur.clear();
        }else
        if(c.isSpace()){
            bex<<cur.trimmed();
            cur.clear();
        }else
        if(c=='&' || c=='|'){
            if(isOp)cur+=c;
            else{
                bex<<cur.trimmed();
                cur.clear();
                isOp=true;
                cur+=c;
            }
        }else{
            if(isOp){
                bex<<cur.trimmed();
                cur.clear();
                cur+=c;
                isOp=false;
            }else cur+=c;
        }
    }
    bex<<cur;
    //eliminate empty elements
    QStringList bex2;
    for(QString s:bex){s=s.trimmed();if(!s.isEmpty())bex2<<s;}
    //evaluate bool expression
    bool neg=false;
    while(bex2.size()>0 && bex2.value(0)=="!"){
        neg=!neg;
        bex2.takeFirst();
    }
    if(bex2.size()==0){
        qDebug()<<"Warning: no value in boolean expression"<<value;
        return false;
    }
    bool res=str2bool_ext(bex2.takeFirst())^neg;
    while(bex2.size()>0){
        QString op=bex2.takeFirst();
        neg=false;
        while(bex2.size()>0 && bex2.value(0)=="!"){
            neg=!neg;
            bex2.takeFirst();
        }
        if(bex2.size()==0){
            qDebug()<<"Warning: missing last value in boolean expression"<<value;
            return false;
        }
        bool vtmp=str2bool_ext(bex2.takeFirst())^neg;
        if(op=="&&")res=res&&vtmp;else
        if(op=="||")res=res||vtmp;
        else{
            qDebug()<<"Warning: unknown binary operator"<<op<<"in boolean expression"<<value;
            return false;
        }
    }

    return res;
}

QString WocGenericExpression::transform(QString value, QString transform, QStringList params) const
{
    if(transform=="toLower")return value.toLower();
    if(transform=="toUpper")return value.toUpper();
    if(transform=="urlEncode")return QUrl::toPercentEncoding(value);
    if(transform=="xmlEncode")return value.toHtmlEscaped();
    if(transform=="toCString")return "\""+value.replace('\\',"\\\\").replace('\'',"\\\'").replace('\"',"\\\"").replace('\n',"\\n").replace('\r',"\\r")+"\"";
    if(transform=="oneLine")return value.replace('\n',params.size()==0?" ":params[0]);
    if(transform=="trim")return value.trimmed();
    if(transform=="linetrim"){
        QStringList r;
        for(QString s:value.split("\n",Qt::KeepEmptyParts))r<<s.trimmed();
        return r.join("\n");
    }
    if(transform=="skipEmptyLines"){
        QStringList r;
        for(QString s:value.split("\n",Qt::SkipEmptyParts))
            if(!s.trimmed().isEmpty())r<<s;
        return r.join("\n");
    }
    if(transform=="replace"){
        if(params.size()<1){
            qDebug()<<"Warning: transformation replace requires 1 or 2 parameters.";
            return value;
        }
        return value.replace(params[0],params.size()>1?params[1]:"");
    }
    if(transform=="contains"){
        if(params.size()<1){
            qDebug()<<"Warning: transformation contains requires 1 parameter.";
            return "false";
        }
        return value.contains(params[0])?"true":"false";
    }
    if(transform=="sort"){
        QStringList vl=value.split("\n",Qt::KeepEmptyParts);
        std::sort(vl.begin(),vl.end());
        return vl.join("\n");
    }
    if(transform=="unique"){
        QString last;QStringList vl;
        for(QString s:value.split("\n"))
            if(s!=last){
                vl<<s;
                last=s;
            }
        return vl.join("\n");
    }
    if(transform=="isEmpty")return value.trimmed().isEmpty()?"true":"false";
    if(transform=="isNotEmpty")return value.trimmed().isEmpty()?"false":"true";
    //prepend/indent
    if(transform=="prepend"){
        if(params.size()<1){
            qDebug()<<"Warning: transformation prepend requires 1 parameters.";
            return value;
        }
        QStringList vl=value.split("\n",Qt::KeepEmptyParts);
        value.clear();
        for(QString v:vl)value+=(value.isEmpty()?"":"\n") +params[0]+v;
        return value;
    }
    if(transform=="indent"){
        if(params.size()<1){
            qDebug()<<"Warning: transformation indent requires 1 parameters.";
            return value;
        }
        bool b;
        int isz=params[0].toInt(&b,0);
        if(!b){
            qDebug()<<"Warning: indent size must be a non-negative integer! Indent size was"<<params[0];
            return value;
        }
        QString ind;for(int i=0;i<isz;i++)ind+=" ";
        QStringList vl=value.split("\n",Qt::KeepEmptyParts);
        value.clear();
        for(QString v:vl)value+=(value.isEmpty()?"":"\n") +ind+v;
        return value;
    }
    //bool based
    if(transform=="negate")return str2bool_ext(value)?"false":"true";
    if(transform=="if"){
        if(params.size()<1){
            qDebug()<<"Warning: transformation if requires 1 or 2 parameters.";
            return QString();
        }
        return str2bool_ext(value)?params[0]:(params.size()>1?params[1]:QString());
    }

    //no such transform
    qDebug()<<"Warning: unknown transformation"<<transform<<"- doing nothing instead.";
    return value;
}
