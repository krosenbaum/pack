// Copyright (C) 2018 by Konrad Rosenbaum <konrad@silmor.de>
// protected under the GNU GPL version 3 or at your option any newer.
// See COPYING.GPL file that comes with this distribution.
//

#ifndef WOC_GENERICEXPR_H
#define WOC_GENERICEXPR_H

#include <QList>
#include <QString>
#include <QPair>


class WocGenericVariables;

///Variable Expression Parser and Evaluator class.
///
/// Usage: 
/// \code
/// QString result=WocGenericExpression("{hello} world", getConfiguredMarkers(), m_myvars).evaluateToString();
/// bool decide=WocGenericExpression("{isNice}&&{isClever}", getConfiguredMarkers(), m_myvars).evaluateToBool();
/// \endcode
class WocGenericExpression
{
public:
    ///Creates an empty expression
    WocGenericExpression(){}
    ///Creates a copy of the expression
    WocGenericExpression(const WocGenericExpression&)=default;
    ///Parses the expression and makes it ready for evaluation
    /// \param expr the expression to be parsed
    /// \param marker the variable start/end markers to be used during parsing
    /// \param vars the variable repository that is used for value lookups during evaluation
    WocGenericExpression(QString expr,QPair<QString,QString>marker,WocGenericVariables*vars);
    
    ///Makes this expression identical to other.
    WocGenericExpression& operator=(const WocGenericExpression&)=default;
    
    ///returns true if this expression is empty
    bool isNull()const{return mparts.size()==0;}
    
    ///evaluates the expression by replacing all marked variables with (optionally transformed) values and returns the result as string
    QString evaluateToString()const;

    ///evaluates the expression by replacing all marked variables with (optionally transformed) values and 
    ///then interprets the result as a boolean formula, returning the result of calculating it
    bool evaluateToBool()const;
private:
    ///represents a single transformation step inside a value expression
    struct Transform{
        Transform(){}
        Transform(QString fn):funcName(fn){}
        Transform(const Transform&)=default;
        QString funcName;
        QStringList params;
    };
    ///represents one part of the complete expression, a part can be constant text or a value expression
    struct Part{
        ///true: the part is constant text, use "text"; false: the part is a value expression
        bool isConst=true;
        ///isConst==true: constant text; isConst==false: name of the variable to be evaluated
        QString textorvarname;
        ///isConst==false: transformations to be done on the value
        QList<Transform>transforms;
    };
    ///parsed parts of the expression
    QList<Part>mparts;
    ///reference to the variables to be used during evaluation
    WocGenericVariables*mvars;
    
    ///carries out a single transformation
    QString transform(QString value,QString transform,QStringList params)const;
};


#endif
