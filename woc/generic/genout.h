// Copyright (C) 2016-18 by Konrad Rosenbaum <konrad@silmor.de>
// protected under the GNU GPL version 3 or at your option any newer.
// See COPYING.GPL file that comes with this distribution.
//

#ifndef WOC_GENERICOUT_H
#define WOC_GENERICOUT_H

#include <QObject>
#include <QString>

#include "processor.h"

#include "mfile.h"

#include "genproc.h"

class QDomElement;

class WocGenericOut;
class WocGenericVariables;
class WocGenericFile;
class WocSimpleVariables;

class WocGenericPrecalc
{
    public:
        WocGenericPrecalc(const QDomElement&);
        QString triggerName()const{return mtrigger;}
        void calculate(WocGenericOut&,WocSimpleVariables&)const;
    private:
        QString mtrigger,mdelete;
        struct Rule{QString name,value,valueFalse,condition;};
        QList<Rule>mrules;
};

///Base class for generic output (used for test mock).
class WocGenericOutBase:public WocOutput
{
    public:
        WocGenericOutBase(){}
        
        virtual QString outputBaseDir()const=0;
        virtual QString outputSubDir()const=0;
        virtual QString outputLanguage()const=0;
        
        virtual QString syntaxComment()const=0;
        virtual QString syntaxSection()const=0;
        virtual QString syntaxVariableStart()const=0;
        virtual QString syntaxVariableEnd()const=0;
        virtual QPair<QString,QString> syntaxVariable()const=0;
        
        inline QPair<QString,QString> syntaxVariableMeta()const{return QPair<QString,QString>("{","}");}
        
        virtual QString outputTargetDir()const=0;
        
        virtual QString mapType(QString n,const WocClass*context=nullptr)const=0;
};

///Class for Generic client and server output.
class WocGenericOut:public WocGenericOutBase
{
	public:
		///creates a Generic output object from the corresponding XML tag
		///that specifies what kind of output is desired.
		explicit WocGenericOut(PatternDir pattern,const QDomElement&);
		///deletes the output object
		~WocGenericOut();
		
	protected:
		///overloaded slot, called when parsing is finished
		virtual void finalize()override;
		///overloaded slot, called for each new class
		virtual void newClass(const WocClass&)override;
		///overloaded slot, called for each new transaction
		virtual void newTransaction(const WocTransaction&)override;
		///overloaded slot, called for each new table
		virtual void newTable(const WocTable&)override;
        
        ///called when all patterns have been loaded, triggers project.new
        void initialize();
        
    public:
        QString outputBaseDir()const override{return m_basedir;}
        QString outputSubDir()const override{return m_subdir;}
        QString outputLanguage()const override{return m_lang;}
        
        QString syntaxComment()const override{return m_syntComment;}
        QString syntaxSection()const override{return m_syntSection;}
        QString syntaxVariableStart()const override{return m_syntVarStart;}
        QString syntaxVariableEnd()const override{return m_syntVarEnd;}
        QPair<QString,QString> syntaxVariable()const override{return QPair<QString,QString>(m_syntVarStart,m_syntVarEnd);}
        
        QString outputTargetDir()const override{return m_basedir+"/"+m_subdir;}
        
        QString mapType(QString n,const WocClass*context=nullptr)const override;
		
	private:
		QString m_basedir,m_subdir;
		bool m_clean;
		QString m_lang;
        WocSimpleVariables*m_vars;
        QString m_syntComment,m_syntSection,m_syntVarStart,m_syntVarEnd;
        int m_numTables=0;
        
        enum class FeatureType {String,Bool};
        struct Feature {FeatureType type;QString name,defaultval,realval;};
        QMap<QString,Feature> m_features;
        
        struct FileConf{QString trigger,tag,name,pattern;};
        QMap<QPair<QString,QString>,FileConf> m_fileconf;
        QList<WocGenericFile> m_files;
        
        QMap<QString,QString> m_typemap;
        
        QList<WocGenericPrecalc> m_precalc;

        ///read the meta.xml file and initialize internal structures
        bool readMeta(QString metafile);
        ///read and parse pattern files
        void readFiles();
        ///trigger a single event and propagate it through all active files
        void trigger(QString);
        ///overloaded method, called for each new table and audit table
        void newTable(const WocTable&,QString basename,bool isaudit);
};

#endif
