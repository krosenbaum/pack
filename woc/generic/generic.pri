SOURCES+= \
	$$PWD/genproc.cpp \
	$$PWD/genout.cpp \
	$$PWD/genfile.cpp \
	$$PWD/genexpr.cpp \
	$$PWD/genvar.cpp

HEADERS+= \
	$$PWD/genproc.h \
	$$PWD/genout.h \
	$$PWD/genfile.h \
	$$PWD/genexpr.h \
	$$PWD/genvar.h

INCLUDEPATH += $$PWD
