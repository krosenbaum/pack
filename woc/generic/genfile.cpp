// Copyright (C) 2016 by Konrad Rosenbaum <konrad@silmor.de>
// protected under the GNU GPL version 3 or at your option any newer.
// See COPYING.GPL file that comes with this distribution.
//

#include "genfile.h"
#include "genout.h"
#include "genvar.h"

#include <QUrl>
#include <QDebug>


// =======================================================================
// Files

WocGenericFile::WocGenericFile(WocGenericOutBase*parent,QString type, QString pattern, QString patternfile, WocGenericVariables*globalvars)
: mfiletype(type), mparent(parent), mvars(new WocSectionVariables(globalvars)), mfilepattern(pattern,parent->syntaxVariableMeta(),mvars)
{
    QFile fd(patternfile);
    if(!fd.open(QIODevice::ReadOnly)){
        qDebug()<<"Warning: unable to open pattern file"<<patternfile;
        return;
    }
//     qDebug()<<"instantiating pattern file"<<patternfile;

    //get split comment
    const QString comment=mparent->syntaxComment();
    const QString splitter=mparent->syntaxSection();
    const QPair<QString,QString> markpat=mparent->syntaxVariable();
    const QPair<QString,QString> markmeta=mparent->syntaxVariableMeta();

    //parse file (TODO: make encoding and line ending configurable)
    QStringList lines=QString::fromUtf8(fd.readAll()).split('\n',Qt::KeepEmptyParts);
    fd.close();
    //eliminate lines before first section
    int linenum=0;
    bool startWarn=false;
    while(lines.size()>0){
        if(lines[0].startsWith(splitter))break;
        QString l=lines.takeFirst();
        linenum++;
        if(!startWarn && (!l.startsWith(comment) || l.trimmed().isEmpty())){
            qDebug()<<"Warning: there are active lines before the first section in pattern file"<<patternfile;
            startWarn=true;
        }
    }
    if(lines.size()==0){
        qDebug()<<"Warning: there are no active sections in pattern file"<<patternfile;
        return;
    }
    //parse sections
    Section current;
    QStringList content;
    while(lines.size()>0){
        QString line=lines.takeFirst();
        linenum++;
        if(line.startsWith(comment))continue;
        if(line.startsWith(splitter)){
            current.setContent(content,markpat,mvars);
            if(current.isValid())msections<<current;
            content.clear();
            //parse line
            current=Section();
            line=line.mid(splitter.size()).trimmed();
            if(!current.startSection(line,markpat,mvars)){
                qDebug()<<"Warning: unable to interpret line"<<linenum<<"in pattern file"<<patternfile<<"- aborting.";
                return;
            }
            current.linenum=linenum;
//             qDebug()<<" start"<<current.trigger<<current.targetSection;
        }else
            content<<line;
    }
    if(current.isValid()){
        current.setContent(content,markpat,mvars);
        msections<<current;
    }
//     qDebug()<<" have"<<msections.size()<<"sections";

    // verify if-else chains in sections
    QString chain;
    for(const Section&s:msections){
        //not part of a chain - breaks it
        if(!s.iselse && s.condition.isNull()){
            chain.clear();
            continue;
        }
        //else, but no chain: warning!
        if(chain.isEmpty() && s.iselse){
            qDebug()<<"WARNING: pattern file"<<patternfile<<"contains free-floating 'else' in line"<<s.linenum<<"- generator behavior may be irrational.";
            continue;
        }
        //beginning of chain
        if(!s.iselse && !s.condition.isNull()){
            chain=s.trigger;
            continue;
        }
        //continuation?
        if(s.iselse){
            //switch of trigger
            if(chain!=s.trigger)
                qDebug()<<"WARNING: pattern file"<<patternfile<<"switches trigger in the middle of if-else-chain, line"<<s.linenum<<"- generator behavior may be irrational.";
            //end of chain?
            if(s.condition.isNull())chain.clear();
        }
    }
}

void WocGenericFile::Section::setContent(const QStringList&cl,QPair<QString,QString>mark,WocGenericVariables*vars)
{
    QString cont=cl.join('\n')+"\n";
    content=WocGenericExpression(cont,mark,vars);
}

bool WocGenericFile::Section::startSection(QString line, QPair<QString,QString>mark, WocGenericVariables*vars)
{
    line=line.trimmed();
    //section target
    int pos;
    for(pos=0;pos<line.size();pos++)
        if(line[pos].isSpace())break;
    targetSection=line.left(pos);
    line=line.mid(pos).trimmed();
    //trigger
    for(pos=0;pos<line.size();pos++)
        if(line[pos].isSpace())break;
    trigger=line.left(pos);
    line=line.mid(pos).trimmed();
    //options
    while(line.size()>0){
        if(line.startsWith("clear")){
            line=line.mid(5);
            if(line.size()>0 && !line[0].isSpace())
                return true;
            clear=true;
            line=line.trimmed();
            continue;
        }
        if(line.startsWith("else")){
            line=line.mid(4);
            if(line.size()>0 && !line[0].isSpace())
                return true;
            iselse=true;
            line=line.trimmed();
            continue;
        }
        if(line.startsWith("if")){
            line=line.mid(2).trimmed();
            if(line.size()==0){
                qDebug()<<"Warning: if option needs a condition";
                return false;
            }
            QChar sc=line[0],ec;
            if(sc=='{')ec='}';else if(sc=='[')ec=']';else if(sc=='>')ec='>';else if(sc=='(')ec=')';
            else{
                qDebug()<<"Warning: unexpected symbol"<<sc<<"in if option.";
                return false;
            }
            pos=line.indexOf(ec);
            if(pos<0){
                qDebug()<<"Warning: unterminated if option.";
                pos=line.size();
            }
            condition=WocGenericExpression(line.mid(1,pos-1),mark,vars);
            line=line.mid(pos+1).trimmed();
            continue;
        }
        //no match
        qDebug()<<"Warning: unknown section option"<<line;
        return false;
    }
    return true;
}

WocGenericFile::~WocGenericFile()
{
	closeFile();
}

void WocGenericFile::createNewFile()
{
    closeFile();
    const QString fn=mfilepattern.evaluateToString();
    mcurrentfile=new MFile(mparent->outputTargetDir()+"/"+fn);
    if(!mcurrentfile->open(QIODevice::WriteOnly|QIODevice::Truncate))
        qDebug()<<"Warning: unable to create file"<<fn;
}

void WocGenericFile::closeFile()
{
	if(mcurrentfile){
		mcurrentfile->close();
		delete mcurrentfile;
		mcurrentfile=nullptr;
	}
}

void WocGenericFile::trigger(QString triggerName)
{
    //is this a signal to create a new file?
    if(triggerName == mfiletype+".new")createNewFile();
    //go through patterns and execute
    bool chainexecd=false;
    if(mcurrentfile && mcurrentfile->isOpen())
    for(Section&sec:msections){
        //match
        if(sec.trigger!=triggerName)continue;
        if(sec.iselse){
            if(chainexecd)continue;
            if(!sec.condition.isNull()){
                if(sec.condition.evaluateToBool())
                    chainexecd=true;
                else
                    continue;
            }
        }else{
            chainexecd=false;
            if(!sec.condition.isNull()){
                if(sec.condition.evaluateToBool())
                    chainexecd=true;
                else
                    continue;
            }
        }
        //calculate
        QString cont=sec.content.evaluateToString();
        //append
        if(sec.targetSection=="*")
            mcurrentfile->write(cont.toUtf8());
        else if(sec.targetSection=="*ERROR")
            qFatal("ERROR: %s",cont.toLocal8Bit().data());
        else if(sec.targetSection=="*WARNING")
            qDebug()<<"WARNING:"<<cont;
        else{
            QString vname="#"+sec.targetSection;
            if(sec.clear)
                mvars->setVariable(vname,cont);
            else
                mvars->setVariable(vname,mvars->getVariable(vname)+cont);
        }
    }
    //should we close the file now?
    if(triggerName == mfiletype+".close")closeFile();
}

