// Copyright (C) 2016-18 by Konrad Rosenbaum <konrad@silmor.de>
// protected under the GNU GPL version 3 or at your option any newer.
// See COPYING.GPL file that comes with this distribution.
//

#ifndef WOC_GENERICVARIABLE_H
#define WOC_GENERICVARIABLE_H

#include <QString>
#include <QMap>

///Abstract base class of variable stores.
///Variable stores are used to find the values of variables during generation of files.
///A store can have a parent store, which is asked if the variable does not exist locally.
///This can be used to separate system-wide constants from output specific variables and even file specific variables/sections.
class WocGenericVariables
{
public:
    ///instantiates a new variable store
    ///\param parent the parent store that is asked if a variable does not exist locally.
	explicit WocGenericVariables(WocGenericVariables*parent=nullptr):mparent(parent){}
	///deletes the store
	virtual ~WocGenericVariables();
	
    ///abstract: returns the value of a variable
	virtual QString getVariable(QString name)=0;
    ///default implementation for setting variable values
	virtual bool setVariable(QString name,QString value)
	{
		Q_UNUSED(name);Q_UNUSED(value);
		return false;
	}
	
protected:
    ///parent store to be asked if a variable cannot be found locally
	WocGenericVariables*mparent=nullptr;
};


class WocProjectVariables:public WocGenericVariables
{
public:
	static WocProjectVariables* instance();
	QString getVariable(QString)override;
    
private:    
    explicit WocProjectVariables();
};

///The variable store used for normal write-able variables defined in meta.xml and elsewhere.
class WocSimpleVariables:public WocGenericVariables
{
public:
    ///instantiates the variable store
    ///\param parent the variable store that is used as a fall-back if a variable is not found
	explicit WocSimpleVariables(WocGenericVariables*parent):WocGenericVariables(parent){}
	
	///gets the value of a variable
	QString getVariable(QString name)override
	{
		if(mvars.contains(name))return mvars[name].value;
		else 
            if(mparent)return mparent->getVariable(name);
            else return QString();
	}
	
	///sets or overrides a variable in this store;
	///the variable is valid for the life-time of the store
	///\returns true on success, false if the variable name is invalid for this store
	bool setVariable(QString name,QString value)override
	{
		if(!isValidName(name))return false;
		mvars.insert(name,value);
		return true;
	}
	///sets or overrides a variable in this store;
	///the variables is deleted on deleteTrigger (if it shadows another variable, the original becomes visible again)
	///\returns true on success, false if the variable name is invalid for this store
	virtual bool setVariable(QString name,QString value,QString deleteTrigger)
	{
        if(!isValidName(name))return false;
        mvars.insert(name,Variable(value,deleteTrigger));
        return true;
    }
    ///deletes all variables with a specific delete trigger
    virtual void deleteOldVariables(QString trigger)
    {
        for(QString k:mvars.keys())
            if(mvars[k].deleteTrigger==trigger)
                mvars.remove(k);
    }
private:
    ///represents a single variable value
    struct Variable{
        Variable(){}
        Variable(QString v):value(v){}
        Variable(QString v,QString d):value(v),deleteTrigger(d){}
        QString value,deleteTrigger;
    };
    ///stores all variables of this store
	QMap<QString,Variable>mvars;
	
    ///checks that the variable name is valid according to the rules of this store
	virtual bool isValidName(QString);
};

///Represents Pattern File Sections as variables, accepts only variables starting with "#".
class WocSectionVariables:public WocSimpleVariables
{
public:
    explicit WocSectionVariables(WocGenericVariables*parent):WocSimpleVariables(parent){}
private:
    virtual bool isValidName(QString);
};


#endif
