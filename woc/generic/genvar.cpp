// Copyright (C) 2016-18 by Konrad Rosenbaum <konrad@silmor.de>
// protected under the GNU GPL version 3 or at your option any newer.
// See COPYING.GPL file that comes with this distribution.
//

#include "genvar.h"
#include "../proc/processor.h"

#include <QDebug>
#include <QRegExp>

// =======================================================================
// Variables

WocGenericVariables::~WocGenericVariables(){}

//RegExp pattern to allow the following rules:
// - first component must start with letter or underscore
// - components may contain letters, digits, underscores
// - components must not be empty
// - components are separated by dot
// - must contain at least one component, up to any amount
// - leading or trailing dot is not allowed
#define VVPAT "[a-zA-Z_]([a-zA-Z0-9_]*)(([.]([a-zA-Z0-9_]+))*)"
//regexp for regular variables defined in meta.xml
static const QRegExp validVarName(VVPAT);
//regexp for Section variables in pattern files (this is more permissive than section syntax itself)
static const QRegExp validVarNameSection("#" VVPAT);


bool WocSimpleVariables::isValidName(QString name)
{
    return validVarName.exactMatch(name);
}

bool WocSectionVariables::isValidName(QString name)
{
    return validVarNameSection.exactMatch(name);
}

WocProjectVariables::WocProjectVariables(){}

WocProjectVariables* WocProjectVariables::instance()
{
    static WocProjectVariables inst;
    return &inst;
}

#ifndef NO_PACK_VERSION_H
#include "../../vinfo/staticVersion.h"
#endif

static inline QString auth2str(WocProcessor::AuthMode m)
{
    switch(m){
        case WocProcessor::NoAuth:return "none";
        case WocProcessor::SessionAuth:return "session";
        case WocProcessor::BasicAuth:return "basic";
        case WocProcessor::UnknownAuth:return "unknown";
    }
    return "error?";//suppress useless warning
}
static inline QString enc2str(WocProcessor::MessageEncoding m)
{
    switch(m){
        case WocProcessor::DefaultEncoding:
        case WocProcessor::WobEncoding:return "wob";
        case WocProcessor::SoapEncoding:return "soap";
    }
    return "error?";//suppress useless warning
}


QString WocProjectVariables::getVariable(QString vname)
{
    if("project.name"==vname)return WocProcessor::instance()->projectName();
    if("project.doc"==vname)return WocProcessor::instance()->docStrings().join("\n\n");
    if("project.baseDir"==vname)return WocProcessor::instance()->baseDir();
    if("project.wobDir"==vname)return WocProcessor::instance()->wobDir();
    if("project.xmlNamespace"==vname)return WocProcessor::instance()->xmlProjectNamespace();
    if("project.auth"==vname)return auth2str(WocProcessor::instance()->authMode());
    if("project.encoding"==vname)return enc2str(WocProcessor::instance()->messageEncoding());

    if("version.comm"==vname)return WocProcessor::instance()->verComm();
    if("version.needComm"==vname)return WocProcessor::instance()->verNeedComm();
    if("version.human"==vname)return WocProcessor::instance()->verHR();
    if("version.system"==vname)return WocProcessor::instance()->versionInfo().value("System");
    if("version.gentime"==vname)return WocProcessor::instance()->versionInfo().value("GenTime");
    if("version.author"==vname)return WocProcessor::instance()->versionInfo().value("Author");
    if("version.modified"==vname)return WocProcessor::instance()->versionInfo().value("LocallyModified");
    if("version.number"==vname)return WocProcessor::instance()->versionInfo().value("Number");
    if("version.path"==vname)return WocProcessor::instance()->versionInfo().value("Path");
    if("version.rootUrl"==vname)return WocProcessor::instance()->versionInfo().value("RootURL");
    if("version.time"==vname)return WocProcessor::instance()->versionInfo().value("Time");

#ifndef NO_PACK_VERSION_H
    if("version.woc.human"==vname)return WOCgenerated_versionInfo(WOb::VersionHR);
    if("version.woc.system"==vname)return WOCgenerated_versionInfo(WOb::VersionSystem);
    if("version.woc.gentime"==vname)return WOCgenerated_versionInfo(WOb::VersionGenTime);
    if("version.woc.author"==vname)return WOCgenerated_versionInfo(WOb::VersionAuthor);
    if("version.woc.modified"==vname)return WOCgenerated_versionInfo(WOb::VersionLocallyModified);
    if("version.woc.number"==vname)return WOCgenerated_versionInfo(WOb::VersionNumber);
    if("version.woc.path"==vname)return WOCgenerated_versionInfo(WOb::VersionPath);
    if("version.woc.rootUrl"==vname)return WOCgenerated_versionInfo(WOb::VersionRootURL);
    if("version.woc.time"==vname)return WOCgenerated_versionInfo(WOb::VersionTime);
#endif

    if("db.instance"==vname)return WocProcessor::instance()->dbInst();
    if("db.schema"==vname)return WocProcessor::instance()->dbSchema();
    if("db.configTable"==vname)return WocProcessor::instance()->dbConfigTable();
    if("db.configKeyColumn"==vname)return WocProcessor::instance()->dbConfigKeyColumn();
    if("db.configValueColumn"==vname)return WocProcessor::instance()->dbConfigValueColumn();
    if("db.version"==vname)return WocProcessor::instance()->dbVersion();
    if("db.versionRow"==vname)return WocProcessor::instance()->dbVersionRow();

    return QString();
}

