// Copyright (C) 2016-18 by Konrad Rosenbaum <konrad@silmor.de>
// protected under the GNU GPL version 3 or at your option any newer.
// See COPYING.GPL file that comes with this distribution.
//

#include "genout.h"
#include "genfile.h"
#include "genexpr.h"
#include "genvar.h"
#include "../domquery.h"

#include <QDebug>
#include <QDir>
#include <QDomDocument>
#include <QDomElement>
#include <QFile>
#include <QFileInfo>


// =========================================================
// Construction

WocGenericOut::WocGenericOut(PatternDir pattern, const QDomElement&el)
{
	qDebug("Info: creating %s Generator.",el.tagName().toLatin1().data());
    m_lang=pattern.patterntype;
	m_basedir=WocProcessor::instance()->baseDir()+"/"+el.attribute("sourceDir",".");
	m_subdir=el.attribute("subDir","wob");
	m_clean=str2bool(el.attribute("clean","0"));
	//get/create directory
	QDir d(m_basedir+"/"+m_subdir);
	if(!d.exists())QDir(".").mkpath(m_basedir+"/"+m_subdir);
    //parse meta.xml
    m_vars=new WocSimpleVariables(WocProjectVariables::instance());
    if(!readMeta(pattern.dirname+"/meta.xml")){
        qDebug()<<"Error: unable to initialize output defined by tag"<<el.tagName()<<"on line"<<el.lineNumber();
        WocProcessor::instance()->errorFound();
        return;
    }
    //read features
    for(QString attr:m_features.keys()){
        m_features[attr].realval=el.attribute(attr,m_features[attr].defaultval);
        m_vars->setVariable(attr,m_features[attr].realval);
//         qDebug()<<"read feature"<<attr<<m_features[attr].realval;
    }
    //initialize file patterns
    readFiles();
    //start processing
    initialize();
}

WocGenericOut::~WocGenericOut()
{
    m_files.clear();
    m_precalc.clear();
    if(m_vars){
        delete m_vars;
        m_vars=nullptr;
    }
}

// =========================================================
// reading/parsing pattern files

bool WocGenericOut::readMeta(QString mfile)
{
    //open file, base checks
    QFileInfo info(mfile);
    if(!info.exists() || !info.isFile() || !info.isReadable()){
        qDebug()<<"Error: meta file"<<info.absoluteFilePath()<<"does not exist or is not readable.";
        return false;
    }
    QFile fd(info.absoluteFilePath());
    if(!fd.open(QIODevice::ReadOnly)){
        qDebug()<<"Error: cannot read pattern meta file"<<info.absoluteFilePath()<<"giving up!";
        return false;
    }
    QDomDocument doc;
    int line=0,col=0;
    QString err;
    if(!doc.setContent(&fd,false,&err,&line,&col)){
        qDebug()<<"Error: pattern meta file"<<info.absoluteFilePath()<<"is not a valid XML file. Error on line"<<line<<"column"<<col<<":\n"<<err;
        return false;
    }
    fd.close();
    QDomElement root=doc.documentElement();
    if(root.tagName()!="Meta"){
        qDebug()<<"Error: pattern meta file"<<info.absoluteFilePath()<<"is not a valid meta file.";
        return false;
    }
    //process includes
    const QString pdir=info.absolutePath();
    for(QDomElement el:MDomQuery(root,"Include").toElementList()){
        QString fn=el.attribute("file");
        if(!readMeta(pdir+"/"+fn)){
            qDebug()<<"Error including meta file"<<fn<<"from"<<mfile;
            return false;
        }
    }
    //process syntax
    QDomNodeList nl=root.elementsByTagName("Syntax");
    if(nl.size()>1){
        qDebug()<<"Error: Too many Syntax tags in"<<mfile;
        return false;
    }
    if(nl.size()==1){
        QDomElement syntax=nl.at(0).toElement();
        m_syntComment=syntax.attribute("comment","#WOC:");
        m_syntSection=syntax.attribute("section-start","#SECTION:");
        QStringList vm=syntax.attribute("variable-marker","{ }").split(' ',Qt::SkipEmptyParts);
        if(vm.size()!=2){
            qDebug()<<"Error: variable marker defined in"<<mfile<<"is invalid.";
            return false;
        }
        m_syntVarStart=vm[0];
        m_syntVarEnd=vm[1];
    }
    //process features
    for(const QDomElement &el:MDomQuery(root,"Feature").toElementList()){
        Feature f;
        f.name=el.attribute("name").trimmed();
        if(f.name.isEmpty()){
            qDebug()<<"Error: nameless feature in"<<mfile<<"line"<<el.lineNumber();
            return false;
        }
        f.defaultval=el.attribute("default");
        const QString tp=el.attribute("type","string").trimmed().toLower();
        if(tp=="string")f.type=FeatureType::String;else
        if(tp=="bool"||tp=="boolean")f.type=FeatureType::Bool;
        else{
            qDebug()<<"Error: invalid feature type"<<tp<<"in file"<<mfile<<"line"<<el.lineNumber();
            return false;
        }
        //TODO: check that bool features are actual bools
        m_features.insert(f.name,f);
    }
    //process file definitions
    for(const QDomElement&el:MDomQuery(root,"File").toElementList()){
        FileConf fc;
        fc.trigger=el.attribute("type").trimmed().toLower();
        fc.tag=el.attribute("tag");
        fc.pattern=pdir+"/"+el.attribute("pattern").trimmed();
        fc.name=el.attribute("name",QFileInfo(fc.pattern).fileName()).trimmed();
        m_fileconf.insert(QPair<QString,QString>(fc.trigger,fc.tag),fc);
    }
    //process type mappings
    for(const QDomElement&el:MDomQuery(root,"Type").toElementList())
        m_typemap.insert(el.attribute("config").trimmed(),el.attribute("map").trimmed());
    //process precalculations
    for(const QDomElement&el:MDomQuery(root,"Precalc").toElementList())
        m_precalc<<WocGenericPrecalc(el);
    //done
    return true;
}

void WocGenericOut::readFiles()
{
    for(FileConf fconf:m_fileconf)
        m_files<<WocGenericFile(this,fconf.trigger,fconf.name,fconf.pattern,m_vars);
}



// =========================================================
// generator routines

void WocGenericOut::initialize()
{
    //basic variables: project
    m_vars->setVariable("project.sourceDir",m_basedir);
    m_vars->setVariable("project.subDir",m_subdir);
    //all other basic variables for project, version and DB are defined in WocProjectVariables (see genvar.cpp)
    //start base triggers
    trigger("project.new");
    trigger("version.new");
    trigger("version.close");
    trigger("interface.new");
}

void WocGenericOut::finalize()
{
    //closing triggers
    if(m_numTables>0)trigger("db.close");
    trigger("interface.close");
    trigger("project.close");
    //cleanup directory (remove untouched normal files, assume remainder is harmless)
    QDir d(m_basedir+"/"+m_subdir);
    if(m_clean){
        QStringList ent=d.entryList(QDir::Files);
        for(int i=0;i<ent.size();i++)
            if(!MFile::touchedFile(m_basedir+"/"+m_subdir+"/"+ent[i])){
                qDebug("Info: removing old file %s",ent[i].toLatin1().data());
                d.remove(ent[i]);
            }
    }
}

static inline QString delisttype(QString t)
{
    if(t.startsWith("List:"))return t.mid(5);
    else return t;
}

void WocGenericOut::newClass(const WocClass&cls)
{
    Q_UNUSED(cls);
    //set class variables
    m_vars->setVariable("class.name",cls.name(),"class.close");
    m_vars->setVariable("class.abstract",(cls.isAbstract(m_lang)?"true":"false"),"class.close");
    m_vars->setVariable("class.base",cls.baseClass(m_lang,""),"class.close");
    m_vars->setVariable("class.doc",cls.docStrings().join("\n\n"),"class.close");
    m_vars->setVariable("class.abstractLangs",cls.abstractLangs().join("\n"),"class.close");
    //start
    trigger("class.new");
    //generate enums
    for(QString entype:cls.enumTypes()){
        m_vars->setVariable("class.enum.name",entype,"class.enum.close");
        m_vars->setVariable("class.enum.doc",cls.enumDoc(entype),"class.enum.close");
        trigger("class.enum.new");
        for(WocEnum enval:cls.enumValues(entype)){
            m_vars->setVariable("class.enum.value.name",enval.name,"class.enum.value");
            m_vars->setVariable("class.enum.value.numvalue",QString::number(enval.val),"class.enum.value");
            m_vars->setVariable("class.enum.value.doc",enval.doc,"class.enum.value");
            trigger("class.enum.value");
        }
        trigger("class.enum.close");
    }
    //generate properties
    for(QString prop:cls.propertyNames()){
        m_vars->setVariable("class.property.name",prop,"class.property");
        m_vars->setVariable("class.property.type",cls.propertyType(prop),"class.property");
        m_vars->setVariable("class.property.maptype",mapType(cls.propertyType(prop),&cls),"class.property");
        m_vars->setVariable("class.property.elementtype",delisttype(cls.propertyType(prop)),"class.property");
        m_vars->setVariable("class.property.elementmaptype",mapType(delisttype(cls.propertyType(prop)),&cls),"class.property");
        m_vars->setVariable("class.property.doc",cls.propDoc(prop),"class.property");
        m_vars->setVariable("class.property.isidentity",(cls.propertyIsIdentity(prop)?"true":"false"),"class.property");
        m_vars->setVariable("class.property.isabstract",(cls.propertyIsAbstract(prop)?"true":"false"),"class.property");
        m_vars->setVariable("class.property.isattribute",(cls.propertyIsAttribute(prop)?"true":"false"),"class.property");
        m_vars->setVariable("class.property.iselement",(cls.propertyIsElement(prop)?"true":"false"),"class.property");
        m_vars->setVariable("class.property.issimpleelement",(cls.propertyIsSimpleElement(prop)?"true":"false"),"class.property");
        m_vars->setVariable("class.property.isenum",(cls.propertyIsEnum(prop)?"true":"false"),"class.property");
        m_vars->setVariable("class.property.islist",(cls.propertyIsList(prop)?"true":"false"),"class.property");
        m_vars->setVariable("class.property.isint",(cls.propertyIsInt(prop)?"true":"false"),"class.property");
        m_vars->setVariable("class.property.isstring",(cls.propertyIsString(prop)?"true":"false"),"class.property");
        m_vars->setVariable("class.property.isblob",(cls.propertyIsBlob(prop)?"true":"false"),"class.property");
        m_vars->setVariable("class.property.isobject",(cls.propertyIsObject(prop)?"true":"false"),"class.property");
        trigger("class.property");
    }
    //generate mappings
    for(QString mapt:cls.mappingTables()){
        m_vars->setVariable("class.map.table",mapt,"class.map.close");
        m_vars->setVariable("class.map.doc","","class.map.close");//?no docu yet?
        trigger("class.map.new");
        QMap<QString,QString>map;
        for(QString prop:cls.mappingProperties(mapt)){
            m_vars->setVariable("class.map.value.property",prop,"class.map.value");
            m_vars->setVariable("class.map.value.column",map.value(prop),"class.map.value");
            m_vars->setVariable("class.map.value.call",cls.mapMethod(mapt,prop,m_lang),"class.map.value");
            trigger("class.map.value");
        }
        trigger("class.map.close");
    }
    //done
    trigger("class.close");
}

static inline QString trnAuthMode2str(WocTransaction::AuthMode m)
{
    switch(m){
        case WocTransaction::Auth:return "auth";
        case WocTransaction::Open:return "open";
        case WocTransaction::Checked:return "checked";
    }
    return "unknown";
}

void WocGenericOut::newTransaction(const WocTransaction&trn)
{
    //set transaction variables
    m_vars->setVariable("transaction.name",trn.name(),"transaction.close");
    m_vars->setVariable("transaction.updating",(trn.isDbUpdating()?"true":"false"),"transaction.close");
    m_vars->setVariable("transaction.doc",trn.docStrings().join("\n\n"),"transaction.close");
    m_vars->setVariable("transaction.mode",trnAuthMode2str(trn.authMode()),"transaction.close");
    m_vars->setVariable("transaction.nologReq",(trn.logMode()&WocTransaction::NoLogRequest?"true":"false"),"transaction.close");
    m_vars->setVariable("transaction.nologRsp",(trn.logMode()&WocTransaction::NoLogResponse?"true":"false"),"transaction.close");
    //start
    trigger("transaction.new");
    //create inputs
    trigger("transaction.input.new");
    for(QString name:trn.inputNames()){
        const QString typ=trn.inputType(name);
        m_vars->setVariable("transaction.input.name",name,"transaction.input.value");
        m_vars->setVariable("transaction.input.type",typ,"transaction.input.value");
        m_vars->setVariable("transaction.input.maptype",mapType(typ),"transaction.input.value");
        m_vars->setVariable("transaction.input.doc",trn.inputDoc(name),"transaction.input.value");
        m_vars->setVariable("transaction.input.isattribute",(WocTransaction::isAttributeType(typ)?"true":"false"),"transaction.input.value");
        m_vars->setVariable("transaction.input.iselement",(WocTransaction::isElementType(typ)?"true":"false"),"transaction.input.value");
        m_vars->setVariable("transaction.input.issimpleelement",(WocTransaction::isElementType(typ)&&!WocTransaction::isObjectType(typ)?"true":"false"),"transaction.input.value");
        m_vars->setVariable("transaction.input.islist",(WocTransaction::isListType(typ)?"true":"false"),"transaction.input.value");
        m_vars->setVariable("transaction.input.isint",(WocTransaction::isIntType(typ)?"true":"false"),"transaction.input.value");
        m_vars->setVariable("transaction.input.isstring",(WocTransaction::isStringType(typ)?"true":"false"),"transaction.input.value");
        m_vars->setVariable("transaction.input.isblob",(WocTransaction::isBlobType(typ)?"true":"false"),"transaction.input.value");
        m_vars->setVariable("transaction.input.isobject",(WocTransaction::isObjectType(typ)?"true":"false"),"transaction.input.value");
        trigger("transaction.input.value");
    }
    trigger("transaction.input.close");
    //create outputs
    trigger("transaction.output.new");
    for(QString name:trn.outputNames()){
        const QString typ=trn.outputType(name);
        m_vars->setVariable("transaction.output.name",name,"transaction.output.value");
        m_vars->setVariable("transaction.output.type",typ,"transaction.output.value");
        m_vars->setVariable("transaction.output.maptype",mapType(typ),"transaction.output.value");
        m_vars->setVariable("transaction.output.doc",trn.outputDoc(name),"transaction.output.value");
        m_vars->setVariable("transaction.output.isattribute",(WocTransaction::isAttributeType(typ)?"true":"false"),"transaction.output.value");
        m_vars->setVariable("transaction.output.iselement",(WocTransaction::isElementType(typ)?"true":"false"),"transaction.output.value");
        m_vars->setVariable("transaction.output.issimpleelement",(WocTransaction::isElementType(typ)&&!WocTransaction::isObjectType(typ)?"true":"false"),"transaction.output.value");
        m_vars->setVariable("transaction.output.islist",(WocTransaction::isListType(typ)?"true":"false"),"transaction.output.value");
        m_vars->setVariable("transaction.output.isint",(WocTransaction::isIntType(typ)?"true":"false"),"transaction.output.value");
        m_vars->setVariable("transaction.output.isstring",(WocTransaction::isStringType(typ)?"true":"false"),"transaction.output.value");
        m_vars->setVariable("transaction.output.isblob",(WocTransaction::isBlobType(typ)?"true":"false"),"transaction.output.value");
        m_vars->setVariable("transaction.output.isobject",(WocTransaction::isObjectType(typ)?"true":"false"),"transaction.output.value");
        trigger("transaction.output.value");
    }
    trigger("transaction.output.close");
    //create privileges
    for(QString priv:trn.privileges()){
        m_vars->setVariable("transaction.privilege.name",priv,"transaction.privilege");
        m_vars->setVariable("transaction.privilege.doc",trn.privilegeDoc(priv),"transaction.privilege");
        trigger("transaction.privilege");
    }
    //create call; TODO: may need some fixing, check call structure in WocTransaction
    m_vars->setVariable("transaction.call.code",trn.callFunction(m_lang),"transaction.call");
    trigger("transaction.call");
    //done
    trigger("transaction.close");
}

void WocGenericOut::newTable(const WocTable&tab)
{
    //first table? yes: this is the start of the DB (global db.* vars are in WocProjectVariables (genvar.cpp))
    if(m_numTables==0)trigger("db.new");
    m_numTables++;
    // non-audit table
    newTable(tab,tab.name(),false);
    // audit table
    if(tab.isAuditable())newTable(tab.auditTable(),tab.name(),true);
}

void WocGenericOut::newTable(const WocTable&tab,QString basename,bool isaudit)
{
    //do the actual table...
    //set variables
    m_vars->setVariable("table.name",tab.name(),"table.close");
    m_vars->setVariable("table.base",tab.baseClass(),"table.close");
    m_vars->setVariable("table.doc",tab.docStrings().join("\n\n"),"table.close");
    m_vars->setVariable("table.backup",tab.inBackup()?"true":"false","table.close");
    m_vars->setVariable("table.backupKey",tab.backupKey(),"table.close");
    const int bgs=tab.backupGroupSize();
    m_vars->setVariable("table.backupGroupSize",QString::number(bgs>0?bgs:0),"table.close");

    if(isaudit){
        m_vars->setVariable("table.hasaudit","false","table.close");
        m_vars->setVariable("table.isaudit","true","table.close");
        m_vars->setVariable("table.auditname",basename,"table.close");
    }else{
        m_vars->setVariable("table.hasaudit",(tab.isAuditable()?"true":"false"),"table.close");
        m_vars->setVariable("table.isaudit","false","table.close");
        m_vars->setVariable("table.auditname",tab.auditTableName(),"table.close");
    }
    //start
    trigger("table.new");
    //create columns
    for(QString col:tab.columns()){
        // set data
        m_vars->setVariable("table.column.name",col,"table.column.close");
        m_vars->setVariable("table.column.doc",tab.columnDoc(col),"table.column.close");
        m_vars->setVariable("table.column.type",tab.columnType(col),"table.column.close");
        m_vars->setVariable("table.column.null",tab.columnIsNull(col)?"true":"false","table.column.close");
        m_vars->setVariable("table.column.default",tab.columnDefault(col),"table.column.close");
        m_vars->setVariable("table.column.isforeignkey",tab.columnForeign(col).isEmpty()?"false":"true","table.column.close");
        m_vars->setVariable("table.column.foreignkey",tab.columnForeign(col),"table.column.close");
        QStringList fk=tab.columnForeign(col).split(":");
        m_vars->setVariable("table.column.foreignkeytable",fk.value(0),"table.column.close");
        m_vars->setVariable("table.column.foreignkeycolumn",fk.value(1),"table.column.close");
        m_vars->setVariable("table.column.primarykey",tab.columnIsPrimary(col)?"true":"false","table.column.close");
        m_vars->setVariable("table.column.isaudit",tab.columnIsAudit(col)?"true":"false","table.column.close");
        m_vars->setVariable("table.column.isindexed",tab.columnIsIndexed(col)?"true":"false","table.column.close");
        m_vars->setVariable("table.column.isunique",tab.columnIsUnique(col)?"true":"false","table.column.close");
        //trigger
        trigger("table.column.new");
        //create enums
        for(const WocEnum&e:tab.columnEnums(col)){
            m_vars->setVariable("table.column.enum.name",e.name,"table.column.enum.value");
            m_vars->setVariable("table.column.enum.numvalue",QString::number(e.val),"table.column.enum.value");
            m_vars->setVariable("table.column.enum.doc",e.doc,"table.column.enum.value");
            trigger("table.column.enum.value");
        }
        //done
        trigger("table.column.close");
    }
    //create presets
    if(tab.presets().size()){
        trigger("table.preset.new");
        for(auto row:tab.presetList()){
            trigger("table.preset.row.new");
            for(QString col:tab.columns()){
                //data
                bool isset=row.contains(col);
                m_vars->setVariable("table.preset.colname",col,"table.preset.row.close");
                m_vars->setVariable("table.preset.isset",isset?"true":"false","table.preset.row.close");
                m_vars->setVariable("table.preset.value",row.value(col).value,"table.preset.row.close");
                m_vars->setVariable("table.preset.code",row.value(col).call,"table.preset.row.close");
                m_vars->setVariable("table.preset.iscode",row.value(col).call.isEmpty()?"false":"true","table.preset.row.close");
                m_vars->setVariable("table.preset.type",tab.columnType(col),"table.preset.row.close");
                m_vars->setVariable("table.preset.isstring",tab.columnIsString(col)?"true":"false","table.preset.row.close");
                m_vars->setVariable("table.preset.isint",tab.columnIsInt(col)?"true":"false","table.preset.row.close");
                m_vars->setVariable("table.preset.isblob",tab.columnIsBlob(col)?"true":"false","table.preset.row.close");
                //trigger
                trigger("table.preset.row.column");
                if(isset)trigger("table.preset.row.value");

            }
            trigger("table.preset.row.close");
        }
        trigger("table.preset.close");
    }
    //create constraints
    for(QString ucon:tab.uniqueConstraints()){
        m_vars->setVariable("table.constraint.type","unique","table.constraint");
        m_vars->setVariable("table.constraint.columns",ucon,"table.constraint");
        trigger("table.constraint");
    }
    //create foreign calls
    for(QString f:tab.foreigns()){
        m_vars->setVariable("table.foreigncall.method",f,"table.foreigncall");
        QStringList match=tab.foreignQuery(f).split("=");
        QStringList fkey=match.value(0).split(":");
        m_vars->setVariable("table.foreigncall.column",match.value(1),"table.foreigncall");
        m_vars->setVariable("table.foreigncall.foreigntable",fkey.value(0),"table.foreigncall");
        m_vars->setVariable("table.foreigncall.foreigncolumn",fkey.value(1),"table.foreigncall");
        m_vars->setVariable("table.foreigncall.doc",tab.foreignDoc(f),"table.foreigncall");
        trigger("table.foreigncall");
    }
    //done
    trigger("table.close");
}

void WocGenericOut::trigger(QString triggerName)
{
    //precalculations
    for(const auto&pc:m_precalc)
        if(pc.triggerName()==triggerName)
            pc.calculate(*this,*m_vars);
    //signal files
    for(auto&file:m_files)file.trigger(triggerName);
    //variable deletions
    m_vars->deleteOldVariables(triggerName);
}

QString WocGenericOut::mapType(QString type,const WocClass*context)const
{
    //simple mappings
    if(m_typemap.contains(type))
        return m_typemap[type];
    if(type.startsWith("string:") && m_typemap.contains("string:*"))
        return QString(m_typemap["string:*"]).replace("*",type.mid(7));

    //list mapping
    if(type.startsWith("List:") && m_typemap.contains("List:*"))
        return QString(m_typemap["List:*"]).replace("*",mapType(type.mid(5)));

    //object mapping
    if(WocProcessor::instance()->hasClass(type) && m_typemap.contains("Object"))
        return WocGenericExpression(m_typemap["Object"],syntaxVariableMeta(),m_vars).evaluateToString().replace("*",type);

    //enum types (needs context)
    if(context && context->hasEnumType(type) && m_typemap.contains("Enum"))
        return WocGenericExpression(m_typemap["Enum"],syntaxVariableMeta(),m_vars).evaluateToString().replace("*",type);

    //fall back
    return type;
}


// =========================================================
// Precalculation

WocGenericPrecalc::WocGenericPrecalc(const QDomElement&el)
{
    mtrigger=el.attribute("trigger");
    mdelete=el.attribute("deleteOn");
    for(QDomElement vel:MDomQuery(el,"Variable").toElementList()){
        Rule r;
        r.name=vel.attribute("name");
        r.condition=vel.attribute("if").trimmed();
        if(r.condition.isEmpty()){
            r.value=vel.attribute("value");
        }else{
            r.value=vel.attribute("valueTrue");
            r.valueFalse=vel.attribute("valueFalse");
        }
        mrules<<r;
    }
}

void WocGenericPrecalc::calculate(WocGenericOut&parent, WocSimpleVariables&vars) const
{
//     qDebug()<<"Precalc trg"<<mtrigger<<mdelete;
    for(const Rule&r:mrules){
        QString val;
        if(r.condition.isEmpty())
            val=WocGenericExpression(r.value,parent.syntaxVariableMeta(),&vars).evaluateToString();
        else{
            bool c=WocGenericExpression(r.condition,parent.syntaxVariableMeta(),&vars).evaluateToBool();
            QString expr=c?r.value:r.valueFalse;
            val=WocGenericExpression(expr,parent.syntaxVariableMeta(),&vars).evaluateToString();
        }
        vars.setVariable(r.name,val,mdelete);
//         qDebug()<<"  Precalc"<<r.name<<val;
    }
}
