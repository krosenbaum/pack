// Copyright (C) 2016 by Konrad Rosenbaum <konrad@silmor.de>
// protected under the GNU GPL version 3 or at your option any newer.
// See COPYING.GPL file that comes with this distribution.
//

#include "genproc.h"
#include "genout.h"

#include <QMap>
#include <QDir>
#include <QDebug>
#include <QDomDocument>

// pattern map: tag => pattern
static QMap<QString,PatternDir> genmap;

void WalkPattern(QString dir,QString tname=QString())
{
	// recurse into sub-dirs
	for(auto entry:QDir(dir).entryInfoList(QDir::AllDirs)){
		if(entry.fileName()=="." || entry.fileName()=="..")continue;
		if(entry.isDir())
			WalkPattern(entry.absoluteFilePath(),tname+"/"+entry.fileName());
	}

	// check whether this is a pattern dir
	QFileInfo info(dir+"/meta.xml");
	if(!info.exists() || !info.isFile() || !info.isReadable())return;
	PatternDir pat;
	pat.dirname=dir;
	//parse meta and find tag name
	QFile fd(info.absoluteFilePath());
	if(!fd.open(QIODevice::ReadOnly)){
		qDebug()<<"Error: cannot read pattern meta file"<<info.absoluteFilePath()<<"ignoring it!";
		return;
	}
	QDomDocument doc;
    int line=0,col=0;
    QString err;
    if(!doc.setContent(&fd,false,&err,&line,&col)){
        qDebug()<<"Error: pattern meta file"<<info.absoluteFilePath()<<"is not a valid XML file. Error on line"<<line<<"column"<<col<<":\n"<<err;
        return;
    }
	fd.close();
    QDomElement root=doc.documentElement();
    if(root.tagName()!="Meta"){
        qDebug()<<"Error: pattern meta file"<<info.absoluteFilePath()<<"is not a valid meta file.";
        return;
    }
    pat.tagname=root.attribute("tag").trimmed();
    pat.patterntype=root.attribute("lang").trimmed();
    if(pat.tagname.isEmpty()){
        qDebug()<<"Warning: pattern in"<<info.absoluteFilePath()<<"has no valid tag name! Ignoring it!";
        return;
    }
    if(pat.patterntype.isEmpty()){
        qDebug()<<"Warning: pattern in"<<info.absoluteFilePath()<<"has no language type! Ignoring it!";
        return;
    }
    qDebug()<<"Found pattern"<<pat.patterntype<<"with tag"<<pat.tagname<<"in"<<pat.dirname;
	genmap.insert(pat.tagname,pat);
}

void InitializeGeneric(QString dir)
{
	//go through patterns recursively
	if(dir.isEmpty())
		WalkPattern(":/pattern");
	else
		WalkPattern(dir);
}

bool CreateGenericOutput(QString tagname, const QDomElement&element)
{
	if(!genmap.contains(tagname))
		return false;
	PatternDir pat=genmap[tagname];
	new WocGenericOut(pat,element);
	return true;
}
