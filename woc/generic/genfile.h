// Copyright (C) 2016-18 by Konrad Rosenbaum <konrad@silmor.de>
// protected under the GNU GPL version 3 or at your option any newer.
// See COPYING.GPL file that comes with this distribution.
//

#ifndef WOC_GENERICFILE_H
#define WOC_GENERICFILE_H

#include <QObject>
#include "../mfile.h"
#include "genexpr.h"

class WocGenericOutBase;
class WocGenericVariables;

///Represents a pattern file during output generation.
class WocGenericFile
{
public:
    WocGenericFile(WocGenericOutBase*parent,QString type,QString namepattern,QString patternfile,WocGenericVariables*globalvars);
    virtual ~WocGenericFile();
    void trigger(QString triggerName);
private:
    MFile*mcurrentfile=nullptr;
    QString mfiletype;
    WocGenericOutBase*mparent;
    WocGenericVariables*mvars;
    WocGenericExpression mfilepattern;
    struct Section{
        Section()=default;
        Section(const Section&)=default;
        Section& operator=(const Section&)=default;
        
        bool isValid()const{return !targetSection.isEmpty() && !trigger.isEmpty();}
        void setContent(const QStringList&,QPair<QString,QString>,WocGenericVariables*);
        bool startSection(QString,QPair<QString,QString>,WocGenericVariables*);
        
        QString targetSection,trigger;
        bool clear=false,iselse=false;
        int linenum=0;
        WocGenericExpression content,condition;
    };
    QList<Section>msections;
    
    void createNewFile();
    void closeFile();
};


#endif
