// Copyright (C) 2016 by Konrad Rosenbaum <konrad@silmor.de>
// protected under the GNU GPL version 3 or at your option any newer.
// See COPYING.GPL file that comes with this distribution.
//

#ifndef WOC_GENPROC_H
#define WOC_GENPROC_H

#include "processor.h"
#include <QString>

class QDomElement;

//pattern descriptions
struct PatternDir{
	QString tagname,patterntype,dirname;
};



///Scans a directory and looks for valid patterns.
void InitializeGeneric(QString dirs=QString());

///Creates the generic pattern based output object based on a tag name.
bool CreateGenericOutput(QString tagname,const QDomElement&);

#endif
