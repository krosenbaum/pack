#Include this file to be able to automatically generate QRC files from directories
#
# Setting this variable:
#  GRESOURCES += directory
# will auto-generate directory.qrc and include it into the final binary.


qrcgen.output  = ${QMAKE_FILE_NAME}.qrc
qrcgen.variable_out = RESOURCES
qrcgen.commands = $$PWD/qrcgen ${QMAKE_FILE_NAME} ${QMAKE_FILE_NAME} ${QMAKE_FILE_NAME}.qrc
qrcgen.depend_command = find ${QMAKE_FILE_NAME} -type f
qrcgen.input = GRESOURCES
QMAKE_EXTRA_COMPILERS += qrcgen
