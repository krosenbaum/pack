#include <QCoreApplication>
#include <QDir>
#include <QDebug>

#define PROLOG "<!DOCTYPE RCC>\n\
<!-- AUTO GENERATED FILE -->\n\
<RCC version=\"1.0\">\n<qresource>\n"

#define EPILOG "</qresource></RCC>\n"

void writeOut(QFile&out,QString source,QString target)
{
	QDir dir(source);
	qDebug()<<"Scanning directory"<<source<<(dir.isReadable()?"(ok)":"(NOT READABLE)")<<"as"<<target<<"into"<<out.fileName();
	for(QString fn:dir.entryList(QDir::Files|QDir::Dirs)){
		if(fn=="." || fn=="..")continue;
		if(QFileInfo(source+"/"+fn).isDir()){
			writeOut(out,source+"/"+fn,target.isEmpty()?fn:(target+"/"+fn));
		}else
			out.write(QString("<file alias=\"%3/%1\">%2/%1</file>\n").arg(fn).arg(source).arg(target).toLatin1());
	}
}

int main(int ac,char**av)
{
	QCoreApplication app(ac,av);
	qDebug()<<"Generatin QRCs from"<<QDir::current().absolutePath();
    if(app.arguments().size()!=4){
        qFatal("Usage: %s <sourcePath> <resourcePath> <resourceFileName>",app.arguments().at(0).toLocal8Bit().data());
        return 1;
    }
    const QStringList pathes=app.arguments().mid(1);
    if(!pathes[2].endsWith(".qrc")){
        qFatal("Resource File Name %s is invalid - resource file name must end in .qrc", pathes[2].toLatin1().data());
        return 1;
    }
    QFile out(pathes[2]);
    if(!out.open(QIODevice::WriteOnly|QIODevice::Truncate)){
        qFatal("Unable to create %s - giving up.", pathes[2].toLatin1().data());
        return 1;
    }
    out.write(PROLOG);
    writeOut(out,pathes[0],pathes[1]);
    out.write(EPILOG);
    out.close();
	return 0;
}
