// Copyright (C) 2009-2011 by Konrad Rosenbaum <konrad@silmor.de>
// protected under the GNU GPL version 3 or at your option any newer.
// See COPYING.GPL file that comes with this distribution.
//

#include "soapout.h"

#include "mfile.h"

#include <QDebug>
#include <QDir>

WocSoapOut::WocSoapOut(const QDomElement& el): WocOutput()
{
	WocProcessor*woc=WocProcessor::instance();
	m_auth=woc->authMode();
	//parse XML config
	m_name=el.attribute("fileprefix");
	m_dir=woc->baseDir()+"/"+el.attribute("sourceDir");
	//sanity check
	if(woc->messageEncoding()==WocProcessor::WobEncoding){
		if(str2bool(el.attribute("noerror","false"))){
			qDebug("Warning: SoapOutput generating WSDL file, while not in SOAP mode - the WSDL will not work with the generated code!");
		}else{
			qDebug("Error: SoapOutput active although the project is in Wob mode. Stopping.");
			emit errorFound();
			return;
		}
	}
	//make sure directory exists
	QDir().mkpath(m_dir);
	//create schema stuff
	m_schema=new WocSchemaOut(el.attribute("sourceDir"),m_name+".xsd");
	m_schema->addStaticSchemas(m_name+"_all.xsd");
	//create document and root node
	m_doc.appendChild(m_doc.createComment("Automatically generated WSDL file for project "+woc->projectName()+"\nDO NOT CHANGE THIS FILE DIRECTLY!"));
	m_root=m_doc.createElementNS("http://schemas.xmlsoap.org/wsdl/","ws:definitions");
	m_root.setAttribute("targetNamespace",woc->xmlProjectNamespace());
	m_root.setAttribute("xmlns",woc->xmlProjectNamespace());
	m_root.setAttribute("xmlns:xs",woc->xmlSchemaNamespace());
	m_root.setAttribute("xmlns:soap","http://schemas.xmlsoap.org/wsdl/soap/");
	m_root.setAttribute("elementFormDefault","qualified");
	
	schemaImport();
	headerMsg();
	mainElems();
}

void WocSoapOut::schemaImport()
{
	WocProcessor*woc=WocProcessor::instance();
	QDomElement wt=m_doc.createElement("ws:types");
	QDomElement sm=m_doc.createElement("xs:schema");
	QDomElement si;
	si=m_doc.createElement("xs:import");
	si.setAttribute("namespace",woc->xmlProjectNamespace());
	si.setAttribute("schemaLocation",m_name+".xsd");
	sm.appendChild(si);
	if(m_auth!=WocProcessor::NoAuth){
		si=m_doc.createElement("xs:import");
		si.setAttribute("namespace",woc->xmlPackNamespace()+"/Header");
		si.setAttribute("schemaLocation","wob-header.xsd");
		sm.appendChild(si);
	}
	wt.appendChild(sm);
	m_root.appendChild(wt);
}

void WocSoapOut::headerMsg()
{
	//skip if not authenticated
	if(m_auth==WocProcessor::NoAuth)return;
	//create header
	QDomElement msg=m_doc.createElement("ws:message");
	msg.setAttribute("name","Wob-Header");
	QDomElement prt=m_doc.createElement("ws:part");
	QString msgname=authMode2str();
	prt.setAttribute("name",msgname);
	prt.setAttribute("element",msgname);
	msg.appendChild(prt);
	m_root.appendChild(msg);
}

QString WocSoapOut::authMode2str()
{
	switch(m_auth){
		case WocProcessor::SessionAuth:return "SessionAuth";break;
		case WocProcessor::BasicAuth:return "BasicAuth";break;
		case WocProcessor::NoAuth:return "NoAuth";break;
		default:
			qDebug("Warning: oops. Unknown authentication mode in SoapOutput while generating header.");
			return "NoAuth";
			break;
	}
}

void WocSoapOut::mainElems()
{
	//create portType
	m_port=m_doc.createElement("ws:portType");
	m_port.setAttribute("name","ProjectPort");
	//create binding
	m_bind=m_doc.createElement("ws:binding");
	m_bind.setAttribute("name","ProjectBinding");
	m_bind.setAttribute("type","ProjectPort");
	QDomElement el=m_doc.createElement("soap:binding");
	el.setAttribute("style","document");
	el.setAttribute("transport","http://schemas.xmlsoap.org/soap/http");
	m_bind.appendChild(el);
}

void WocSoapOut::serviceTag()
{
	QDomElement svc=m_doc.createElement("ws:service");
	svc.setAttribute("name","ProjectService");
	QDomElement prt=m_doc.createElement("ws:port");
	prt.setAttribute("name","ProjectPort");
	prt.setAttribute("binding","ProjectBinding");
	QDomElement adr=m_doc.createElement("soap:address");
	adr.setAttribute("location","http://example.org");
	prt.appendChild(adr);
	svc.appendChild(prt);
	m_root.appendChild(svc);
}


void WocSoapOut::finalize()
{
	//finish off
	m_root.appendChild(m_port);
	m_root.appendChild(m_bind);
	serviceTag();
	m_doc.appendChild(m_root);
	//write
	MFile fd(m_dir+"/"+m_name+".wsdl");
	fd.open(QIODevice::WriteOnly);
	fd.write(m_doc.toByteArray());
}

void WocSoapOut::newTransaction(const WocTransaction& trn)
{
	m_root.appendChild(m_doc.createComment("Transaction "+trn.name()));
	WocProcessor *woc=WocProcessor::instance();
	//create messages
	QDomElement msg,prt,el;
	msg=m_doc.createElement("ws:message");
	msg.setAttribute("name","WobRequest-"+trn.name());
	prt=m_doc.createElement("ws:part");
	prt.setAttribute("name","WobRequest-"+trn.name());
	prt.setAttribute("type","WobRequest-"+trn.name());
	msg.appendChild(prt);
	m_root.appendChild(msg);
	msg=m_doc.createElement("ws:message");
	msg.setAttribute("name","WobResponse-"+trn.name());
	prt=m_doc.createElement("ws:part");
	prt.setAttribute("name","WobResponse-"+trn.name());
	prt.setAttribute("type","WobResponse-"+trn.name());
	msg.appendChild(prt);
	m_root.appendChild(msg);
	//create port
	msg=m_doc.createElement("ws:operation");
	msg.setAttribute("name",trn.name());
	prt=m_doc.createElement("ws:input");
	prt.setAttribute("name","WobRequest-"+trn.name());
	prt.setAttribute("message","WobRequest-"+trn.name());
	msg.appendChild(prt);
	prt=m_doc.createElement("ws:output");
	prt.setAttribute("name","WobResponse-"+trn.name());
	prt.setAttribute("message","WobResponse-"+trn.name());
	msg.appendChild(prt);
	m_port.appendChild(msg);
	//create binding
	msg=m_doc.createElement("ws:operation");
	msg.setAttribute("name",trn.name());
	prt=m_doc.createElement("soap:operation");
	prt.setAttribute("style","document");
	prt.setAttribute("soapAction",woc->xmlProjectNamespace()+"/Transaction/"+trn.name());
	msg.appendChild(prt);
	prt=m_doc.createElement("ws:input");
	prt.setAttribute("name","WobRequest-"+trn.name());
	if(m_auth!=WocProcessor::NoAuth && trn.authMode()!=WocTransaction::Open){
		el=m_doc.createElement("soap:header");
		el.setAttribute("message","Wob-Header");
		el.setAttribute("use","literal");
		el.setAttribute("required","true");
		el.setAttribute("part",authMode2str());
		prt.appendChild(el);
	}
	el=m_doc.createElement("soap:body");
	el.setAttribute("use","literal");
	prt.appendChild(el);
	msg.appendChild(prt);
	prt=m_doc.createElement("ws:output");
	prt.setAttribute("name","WobResponse-"+trn.name());
	el=m_doc.createElement("soap:body");
	el.setAttribute("use","literal");
	prt.appendChild(el);
	msg.appendChild(prt);
	m_bind.appendChild(msg);
}

void WocSoapOut::newClass(const WocClass& )
{
	/*classes are not reflected in the WSDL*/
}

void WocSoapOut::newTable(const WocTable& )
{
	/*tables are not exposed to the network, hence no WSDL*/
}
