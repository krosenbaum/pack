// Copyright (C) 2009-2011 by Konrad Rosenbaum <konrad@silmor.de>
// protected under the GNU GPL version 3 or at your option any newer.
// See COPYING.GPL file that comes with this distribution.
//

#ifndef WOC_SOAPOUT_H
#define WOC_SOAPOUT_H

#include "schemaout.h"

/**generates output for a schema file*/
class WocSoapOut:public WocOutput
{
	public:
		/**initializes the output object with the given DOM element*/
		WocSoapOut(const QDomElement&);
		
	protected:
		/**writes any last words after parsing finished*/
		virtual void finalize();
		/**creates a class*/
		virtual void newClass(const WocClass&);
		/**creates a table*/
		virtual void newTable(const WocTable&);
		/**creates a transaction*/
		virtual void newTransaction(const WocTransaction&);
	private:
		QString m_name,m_dir;
		QDomDocument m_doc;
		QDomElement m_root,m_bind,m_port;
		WocSchemaOut*m_schema;
		WocProcessor::AuthMode m_auth;
		
		/**creates schema imports*/
		void schemaImport();
		/**creates header message*/
		void headerMsg();
		/**create main elements for port and binding*/
		void mainElems();
		/**create service*/
		void serviceTag();
		
		/**return name of header element for authentication mode*/
		QString authMode2str();
};

#endif
