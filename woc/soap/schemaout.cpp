// Copyright (C) 2009-2011 by Konrad Rosenbaum <konrad@silmor.de>
// protected under the GNU GPL version 3 or at your option any newer.
// See COPYING.GPL file that comes with this distribution.
//

#include "schemaout.h"

#include "mfile.h"

#include <QDebug>
#include <QDir>

WocSchemaOut::WocSchemaOut(QString dname,QString fname): WocOutput()
{
	m_dir=WocProcessor::instance()->baseDir()+"/"+dname;
	m_name=fname;
	initialize();
}

WocSchemaOut::WocSchemaOut(const QDomElement& el): WocOutput()
{
	WocProcessor*woc=WocProcessor::instance();
	m_name=el.attribute("filename");
	m_dir=woc->baseDir()+"/"+el.attribute("sourceDir");
	initialize();
	if(el.hasAttribute("compound"))
		addStaticSchemas(el.attribute("compound"));
}


void WocSchemaOut::initialize()
{
	//make sure directory exists
	QDir().mkpath(m_dir);
	//create document and root node
	WocProcessor*woc=WocProcessor::instance();
	m_doc.appendChild(m_doc.createComment("Automatically generated main Schema file for project "+woc->projectName()+"\nThis file covers all classes and transactions defined in the project.\nDO NOT CHANGE THIS FILE DIRECTLY!"));
	m_root=m_doc.createElementNS(woc->xmlSchemaNamespace(),"xs:schema");
	m_root.setAttribute("targetNamespace",woc->xmlProjectNamespace());
	m_root.setAttribute("xmlns",woc->xmlProjectNamespace());
	m_root.setAttribute("elementFormDefault","qualified");
}

static inline void copyFile(QString dir,QString file,QDomDocument*doc=0)
{
	QFile in(":/"+file);
	in.open(QIODevice::ReadOnly);
	if(doc){
		doc->setContent(&in);
		in.seek(0);
	}
	MFile out(dir+"/"+file);
	out.open(QIODevice::WriteOnly);
	out.write(in.readAll());
}

static inline void copyAndImport(QString dir,QString file,QDomElement&root)
{
	//copy it
	QDomDocument doc;
	copyFile(dir,file,&doc);
	//get namespace
	QString dns=doc.documentElement().attribute("targetNamespace");
	//create import
	QDomElement el=root.ownerDocument().createElement("xs:import");
	el.setAttribute("namespace",dns);
	el.setAttribute("schemaLocation",file);
	root.appendChild(el);
}

void WocSchemaOut::addStaticSchemas(QString compound, WocProcessor::MessageEncoding encoding)
{
	//create compound document
	WocProcessor*woc=WocProcessor::instance();
	QDomDocument doc;
	doc.appendChild(doc.createComment("Automatically generated compound Schema file for project "+woc->projectName()+"\nThis file simply imports all relevant schema files.\nDO NOT CHANGE THIS FILE DIRECTLY!"));
	QDomElement root=doc.createElementNS(woc->xmlSchemaNamespace(),"xs:schema");
	QString cns=woc->xmlPackNamespace()+"/CompoundNameSpace";
	root.setAttribute("targetNamespace",cns);
	root.setAttribute("xmlns",cns);
	root.setAttribute("elementFormDefault","qualified");
	doc.appendChild(root);
	//import project
	QDomElement el=doc.createElement("xs:import");
	el.setAttribute("namespace",woc->xmlProjectNamespace());
	el.setAttribute("schemaLocation",m_name);
	root.appendChild(el);
	//copy parts
	if(encoding==WocProcessor::DefaultEncoding)
		encoding=woc->messageEncoding();
	copyAndImport(m_dir,"wob-base.xsd",root);
	if(encoding==WocProcessor::Soap12Encoding){
		copyAndImport(m_dir,"soap12.xsd",root);
		copyFile(m_dir,"xml.xsd");//already imported from soap12.xsd
	}
	//write compound
	MFile cfd(m_dir+"/"+compound);
	cfd.open(QIODevice::WriteOnly);
	cfd.write(doc.toByteArray());
}

void WocSchemaOut::finalize()
{
	//finish off
	m_doc.appendChild(m_root);
	//write
	MFile fd(m_dir+"/"+m_name);
	fd.open(QIODevice::WriteOnly);
	fd.write(m_doc.toByteArray());
}

void WocSchemaOut::newClass(const WocClass& cls)
{
	m_root.appendChild(m_doc.createComment("Class "+cls.name()));
	//create enums
	QStringList enl=cls.enumTypes();
	for(int i=0;i<enl.size();i++){
		QDomElement eel=m_doc.createElement("xs:simpleType");
		eel.setAttribute("name","enum-"+cls.name()+"-"+enl[i]);
		QDomElement rel=m_doc.createElement("xs:restriction");
		rel.setAttribute("base","xs:NMTOKEN");
		QList<WocEnum> vll=cls.enumValues(enl[i]);
		for(int j=0;j<vll.size();j++){
			QDomElement nel=m_doc.createElement("xs:enumeration");
			nel.setAttribute("value",vll[j].name);
			rel.appendChild(nel);
		}
		eel.appendChild(rel);
		m_root.appendChild(eel);
	}
	//create type
	QDomElement cel=m_doc.createElement("xs:complexType");
	cel.setAttribute("name","class-"+cls.name());
	QDomElement sel=m_doc.createElement("xs:sequence");
	cel.appendChild(sel);
	//get properties
	QStringList prl=cls.propertyNames();
	for(int i=0;i<prl.size();i++){
		if(cls.propertyIsElement(prl[i])){
			//create elements
			QDomElement eel=m_doc.createElement("xs:element");
			eel.setAttribute("name",prl[i]);
			eel.setAttribute("type",schemaType(cls,prl[i]));
			eel.setAttribute("minOccurs","0");
			if(cls.propertyIsList(prl[i]))
				eel.setAttribute("maxOccurs","unbounded");
			sel.appendChild(eel);
		}else{
			//create attributes
			QDomElement ael=m_doc.createElement("xs:attribute");
			ael.setAttribute("name",prl[i]);
			ael.setAttribute("type",schemaType(cls,prl[i]));
			ael.setAttribute("use","optional");
			cel.appendChild(ael);
		}
	}
	
	//add to root
	m_root.appendChild(cel);
}

void WocSchemaOut::newTransaction(const WocTransaction& trn)
{
	m_root.appendChild(m_doc.createComment("Transaction "+trn.name()));
	//create elements
	QDomElement tel=m_doc.createElement("xs:element");
	tel.setAttribute("name","WobRequest-"+trn.name());
	tel.setAttribute("type",trn.name()+"-Request");
	m_root.appendChild(tel);
	tel=m_doc.createElement("xs:element");
	tel.setAttribute("name","WobResponse-"+trn.name());
	tel.setAttribute("type",trn.name()+"-Response");
	m_root.appendChild(tel);
	
	//create type: req
	tel=m_doc.createElement("xs:complexType");
	tel.setAttribute("name",trn.name()+"-Request");
	QDomElement wel=m_doc.createElement("xs:sequence");
	tel.appendChild(wel);
	QStringList vars=trn.inputNames();
	for(int i=0;i<vars.size();i++){
		QString tp=trn.inputType(vars[i]);
		if(trn.isAttributeType(tp)){
			QDomElement at=m_doc.createElement("xs:attribute");
			at.setAttribute("name",vars[i]);
			at.setAttribute("type",schemaType(tp));
			at.setAttribute("use","optional");
			tel.appendChild(at);
		}else{
			QDomElement el=m_doc.createElement("xs:element");
			el.setAttribute("name",vars[i]);
			el.setAttribute("type",schemaType(tp));
			el.setAttribute("minOccurs","0");
			if(trn.isListType(tp))
				el.setAttribute("maxOccurs","unbounded");
			wel.appendChild(el);
		}
	}
	m_root.appendChild(tel);
	//create type: rsp
	tel=m_doc.createElement("xs:complexType");
	tel.setAttribute("name",trn.name()+"-Response");
	wel=m_doc.createElement("xs:sequence");
	tel.appendChild(wel);
	vars=trn.outputNames();
	for(int i=0;i<vars.size();i++){
		QString tp=trn.outputType(vars[i]);
		if(trn.isAttributeType(tp)){
			QDomElement at=m_doc.createElement("xs:attribute");
			at.setAttribute("name",vars[i]);
			at.setAttribute("type",schemaType(tp));
			at.setAttribute("use","optional");
			tel.appendChild(at);
		}else{
			QDomElement el=m_doc.createElement("xs:element");
			el.setAttribute("name",vars[i]);
			el.setAttribute("type",schemaType(tp));
			el.setAttribute("minOccurs","0");
			if(trn.isListType(tp))
				el.setAttribute("maxOccurs","unbounded");
			wel.appendChild(el);
		}
	}
	m_root.appendChild(tel);
}

void WocSchemaOut::newTable(const WocTable& )
{/*tables are not exposed to the network, hence no XML schema*/
}

QString WocSchemaOut::schemaType(QString tp)
{
	tp=WocTransaction::plainType(tp);
	if(WocTransaction::isObjectType(tp))return "class-"+tp;
	if(WocTransaction::isStringType(tp))return "xs:string";
	if(WocTransaction::isBlobType(tp))return "xs:base64Binary";
	if(WocTransaction::isBoolType(tp))return "xs:boolean";
	if(WocTransaction::isIntType(tp))return "xs:integer";
	//fallback, not pretty
	qDebug()<<"Warning: schema generator found unknown type"<<tp;
	return "xs:anysimpleType";
}

QString WocSchemaOut::schemaType(const WocClass& cls, QString prop)
{
	if(cls.propertyIsEnum(prop))return "enum-"+cls.name()+"-"+cls.propertyPlainType(prop);
	return schemaType(cls.propertyType(prop));
}
