// Copyright (C) 2009-2011 by Konrad Rosenbaum <konrad@silmor.de>
// protected under the GNU GPL version 3 or at your option any newer.
// See COPYING.GPL file that comes with this distribution.
//

#ifndef WOC_SCHEMAOUT_H
#define WOC_SCHEMAOUT_H

#include "processor.h"

#include <QDomDocument>
#include <QDomElement>

/**generates output for a schema file*/
class WocSchemaOut:public WocOutput
{
	public:
		/**initializes the output object with the given directory and file name that will receive the schema for this project
		\param dirname the directory below the base directory to store all Schema files in
		\param filename the file that will contain the projects own XML Schema, per default "project.xsd" */
		WocSchemaOut(QString dirname,QString filename="project.xsd");
		
		/**initializes the output object with the given DOM element*/
		WocSchemaOut(const QDomElement&);
		
		/**copies the static parts of the schemas into the target directory
		\param compound if not empty: the file name of a schema file that includes all schema files copied and created in the target directory
		\param encoding if given the encoding that will be validated by the compound schema file*/
		void addStaticSchemas(QString compound=QString(),WocProcessor::MessageEncoding encoding=WocProcessor::DefaultEncoding);
	protected:
		/**writes any last words after parsing finished*/
		virtual void finalize();
		/**creates a class*/
		virtual void newClass(const WocClass&);
		/**creates a table*/
		virtual void newTable(const WocTable&);
		/**creates a transaction*/
		virtual void newTransaction(const WocTransaction&);
	private:
		QString m_name,m_dir;
		QDomDocument m_doc;
		QDomElement m_root;
		
		/**helper for the constructor*/
		void initialize();
		
		/**helper: returns the corresponding schema type for a WOB type*/
		QString schemaType(QString);
		/**helper: returns the corresponding schema type for a WOB type*/
		QString schemaType(const WocClass&,QString);
};

#endif
