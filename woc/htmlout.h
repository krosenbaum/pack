// Copyright (C) 2009-2011 by Konrad Rosenbaum <konrad@silmor.de>
// protected under the GNU GPL version 3 or at your option any newer.
// See COPYING.GPL file that comes with this distribution.
//

#ifndef WOC_HTMLOUT_H
#define WOC_HTMLOUT_H

#include "processor.h"

#include <QFile>

class QDomElement;

///Output target for HTML documentation.
///This class does not create actual code, but documentation for the WOLF file.
class WocHtmlOut:public WocOutput
{
	public:
		///generates the output object from the corresponding XML tag that describes it
		explicit WocHtmlOut(QDomElement&);
		~WocHtmlOut();
	protected:
		///called to finalize all output, creates the index
		virtual void finalize();
		///creates an HTML page for a class
		virtual void newClass(const WocClass&);
		///creates an HTML page for a DB table
		virtual void newTable(const WocTable&);
		///creates an HTML page for a transaction
		virtual void newTransaction(const WocTransaction&);
	private:
		QString m_basedir,m_subdir;
		QFile m_index;
		
		/**helper: generate enums for classes*/
		void classEnums(const WocClass&,QFile&);
		/**helper: generate properties*/
		void classProperties(const WocClass&,QFile&);
		/**helper: generate mappings*/
		void classMappings(const WocClass&,QFile&);
		
};

#endif
