<html>
<title>Web Object Language Files</title>
<body>
<h1>Web Object Language Files</h1>

WOLF (*.wolf) files are simple XML files that describe the interface layers of PACK.

<h2>Overall File Format</h2>

Each wolf file must be enclosed in the &lt;Wolf> and &lt;/Wolf> tags. The first few statements inside must describe the project itself:

<pre>
&lt;Wolf>
  &lt;Doc>Documentation...&lt;/Doc>
  &lt;Project baseDir=".." wobDir="wob" name="MyProject" encoding="wob" xml-namespace="ns:MyProject" auth="none"/>
  &lt;Version comm="0100" needcomm="0100" humanReadable="1.1 alpha" svnTarget="."/>
&lt;/Wolf>
</pre>

Above the &lt;Project> tag tells woc about the project as a whole:
<table frame="1" border="1">
<tr><td><b>Attribute</b></td><td><b>Description</b></td></tr>
<tr><td>baseDir</td><td>the overall project directory (relative to its current working directory); default: current working directory</td></tr>
<tr><td>wobDir</td><td>relative to the baseDir, where to find all wolf files, default: baseDir itself</td></tr>
<tr><td>name</td><td>the exact name of the project - among other things this property is used as default interface name in the client</td></tr>
<tr><td>encoding</td><td>the encoding of the communication: <tt>wob</tt> the default encoding, transaction objects are transmitted directly, header information is transmitted in the HTTP headers, <tt>soap12</tt> a SOAP 1.2 wrapper is put around the transaction, header information is transmitted in the SOAP header, <tt>soap</tt> shortcut for the currently newest supported SOAP version</td></tr>
<tr><td>xml-namespace</td><td>the XML namespace that all transmitted objects are put into, PACK does not validate the namespace, but if you are working in SOAP mode this is mandatory to maintain compatibility with other SOAP implementations; default: "ns:" plus the project name in URL encoding, it is recommended to set a more unique namespace</td></tr>
<tr><td>auth</td><td>Authentication mode, default none; valid values are: "none" - no authentication at all, the auth property of transactions is ignored, "session" - one of the transactions generates a session ID that can be verified, "basic" - each authenticated transaction transmits the user name and password</td></tr>
</table><p>

The &lt;Doc> tag can be used to create project documentation. Each tag generates one paragraph. HTML can be embedded by escaping the "&lt;" character as "&amp;lt;".<p>

The &lt;Version> tag describes the communication protocol:

<table frame="1" border="1">
<tr><td><b>Attribute</b></td><td><b>Description</b></td></tr>
<tr><td>comm</td><td>the communication protocol described by this wolf file</td></tr>
<tr><td>needcomm</td><td>the minimum communication protocol this wolf file is compatible with (attention: the processor uses ASCII string comparison, not int comparison)</td></tr>
<tr><td>humanReadable</td><td>the version number shown to the user</td></tr>
<tr><td>svnTarget</td><td>optional; if given: woc will call SVN to find out which repository revision was used</td></tr>
<tr><td>svnExe</td><td>optional; executable to call for subversion (default: svn); if empty or not a reachable executable the SVN related fields in the generated code will be set to empty or defaults</td></tr>
</table><p>

The Include tag can be used to include other wolf files in the processing:
<pre>
&lt;Include file="user.wolf"/>
</pre><p>

<h3>Attribute Types</h3>

Boolean attributes in wolf files allow integers (0 is false, non-zere is true) or the explicit strings "yes", "true", "on" for true and "no", "false", "off" for false.<p>

Integers can be entered in C notation - if it starts with 1 through 9 it is decimal, if it starts with 0 it is octal, if it starts with 0x it is hexadecimal. All integers are interpreted by woc - the generated source will always contain decimal numbers.<p>

Woc itself handles all names as case-sensitive. Some target languages (eg. PHP) may work case-insensitive.

<h2>Chosing Outputs</h2>

Outputs should be declared after the basics have been declared, but before any tables, classes or transactions are declared.

<pre>
&lt;QtClientOutput sourceDir="src" subDir="wob" priInclude="wob.pri" transactionBase="WTransaction" validate="no"/>
&lt;QtServerOutput sourceDir="src" subDir="wob" priInclude="wob.pri" transactionBase="WTransaction" validate="no"/>
&lt;PHPServerOutput sourceDir="www" subDir="inc/wob" extension=".php" clean="yes" transactionBase="WobTransaction" validate="no"/>
&lt;HtmlOutput sourceDir="doc" subDir="wob"/>
&lt;SchemaOutput sourceDir="schema" filename="project.xsd" compound="compound.xsd"/>
&lt;SoapOutput sourceDir="soapdir" fileprefix="myproject"/>
</pre>

The attribute "sourceDir" tells woc which directory (above baseDir) is the root source directory for each sub-project, the "subDir" attribute tells it which subdirectory below the sourceDir should be used for woc's generated output. The attribute "clean" tells woc to first delete all files from that sub-directory before starting the generator. Multiple generators of the same type can be used, but should have different target directories.<p>

The optional validate attribute lets you decide whether incoming messages should be validated against the schema file before processing them. It defaults to "no", which means Schema support is not generated. The value "optional" generates schema support, but does not switch it on, you can control it programmatically. The value "lax" generates schema support, switches it on, but does not stop processing if validation fails. The value "strict" switches schema support on and stops processing if a message fails validation.<p>


The QtClientOutput tag tells woc to create a generator for a Qt based client - the priInclude attribute tells woc which name it should give to the QMake include for its generated output. The <tt>transactionBase</tt> attribute allows to chose an extended class as base class of generated transactions - usually this should not be necessary for Qt client targets.<p>

The QtServerOutput tag tells woc to create a generator for a Qt based server, the attributes are the same as for the client output.<p>

With both Qt bases outputs you can optionally specify a <tt>shareObjects</tt> attribute - if present it contains the class prefix for communication objects. If that prefix is different from the one given as <tt>classPrefix</tt> woc will assume that some other output will create those objects and not create them in this output. This feature is meant for the case in which client and server reside in the same translation unit and share the same objects (transactions and interfaces can unfortunately not be shared).<p>

Another optional attribute is <tt>enableExport</tt> - set it to "yes" to enable export of the generated symbols in case you compile them into a DLL and want users of that DLL to be able to use the generated classes in their code. Set it to "no" to hide those symbols ("no" is the default).<p>

The PHPServerOutput tag tells woc to create a generator for a PHP based server. The "extension" attribute tells woc which file extension to use for the generated files (default is .php). Woc will automatically create a "autoload" (plus extension) file that should be included from the main body of the PHP project to load the automatically generated classes. The <tt>transactionBase</tt> attribute allows to chose an extended class as base class of generated transactions - it is recommended to overwrite some protected functions in order to be able to get more information inside transactions.<p>

The HtmlOutput tag tells woc where to generate API documentation. This target generates generic high level documentation for all configured tables, classes, and transactions. Use doxygen or a similar tool to generate language target specific documentation.<p>

The SchemaOutput tag tells woc to generate XML Schema files that validate the projects generated communication XML documents. The filename tag tells woc which file name to use for the classes and transactions - it will contain a complex type for each class (name: <tt>class-<i>ClassName</i></tt>), and a simple type for each enum (<tt>enum-<i>ClassName-enumName</i></tt>). For each transaction it will generate two global elements, one for the request and one for the responds, including the corresponding complex types. Normal wob encoded communication can be validated directly against this file. If you want to validate errors or SOAP encoded communication you have to specify a compound file name - if specified woc will also copy the relevant Schema files and link them together in the given compound file, which you can use to validate all messages. This output ignores database tables.<p>

The SoapOutput tag tells woc to generate SOAP WSDL and Schema files. This tag generates an error if the project communication mode is not set to use SOAP. This output ignores database tables. The <tt>fileprefix</tt> attribute is used to construct the WSDL and Schema files by adding the proper extension (".wsdl" and ".xsd") to it.<p>

<hr>
<a href="wolf-db.html">Next: DataBase Layer</a>

</html>