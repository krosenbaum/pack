<html>
<title>Web Object Language Files - Communication </title>
<body>
<h1>Web Object Language Files - Communication Layer</h1>

The Communication Abstraction Layer is the servers upper bound towards the client. It hides the complexities of serializing data onto the network transport protocol by providing communication classes that do this automatically. The configuration of this layer is split into two major components: communication classes and transactions. While the communication classes describe what data can be transported over the connection, the transactions describe what operations can be performed.<p>

<h2>Communication Classes</h2>

An example class can be seen here:

<pre>
&lt;Class name="Ticket">
  &lt;Base lang="php/client" class="MyBaseClass"/>
  
  &lt;Enum name="TicketState" refColumn="ticket:status"/>
  &lt;Enum name="StateOfMind">
    &lt;Value name="sad"/>
    &lt;Value name="funny"/>
    &lt;Value name="dontcare"/>
  &lt;/Enum>
  &lt;Property name="ticketid" type="astring" id="yes"/>
  &lt;Property name="eventid" type="int"/>
  &lt;Property name="priceid" type="int"/>
  &lt;Property name="price" type="Price"/>
  &lt;Property name="status" type="TicketState"/>
  &lt;Property name="orderid" type="int"/>
  &lt;Property name="stateofmind" type="StateOfMind" optional="yes"/>
  
  &lt;Abstract lang="php"/>
  
  &lt;Mapping table="ticket">
    &lt;Map column="ticketid"/>
    &lt;Map column="priceid"/>
    &lt;Map property="price">
      &lt;Call lang="php" method="WOPrice::fromtableprice(WTprice::getFromDB($data->getpriceid()))"/>
    &lt;/Map>
    &lt;Map column="eventid"/>
    &lt;Map column="status"/>
    &lt;Map column="orderid" property="orderid"/>
  &lt;/Mapping>
&lt;/Class>
</pre>

The "name" attribute of the Class tag gives the communication class a name by which it can be referenced. The language converters will prefix it with "WO" to make it unique and postfix it with "Abstract" if the class is declared abstract - in this case the user must derive the non-abstract class to make it usable.<p>

Class attributes:
<table frame="1" border="1">
<tr><td><b>Attribute</b></td><td><b>Description</b></td></tr>
<tr><td>name</td><td>the name of the class</td></tr>
<tr><td>abstract</td><td>(optional) marks the class abstract in all generated language targets</td></tr>
<tr><td>base</td><td>(optional) marks this class derived from the class given as base, the base class must already exist, this attribute overrides the use of the Base tag</td></tr>
</table><p>

The Base tag can be used to derive the class from a class different from WObject, although that class must be derived from WObject. If you want to derive from a class defined in the WOLF file, use the base attribute instead.

<h3>Properties and Types</h3>

The Enum tag defines a local enumeration. In most cases they will simply reference an enumeration that has already been defined in a database table with a refColumn attribute (syntax: <tt><i>table</i>:<i>column</i></tt>), but it may also locally define its values by using the same Value tags used for database enums.<p>

The Property tag defines a property of the class that can be read and written to:
<table frame="1" border="1">
<tr><td><b>Attribute</b></td><td><b>Description</b></td></tr>
<tr><td>name</td><td>the name of the property</td></tr>
<tr><td>abstract</td><td>(optional) marks the property abstract - this automatically makes the class abstract and the user has to overwrite the getter and setter methods</td></tr>
<tr><td>id</td><td>(optional, default=no) marks the property to identify the instance in a cache</td></tr>
<tr><td>type</td><td>(mandatory) defines the type of the property, see below</td></tr>
</table><p>

Property types:
<table frame="1" border="1">
<tr><td><b>Type</b></td><td><b>Description</b></td></tr>
<tr><td>int</td><td>an integer - it is at least a 32bit signed type; it is transported as attribute</td></tr>
<tr><td>int32</td><td>an integer - it is at least a 32bit signed type; it is transported as attribute</td></tr>
<tr><td>int64</td><td>an integer - it is at least a 64bit signed type; it is transported as attribute</td></tr>
<tr><td>bool</td><td>a boolean value; it is transported as attribute</td></tr>
<tr><td>astring</td><td>a string that can be safely stored in a XML attribute</td></tr>
<tr><td>string</td><td>a string that is transported as element and can reach any length</td></tr>
<tr><td><i>EnumName</i></td><td>the name of an enum defined in the same class is possible, local storage is as integer, transport is as string equalling the symbolic name of the value in an attribute</td></tr>
<tr><td><i>ClassName</i></td><td>the name of a defined class makes the property an instance of that class; they are transported as sub-elements of this class</td></tr>
<tr><td>List:*</td><td>prefixing the type with "List:" makes the property a list of that type - any of the above types can be made into lists; on transport list values are always serialized as elements</td></tr>
</table><p>

<h3>Mapping Database Tables</h3>

The Mapping tag can be used to provide a shortcut cast between a database table type and the communication class type.<p>

Mappings generate conversions in both directions. However, mappings are only generated for the server side, since the client side does not have a database layer.<p>

If the property attribute is ommitted a property with the same name as the listed column is looked for. If the column attribute is ommitted a column with the same name as the property is looked for. If a property is not listed at all it will be left out of the conversion and hence will remain a null value.<p>

In the normal case of mapping in which no transformations between the database query result and the transport data is necessary it is enough to have just the column and/or property names listed. For more complex cases it is possible to specify code that performs the transformation from the database to the communication class with the Call tag. The method specified should return the desired result as a value valid for that property. It can use the $data variable, which represents the communication class instance that is being worked on and the $table variable that represents the table instance that is being converted from. Mappings are always generated in the order they are listed in the WOLF file, so access is only possible to properties that are listed over the current one.<p>

Classes can be documented by adding Doc subtags. Properties can be documented by adding the description directly into the Property tag. Enum values can be documented by adding the description directly into the Value tag.


<h2>Transactions</h2>

<pre>
&lt;Transaction name="GetTicket" updating="yes">
  &lt;Input>
    &lt;Var name="ticketid" type="astring"/>
  &lt;/Input>
  &lt;Call lang="php" method="MyClass::getTicketFunction($this);"/>
  &lt;Output>
    &lt;Var name="ticket" type="Ticket/Full"/>
  &lt;/Output>
&lt;/Transaction>
</pre>

Each transaction must have a name that identifies it. This name is used in the wire protocol to correctly identify the currently requested transaction.

Transaction attributes:
<table frame="1" border="1">
<tr><td><b>Attribute</b></td><td><b>Description</b></td></tr>
<tr><td>name</td><td>the name of the transaction</td></tr>
<tr><td>mode</td><td>(optional, default=checked) can be used to require less restrictive privileges, see below for values</td></tr>
<tr><td>updating</td><td>optional, tells woc whether the transaction is to be considered writing to the database - the default is given in the global "Database" tag.</td></tr>
<tr><td>nolog</td><td>optional, if given it excludes the request and/or response from the log file (currently: Qt implementation only), use this for transactions that carry passwords or other sensitive information, see below</td></tr>
</table><p>

Transaction modes:
<table frame="1" border="1">
<tr><td><b>Mode</b></td><td><b>Description</b></td></tr>
<tr><td>checked</td><td>the default mode: the session must be authenticated and the user must have the privilege to execute this query</td></tr>
<tr><td>auth</td><td>the session must be authenticated, but no special privilege is required; this can for example be used for the logout function or a function query information about the session itself</td></tr>
<tr><td>open</td><td>no session is necessary for this to work; usually this is only done for login</tr></tr>
</table><p>

NoLog values:
<table frame="1" border="1">
<tr><td><b>Value</b></td><td><b>Description</b></td></tr>
<tr><td>"request" (or: req)</td><td>excludes the request body from logging</td></tr>
<tr><td>"response" (or: rsp)</td><td>excludes the response body from logging</td></tr>
<tr><td>"any" (or: "all" or: "request,response")</td><td>neither request nor response bodies are logged (only the fact that the transaction happened is logged)</tr></tr>
</table><p>

Input defines the input parameters (Var tag) of the transaction (ie. what is sent to the server).<p>

Output defines the data (Var tag) sent back to the client.<p>

Var attributes:
<table frame="1" border="1">
<tr><td><b>Attribute</b></td><td><b>Description</b></td></tr>
<tr><td>name</td><td>the name of the variable</td></tr>
<tr><td>type</td><td>type of the attribute; all types allowed for class properties (except enums) are allowed here</td></tr>
</table><p>

The Call tag defines which function is called once the transaction has been checked and authenticated and the input decoded.<p>

Call attributes:
<table frame="1" border="1">
<tr><td><b>Attribute</b></td><td><b>Description</b></td></tr>
<tr><td>lang</td><td>the language binding this refers to (currently only "php")</td></tr>
<tr><td>method</td><td>the function to call - this can be any function or method</td></tr>
</table><p>

Transactions are represented as classes. On the client side they are used to execute the query and to represent the result. On the server side they internally verify the input and authenticate the transaction and are then used to take and encode the output or possible error conditions.<p>

The Call should use the $this variable to hand over a reference to the transaction itself. The function called should then use the transactions methods to set the result. Please see the binding documentation for details.<p>

Transactions can be documented by adding Doc tags. Inputs and Outputs can be documented by adding the description directly into the Var tags.

<hr>
<a href="wolf-db.html">Previous: DB Layer</a>

</html>
